class TechEntity {
	String mobileno;
	bool isadministrator;
	String username;
	String email;
	String loginid;
	dynamic address;
	int userid;
	String clienttoken;
	int roleid;

	TechEntity({this.mobileno, this.isadministrator, this.username, this.email, this.loginid, this.address, this.userid, this.clienttoken, this.roleid});

	TechEntity.fromJson(Map<String, dynamic> json) {
		mobileno = json['MobileNo'];
		isadministrator = json['IsAdministrator'];
		username = json['UserName'];
		email = json['Email'];
		loginid = json['LoginId'];
		address = json['Address'];
		userid = json['UserId'];
		clienttoken = json['ClientToken'];
		roleid = json['RoleId'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['MobileNo'] = this.mobileno;
		data['IsAdministrator'] = this.isadministrator;
		data['UserName'] = this.username;
		data['Email'] = this.email;
		data['LoginId'] = this.loginid;
		data['Address'] = this.address;
		data['UserId'] = this.userid;
		data['ClientToken'] = this.clienttoken;
		data['RoleId'] = this.roleid;
		return data;
	}
}
