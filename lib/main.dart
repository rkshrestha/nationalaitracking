//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:nbis/features/splash/SplashPage.dart';
import 'package:nbis/s/localization/AppTranslationsDelegate.dart';
//import 'package:nbis/s/localization/Application.dart';
//import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(MyApp());

//class MyApp extends StatelessWidget {
//   This widget is the root of your application.
//
//
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      title: 'National AI Tracking',
//      debugShowCheckedModeBanner: false,
//      theme: ThemeData(
//        primaryColor: Colors.blue,
//        primaryColorDark: Colors.blueAccent,
//
//      ),
//      home: SplashPage(),
//      //app translation
//      localizationsDelegates: [
//        _localeOverrideDelegate,
//        const AppTranslationsDelegate(),
//        //provides localised strings
//        GlobalMaterialLocalizations.delegate,
//        //provides RTL support
//        GlobalWidgetsLocalizations.delegate,
//      ],
//      supportedLocales: application.supportedLocales(),
//
//
//    );
//  }
//}

class MyApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  AppTranslationsDelegate newLocaleDelegate;


  @override
  void initState() {
    super.initState();
//    newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
//    application.onLocaleChanged = onLocaleChange;
  }


  @override
  void didChangeDependencies(){
    super.didChangeDependencies();
//    newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
//    application.onLocaleChanged = onLocaleChange;
  }

  @override
  Widget build(BuildContext context) {
//    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    return MaterialApp(
      title: 'National AI Tracking',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.blue,
        primaryColorDark: Colors.blueAccent,

      ),
      home: SplashPage(),
      //app translation
//      localizationsDelegates: [
//        newLocaleDelegate,
//        const AppTranslationsDelegate(),
//        //provides localised strings
//        GlobalMaterialLocalizations.delegate,
//        //provides RTL support
//        GlobalWidgetsLocalizations.delegate,
//      ],
//      supportedLocales: application.supportedLocales(),


    );
  }



//  void onLocaleChange(Locale locale) {
//    setState(() {
//      newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
//    });
//  }
}
