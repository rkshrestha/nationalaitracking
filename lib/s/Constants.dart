class Constants {
  //cattle
  static const String J = 'J';
  static const String HF = 'HF';
  static const String JC = 'JC';
  static const String HFC = 'HFC';
  static const String Local = 'Local';

  //buffalo
  static const String M = 'M';
  static const String MC = 'MC';

//Goat

  static const String JP = 'JP';
  static const String BO = 'BO';
  static const String BA = 'BA';
  static const String Others = 'Others';


  //list for setting
  static const String Profile = 'Profile';
  static const String Settings ='Settings';
  static const String LogOut ='Log Out';
  static const List<String> settings = <String>[Profile,Settings, LogOut];




  static const List<String> cattle = <String>[J, HF, JC, HFC, Local];
  static const List<String> buffalo = <String>[M, MC];
  static const List<String> goats = <String>[JP, BO, BA, Local, Others];
}
