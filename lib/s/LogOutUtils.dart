import 'package:flutter/material.dart';
import 'package:nbis/features/home/LoginPage.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/Logger.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class LogOutUtils {
  static void logOut(BuildContext context) {
    DialogUtils.showAlertDialog(
        context,
        "Alert",
//        AppTranslations.of(context).text("alert"),
        "Are you sure, you want to exit?", (it) {
//        AppTranslations.of(context).text("exitApp"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.pop(context, true);
            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            var sessionManager = SessionManager.getInstance(context);
//            Session session = Session();
//            session.isSessionActive = true;
//            session.user = it.data;
            sessionManager.deleteAll();

//            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> LoginPage()));

            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => LoginPage()),
                  (Route<dynamic> route) => false,
            );


            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }

//  static onBackPressed(BuildContext context) {
//    return DialogUtils.showAlertDialog(
//            context, "Alert", "Are you sure, you want to exit?", (it) {
//          switch (it.eventType) {
//            case DialogUtils.ON_CANCELABLE:
//              {
//                Logger.v('No clicked');
//                Navigator.pop(context, true);
//                break;
//              }
//            case DialogUtils.ON_CONFIRM:
//              {
//
//
//                Logger.v('Yes clicked');
//                break;
//              }
//          }
//        }, "No", "Yes") ??
//        false;
//  }
}
