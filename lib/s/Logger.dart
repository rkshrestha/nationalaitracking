class Logger {

  static void v(String message){
    print(message);
  }

  static void log(String clazzName,String message){
    print("${clazzName} : ${message}");
  }

}