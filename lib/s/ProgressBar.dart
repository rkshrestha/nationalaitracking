import 'package:flutter/material.dart';
import 'package:nbis/features/home/HomePage.dart';
import 'package:nbis/features/home/LoginPage.dart';
//import 'package:nbis/features/home/SyncServerPage.dart';

class ProgressBar {
  BuildContext context;

  progressBar() {
    return WillPopScope(
        onWillPop: () async => false,
        child: Container(
            color: Color(0x1AFFFFFF),
            child: Center(
                child: Card(
              elevation: 4.0,
              margin: EdgeInsets.all(8.0),
              color: Colors.white,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation(Colors.deepOrange)),
                      padding: EdgeInsets.only(
                          top: 16.0, bottom: 16.0, left: 16.0, right: 16.0)),
                  Padding(
                      child: Text(
                        "Sending Request...  ",
                        style: TextStyle(fontSize: 16.0),
                      ),
                      padding: EdgeInsets.only(top: 16.0, bottom: 16.0))
                ],
              ),
            ))));
  }



//  progressBar() {
//    return AlertDialog(
//      content:  WillPopScope(
//          onWillPop: () async => false,
//          child: Container(
//              color: Color(0x1AFFFFFF),
//              child: Center(
//                  child: Card(
//                    elevation: 4.0,
//                    margin: EdgeInsets.all(8.0),
//                    color: Colors.white,
//                    child: Row(
//                      mainAxisSize: MainAxisSize.min,
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: [
//                        Padding(
//                            child: CircularProgressIndicator(
//                                valueColor:
//                                AlwaysStoppedAnimation(Colors.deepOrange)),
//                            padding: EdgeInsets.only(
//                                top: 16.0, bottom: 16.0, left: 16.0, right: 16.0)),
//                        Padding(
//                            child: Text(
//                              "Sending Request...  ",
//                              style: TextStyle(fontSize: 16.0),
//                            ),
//                            padding: EdgeInsets.only(top: 16.0, bottom: 16.0))
//                      ],
//                    ),
//                  )))),
//    );
//
//
//  }




  show(BuildContext contextt) async {
    await showDialog<Null>(
        context: contextt,
        barrierDismissible: true,
        builder: (BuildContext context) {
          this.context = context;
          return progressBar();
        });
  }

//  dismissDialog(BuildContext context) async {
//    if (this.context != null) {
//      Navigator.of(this.context).pop();
//    }
////    Navigator.pushAndRemoveUntil(
////      context,
////      MaterialPageRoute(builder: (context) => LoginPage()),
////      (Route<dynamic> route) => false,
////    );
//  }

  dismiss(BuildContext contextt) async {
//       await Future.delayed(const Duration(milliseconds: 3000));
    if (this.context != null) {
      Navigator.of(this.context).pop();
    }

//    await new Future.delayed(new Duration(seconds: 2)).then((_) {
//      try {
//
//      } catch (ex) {
//        Logger.v(ex.toString());
//      }
//    });
//      await Future.delayed(Duration.zero, () {

//      });
  }

  dismissToLogin(BuildContext context) async {

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
      (Route<dynamic> route) => false,
    );
  }


  dismissToTest(BuildContext contexxt) async {
    if (this.context != null) {
      Navigator.pushReplacement(
          this.context,
           MaterialPageRoute(builder: (context) => HomePage())
      );
    }
  }

}
