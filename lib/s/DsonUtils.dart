import 'dart:convert';

import 'package:nbis/features/repo/JsonCallable.dart';
import 'package:nbis/s/Logger.dart';


class DsonUtils {
  static T toObject<T>(dynamic json, JsonCallable callable) {
    Logger.v("test" + json.toString());
    Map map;
    if(json.runtimeType.toString() == "String"){
       map = jsonDecode(json);
    }else {
       map = jsonDecode(jsonEncode(json));
    }
    return callable.fromJson(map) as T;
  }

  static String toJsonString(Object object) {
  Logger.v("setString Object");
    return jsonEncode(object);
  }

//   static String toJsonStringWithDynamic<T>(Object object,JsonCallable callable){
//     String data = jsonEncode(object);
//     return jsonEncode(toObject(data, callable));
//   }

  static List<T> toList<T>(String json, JsonCallable callable) {
    List dataList = jsonDecode(json);
    List<T> newList = [];

    for (int i = 0; i < dataList.length; i++) {
      T clazz = callable.fromJson(dataList[i]);
      newList.add(clazz);
    }

    return newList;
  }

//  static List<T> dynamicToList<T>(dynamic json, JsonCallable callable) {
//    Log.v("inside dson  ");
//    String data = jsonEncode(json);
//    Log.v("inside encode  ");
//    List dataList = jsonDecode(data);
//    Log.v("inside dcode"+dataList.length.toString());
//    List<T> newList = [];
//
//    for (int i = 0; i < dataList.length; i++) {
//      try {
//        Log.v("inside loop");
//        T clazz = callable.fromJson(dataList[i]);
//        newList.add(clazz);
//      } catch (e) {
//        Log.v("inside dson dynamicToList " + e);
//        break;
//      }
//    }
//
//    return newList;
//  }

  static List<T> dynamicToList<T>(dynamic json, JsonCallable callable) {
    Logger.v("inside dson  ");
    String data = jsonEncode(json);
    Logger.v("inside encode  ");
    List dataList = jsonDecode(data);
    Logger.v("inside dcode" + dataList.length.toString());
    List<T> newList = [];

    for (int i = 0; i < dataList.length; i++) {
      Logger.v("inside loop" + i.toString());
      T clazz = callable.fromJson(dataList[i]);
      newList.add(clazz);
    }

    return newList;
  }

//  public static HashMap<String, String> getHashMap(Object src) {
//    String data = toString(src);
//    Timber.v( data);
//    return toObject(data, new TypeToken<HashMap<String, String>>() {
//    }.getType());
//  }
}
