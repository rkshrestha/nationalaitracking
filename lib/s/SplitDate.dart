import 'package:nepali_utils/nepali_utils.dart';

class SplitDate {
  NepaliDateTime nepaliDate;
  DateTime englishDate;
  static String date;

  static splitNepali(nepaliDate) {
    return nepaliDate.toIso8601String().split("T").first;
  }

  static splitEnglish(englishDate) {
    return englishDate.toIso8601String().split("T").first;
  }


  static splitDateString(date) {
    return date.split("T").first;
  }
}
