import 'dart:io';

import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/s/Logger.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class FileUtils {
  static String AI_DIR = "National_AI_Tracking";

  static Future<String> get localPath async {
    final directory = await getExternalStorageDirectory();
    return directory.path;
  }

  static Future<File> createFile(String dirPath, String fileName) async {
    final path = await localPath;
    Logger.v("file saved" + path.toString());
    String propertyDirPath = join(path, dirPath);
    Directory dir = Directory(propertyDirPath);
    bool isDirExist = await dir.exists();
    Directory newDir;
    if (!isDirExist) {
      newDir = await dir.create();
    } else {
      newDir = dir;
    }
    return File('${newDir.path}/$fileName');
  }

  static Future<Directory> getDir(String dirPath) async {
    final path = await localPath;
    Logger.v("Directory saved" + path.toString());

    String propertyDirPath = join(path, dirPath);
    Logger.v("Directory saved" + propertyDirPath.toString());


    Directory dir = Directory(propertyDirPath);
    bool isDirExist = await dir.exists();
    Directory newDir;
    if (!isDirExist) {
      newDir = await dir.create();
    }

    return newDir;
  }

  static void saveFile(File selectedFile, String filename) async {
    File savedFile1 = await createFile(AI_DIR, filename);
    savedFile1.writeAsBytes(selectedFile.readAsBytesSync(),
        mode: FileMode.write);
  }

  static void backupDB() async{
   String dbPath= await SQFliteDatabase. getDB().getDBPath();
   File dbFile=File(dbPath+"/"+SQFliteDatabase.DBNAME);
   File newDbFile = await createFile(AI_DIR,SQFliteDatabase.DBNAME );
   newDbFile.writeAsBytes(dbFile.readAsBytesSync(),
       mode: FileMode.write);
  }
}
