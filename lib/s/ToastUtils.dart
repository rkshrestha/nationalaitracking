import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastUtils {
  static void show(String message){
    Fluttertoast.showToast(
        msg: '$message',
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIos: 2,
        fontSize: 16.0,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black,
        textColor: Colors.white);
  }
}
