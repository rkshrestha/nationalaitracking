import 'package:flutter/material.dart';
import 'package:nbis/features/home/Callback.dart';

class DialogUtils {

  static const String ON_CANCELABLE="ON_CANCELABLE";
  static const String ON_CONFIRM="ON_CONFIRM";

  static  showAlertDialog(BuildContext context, String title,
      String message, ValueChanged<Callback> callback, String no, String yes) {
    FlatButton fbNo = FlatButton(
        onPressed: () {
          if (callback != null) {
            callback(Callback(ON_CANCELABLE, ""));
          } else {}
        },
        child: Text(no));

    FlatButton fbYes = FlatButton(
        onPressed: () {
          if (callback != null) {
            callback(Callback(ON_CONFIRM, ""));
          } else {}
        },
        child: Text(yes));

    showDialog(
      barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title, textAlign: TextAlign.center,),
            content: Text(message),
            actions: <Widget>[
              Visibility(
                //      visible: isVisible,
                child: fbNo,
              ),
              Visibility(
                //   visible: isVisible,
                child: fbYes,
              ),
            ],
          );
        });
  }




  //single ok button
  static  showAlertDialogSingleButton(BuildContext context, String title,
      String message, ValueChanged<Callback> callback, String no) {
    FlatButton fbNo = FlatButton(
        onPressed: () {
          if (callback != null) {
            callback(Callback(ON_CANCELABLE, ""));
          } else {}
        },
        child: Text(no));



    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title, textAlign: TextAlign.center,),
            content: Text(message),
            actions: <Widget>[
              Visibility(
                //      visible: isVisible,
                child: fbNo,
              ),

            ],
          );
        });
  }
}
