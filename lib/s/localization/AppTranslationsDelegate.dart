import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nbis/s/localization/Application.dart';


class AppTranslationsDelegate extends LocalizationsDelegate<AppTranslations> {
  final Locale newLocale;

  const AppTranslationsDelegate({this.newLocale});

//  @override
//  bool isSupported(Locale locale) {
//    return ["en", "np"].contains(locale.languageCode);
//  }


  @override
  bool isSupported(Locale locale) {
    return application.supportedLanguagesCodes.contains(locale.languageCode);
  }


  @override
  Future<AppTranslations> load(Locale locale) {
    return AppTranslations.load(newLocale ?? locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppTranslations> old) {
    return true;
  }
}