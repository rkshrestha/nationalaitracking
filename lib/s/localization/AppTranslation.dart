import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class AppTranslations {
  Locale locale;
  static Map<dynamic, dynamic> localisedValues;

  AppTranslations(Locale locale) {
    this.locale = locale;
    localisedValues = null;
  }

  static AppTranslations of(BuildContext context) {
    return Localizations.of<AppTranslations>(context, AppTranslations);
  }

  static Future<AppTranslations> load(Locale locale) async {
    AppTranslations appTranslations = AppTranslations(locale);
    String jsonContent =
    await rootBundle.loadString("assets/localization_${locale.languageCode}.json");
    localisedValues = json.decode(jsonContent);
    return appTranslations;
  }

  get currentLanguage => locale.languageCode;

//  String text(String key) {
//    return localisedValues[key] ?? "$key not found";
//  }

  String text(String key) {
    if (localisedValues == null) {
      return '';
//      return 'Not ready yet';
    }
    else {
      return localisedValues[key] ?? "$key missing";
    }
  }

}
