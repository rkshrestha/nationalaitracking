//import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/sharedPreferences/Session.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SessionManager implements Repository<Session> {
  static final String TAG = 'SessionManager';

  // Shared Preferences
 static SharedPreferences sharedPreferences;

  // Context
  BuildContext context;

  // Shared sharedPreferences mode
  int PRIVATE_MODE = 0;

  // Sharedpref file name
  static final String PREF_NAME = "PREFERENCE_SESSION";

  static SessionManager sessionManager;

  SessionManager(BuildContext context) {
    this.context = context;
    initSharedPreferences();
  }

  static SessionManager getInstance(BuildContext context) {
    if (sessionManager == null) {
      sessionManager = SessionManager(context);
    }
    return sessionManager;
  }

  void initSharedPreferences() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  @override
  void delete(Session entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() async {
    await sharedPreferences.remove(Session.TAG);
  }

  @override
  List<Session> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  Session findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Session findOne() {
    String sessionData = sharedPreferences.getString(Session.TAG);
//    Logger.v("Session Data: "+sessionData);
    if (sessionData != null) {
   //   Map map = jsonDecode(sessionData);
   //  return Session().fromJson(map);
      Session session = DsonUtils.toObject(sessionData, Session());
      return session;
    } else {
      Logger.v("Session is inactive");
    }
    return null;
  }

  @override
  void persist(Session entity) async {
    if (entity == null) {
      Logger.v(" cant persist");
      return;
    }
    //entity.setSessionActive(true);
    Logger.v("User ID: " + entity.user.userid.toString());
    await  initSharedPreferences();
    await sharedPreferences.setString(
        Session.TAG, DsonUtils.toJsonString(entity));
    Logger.v(sharedPreferences.toString());

 //   await sharedPreferences.setString(Session.TAG, jsonEncode(entity));
  }

  @override
  void update(Session entity) async {
    if (entity == null) {
      Logger.v(" cant update");
      return;
    }
    await sharedPreferences.setString(
        Session.TAG, DsonUtils.toJsonString(entity));
  }
}
