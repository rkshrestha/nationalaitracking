import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'User.g.dart';

//
//@JsonSerializable()
//class User implements JsonCallable<User>{
//
//  @JsonKey(name: 'Name')
//  String name;
//  @JsonKey(name: 'DeviceId')
//  String deviceId;
//  @JsonKey(name: 'Token')
//  String token;
//
//
//  User();
//
//  User.make(this.name, this.deviceId, this.token);
//
//  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
//  Map<String, dynamic> toJson() => _$UserToJson(this);
//
//  @override
//  String toJsonString() {
//    return jsonEncode(toJson());
//  }
//
//  @override
//  User fromJson(Map<String, dynamic> json) {
//    return User.fromJson(json);
//  }
//
//}
@JsonSerializable()
class User implements JsonCallable<User> {
  @JsonKey(name: 'MobileNo')
  String mobileno;
  @JsonKey(name: 'IsAdministrator')
  bool isadministrator;
  @JsonKey(name: 'UserName')
  String username;
  @JsonKey(name: 'Email')
  String email;
  @JsonKey(name: 'LoginId')
  String loginid;
  @JsonKey(name: 'Address')
  String address;
  @JsonKey(name: 'UserId')
  int userid;
  @JsonKey(name: 'ClientToken')
  String clienttoken;
  @JsonKey(name: 'RoleId')
  int roleid;
  @JsonKey(name: 'TechnicianId')
  String technicianId;

  User();

  User.make(this.mobileno, this.isadministrator, this.username, this.email,
      this.loginid, this.address, this.userid, this.clienttoken, this.roleid,this.technicianId);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  User fromJson(Map<String, dynamic> json) {
    return User.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}
