import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';
import 'package:nbis/features/sharedPreferences/User.dart';

part 'Session.g.dart';

@JsonSerializable()
class Session implements JsonCallable<Session>{

  static String TAG="Session.TAG";

  User user;
  bool isSessionActive = false;


  Session();

  Session.make(this.user, this.isSessionActive);

  factory Session.fromJson(Map<String, dynamic> json) => _$SessionFromJson(json);
  Map<String, dynamic> toJson() => _$SessionToJson(this);



//  @override
//  Session fromJson(Map<String,dynamic > json) {
//    return Session.make(json['user'] as User,
//      json['isSessionActive'] as bool);
//  }
//
//  @override
//  Map<String,dynamic > toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['user'] = this.user;
//    data['isSessionActive'] = this.isSessionActive;
//    return data;
//  }
//
  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }

  @override
  Session fromJson(Map<String, dynamic> json) {
    return Session.fromJson(json);
  }
}
