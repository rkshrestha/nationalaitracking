// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'User.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..mobileno = json['MobileNo'] as String
    ..isadministrator = json['IsAdministrator'] as bool
    ..username = json['UserName'] as String
    ..email = json['Email'] as String
    ..loginid = json['LoginId'] as String
    ..address = json['Address'] as String
    ..userid = json['UserId'] as int
    ..clienttoken = json['ClientToken'] as String
    ..roleid = json['RoleId'] as int
    ..technicianId = json['TechnicianId'] as String;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'MobileNo': instance.mobileno,
      'IsAdministrator': instance.isadministrator,
      'UserName': instance.username,
      'Email': instance.email,
      'LoginId': instance.loginid,
      'Address': instance.address,
      'UserId': instance.userid,
      'ClientToken': instance.clienttoken,
      'RoleId': instance.roleid,
      'TechnicianId': instance.technicianId
    };
