// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'LiquidNitrogen.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _LiquidNitrogenBean implements Bean<LiquidNitrogen> {
  final lnId = StrField('ln_id');
  final slnId = IntField('sln_id');
  final receivedDate = StrField('received_date');
  final quantity = DoubleField('quantity');
  final userId = IntField('user_id');
  final createdDate = StrField('created_date');
  final updatedDate = StrField('updated_date');
  final createdBy = IntField('created_by');
  final updatedBy = IntField('updated_by');
  final englishReceivedDate = StrField('english_received_date');
  final syncDate = StrField('sync_date');
  final syncToServer = BoolField('sync_to_server');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        lnId.name: lnId,
        slnId.name: slnId,
        receivedDate.name: receivedDate,
        quantity.name: quantity,
        userId.name: userId,
        createdDate.name: createdDate,
        updatedDate.name: updatedDate,
        createdBy.name: createdBy,
        updatedBy.name: updatedBy,
        englishReceivedDate.name: englishReceivedDate,
        syncDate.name: syncDate,
        syncToServer.name: syncToServer,
      };
  LiquidNitrogen fromMap(Map map) {
    LiquidNitrogen model = LiquidNitrogen();
    model.lnId = adapter.parseValue(map['ln_id']);
    model.slnId = adapter.parseValue(map['sln_id']);
    model.receivedDate = adapter.parseValue(map['received_date']);
    model.quantity = adapter.parseValue(map['quantity']);
    model.userId = adapter.parseValue(map['user_id']);
    model.createdDate = adapter.parseValue(map['created_date']);
    model.updatedDate = adapter.parseValue(map['updated_date']);
    model.createdBy = adapter.parseValue(map['created_by']);
    model.updatedBy = adapter.parseValue(map['updated_by']);
    model.englishReceivedDate =
        adapter.parseValue(map['english_received_date']);
    model.syncDate = adapter.parseValue(map['sync_date']);
    model.syncToServer = adapter.parseValue(map['sync_to_server']);

    return model;
  }

  List<SetColumn> toSetColumns(LiquidNitrogen model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(lnId.set(model.lnId));
      ret.add(slnId.set(model.slnId));
      ret.add(receivedDate.set(model.receivedDate));
      ret.add(quantity.set(model.quantity));
      ret.add(userId.set(model.userId));
      ret.add(createdDate.set(model.createdDate));
      ret.add(updatedDate.set(model.updatedDate));
      ret.add(createdBy.set(model.createdBy));
      ret.add(updatedBy.set(model.updatedBy));
      ret.add(englishReceivedDate.set(model.englishReceivedDate));
      ret.add(syncDate.set(model.syncDate));
      ret.add(syncToServer.set(model.syncToServer));
    } else if (only != null) {
      if (only.contains(lnId.name)) ret.add(lnId.set(model.lnId));
      if (only.contains(slnId.name)) ret.add(slnId.set(model.slnId));
      if (only.contains(receivedDate.name))
        ret.add(receivedDate.set(model.receivedDate));
      if (only.contains(quantity.name)) ret.add(quantity.set(model.quantity));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(createdDate.name))
        ret.add(createdDate.set(model.createdDate));
      if (only.contains(updatedDate.name))
        ret.add(updatedDate.set(model.updatedDate));
      if (only.contains(createdBy.name))
        ret.add(createdBy.set(model.createdBy));
      if (only.contains(updatedBy.name))
        ret.add(updatedBy.set(model.updatedBy));
      if (only.contains(englishReceivedDate.name))
        ret.add(englishReceivedDate.set(model.englishReceivedDate));
      if (only.contains(syncDate.name)) ret.add(syncDate.set(model.syncDate));
      if (only.contains(syncToServer.name))
        ret.add(syncToServer.set(model.syncToServer));
    } else /* if (onlyNonNull) */ {
      if (model.lnId != null) {
        ret.add(lnId.set(model.lnId));
      }
      if (model.slnId != null) {
        ret.add(slnId.set(model.slnId));
      }
      if (model.receivedDate != null) {
        ret.add(receivedDate.set(model.receivedDate));
      }
      if (model.quantity != null) {
        ret.add(quantity.set(model.quantity));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.createdDate != null) {
        ret.add(createdDate.set(model.createdDate));
      }
      if (model.updatedDate != null) {
        ret.add(updatedDate.set(model.updatedDate));
      }
      if (model.createdBy != null) {
        ret.add(createdBy.set(model.createdBy));
      }
      if (model.updatedBy != null) {
        ret.add(updatedBy.set(model.updatedBy));
      }
      if (model.englishReceivedDate != null) {
        ret.add(englishReceivedDate.set(model.englishReceivedDate));
      }
      if (model.syncDate != null) {
        ret.add(syncDate.set(model.syncDate));
      }
      if (model.syncToServer != null) {
        ret.add(syncToServer.set(model.syncToServer));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addStr(lnId.name, primary: true, isNullable: false);
    st.addInt(slnId.name, isNullable: true);
    st.addStr(receivedDate.name, isNullable: true);
    st.addDouble(quantity.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addStr(createdDate.name, isNullable: true);
    st.addStr(updatedDate.name, isNullable: true);
    st.addInt(createdBy.name, isNullable: true);
    st.addInt(updatedBy.name, isNullable: true);
    st.addStr(englishReceivedDate.name, isNullable: true);
    st.addStr(syncDate.name, isNullable: true);
    st.addBool(syncToServer.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(LiquidNitrogen model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<LiquidNitrogen> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(LiquidNitrogen model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<LiquidNitrogen> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(LiquidNitrogen model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.lnId.eq(model.lnId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<LiquidNitrogen> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.lnId.eq(model.lnId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<LiquidNitrogen> find(String lnId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.lnId.eq(lnId));
    return await findOne(find);
  }

  Future<int> remove(String lnId) async {
    final Remove remove = remover.where(this.lnId.eq(lnId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<LiquidNitrogen> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.lnId.eq(model.lnId));
    }
    return adapter.remove(remove);
  }
}
