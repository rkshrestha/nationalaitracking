// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Province.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Province _$ProvinceFromJson(Map<String, dynamic> json) {
  return Province()
    ..pId = json['pId'] as int
    ..provinceid = json['ProvinceId'] as int
    ..state = json['States'] as String;
}

Map<String, dynamic> _$ProvinceToJson(Province instance) => <String, dynamic>{
      'pId': instance.pId,
      'ProvinceId': instance.provinceid,
      'States': instance.state
    };
