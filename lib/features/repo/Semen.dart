import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'Semen.g.dart';

part 'Semen.jorm.dart';

@JsonSerializable()
class Semen implements JsonCallable<Semen> {
//  @PrimaryKey(auto: true)
//  @JsonKey(name: 'MsId')
//  int msId;
  @PrimaryKey()
  @JsonKey(name: 'MsId')
  String msId;
  @JsonKey(name: 'SId')
  @Column(isNullable: true)
  int semenId;
  @JsonKey(name: 'UserId')
  @Column(isNullable: true)
  int userId;
  @JsonKey(name: 'ReceivedDate')
  @Column(isNullable: true)
  String receivedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedDate')
  String createdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedDate')
  String updatedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedBy')
  int createdBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedBy')
  int updatedBy;
  @JsonKey(name: 'Jersey')
  @Column(isNullable: true)
  double jersey;
  @JsonKey(name: 'Boar')
  @Column(isNullable: true)
  double boar;
  @JsonKey(name: 'Saanen')
  @Column(isNullable: true)
  double saanen;
  @JsonKey(name: 'Yorkshire')
  @Column(isNullable: true)
  double yorkshire;
  @JsonKey(name: 'Landrace')
  @Column(isNullable: true)
  double landrace;
  @JsonKey(name: 'Duroc')
  @Column(isNullable: true)
  double duroc;
  @JsonKey(name: 'Hampshire')
  @Column(isNullable: true)
  double hampshire;
  @JsonKey(name: 'HolsteinFriesian')
  @Column(isNullable: true)
  double holsteinFriesian;
  @JsonKey(name: 'Murrah')
  @Column(isNullable: true)
  double murrah;
  @JsonKey(name: 'EnglishReceivedDate')
  @Column(isNullable: true)
  String englishReceivedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'SyncDate')
  String syncDate;
  @JsonKey(name: "SyncToServer")
  bool syncToServer = false;

  Semen();

  Semen.make(
      this.msId,
      this.semenId,
      this.userId,
      this.receivedDate,
      this.createdDate,
      this.updatedDate,
      this.createdBy,
      this.updatedBy,
      this.jersey,
      this.boar,
      this.saanen,
      this.yorkshire,
      this.landrace,
      this.duroc,
      this.hampshire,
      this.holsteinFriesian,
      this.murrah,
      this.englishReceivedDate,
      this.syncDate,
      {this.syncToServer = false});

  factory Semen.fromJson(Map<String, dynamic> json) => _$SemenFromJson(json);

  Map<String, dynamic> toJson() => _$SemenToJson(this);

  @override
  Semen fromJson(Map<String, dynamic> json) {
    return Semen.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class SemenBean extends Bean<Semen> with _SemenBean {
  SemenBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'semens';
}
