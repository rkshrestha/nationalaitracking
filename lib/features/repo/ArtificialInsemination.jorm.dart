// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ArtificialInsemination.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _ArtificialInseminationBean
    implements Bean<ArtificialInsemination> {
  final aiId = StrField('ai_id');
  final ainId = IntField('ain_id');
  final sfarmerId = IntField('sfarmer_id');
  final mobileNumber = StrField('mobile_number');
  final speciesCode = StrField('species_code');
  final speciesId = IntField('species_id');
  final breedId = IntField('breed_id');
  final sAnimalId = IntField('s_animal_id');
  final aiDate = StrField('ai_date');
  final currentParity = IntField('current_parity');
  final aiSerial = IntField('ai_serial');
  final remarks = StrField('remarks');
  final createdDate = StrField('created_date');
  final updatedDate = StrField('updated_date');
  final createdBy = IntField('created_by');
  final updatedBy = IntField('updated_by');
  final shireId = StrField('shire_id');
  final mfarmerId = StrField('mfarmer_id');
  final aId = StrField('a_id');
  final userId = IntField('user_id');
  final tentativePDDate = StrField('tentative_p_d_date');
  final farmerName = StrField('farmer_name');
  final englishAiDate = StrField('english_ai_date');
  final englishTentativePDDate = StrField('english_tentative_p_d_date');
  final syncDate = StrField('sync_date');
  final syncToServer = BoolField('sync_to_server');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        aiId.name: aiId,
        ainId.name: ainId,
        sfarmerId.name: sfarmerId,
        mobileNumber.name: mobileNumber,
        speciesCode.name: speciesCode,
        speciesId.name: speciesId,
        breedId.name: breedId,
        sAnimalId.name: sAnimalId,
        aiDate.name: aiDate,
        currentParity.name: currentParity,
        aiSerial.name: aiSerial,
        remarks.name: remarks,
        createdDate.name: createdDate,
        updatedDate.name: updatedDate,
        createdBy.name: createdBy,
        updatedBy.name: updatedBy,
        shireId.name: shireId,
        mfarmerId.name: mfarmerId,
        aId.name: aId,
        userId.name: userId,
        tentativePDDate.name: tentativePDDate,
        farmerName.name: farmerName,
        englishAiDate.name: englishAiDate,
        englishTentativePDDate.name: englishTentativePDDate,
        syncDate.name: syncDate,
        syncToServer.name: syncToServer,
      };
  ArtificialInsemination fromMap(Map map) {
    ArtificialInsemination model = ArtificialInsemination();
    model.aiId = adapter.parseValue(map['ai_id']);
    model.ainId = adapter.parseValue(map['ain_id']);
    model.sfarmerId = adapter.parseValue(map['sfarmer_id']);
    model.mobileNumber = adapter.parseValue(map['mobile_number']);
    model.speciesCode = adapter.parseValue(map['species_code']);
    model.speciesId = adapter.parseValue(map['species_id']);
    model.breedId = adapter.parseValue(map['breed_id']);
    model.sAnimalId = adapter.parseValue(map['s_animal_id']);
    model.aiDate = adapter.parseValue(map['ai_date']);
    model.currentParity = adapter.parseValue(map['current_parity']);
    model.aiSerial = adapter.parseValue(map['ai_serial']);
    model.remarks = adapter.parseValue(map['remarks']);
    model.createdDate = adapter.parseValue(map['created_date']);
    model.updatedDate = adapter.parseValue(map['updated_date']);
    model.createdBy = adapter.parseValue(map['created_by']);
    model.updatedBy = adapter.parseValue(map['updated_by']);
    model.shireId = adapter.parseValue(map['shire_id']);
    model.mfarmerId = adapter.parseValue(map['mfarmer_id']);
    model.aId = adapter.parseValue(map['a_id']);
    model.userId = adapter.parseValue(map['user_id']);
    model.tentativePDDate = adapter.parseValue(map['tentative_p_d_date']);
    model.farmerName = adapter.parseValue(map['farmer_name']);
    model.englishAiDate = adapter.parseValue(map['english_ai_date']);
    model.englishTentativePDDate =
        adapter.parseValue(map['english_tentative_p_d_date']);
    model.syncDate = adapter.parseValue(map['sync_date']);
    model.syncToServer = adapter.parseValue(map['sync_to_server']);

    return model;
  }

  List<SetColumn> toSetColumns(ArtificialInsemination model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(aiId.set(model.aiId));
      ret.add(ainId.set(model.ainId));
      ret.add(sfarmerId.set(model.sfarmerId));
      ret.add(mobileNumber.set(model.mobileNumber));
      ret.add(speciesCode.set(model.speciesCode));
      ret.add(speciesId.set(model.speciesId));
      ret.add(breedId.set(model.breedId));
      ret.add(sAnimalId.set(model.sAnimalId));
      ret.add(aiDate.set(model.aiDate));
      ret.add(currentParity.set(model.currentParity));
      ret.add(aiSerial.set(model.aiSerial));
      ret.add(remarks.set(model.remarks));
      ret.add(createdDate.set(model.createdDate));
      ret.add(updatedDate.set(model.updatedDate));
      ret.add(createdBy.set(model.createdBy));
      ret.add(updatedBy.set(model.updatedBy));
      ret.add(shireId.set(model.shireId));
      ret.add(mfarmerId.set(model.mfarmerId));
      ret.add(aId.set(model.aId));
      ret.add(userId.set(model.userId));
      ret.add(tentativePDDate.set(model.tentativePDDate));
      ret.add(farmerName.set(model.farmerName));
      ret.add(englishAiDate.set(model.englishAiDate));
      ret.add(englishTentativePDDate.set(model.englishTentativePDDate));
      ret.add(syncDate.set(model.syncDate));
      ret.add(syncToServer.set(model.syncToServer));
    } else if (only != null) {
      if (only.contains(aiId.name)) ret.add(aiId.set(model.aiId));
      if (only.contains(ainId.name)) ret.add(ainId.set(model.ainId));
      if (only.contains(sfarmerId.name))
        ret.add(sfarmerId.set(model.sfarmerId));
      if (only.contains(mobileNumber.name))
        ret.add(mobileNumber.set(model.mobileNumber));
      if (only.contains(speciesCode.name))
        ret.add(speciesCode.set(model.speciesCode));
      if (only.contains(speciesId.name))
        ret.add(speciesId.set(model.speciesId));
      if (only.contains(breedId.name)) ret.add(breedId.set(model.breedId));
      if (only.contains(sAnimalId.name))
        ret.add(sAnimalId.set(model.sAnimalId));
      if (only.contains(aiDate.name)) ret.add(aiDate.set(model.aiDate));
      if (only.contains(currentParity.name))
        ret.add(currentParity.set(model.currentParity));
      if (only.contains(aiSerial.name)) ret.add(aiSerial.set(model.aiSerial));
      if (only.contains(remarks.name)) ret.add(remarks.set(model.remarks));
      if (only.contains(createdDate.name))
        ret.add(createdDate.set(model.createdDate));
      if (only.contains(updatedDate.name))
        ret.add(updatedDate.set(model.updatedDate));
      if (only.contains(createdBy.name))
        ret.add(createdBy.set(model.createdBy));
      if (only.contains(updatedBy.name))
        ret.add(updatedBy.set(model.updatedBy));
      if (only.contains(shireId.name)) ret.add(shireId.set(model.shireId));
      if (only.contains(mfarmerId.name))
        ret.add(mfarmerId.set(model.mfarmerId));
      if (only.contains(aId.name)) ret.add(aId.set(model.aId));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(tentativePDDate.name))
        ret.add(tentativePDDate.set(model.tentativePDDate));
      if (only.contains(farmerName.name))
        ret.add(farmerName.set(model.farmerName));
      if (only.contains(englishAiDate.name))
        ret.add(englishAiDate.set(model.englishAiDate));
      if (only.contains(englishTentativePDDate.name))
        ret.add(englishTentativePDDate.set(model.englishTentativePDDate));
      if (only.contains(syncDate.name)) ret.add(syncDate.set(model.syncDate));
      if (only.contains(syncToServer.name))
        ret.add(syncToServer.set(model.syncToServer));
    } else /* if (onlyNonNull) */ {
      if (model.aiId != null) {
        ret.add(aiId.set(model.aiId));
      }
      if (model.ainId != null) {
        ret.add(ainId.set(model.ainId));
      }
      if (model.sfarmerId != null) {
        ret.add(sfarmerId.set(model.sfarmerId));
      }
      if (model.mobileNumber != null) {
        ret.add(mobileNumber.set(model.mobileNumber));
      }
      if (model.speciesCode != null) {
        ret.add(speciesCode.set(model.speciesCode));
      }
      if (model.speciesId != null) {
        ret.add(speciesId.set(model.speciesId));
      }
      if (model.breedId != null) {
        ret.add(breedId.set(model.breedId));
      }
      if (model.sAnimalId != null) {
        ret.add(sAnimalId.set(model.sAnimalId));
      }
      if (model.aiDate != null) {
        ret.add(aiDate.set(model.aiDate));
      }
      if (model.currentParity != null) {
        ret.add(currentParity.set(model.currentParity));
      }
      if (model.aiSerial != null) {
        ret.add(aiSerial.set(model.aiSerial));
      }
      if (model.remarks != null) {
        ret.add(remarks.set(model.remarks));
      }
      if (model.createdDate != null) {
        ret.add(createdDate.set(model.createdDate));
      }
      if (model.updatedDate != null) {
        ret.add(updatedDate.set(model.updatedDate));
      }
      if (model.createdBy != null) {
        ret.add(createdBy.set(model.createdBy));
      }
      if (model.updatedBy != null) {
        ret.add(updatedBy.set(model.updatedBy));
      }
      if (model.shireId != null) {
        ret.add(shireId.set(model.shireId));
      }
      if (model.mfarmerId != null) {
        ret.add(mfarmerId.set(model.mfarmerId));
      }
      if (model.aId != null) {
        ret.add(aId.set(model.aId));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.tentativePDDate != null) {
        ret.add(tentativePDDate.set(model.tentativePDDate));
      }
      if (model.farmerName != null) {
        ret.add(farmerName.set(model.farmerName));
      }
      if (model.englishAiDate != null) {
        ret.add(englishAiDate.set(model.englishAiDate));
      }
      if (model.englishTentativePDDate != null) {
        ret.add(englishTentativePDDate.set(model.englishTentativePDDate));
      }
      if (model.syncDate != null) {
        ret.add(syncDate.set(model.syncDate));
      }
      if (model.syncToServer != null) {
        ret.add(syncToServer.set(model.syncToServer));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addStr(aiId.name, primary: true, isNullable: false);
    st.addInt(ainId.name, isNullable: true);
    st.addInt(sfarmerId.name, isNullable: true);
    st.addStr(mobileNumber.name, isNullable: true);
    st.addStr(speciesCode.name, isNullable: true);
    st.addInt(speciesId.name, isNullable: true);
    st.addInt(breedId.name, isNullable: true);
    st.addInt(sAnimalId.name, isNullable: true);
    st.addStr(aiDate.name, isNullable: true);
    st.addInt(currentParity.name, isNullable: true);
    st.addInt(aiSerial.name, isNullable: true);
    st.addStr(remarks.name, isNullable: true);
    st.addStr(createdDate.name, isNullable: true);
    st.addStr(updatedDate.name, isNullable: true);
    st.addInt(createdBy.name, isNullable: true);
    st.addInt(updatedBy.name, isNullable: true);
    st.addStr(shireId.name, isNullable: true);
    st.addStr(mfarmerId.name, isNullable: true);
    st.addStr(aId.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addStr(tentativePDDate.name, isNullable: true);
    st.addStr(farmerName.name, isNullable: true);
    st.addStr(englishAiDate.name, isNullable: true);
    st.addStr(englishTentativePDDate.name, isNullable: true);
    st.addStr(syncDate.name, isNullable: true);
    st.addBool(syncToServer.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(ArtificialInsemination model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<ArtificialInsemination> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(ArtificialInsemination model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<ArtificialInsemination> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(ArtificialInsemination model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.aiId.eq(model.aiId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<ArtificialInsemination> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.aiId.eq(model.aiId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<ArtificialInsemination> find(String aiId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.aiId.eq(aiId));
    return await findOne(find);
  }

  Future<int> remove(String aiId) async {
    final Remove remove = remover.where(this.aiId.eq(aiId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<ArtificialInsemination> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.aiId.eq(model.aiId));
    }
    return adapter.remove(remove);
  }
}
