
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/ExitReason.dart';
import 'package:nbis/s/Logger.dart';

class ExitReasonDao implements BaseDao<ExitReason>{
  static ExitReasonDao _instance;
  ExitReasonBean bean;

  ExitReasonDao._(this.bean);

  static ExitReasonDao get(ExitReasonBean bean) {
    if (_instance == null) {
      _instance = ExitReasonDao._(bean);
    }
    return _instance;
  }


  @override
  Future<void> delete(ExitReason exitRecord) {
    Logger.v('exitId:${exitRecord.eId}');
    return bean.remove(exitRecord.eId);
  }

  @override
  Future<void> deleteAll() {
 return bean.removeAll();
  }

  @override
  Future<List<ExitReason>> findAll() {
    Find findReason = Find(bean.tableName);
    return bean.findMany(findReason);
  }

  @override
  Future<ExitReason> findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Future<ExitReason> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<void> persist(ExitReason exitReason) {
    Logger.v("reason inserted");
    return bean.insert(exitReason);
  }

  @override
  Future<void> update(ExitReason entity) {
    // TODO: implement update
    return null;
  }
}