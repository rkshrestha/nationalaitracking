// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Farmer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Farmer _$FarmerFromJson(Map<String, dynamic> json) {
  return Farmer()
    ..fId = json['MId'] as String
    ..farmerId = json['FarmerId'] as int
    ..mobileNumber = json['MobileNo'] as String
    ..farmerName = json['FarmerName'] as String
    ..provinceId = json['ProvinceId'] as int
    ..province = json['Province'] as String
    ..districtId = json['DistrictId'] as int
    ..district = json['District'] as String
    ..municipalityId = json['MunicipalityId'] as int
    ..municipality = json['Municipality'] as String
    ..ward = json['Ward'] as int
    ..village = json['Village'] as String
    ..createdDate = json['CreatedDate'] as String
    ..updatedDate = json['UpdatedDate'] as String
    ..createdBy = json['CreatedBy'] as int
    ..updatedBy = json['UpdatedBy'] as int
    ..userId = json['UserId'] as int
    ..syncDate = json['SyncDate'] as String
    ..syncToServer = json['SyncToServer'] as bool;
}

Map<String, dynamic> _$FarmerToJson(Farmer instance) => <String, dynamic>{
      'MId': instance.fId,
      'FarmerId': instance.farmerId,
      'MobileNo': instance.mobileNumber,
      'FarmerName': instance.farmerName,
      'ProvinceId': instance.provinceId,
      'Province': instance.province,
      'DistrictId': instance.districtId,
      'District': instance.district,
      'MunicipalityId': instance.municipalityId,
      'Municipality': instance.municipality,
      'Ward': instance.ward,
      'Village': instance.village,
      'CreatedDate': instance.createdDate,
      'UpdatedDate': instance.updatedDate,
      'CreatedBy': instance.createdBy,
      'UpdatedBy': instance.updatedBy,
      'UserId': instance.userId,
      'SyncDate': instance.syncDate,
      'SyncToServer': instance.syncToServer
    };
