// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ExitRecord.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExitRecord _$ExitRecordFromJson(Map<String, dynamic> json) {
  return ExitRecord()
    ..eId = json['MExitRecordId'] as String
    ..erId = json['ExitRecordId'] as int
    ..farmerId = json['FarmerId'] as int
    ..animalId = json['AnimalId'] as int
    ..speciesId = json['SpeciesId'] as int
    ..breedId = json['BreedId'] as int
    ..dateOfExit = json['DateOfExit'] as String
    ..exitReason = json['ExitReason'] as String
    ..mobileNumber = json['MobileNo'] as String
    ..speciesCode = json['SpeciesCode'] as String
    ..createdDate = json['CreatedDate'] as String
    ..updatedDate = json['UpdatedDate'] as String
    ..createdBy = json['CreatedBy'] as int
    ..updatedBy = json['UpdatedBy'] as int
    ..mFarmerId = json['MFarmerId'] as String
    ..mAnimalId = json['MAnimalId'] as String
    ..userId = json['UserId'] as int
    ..farmerName = json['FarmerName'] as String
    ..englishExitDate = json['EnglishDateOfExit'] as String
    ..syncDate = json['SyncDate'] as String
    ..syncToServer = json['SyncToServer'] as bool;
}

Map<String, dynamic> _$ExitRecordToJson(ExitRecord instance) =>
    <String, dynamic>{
      'MExitRecordId': instance.eId,
      'ExitRecordId': instance.erId,
      'FarmerId': instance.farmerId,
      'AnimalId': instance.animalId,
      'SpeciesId': instance.speciesId,
      'BreedId': instance.breedId,
      'DateOfExit': instance.dateOfExit,
      'ExitReason': instance.exitReason,
      'MobileNo': instance.mobileNumber,
      'SpeciesCode': instance.speciesCode,
      'CreatedDate': instance.createdDate,
      'UpdatedDate': instance.updatedDate,
      'CreatedBy': instance.createdBy,
      'UpdatedBy': instance.updatedBy,
      'MFarmerId': instance.mFarmerId,
      'MAnimalId': instance.mAnimalId,
      'UserId': instance.userId,
      'FarmerName': instance.farmerName,
      'EnglishDateOfExit': instance.englishExitDate,
      'SyncDate': instance.syncDate,
      'SyncToServer': instance.syncToServer
    };
