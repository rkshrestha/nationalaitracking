import 'dart:async';

import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/AIDao.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalDao.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/CalvingDao.dart';
import 'package:nbis/features/repo/District.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/ExitRecordDao.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerDao.dart';
import 'package:nbis/features/repo/Municipality.dart';
import 'package:nbis/features/repo/PDDao.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Province.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:uuid/uuid.dart';

class FarmerRepo implements Repository<Farmer> {
  SQFliteDatabase sqFliteDatabase;
  FarmerDao farmerDao;
  SessionManager sessionManager;
  List<Farmer> farmerList;
  BuildContext context;
  int farmerServerId;

  AnimalDao animalDao;
  AIDao aiDao;
  PDDao pdDao;
  CalvingDao calvingDao;
  ExitRecordDao exitDao;

  FarmerRepo(context) {
    Logger.v("inside farmerRepo");
    init();
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
  }

  void saveFarmer(context, Farmer farmer, Province province, District district,
      Municipality municipality, StreamSink<SCResponse<String>> sSink) async {
    try {
      farmerDao = await sqFliteDatabase.farmerDao();

      var user = this.sessionManager
          .findOne()
          .user;
      Farmer farmers = await farmerDao.findBySameMobileNumber(farmer);

      DateTime today = new DateTime.now();
      DateTime todayDate = new DateTime(today.year + 56, today.month + 8,
          today.day + 17, today.hour, today.minute, today.second);
      var uuid = new Uuid();
      var uid= uuid.v1();

//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
      if (farmers == null) {
        int farmerId = await farmerDao.persist(new Farmer.make(
//            farmer.fId,
        uid,
            0,
            farmer.mobileNumber,
            farmer.farmerName,
            province.provinceid,
            province.state,
            district.districtid,
            district.name,
            municipality.municipalityid,
            municipality.name,
            farmer.ward,
            farmer.village,
            today.toIso8601String(),
            null,
            user.userid,
            0,
            user.userid,
            null,
            syncToServer: false));

        Navigator.pop(context, true);

//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("farmer_add_success"),
//        ));
        sSink.add(SCResponse.success("Farmer has been successfully added"));
      } else {
        sSink.add(SCResponse.error("Farmer already exists"));
//        sSink.add(SCResponse.error(
//          AppTranslations.of(context).text("farmer_exist"),
//        ));
      }
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't add farmer"));
      Logger.v(ex.toString());
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("farmer_add_fail"),
//      ));
    }
  }

  void updateFarmer(context,
      Farmer farmer,
      Province province,
      District district,
      Municipality municipality,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try updateFarmer' + farmer.farmerName);

      farmerDao = await sqFliteDatabase.farmerDao();
      animalDao = await sqFliteDatabase.animalDao();
      aiDao = await sqFliteDatabase.aiDao();
      pdDao = await sqFliteDatabase.pdDao();
      calvingDao = await sqFliteDatabase.calvingDao();
      exitDao = await sqFliteDatabase.exitRecordDao();
      var user = this.sessionManager
          .findOne()
          .user;
      Logger.v(user.userid.toString());

//      final dates = DateTime.now().toIso8601String();
//
//    String today = DateFormat('yyyy-MM-dd').format(dates);
      DateTime today = new DateTime.now();
      DateTime todayDate = new DateTime(today.year + 56, today.month + 8,
          today.day + 17, today.hour, today.minute, today.second);

//      Farmer farmers = await farmerDao.findBySameMobileNumberUpdate(farmer);

//      if (farmers == null) {
        int farmerUpdate = await farmerDao.update(new Farmer.make(
            farmer.fId,
            farmer.farmerId,
            farmer.mobileNumber,
            farmer.farmerName,
            province.provinceid,
            province.state,
            district.districtid,
            district.name,
            municipality.municipalityid,
            municipality.name,
            farmer.ward,
            farmer.village,
//          todayDate.toIso8601String(),
            farmer.createdDate,
            today.toIso8601String(),
//          todayNepaliDate,
            user.userid,
            user.userid,
            user.userid,
            farmer.syncDate,
            syncToServer: false));

        await animalDao.updateFarmerMblNumber(farmer);
        await aiDao.updateFarmerMblNumber(farmer);
        await pdDao.updateFarmerMblNumber(farmer);
        await calvingDao.updateFarmerMblNumber(farmer);
        await exitDao.updateFarmerMblNumber(farmer);
        Navigator.pop(context, true);
//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("farmer_update_success"),
//        ));

        sSink.add(SCResponse.success("Farmer has been successfully updated"));
//      } else {
//        sSink.add(SCResponse.error("Farmer already exists"));
////        sSink.add(SCResponse.error(
////          AppTranslations.of(context).text("farmer_exist"),
////        ));
//      }
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't update Farmer"));
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("farmer_update_fail"),
//      ));
      Logger.v(ex.toString());
    }
  }

  void getFarmerList(StreamSink<SCResponse<List<Farmer>>> stSink) async {
    try {
      farmerDao = await sqFliteDatabase.farmerDao();

      farmerList = await farmerDao.findAll();

      for (Farmer farmer in farmerList) {
        Logger.v(farmer.farmerName);
      }

      stSink.add(SCResponse.success(farmerList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getUnSyncFarmerList(StreamSink<SCResponse<List<Farmer>>> stSink) async {
    try {
      sqFliteDatabase = SQFliteDatabase.getDB();

      await sqFliteDatabase.connect();
      farmerDao = await sqFliteDatabase.farmerDao();

      farmerList = await farmerDao.findAllUnSyncData();

      for (Farmer farmer in farmerList) {
        Logger.v(farmer.farmerName);
      }

      stSink.add(SCResponse.success(farmerList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSyncFarmerList(StreamSink<SCResponse<List<Farmer>>> stSink) async {
    try {
      farmerDao = await sqFliteDatabase.farmerDao();

      farmerList = await farmerDao.findAllSyncData();

      for (Farmer farmer in farmerList) {
        Logger.v(farmer.farmerName);
      }

      stSink.add(SCResponse.success(farmerList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

//  void getFarmer(
//      Farmer farmer, StreamController<SCResponse<Farmer>> stSink) async {
//    try {
//      farmerDao = await sqFliteDatabase.farmerDao();
//      Farmer farmer = await farmerDao.findById(1);
//
//      Logger.v('printing one id: ' + farmer.farmerName);
//
//      stSink.add(SCResponse.success(farmer));
//    } catch (ex) {
//      Logger.v(ex.toString());
//    }
//  }

  void getFarmerByMobileNumber(context, String mblNumber,
      StreamController<SCResponse<Farmer>> stSink) async {

    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {


      farmerDao = await sqFliteDatabase.farmerDao();
      Logger.v('mblNumber: ' + mblNumber);
      var farmer = await farmerDao.getFarmerByMobileNumber(mblNumber);
      Logger.v("show details");
      Logger.v('printing: ' + farmer.toString());
      if (farmer == null) {
//        stSink.add(SCResponse.error(
//          AppTranslations.of(context).text("not_match"),
//        ));
        stSink.add(SCResponse.error("Not match found"));
      } else {
        stSink.add(SCResponse.success(farmer));
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //get farmer update
  void getFarmerByfId(String farmerId,
      StreamController<SCResponse<Farmer>> stSink) async {
    try {
      farmerDao = await sqFliteDatabase.farmerDao();
      var farmer = await farmerDao.getFarmerById(farmerId);

      stSink.add(SCResponse.success(farmer));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //get mobile and farmer
  void getFarmerMobileNumberById(String fId,
      StreamController<SCResponse<Farmer>> stSink) async {
    try {
      Logger.v("getIdsssss: " + fId.toString());
      farmerDao = await sqFliteDatabase.farmerDao();
      Logger.v("after farmerDao: " + fId.toString());
      var farmer = await farmerDao.getFarmerMobileNumberById(fId);
      Logger.v('printingMblfirst ' + farmer.mobileNumber.toString());

      if (farmer != null) {
        Logger.v('printingMbl ' + farmer.mobileNumber.toString());
        stSink.add(SCResponse.success(farmer));
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  deleteFarmer(context, Farmer farmer,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      farmerDao = await sqFliteDatabase.farmerDao();

      await farmerDao.delete(farmer);

//      sSink.add(SCResponse.success(
//        AppTranslations.of(context).text("farmer_delete_success"),
//      ));
      sSink.add(SCResponse.success("Farmer deleted Successfully"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("farmer_delete_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't delete farmer"));
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local start
  void updateServerIdAndStatus(List<Farmer> farmerList,
      StreamSink<SCResponse<List<Farmer>>> sSink) async {
    try {
      sqFliteDatabase = SQFliteDatabase.getDB();
      await sqFliteDatabase.connect();
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      NetworkClient networkClient = NetworkClient();
      farmerDao = await sqFliteDatabase.farmerDao();
      animalDao = await sqFliteDatabase.animalDao();
      aiDao = await sqFliteDatabase.aiDao();
      pdDao = await sqFliteDatabase.pdDao();
      calvingDao = await sqFliteDatabase.calvingDao();
      exitDao = await sqFliteDatabase.exitRecordDao();
      var user = this.sessionManager
          .findOne()
          .user;


      //get list
      List<Animal> unsyncedAnimalList = await animalDao.findAllUnSyncData();
      List<ArtificialInsemination> unsyncedAIList =
      await aiDao.findAllUnSyncData();
      List<PregnancyDiagnosis> unsyncedPDList = await pdDao.findAllUnSyncData();
      List<Calving> unsyncedCalvingList = await calvingDao.findAllUnSyncData();
      List<ExitRecord> unsyncedExitList = await exitDao.findAllUnSyncData();

      for (Farmer syncedFarmer in farmerList) {
        await farmerDao.updateByMobileNumberSync(syncedFarmer);

        await farmerDao.updateByMobileNumber(syncedFarmer);

        //update server farmer id in animal table
        if (unsyncedAnimalList != null) {
          for (Animal unsyncedAnimal in unsyncedAnimalList) {
            await animalDao.updateFarmerId(syncedFarmer);
          }
        }

        //update server farmer id in ai table

        if (unsyncedAIList != null) {
          for (ArtificialInsemination ai in unsyncedAIList) {
            await aiDao.updateFarmerIdInAI(syncedFarmer);
          }
        }

        //update server farmer id in pd table

        if (unsyncedPDList != null) {
          for (PregnancyDiagnosis pd in unsyncedPDList) {
            await pdDao.updateFarmerIdInPD(syncedFarmer);
          }
        }

        //update server farmer id in calving table

        if (unsyncedCalvingList != null) {
          for (Calving calving in unsyncedCalvingList) {
            await calvingDao.updateFarmerIdInCalving(syncedFarmer);
          }
        }

        //update server farmer id in exit table

        if (unsyncedExitList != null) {
          for (ExitRecord exit in unsyncedExitList) {
            await exitDao.updateFarmerIdInExit(syncedFarmer);
          }
        }
      }


    }catch(ex){
      Logger.v(ex.toString());
    }
  }


  //end of server id update in local


  //syc to server

  //farmer send
  void sendFarmerData(List<Farmer> farmerList,
      StreamSink<SCResponse<List<Farmer>>> sSink) async {
    Logger.v("Test  :" + DsonUtils.toJsonString(farmerList));
    try {
//      sqFliteDatabase = SQFliteDatabase.getDB();
//      await sqFliteDatabase.connect();

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();
      farmerDao = await sqFliteDatabase.farmerDao();
      animalDao = await sqFliteDatabase.animalDao();
      aiDao = await sqFliteDatabase.aiDao();
      pdDao = await sqFliteDatabase.pdDao();
      calvingDao = await sqFliteDatabase.calvingDao();
      exitDao = await sqFliteDatabase.exitRecordDao();
      var user = this.sessionManager
          .findOne()
          .user;

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      List<Farmer> unsyncedFarmerList = await farmerDao.findAllBySyncStatus();

      List<Animal> unsyncedAnimalList = await animalDao.findAllUnSyncData();
      List<ArtificialInsemination> unsyncedAIList =
      await aiDao.findAllUnSyncData();
      List<PregnancyDiagnosis> unsyncedPDList = await pdDao.findAllUnSyncData();
      List<Calving> unsyncedCalvingList = await calvingDao.findAllUnSyncData();
      List<ExitRecord> unsyncedExitList = await exitDao.findAllUnSyncData();

      Logger.v(
          "Test FarmerList :" + DsonUtils.toJsonString(farmerList));
//          "Test FarmerList :" + DsonUtils.toJsonString(unsyncedFarmerList));
      SCResponse scResponse = await networkClient.call<Farmer, List<Farmer>>(
          networkApi.sendFarmerData(farmerList, user.userid),
//          networkApi.sendFarmerData(unsyncedFarmerList, user.userid),
          callable: Farmer());

      if (scResponse.status == Status.SUCCESS) {
        List<Farmer> syncedFarmerList = scResponse.data;

        for (Farmer syncedFarmer in syncedFarmerList) {
          await farmerDao.updateByMobileNumberSync(syncedFarmer);

          await farmerDao.updateByMobileNumber(syncedFarmer);

          //update server farmer id in animal table
          if (unsyncedAnimalList != null) {
            for (Animal unsyncedAnimal in unsyncedAnimalList) {
              await animalDao.updateFarmerId(syncedFarmer);
            }
          }

          //update server farmer id in ai table

          if (unsyncedAIList != null) {
            for (ArtificialInsemination ai in unsyncedAIList) {
              await aiDao.updateFarmerIdInAI(syncedFarmer);
            }
          }

          //update server farmer id in pd table

          if (unsyncedPDList != null) {
            for (PregnancyDiagnosis pd in unsyncedPDList) {
              await pdDao.updateFarmerIdInPD(syncedFarmer);
            }
          }

          //update server farmer id in calving table

          if (unsyncedCalvingList != null) {
            for (Calving calving in unsyncedCalvingList) {
              await calvingDao.updateFarmerIdInCalving(syncedFarmer);
            }
          }

          //update server farmer id in exit table

          if (unsyncedExitList != null) {
            for (ExitRecord exit in unsyncedExitList) {
              await exitDao.updateFarmerIdInExit(syncedFarmer);
            }
          }
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }

      sSink.add(scResponse);
    } catch (ex) {
      Logger.v(ex.toString());
    }
//    finally{
//      sqFliteDatabase.close();
//    }
  }

  //get farmer from server
  void syncGetFarmerData(StreamSink<SCResponse<List<Farmer>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager
          .findOne()
          .user;
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient.call<Farmer, List<Farmer>>(
          networkApi.syncGetFarmerList(user.userid),
          callable: Farmer());

//      if (scResponse.status == Status.SUCCESS) {
//        List<Farmer> farmerList = scResponse.data;
//        farmerDao = await sqFliteDatabase.farmerDao();
//
//        for (Farmer farmerServer in farmerList) {
//          farmerServerId = farmerServer.farmerId;
//          Logger.v("ServerId " + farmerServerId.toString());
//          Farmer farmer = await farmerDao.findByFarmerId(farmerServerId);
//
//          if (farmer == null) {
//            farmerDao.delete(farmerServer);
//            farmerDao.persist(new Farmer.make(
//                farmerServer.fId,
//                farmerServer.farmerId,
//                farmerServer.userId,
//                farmerServer.mobileNumber,
//                farmerServer.farmerName,
//                farmerServer.provinceId,
//                farmerServer.province,
//                farmerServer.districtId,
//                farmerServer.district,
//                farmerServer.municipalityId,
//                farmerServer.municipality,
//                farmerServer.ward,
//                farmerServer.village));
//          } else {
//            if (farmer.fId == farmerServer.fId) {
//              farmerDao.update(Farmer.make(
//                  farmerServer.fId,
//                  farmerServer.farmerId,
//                  farmerServer.userId,
//                  farmerServer.mobileNumber,
//                  farmerServer.farmerName,
//                  farmerServer.provinceId,
//                  farmerServer.province,
//                  farmerServer.districtId,
//                  farmerServer.district,
//                  farmerServer.municipalityId,
//                  farmerServer.municipality,
//                  farmerServer.ward,
//                  farmerServer.village));
//            }
//          }
//        }
//
//
//        sSink.add(SCResponse.success("Farmer Sync success"));
//      }
      if (scResponse.status == Status.SUCCESS) {
        List<Farmer> farmerLists = scResponse.data;
        farmerDao = await sqFliteDatabase.farmerDao();

        for (Farmer farmerServer in farmerLists) {
          farmerServerId = farmerServer.farmerId;
          Logger.v("ServerSId " + farmerServerId.toString());
          Logger.v("Farmer Data Get " + DsonUtils.toJsonString(farmerServer));
          Farmer farmer = await farmerDao.findByFarmerId(farmerServerId);
          if (farmer == null) {
            var formattedCreatedDate = farmerServer.createdDate == null
                ? ""
                : farmerServer.createdDate.split("T")[0];
            var formattedUpdatedDate = farmerServer.updatedDate == null
                ? ""
                : farmerServer.updatedDate.split("T")[0];

            farmerDao.persist(new Farmer.make(
                farmerServer.fId,
                farmerServer.farmerId,
                farmerServer.mobileNumber,
                farmerServer.farmerName,
                farmerServer.provinceId,
                farmerServer.province,
                farmerServer.districtId,
                farmerServer.district,
                farmerServer.municipalityId,
                farmerServer.municipality,
                farmerServer.ward,
                farmerServer.village,
                formattedCreatedDate,
                formattedUpdatedDate,
                farmerServer.createdBy,
                farmerServer.updatedBy,
                farmerServer.userId,
                farmerServer.syncDate,
                syncToServer: true)

//            Farmer.make(
//                farmerServer.fId,
//                farmerServer.farmerId,
////                farmerServer.userId,
//                farmerServer.mobileNumber,
//                farmerServer.farmerName,
//                farmerServer.provinceId,
//                farmerServer.province,
//                farmerServer.districtId,
//                farmerServer.district,
//                farmerServer.municipalityId,
//                farmerServer.municipality,
//                farmerServer.ward,
//                farmerServer.village,
//                syncToServer: true)
            );
          } else {
            await farmerDao.updateByAllSync(farmerServer);
//            farmerDao.updateByServerFarmerId(new Farmer.make(
//                farmerServer.fId,
//                farmerServer.farmerId,
////                farmerServer.userId,
//                farmerServer.mobileNumber,
//                farmerServer.farmerName,
//                farmerServer.provinceId,
//                farmerServer.province,
//                farmerServer.districtId,
//                farmerServer.district,
//                farmerServer.municipalityId,
//                farmerServer.municipality,
//                farmerServer.ward,
//                farmerServer.village,
//                syncToServer: true));
          }
        }

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Farmer Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //get farmer from server single button
  //
  @override
  void delete(Farmer entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<Farmer> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  Farmer findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Farmer findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(Farmer entity) {
    // TODO: implement persist
  }

  @override
  void update(Farmer entity) {
    // TODO: implement update
  }
}
