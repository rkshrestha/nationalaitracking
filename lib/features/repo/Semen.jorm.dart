// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Semen.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _SemenBean implements Bean<Semen> {
  final msId = StrField('ms_id');
  final semenId = IntField('semen_id');
  final userId = IntField('user_id');
  final receivedDate = StrField('received_date');
  final createdDate = StrField('created_date');
  final updatedDate = StrField('updated_date');
  final createdBy = IntField('created_by');
  final updatedBy = IntField('updated_by');
  final jersey = DoubleField('jersey');
  final boar = DoubleField('boar');
  final saanen = DoubleField('saanen');
  final yorkshire = DoubleField('yorkshire');
  final landrace = DoubleField('landrace');
  final duroc = DoubleField('duroc');
  final hampshire = DoubleField('hampshire');
  final holsteinFriesian = DoubleField('holstein_friesian');
  final murrah = DoubleField('murrah');
  final englishReceivedDate = StrField('english_received_date');
  final syncDate = StrField('sync_date');
  final syncToServer = BoolField('sync_to_server');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        msId.name: msId,
        semenId.name: semenId,
        userId.name: userId,
        receivedDate.name: receivedDate,
        createdDate.name: createdDate,
        updatedDate.name: updatedDate,
        createdBy.name: createdBy,
        updatedBy.name: updatedBy,
        jersey.name: jersey,
        boar.name: boar,
        saanen.name: saanen,
        yorkshire.name: yorkshire,
        landrace.name: landrace,
        duroc.name: duroc,
        hampshire.name: hampshire,
        holsteinFriesian.name: holsteinFriesian,
        murrah.name: murrah,
        englishReceivedDate.name: englishReceivedDate,
        syncDate.name: syncDate,
        syncToServer.name: syncToServer,
      };
  Semen fromMap(Map map) {
    Semen model = Semen();
    model.msId = adapter.parseValue(map['ms_id']);
    model.semenId = adapter.parseValue(map['semen_id']);
    model.userId = adapter.parseValue(map['user_id']);
    model.receivedDate = adapter.parseValue(map['received_date']);
    model.createdDate = adapter.parseValue(map['created_date']);
    model.updatedDate = adapter.parseValue(map['updated_date']);
    model.createdBy = adapter.parseValue(map['created_by']);
    model.updatedBy = adapter.parseValue(map['updated_by']);
    model.jersey = adapter.parseValue(map['jersey']);
    model.boar = adapter.parseValue(map['boar']);
    model.saanen = adapter.parseValue(map['saanen']);
    model.yorkshire = adapter.parseValue(map['yorkshire']);
    model.landrace = adapter.parseValue(map['landrace']);
    model.duroc = adapter.parseValue(map['duroc']);
    model.hampshire = adapter.parseValue(map['hampshire']);
    model.holsteinFriesian = adapter.parseValue(map['holstein_friesian']);
    model.murrah = adapter.parseValue(map['murrah']);
    model.englishReceivedDate =
        adapter.parseValue(map['english_received_date']);
    model.syncDate = adapter.parseValue(map['sync_date']);
    model.syncToServer = adapter.parseValue(map['sync_to_server']);

    return model;
  }

  List<SetColumn> toSetColumns(Semen model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(msId.set(model.msId));
      ret.add(semenId.set(model.semenId));
      ret.add(userId.set(model.userId));
      ret.add(receivedDate.set(model.receivedDate));
      ret.add(createdDate.set(model.createdDate));
      ret.add(updatedDate.set(model.updatedDate));
      ret.add(createdBy.set(model.createdBy));
      ret.add(updatedBy.set(model.updatedBy));
      ret.add(jersey.set(model.jersey));
      ret.add(boar.set(model.boar));
      ret.add(saanen.set(model.saanen));
      ret.add(yorkshire.set(model.yorkshire));
      ret.add(landrace.set(model.landrace));
      ret.add(duroc.set(model.duroc));
      ret.add(hampshire.set(model.hampshire));
      ret.add(holsteinFriesian.set(model.holsteinFriesian));
      ret.add(murrah.set(model.murrah));
      ret.add(englishReceivedDate.set(model.englishReceivedDate));
      ret.add(syncDate.set(model.syncDate));
      ret.add(syncToServer.set(model.syncToServer));
    } else if (only != null) {
      if (only.contains(msId.name)) ret.add(msId.set(model.msId));
      if (only.contains(semenId.name)) ret.add(semenId.set(model.semenId));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(receivedDate.name))
        ret.add(receivedDate.set(model.receivedDate));
      if (only.contains(createdDate.name))
        ret.add(createdDate.set(model.createdDate));
      if (only.contains(updatedDate.name))
        ret.add(updatedDate.set(model.updatedDate));
      if (only.contains(createdBy.name))
        ret.add(createdBy.set(model.createdBy));
      if (only.contains(updatedBy.name))
        ret.add(updatedBy.set(model.updatedBy));
      if (only.contains(jersey.name)) ret.add(jersey.set(model.jersey));
      if (only.contains(boar.name)) ret.add(boar.set(model.boar));
      if (only.contains(saanen.name)) ret.add(saanen.set(model.saanen));
      if (only.contains(yorkshire.name))
        ret.add(yorkshire.set(model.yorkshire));
      if (only.contains(landrace.name)) ret.add(landrace.set(model.landrace));
      if (only.contains(duroc.name)) ret.add(duroc.set(model.duroc));
      if (only.contains(hampshire.name))
        ret.add(hampshire.set(model.hampshire));
      if (only.contains(holsteinFriesian.name))
        ret.add(holsteinFriesian.set(model.holsteinFriesian));
      if (only.contains(murrah.name)) ret.add(murrah.set(model.murrah));
      if (only.contains(englishReceivedDate.name))
        ret.add(englishReceivedDate.set(model.englishReceivedDate));
      if (only.contains(syncDate.name)) ret.add(syncDate.set(model.syncDate));
      if (only.contains(syncToServer.name))
        ret.add(syncToServer.set(model.syncToServer));
    } else /* if (onlyNonNull) */ {
      if (model.msId != null) {
        ret.add(msId.set(model.msId));
      }
      if (model.semenId != null) {
        ret.add(semenId.set(model.semenId));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.receivedDate != null) {
        ret.add(receivedDate.set(model.receivedDate));
      }
      if (model.createdDate != null) {
        ret.add(createdDate.set(model.createdDate));
      }
      if (model.updatedDate != null) {
        ret.add(updatedDate.set(model.updatedDate));
      }
      if (model.createdBy != null) {
        ret.add(createdBy.set(model.createdBy));
      }
      if (model.updatedBy != null) {
        ret.add(updatedBy.set(model.updatedBy));
      }
      if (model.jersey != null) {
        ret.add(jersey.set(model.jersey));
      }
      if (model.boar != null) {
        ret.add(boar.set(model.boar));
      }
      if (model.saanen != null) {
        ret.add(saanen.set(model.saanen));
      }
      if (model.yorkshire != null) {
        ret.add(yorkshire.set(model.yorkshire));
      }
      if (model.landrace != null) {
        ret.add(landrace.set(model.landrace));
      }
      if (model.duroc != null) {
        ret.add(duroc.set(model.duroc));
      }
      if (model.hampshire != null) {
        ret.add(hampshire.set(model.hampshire));
      }
      if (model.holsteinFriesian != null) {
        ret.add(holsteinFriesian.set(model.holsteinFriesian));
      }
      if (model.murrah != null) {
        ret.add(murrah.set(model.murrah));
      }
      if (model.englishReceivedDate != null) {
        ret.add(englishReceivedDate.set(model.englishReceivedDate));
      }
      if (model.syncDate != null) {
        ret.add(syncDate.set(model.syncDate));
      }
      if (model.syncToServer != null) {
        ret.add(syncToServer.set(model.syncToServer));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addStr(msId.name, primary: true, isNullable: false);
    st.addInt(semenId.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addStr(receivedDate.name, isNullable: true);
    st.addStr(createdDate.name, isNullable: true);
    st.addStr(updatedDate.name, isNullable: true);
    st.addInt(createdBy.name, isNullable: true);
    st.addInt(updatedBy.name, isNullable: true);
    st.addDouble(jersey.name, isNullable: true);
    st.addDouble(boar.name, isNullable: true);
    st.addDouble(saanen.name, isNullable: true);
    st.addDouble(yorkshire.name, isNullable: true);
    st.addDouble(landrace.name, isNullable: true);
    st.addDouble(duroc.name, isNullable: true);
    st.addDouble(hampshire.name, isNullable: true);
    st.addDouble(holsteinFriesian.name, isNullable: true);
    st.addDouble(murrah.name, isNullable: true);
    st.addStr(englishReceivedDate.name, isNullable: true);
    st.addStr(syncDate.name, isNullable: true);
    st.addBool(syncToServer.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Semen model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<Semen> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Semen model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<Semen> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Semen model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.msId.eq(model.msId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Semen> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.msId.eq(model.msId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Semen> find(String msId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.msId.eq(msId));
    return await findOne(find);
  }

  Future<int> remove(String msId) async {
    final Remove remove = remover.where(this.msId.eq(msId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Semen> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.msId.eq(model.msId));
    }
    return adapter.remove(remove);
  }
}
