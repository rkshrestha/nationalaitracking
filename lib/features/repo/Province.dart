import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'Province.jorm.dart';
part 'Province.g.dart';

@JsonSerializable()
class Province implements JsonCallable<Province> {
  @PrimaryKey(auto: true)
  int pId;
  @JsonKey(name: 'ProvinceId')
  int provinceid;
  @JsonKey(name: 'States')
  String state;

  Province();


  Province.make(this.provinceid, this.state);

  factory Province.fromJson(Map<String, dynamic> json) =>
      _$ProvinceFromJson(json);

  Map<String, dynamic> toJson() => _$ProvinceToJson(this);

  @override
  Province fromJson(Map<String, dynamic> json) {
    return Province.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class ProvinceBean extends Bean<Province> with _ProvinceBean {
  ProvinceBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'province';
}
