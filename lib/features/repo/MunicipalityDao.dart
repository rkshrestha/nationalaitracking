import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Municipality.dart';
import 'package:nbis/s/Logger.dart';

class MunicipalityDao implements BaseDao<Municipality> {
  static MunicipalityDao _instance;
  MunicipalityBean bean;

  MunicipalityDao._(this.bean);

  static MunicipalityDao get(MunicipalityBean bean) {
    if (_instance == null) {
      _instance = MunicipalityDao._(bean);
    }
    return _instance;
  }

  getMunicipalityByDistrictId(int districtId) async {
    Find findMuncipality = Find(bean.tableName);

    findMuncipality.where(bean.dcode.eq(districtId));

    return bean.findMany(findMuncipality);
  }

  getMunicipality(int municipalityId) async {
    Find findMunicipality= Find(bean.tableName);

    findMunicipality.where(bean.municipalityid.eq(municipalityId));

    return bean.findOne(findMunicipality);
  }



  @override
  Future<void> delete(Municipality entity) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<void> deleteAll() {
   return bean.removeAll();
  }

  @override
  Future<List<Municipality>> findAll() {
    Find findMunicipality = Find(bean.tableName);
    return bean.findMany(findMunicipality);
  }

  @override
  Future<Municipality> findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Future<Municipality> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<void> persist(Municipality municipality) {
    Logger.v("muncipality inserted");
    return bean.insert(municipality);
  }

  @override
  Future<void> update(Municipality entity) {
    // TODO: implement update
    return null;
  }
}
