// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Species.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Species _$SpeciesFromJson(Map<String, dynamic> json) {
  return Species()
    ..sId = json['sId'] as int
    ..ids = json['AnimalId'] as int
    ..name = json['AnimalName'] as String;
}

Map<String, dynamic> _$SpeciesToJson(Species instance) => <String, dynamic>{
      'sId': instance.sId,
      'AnimalId': instance.ids,
      'AnimalName': instance.name
    };
