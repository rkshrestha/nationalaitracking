import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'ExitRecord.g.dart';

part 'ExitRecord.jorm.dart';

@JsonSerializable()
class ExitRecord implements JsonCallable<ExitRecord> {
  @PrimaryKey()
  @JsonKey(name: 'MExitRecordId')
  String eId;
//  @PrimaryKey(auto: true)
//  @JsonKey(name: 'MExitRecordId')
//  int eId;
  @JsonKey(name: 'ExitRecordId')
  @Column(isNullable: true)
  int erId;
  @JsonKey(name: 'FarmerId')
  @Column(isNullable: true)
  int farmerId;
  @JsonKey(name: 'AnimalId')
  @Column(isNullable: true)
  int animalId;
  @JsonKey(name: 'SpeciesId')
  @Column(isNullable: true)
  int speciesId;
  @JsonKey(name: 'BreedId')
  @Column(isNullable: true)
  int breedId;
  @JsonKey(name: 'DateOfExit')
  @Column(isNullable: true)
  String dateOfExit;
  @JsonKey(name: 'ExitReason')
  @Column(isNullable: true)
  String exitReason;
  @JsonKey(name: 'MobileNo')
  String mobileNumber;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesCode')
  String speciesCode;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedDate')
  String createdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedDate')
  String updatedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedBy')
  int createdBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedBy')
  int updatedBy;
  @Column(isNullable: true)
  @JsonKey(name: 'MFarmerId')
  String mFarmerId;
  @Column(isNullable: true)
  @JsonKey(name: 'MAnimalId')
  String mAnimalId;
  @Column(isNullable: true)
  @JsonKey(name: 'UserId')
  int userId;
  @Column(isNullable: true)
  @JsonKey(name: 'FarmerName')
  String farmerName;
  @Column(isNullable: true)
  @JsonKey(name: 'EnglishDateOfExit')
  String englishExitDate;
  @Column(isNullable: true)
  @JsonKey(name: 'SyncDate')
  String syncDate;
  @Column(isNullable: true)
  @JsonKey(name: "SyncToServer")
  bool syncToServer = false;

  ExitRecord();

  ExitRecord.make(
      this.eId,
      this.erId,
      this.farmerId,
      this.animalId,
      this.speciesId,
      this.breedId,
      this.dateOfExit,
      this.exitReason,
      this.mobileNumber,
      this.speciesCode,
      this.createdDate,
      this.updatedDate,
      this.createdBy,
      this.updatedBy,
      this.mFarmerId,
      this.mAnimalId,
      this.userId,
      this.farmerName,
      this.englishExitDate,
      this.syncDate,
      {this.syncToServer = false});

  factory ExitRecord.fromJson(Map<String, dynamic> json) =>
      _$ExitRecordFromJson(json);

  Map<String, dynamic> toJson() => _$ExitRecordToJson(this);

  @override
  ExitRecord fromJson(Map<String, dynamic> json) {
    return ExitRecord.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class ExitRecordBean extends Bean<ExitRecord> with _ExitRecordBean {
  ExitRecordBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'exit_records';
}
