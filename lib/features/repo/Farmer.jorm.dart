// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Farmer.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _FarmerBean implements Bean<Farmer> {
  final fId = StrField('f_id');
  final farmerId = IntField('farmer_id');
  final mobileNumber = StrField('mobile_number');
  final farmerName = StrField('farmer_name');
  final provinceId = IntField('province_id');
  final province = StrField('province');
  final districtId = IntField('district_id');
  final district = StrField('district');
  final municipalityId = IntField('municipality_id');
  final municipality = StrField('municipality');
  final ward = IntField('ward');
  final village = StrField('village');
  final createdDate = StrField('created_date');
  final updatedDate = StrField('updated_date');
  final createdBy = IntField('created_by');
  final updatedBy = IntField('updated_by');
  final userId = IntField('user_id');
  final syncDate = StrField('sync_date');
  final syncToServer = BoolField('sync_to_server');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        fId.name: fId,
        farmerId.name: farmerId,
        mobileNumber.name: mobileNumber,
        farmerName.name: farmerName,
        provinceId.name: provinceId,
        province.name: province,
        districtId.name: districtId,
        district.name: district,
        municipalityId.name: municipalityId,
        municipality.name: municipality,
        ward.name: ward,
        village.name: village,
        createdDate.name: createdDate,
        updatedDate.name: updatedDate,
        createdBy.name: createdBy,
        updatedBy.name: updatedBy,
        userId.name: userId,
        syncDate.name: syncDate,
        syncToServer.name: syncToServer,
      };
  Farmer fromMap(Map map) {
    Farmer model = Farmer();
    model.fId = adapter.parseValue(map['f_id']);
    model.farmerId = adapter.parseValue(map['farmer_id']);
    model.mobileNumber = adapter.parseValue(map['mobile_number']);
    model.farmerName = adapter.parseValue(map['farmer_name']);
    model.provinceId = adapter.parseValue(map['province_id']);
    model.province = adapter.parseValue(map['province']);
    model.districtId = adapter.parseValue(map['district_id']);
    model.district = adapter.parseValue(map['district']);
    model.municipalityId = adapter.parseValue(map['municipality_id']);
    model.municipality = adapter.parseValue(map['municipality']);
    model.ward = adapter.parseValue(map['ward']);
    model.village = adapter.parseValue(map['village']);
    model.createdDate = adapter.parseValue(map['created_date']);
    model.updatedDate = adapter.parseValue(map['updated_date']);
    model.createdBy = adapter.parseValue(map['created_by']);
    model.updatedBy = adapter.parseValue(map['updated_by']);
    model.userId = adapter.parseValue(map['user_id']);
    model.syncDate = adapter.parseValue(map['sync_date']);
    model.syncToServer = adapter.parseValue(map['sync_to_server']);

    return model;
  }

  List<SetColumn> toSetColumns(Farmer model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(fId.set(model.fId));
      ret.add(farmerId.set(model.farmerId));
      ret.add(mobileNumber.set(model.mobileNumber));
      ret.add(farmerName.set(model.farmerName));
      ret.add(provinceId.set(model.provinceId));
      ret.add(province.set(model.province));
      ret.add(districtId.set(model.districtId));
      ret.add(district.set(model.district));
      ret.add(municipalityId.set(model.municipalityId));
      ret.add(municipality.set(model.municipality));
      ret.add(ward.set(model.ward));
      ret.add(village.set(model.village));
      ret.add(createdDate.set(model.createdDate));
      ret.add(updatedDate.set(model.updatedDate));
      ret.add(createdBy.set(model.createdBy));
      ret.add(updatedBy.set(model.updatedBy));
      ret.add(userId.set(model.userId));
      ret.add(syncDate.set(model.syncDate));
      ret.add(syncToServer.set(model.syncToServer));
    } else if (only != null) {
      if (only.contains(fId.name)) ret.add(fId.set(model.fId));
      if (only.contains(farmerId.name)) ret.add(farmerId.set(model.farmerId));
      if (only.contains(mobileNumber.name))
        ret.add(mobileNumber.set(model.mobileNumber));
      if (only.contains(farmerName.name))
        ret.add(farmerName.set(model.farmerName));
      if (only.contains(provinceId.name))
        ret.add(provinceId.set(model.provinceId));
      if (only.contains(province.name)) ret.add(province.set(model.province));
      if (only.contains(districtId.name))
        ret.add(districtId.set(model.districtId));
      if (only.contains(district.name)) ret.add(district.set(model.district));
      if (only.contains(municipalityId.name))
        ret.add(municipalityId.set(model.municipalityId));
      if (only.contains(municipality.name))
        ret.add(municipality.set(model.municipality));
      if (only.contains(ward.name)) ret.add(ward.set(model.ward));
      if (only.contains(village.name)) ret.add(village.set(model.village));
      if (only.contains(createdDate.name))
        ret.add(createdDate.set(model.createdDate));
      if (only.contains(updatedDate.name))
        ret.add(updatedDate.set(model.updatedDate));
      if (only.contains(createdBy.name))
        ret.add(createdBy.set(model.createdBy));
      if (only.contains(updatedBy.name))
        ret.add(updatedBy.set(model.updatedBy));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(syncDate.name)) ret.add(syncDate.set(model.syncDate));
      if (only.contains(syncToServer.name))
        ret.add(syncToServer.set(model.syncToServer));
    } else /* if (onlyNonNull) */ {
      if (model.fId != null) {
        ret.add(fId.set(model.fId));
      }
      if (model.farmerId != null) {
        ret.add(farmerId.set(model.farmerId));
      }
      if (model.mobileNumber != null) {
        ret.add(mobileNumber.set(model.mobileNumber));
      }
      if (model.farmerName != null) {
        ret.add(farmerName.set(model.farmerName));
      }
      if (model.provinceId != null) {
        ret.add(provinceId.set(model.provinceId));
      }
      if (model.province != null) {
        ret.add(province.set(model.province));
      }
      if (model.districtId != null) {
        ret.add(districtId.set(model.districtId));
      }
      if (model.district != null) {
        ret.add(district.set(model.district));
      }
      if (model.municipalityId != null) {
        ret.add(municipalityId.set(model.municipalityId));
      }
      if (model.municipality != null) {
        ret.add(municipality.set(model.municipality));
      }
      if (model.ward != null) {
        ret.add(ward.set(model.ward));
      }
      if (model.village != null) {
        ret.add(village.set(model.village));
      }
      if (model.createdDate != null) {
        ret.add(createdDate.set(model.createdDate));
      }
      if (model.updatedDate != null) {
        ret.add(updatedDate.set(model.updatedDate));
      }
      if (model.createdBy != null) {
        ret.add(createdBy.set(model.createdBy));
      }
      if (model.updatedBy != null) {
        ret.add(updatedBy.set(model.updatedBy));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.syncDate != null) {
        ret.add(syncDate.set(model.syncDate));
      }
      if (model.syncToServer != null) {
        ret.add(syncToServer.set(model.syncToServer));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addStr(fId.name, primary: true, isNullable: false);
    st.addInt(farmerId.name, isNullable: true);
    st.addStr(mobileNumber.name, isNullable: true);
    st.addStr(farmerName.name, isNullable: true);
    st.addInt(provinceId.name, isNullable: true);
    st.addStr(province.name, isNullable: true);
    st.addInt(districtId.name, isNullable: true);
    st.addStr(district.name, isNullable: true);
    st.addInt(municipalityId.name, isNullable: true);
    st.addStr(municipality.name, isNullable: true);
    st.addInt(ward.name, isNullable: true);
    st.addStr(village.name, isNullable: true);
    st.addStr(createdDate.name, isNullable: true);
    st.addStr(updatedDate.name, isNullable: true);
    st.addInt(createdBy.name, isNullable: true);
    st.addInt(updatedBy.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addStr(syncDate.name, isNullable: true);
    st.addBool(syncToServer.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Farmer model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<Farmer> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Farmer model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<Farmer> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Farmer model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.fId.eq(model.fId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Farmer> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.fId.eq(model.fId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Farmer> find(String fId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.fId.eq(fId));
    return await findOne(find);
  }

  Future<int> remove(String fId) async {
    final Remove remove = remover.where(this.fId.eq(fId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Farmer> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.fId.eq(model.fId));
    }
    return adapter.remove(remove);
  }
}
