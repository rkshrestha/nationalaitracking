// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Species.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _SpeciesBean implements Bean<Species> {
  final sId = IntField('s_id');
  final ids = IntField('ids');
  final name = StrField('name');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        sId.name: sId,
        ids.name: ids,
        name.name: name,
      };
  Species fromMap(Map map) {
    Species model = Species();
    model.sId = adapter.parseValue(map['s_id']);
    model.ids = adapter.parseValue(map['ids']);
    model.name = adapter.parseValue(map['name']);

    return model;
  }

  List<SetColumn> toSetColumns(Species model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.sId != null) {
        ret.add(sId.set(model.sId));
      }
      ret.add(ids.set(model.ids));
      ret.add(name.set(model.name));
    } else if (only != null) {
      if (model.sId != null) {
        if (only.contains(sId.name)) ret.add(sId.set(model.sId));
      }
      if (only.contains(ids.name)) ret.add(ids.set(model.ids));
      if (only.contains(name.name)) ret.add(name.set(model.name));
    } else /* if (onlyNonNull) */ {
      if (model.sId != null) {
        ret.add(sId.set(model.sId));
      }
      if (model.ids != null) {
        ret.add(ids.set(model.ids));
      }
      if (model.name != null) {
        ret.add(name.set(model.name));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(sId.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(ids.name, isNullable: true);
    st.addStr(name.name, isNullable: true);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Species model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(sId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Species newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Species> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Species model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(sId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Species newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Species> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Species model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.sId.eq(model.sId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Species> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.sId.eq(model.sId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Species> find(int sId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.sId.eq(sId));
    return await findOne(find);
  }

  Future<int> remove(int sId) async {
    final Remove remove = remover.where(this.sId.eq(sId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Species> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.sId.eq(model.sId));
    }
    return adapter.remove(remove);
  }
}
