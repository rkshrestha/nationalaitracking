
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Province.dart';
import 'package:nbis/s/Logger.dart';

class ProvinceDao implements BaseDao<Province>{

  static ProvinceDao _instance;
  ProvinceBean bean;

  ProvinceDao._(this.bean);

  static ProvinceDao get(ProvinceBean bean) {
    if (_instance == null) {
      _instance = ProvinceDao._(bean);
    }
    return _instance;
  }

  getProvince(int provinceId) async {
    Find findProvince = Find(bean.tableName);

    findProvince.where(bean.provinceid.eq(provinceId));

    return bean.findOne(findProvince);
  }


//  countData()async{
//    int count = Sqflite.firstIntValue(await bean.('SELECT COUNT(*) FROM table_name'));
//  }



  @override
  Future<void> delete(Province entity) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<void> deleteAll() {
  //  Find allProvince = Find(bean.tableName);
  return bean.removeAll();
  }

  @override
  Future<List<Province>> findAll() {
    Find findProvince = Find(bean.tableName);
    return bean.findMany(findProvince);
  }

  @override
  Future<Province> findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Future<Province> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<void> persist(Province province) {
    Logger.v(" province inserted");
    return bean.insert(province);
  }

  @override
  Future<void> update(Province entity) {
    // TODO: implement update
    return null;
  }
}