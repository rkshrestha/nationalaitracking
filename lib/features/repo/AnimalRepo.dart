import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/AIDao.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalDao.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Breed.dart';
import 'package:nbis/features/repo/BreedDao.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/CalvingDao.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/ExitRecordDao.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/PDDao.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/repo/Species.dart';
import 'package:nbis/features/repo/SpeciesDao.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';
import 'package:uuid/uuid.dart';

class AnimalRepo implements Repository<Animal> {
  SQFliteDatabase sqFliteDatabase;
  AnimalDao animalDao;
  List<Animal> animalList;
  List<Species> speciesList;
  List<Breed> breedList;
  SessionManager sessionManager;
  BuildContext context;

//  static String TAG_CLAZZ;
  int animalServerId;

  SpeciesDao speciesDao;
  BreedDao breedDao;
  AIDao aiDao;
  PDDao pdDao;
  CalvingDao calvingDao;
  ExitRecordDao exitRecordDao;

  AnimalRepo(this.context) {
//    TAG_CLAZZ = this.runtimeType.toString();
//    Logger.log(TAG_CLAZZ, "inside animalRepo");
    init();

  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    Logger.v('connection repo');
  }

  void saveAnimal(context, Animal animal, Farmer farmer, Species species,
      Breed breed, StreamSink<SCResponse<String>> sSink) async {
    Logger.v("saveAnimal sync");

    try {
      Logger.v('try saveAnimal');
//      Logger.log(TAG_CLAZZ, 'check animal add ');
      Logger.v("Session" + this.sessionManager.toString());
      animalDao = await sqFliteDatabase.animalDao();

      var user = this.sessionManager.findOne().user;
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);

      DateTime today = new DateTime.now();
//      DateTime todayNepaliDate = new DateTime(today.year + 56, today.month + 8,
//          today.day + 17, today.hour, today.minute, today.second);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);

      var uuid = new Uuid();
    var uid =  uuid.v1();


      Animal animals = await animalDao.findBySameSpeciesCode(animal);
      if (animals == null) {
//
        int animalId = await animalDao.persist(new Animal.make(
            uid,
//            animal.aId,
            0,
            farmer.fId,
            species.ids,
            species.name,
            breed.bids,
            breed.name,
            animal.speciesCode,
            animal.mobileNumber,
            today.toIso8601String(),
            null,
            user.userid,
            0,
            farmer.farmerId,
            user.userid,
            animal.farmerName,
            "Female",
            null,
            status: true,
            syncToServer: false));

        Navigator.pop(context, true);

//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("animal_add_success"),
//        ));

        sSink.add(SCResponse.success("Animal has been successfully added"));
      } else {
        sSink.add(SCResponse.error(
          "Animal code already exist !!",
//          AppTranslations.of(context).text("animal_exist"),
        ));
      }

//      Logger.v(farmer.fId.toString() +
//          "\n" +
//          user.userid.toString() +
//          "\n" +
//          user.mobileno.toString() +
//          "\n" +
//          breed.name +
//          "\n" +
//          species.name +
//          "\n" +
//          animal.speciesCode);

    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("animal_add_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't insert animal"));
      Logger.v(ex.toString());
    }
  }

  void updateAnimal(context, Animal animal, Farmer farmer, Species species,
      Breed breed, StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try updateAnimal' + animal.breedName);

      animalDao = await sqFliteDatabase.animalDao();
      aiDao = await sqFliteDatabase.aiDao();
      pdDao = await sqFliteDatabase.pdDao();
      calvingDao = await sqFliteDatabase.calvingDao();
      exitRecordDao = await sqFliteDatabase.exitRecordDao();
      var user = this.sessionManager.findOne().user;
//      Logger.v(user.userid.toString());
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);
      DateTime today = new DateTime.now();
//      DateTime todayNepaliDate = new DateTime(today.year + 56, today.month + 8,
//          today.day + 17, today.hour, today.minute, today.second);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);

//      Animal animals = await animalDao.findBySameSpeciesCodeUpdate(animal);

//      if (animals == null) {
      int animalUpdate = await animalDao.update(new Animal.make(
          animal.aId,
          animal.animalId,
          farmer.fId,
          species.ids,
          species.name,
          breed.bids,
          breed.name,
          animal.speciesCode,
          animal.mobileNumber,
//              todayNepaliDate.toIso8601String(),
          animal.createdDate,
          today.toIso8601String(),
          user.userid,
          user.userid,
          farmer.farmerId,
          user.userid,
          animal.farmerName,
          animal.gender,
          animal.syncDate,
          status: true,
          syncToServer: false));
      //update speciesCode to ai data
      await aiDao.updateAnimalSpeciesCode(animal);
      //update speciesCode to animal  data
      await pdDao.updateAnimalSpeciesCode(animal);
      //update speciesCode to calving data
      await calvingDao.updateAnimalSpeciesCode(animal);
      //For update of calf code editing from update of animal.
      await calvingDao.updateAnimalToCalving(animal);
      //update speciesCode to exitRecord data
      await exitRecordDao.updateAnimalSpeciesCode(animal);
      Navigator.pop(context, true);
//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("animal_update_success"),
//        ));
      sSink.add(SCResponse.success("Animal has been successfully updated"));
//      } else {
//        sSink.add(SCResponse.error(
//          "Animal code already exist !!",
////          AppTranslations.of(context).text("animal_exist"),
//        ));
//      }
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't update animal"));
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("animal_update_fail"),
//      ));
      Logger.v(ex.toString());
    }
  }

  void getAnimalByaId(
      String animalId, StreamController<SCResponse<Animal>> stSink) async {
    try {
      animalDao = await sqFliteDatabase.animalDao();
      var animal = await animalDao.findByaId(animalId);

      stSink.add(SCResponse.success(animal));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getAnimalListById(
      String animalId, StreamSink<SCResponse<List<Animal>>> stSink) async {
    try {
      animalDao = await sqFliteDatabase.animalDao();
      animalList = await animalDao.getAnimalById(animalId);

      for (Animal animal in animalList) {
        Logger.v(animal.breedName);
      }

      stSink.add(SCResponse.success(animalList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getAnimalListByFarmer(
      Farmer farmer, StreamSink<SCResponse<List<Animal>>> stSink) async {
    try {
      animalDao = await sqFliteDatabase.animalDao();

      animalList = await animalDao.getAnimalByFarmer(farmer.mobileNumber);

      for (Animal animal in animalList) {
        Logger.v(animal.breedName);
      }

      stSink.add(SCResponse.success(animalList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getUnSyncAnimalListByFarmer(
      Farmer farmer, StreamSink<SCResponse<List<Animal>>> stSink) async {
    try {
      animalDao = await sqFliteDatabase.animalDao();

      Logger.v("getfId:" + farmer.fId.toString());

      animalList = await animalDao.getUnSyncAnimalByFarmer(farmer.fId);

      for (Animal animal in animalList) {
        Logger.v(animal.breedName);
      }

      stSink.add(SCResponse.success(animalList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSyncAnimalListByFarmer(
      Farmer farmer, StreamSink<SCResponse<List<Animal>>> stSink) async {
    try {
      animalDao = await sqFliteDatabase.animalDao();

      Logger.v("getfId:" + farmer.fId.toString());

      animalList = await animalDao.getSyncAnimalByFarmer(farmer.fId);

      for (Animal animal in animalList) {
        Logger.v(animal.breedName);
      }

      stSink.add(SCResponse.success(animalList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getAnimalList(StreamSink<SCResponse<List<Animal>>> stSink) async {
    try {
      animalDao = await sqFliteDatabase.animalDao();
      animalList = await animalDao.findAll();
      for (Animal animal in animalList) {
        Logger.v(animal.breedName);
      }
      stSink.add(SCResponse.success(animalList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

//  void getUnSyncAnimalList(StreamSink<SCResponse<List<Animal>>> stSink) async {
//    try {
//      sqFliteDatabase = SQFliteDatabase.getDB();
//      await sqFliteDatabase.connect();
//
//      animalDao = await sqFliteDatabase.animalDao();
//      animalList = await animalDao.findAllUnSyncData();
//
//      for (Animal animal in animalList) {
//        Logger.v("animal Data" + animal.breedName);
//      }
//      stSink.add(SCResponse.success(animalList));
//    } catch (ex) {
//      Logger.v(ex.toString());
//      stSink.add(SCResponse.error(ex.toString()));
//    }
//  }

  void getUnSyncAnimalList(StreamSink<SCResponse<List<Animal>>> stSink) async {
    try {
      sqFliteDatabase = SQFliteDatabase.getDB();
      await sqFliteDatabase.connect();

      animalDao = await sqFliteDatabase.animalDao();
      animalList = await animalDao.findAllUnSyncData();

      for (Animal animal in animalList) {
        Logger.v("animal Data" + animal.breedName);
      }
      stSink.add(SCResponse.success(animalList));
    } catch (ex) {
      Logger.v(ex.toString());
      stSink.add(SCResponse.error(ex.toString()));
    }
  }

  void getSyncAnimalList(StreamSink<SCResponse<List<Animal>>> stSink) async {
    try {
      animalDao = await sqFliteDatabase.animalDao();
      animalList = await animalDao.findAllSyncData();
      for (Animal animal in animalList) {
        Logger.v(animal.breedName);
      }
      stSink.add(SCResponse.success(animalList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getAnimalBySpeciesCode(context, String speciesCode,
      StreamController<SCResponse<Animal>> stSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      animalDao = await sqFliteDatabase.animalDao();
      var animal = await animalDao.getAnimalBySpeciesCode(speciesCode);

      if (animal == null) {
        stSink.add(SCResponse.error(
          "Animal code not matched !!",
//          AppTranslations.of(context).text("animal_not_match"),
        ));
      } else {
        stSink.add(SCResponse.success(animal));
      }
    } catch (ex) {
      Logger.v("getAnimalBySpeciesCode" + ex.toString());
    }
  }

  //species sync
//  void syncSpeciesList(StreamSink<SCResponse<String>> sSink) async {
//    try {
//      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
//
//      if (!isNetworkAvailable) {
//        sSink.add(SCResponse.noInternetAvailable());
//        return;
//      }
//      NetworkClient networkClient = NetworkClient();
//      NetworkApi networkApi = NetworkApi.create(
//          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      SCResponse scResponse = await networkClient.call<Species, List<Species>>(
//          networkApi.syncSpeciesList(),
//          callable: Species());
//
//      if (scResponse.status == Status.SUCCESS) {
//        List<Species> speciesList = scResponse.data;
//        speciesDao = await sqFliteDatabase.speciesDao();
//
//        speciesDao.deleteAll();
//
//        for (Species species in speciesList) {
//          speciesDao.persist(new Species.make(species.ids, species.name));
//        }
//
//        Logger.v(speciesList[0].name);
//        //   sSink.add(scResponse);
//        sSink.add(SCResponse.success("Species Sync success"));
//      } else if (scResponse.status == Status.ERROR) {
//        Logger.v(scResponse.errorMessage);
//        sSink.add(scResponse);
//      }
//    } catch (ex) {
//      Logger.v(ex.toString());
//    }
//  }

  void syncSpeciesList(StreamSink<SCResponse<List<Species>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient.call<Species, List<Species>>(
          networkApi.syncSpeciesList(),
          callable: Species());

      if (scResponse.status == Status.SUCCESS) {
        List<Species> speciesList = scResponse.data;
        speciesDao = await sqFliteDatabase.speciesDao();

        speciesDao.deleteAll();

        for (Species species in speciesList) {
          speciesDao.persist(new Species.make(species.ids, species.name));
        }

        Logger.v(speciesList[0].name);
        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Species Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSpeciesList(StreamSink<SCResponse<List<Species>>> stSink) async {
    try {
      speciesDao = await sqFliteDatabase.speciesDao();

      speciesList = await speciesDao.findAll();

      for (Species species in speciesList) {
        Logger.v(species.name);
      }

      stSink.add(SCResponse.success(speciesList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //breed sync

//  void syncBreedList(StreamSink<SCResponse<String>> sSink) async {
//    try {
//      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
//
//      if (!isNetworkAvailable) {
//        sSink.add(SCResponse.noInternetAvailable());
//        return;
//      }
//      NetworkClient networkClient = NetworkClient();
//      NetworkApi networkApi = NetworkApi.create(
//          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      SCResponse scResponse = await networkClient.call<Breed, List<Breed>>(
//          networkApi.syncBreedList(),
//          callable: Breed());
//
//      if (scResponse.status == Status.SUCCESS) {
//        List<Breed> breedList = scResponse.data;
//        breedDao = await sqFliteDatabase.breedDao();
//
//        breedDao.deleteAll();
//
//        for (Breed breed in breedList) {
//          breedDao.persist(new Breed.make(breed.bids, breed.aids, breed.name));
//        }
//
//        Logger.v(breedList[0].name);
//        sSink.add(SCResponse.success("Breed Sync success"));
//      } else if (scResponse.status == Status.ERROR) {
//        Logger.v(scResponse.errorMessage);
//        sSink.add(scResponse);
//      }
//    } catch (ex) {
//      Logger.v(ex.toString());
//    }
//  }

  void syncBreedList(StreamSink<SCResponse<List<Breed>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient.call<Breed, List<Breed>>(
          networkApi.syncBreedList(),
          callable: Breed());

      if (scResponse.status == Status.SUCCESS) {
        List<Breed> breedList = scResponse.data;
        breedDao = await sqFliteDatabase.breedDao();

        breedDao.deleteAll();

        for (Breed breed in breedList) {
          breedDao.persist(new Breed.make(breed.bids, breed.aids, breed.name));
        }

        Logger.v(breedList[0].name);
        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Breed Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getBreedList(
      int speciesId, StreamSink<SCResponse<List<Breed>>> stSink) async {
    try {
      breedDao = await sqFliteDatabase.breedDao();

      breedList = await breedDao.getBreedBySpeciesId(speciesId);

      for (Breed breed in breedList) {
        Logger.v(breed.name);
      }

      stSink.add(SCResponse.success(breedList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //delete animal

  deleteAnimal(
      context, Animal animal, StreamSink<SCResponse<String>> sSink) async {
    try {
      animalDao = await sqFliteDatabase.animalDao();

      await animalDao.delete(animal);

//      sSink.add(SCResponse.success(
//        AppTranslations.of(context).text("animal_delete_success"),
//      ));
      sSink.add(SCResponse.success("Animal deleted Successfully"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("animal_delete_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't delete animal"));
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local start

  void updateAnimalServerIdAndStatus(List<Animal> animalList,
      StreamSink<SCResponse<List<Animal>>> sSink) async {
    try {
      sqFliteDatabase = SQFliteDatabase.getDB();
      await sqFliteDatabase.connect();
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      NetworkClient networkClient = NetworkClient();
      animalDao = await sqFliteDatabase.animalDao();
      aiDao = await sqFliteDatabase.aiDao();
      pdDao = await sqFliteDatabase.pdDao();
      calvingDao = await sqFliteDatabase.calvingDao();
      exitRecordDao = await sqFliteDatabase.exitRecordDao();

      var user = this.sessionManager.findOne().user;
      List<ArtificialInsemination> unsyncedAIList =
          await aiDao.findAllUnSyncData();
      List<PregnancyDiagnosis> unsyncedPDList = await pdDao.findAllUnSyncData();
      List<Calving> unsyncedCalvingList = await calvingDao.findAllUnSyncData();
      List<ExitRecord> unsyncedExitList =
          await exitRecordDao.findAllUnSyncData();

      for (Animal syncedAnimal in animalList) {
        animalDao.updateBySpeciesCodeSync(syncedAnimal);

        animalDao.updateBySpeciesCode(syncedAnimal);

        //update server animal id in ai table
        if (unsyncedAIList != null) {
          for (ArtificialInsemination ai in unsyncedAIList) {
            await aiDao.updateAnimalIdInAI(syncedAnimal);
          }
        }

        //update server animal id in pd table

        if (unsyncedPDList != null) {
          for (PregnancyDiagnosis pd in unsyncedPDList) {
            await pdDao.updateAnimalIdInPD(syncedAnimal);
          }
        }

        //update server animal id in calving table

        if (unsyncedCalvingList != null) {
          for (Calving calving in unsyncedCalvingList) {
            await calvingDao.updateAnimalIdInCalving(syncedAnimal);
          }
        }

        //update server animal id in exit table

        if (unsyncedExitList != null) {
          for (ExitRecord exit in unsyncedExitList) {
            await exitRecordDao.updateAnimalIdInExit(syncedAnimal);
          }
        }
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local end

  //send to server
  //animal send
  void sendAnimalData(List<Animal> animalList,
      StreamSink<SCResponse<List<Animal>>> sSink) async {
    try {
//      sqFliteDatabase = SQFliteDatabase.getDB();
//      await sqFliteDatabase.connect();

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");

      Logger.v("getIt: " + DsonUtils.toJsonString(animalList));
//      farmerDao = await sqFliteDatabase.farmerDao();
      animalDao = await sqFliteDatabase.animalDao();
      aiDao = await sqFliteDatabase.aiDao();
      pdDao = await sqFliteDatabase.pdDao();
      calvingDao = await sqFliteDatabase.calvingDao();
      exitRecordDao = await sqFliteDatabase.exitRecordDao();
      var user = this.sessionManager.findOne().user;
      List<ArtificialInsemination> unsyncedAIList =
          await aiDao.findAllUnSyncData();
      List<PregnancyDiagnosis> unsyncedPDList = await pdDao.findAllUnSyncData();
      List<Calving> unsyncedCalvingList = await calvingDao.findAllUnSyncData();
      List<ExitRecord> unsyncedExitList =
          await exitRecordDao.findAllUnSyncData();

      Logger.v("getIt: " + DsonUtils.toJsonString(animalList));
      NetworkClient networkClient = NetworkClient();

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      List<Animal> unsyncedAnimalList = await animalDao.findAllBySyncStatus();
      animalDao = await sqFliteDatabase.animalDao();

//      var user = this.sessionManager.findOne().user;
      SCResponse scResponse = await networkClient.call<Animal, List<Animal>>(
//          networkApi.sendAnimalData(unsyncedAnimalList, user.userid),
          networkApi.sendAnimalData(animalList, user.userid),
          callable: Animal());
      if (scResponse.status == Status.SUCCESS) {
        List<Animal> syncedAnimalList = scResponse.data;

        for (Animal syncedAnimal in syncedAnimalList) {
//        for (int i = 0; i < syncedAnimalList.length; i++) {
//          Logger.v("count Animal"+syncedAnimalList.length.toString());
//             Animal syncedAnimal = scResponse.data[i];
          animalDao.updateBySpeciesCodeSync(syncedAnimal);

          animalDao.updateBySpeciesCode(syncedAnimal);

          //update server animal id in ai table
          if (unsyncedAIList != null) {
            for (ArtificialInsemination ai in unsyncedAIList) {
              await aiDao.updateAnimalIdInAI(syncedAnimal);
            }
          }

          //update server animal id in pd table

          if (unsyncedPDList != null) {
            for (PregnancyDiagnosis pd in unsyncedPDList) {
              await pdDao.updateAnimalIdInPD(syncedAnimal);
            }
          }

          //update server animal id in calving table

          if (unsyncedCalvingList != null) {
            for (Calving calving in unsyncedCalvingList) {
              await calvingDao.updateAnimalIdInCalving(syncedAnimal);
            }
          }

          //update server animal id in exit table

          if (unsyncedExitList != null) {
            for (ExitRecord exit in unsyncedExitList) {
              await exitRecordDao.updateAnimalIdInExit(syncedAnimal);
            }
          }
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }

      sSink.add(scResponse);
    } catch (ex) {
      Logger.v(ex.toString());
    }
//    finally{
//      sqFliteDatabase.close();
//    }
  }

  //get animal from server
  void syncGetAnimalData(StreamSink<SCResponse<List<Animal>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager.findOne().user;

      NetworkClient networkClient = NetworkClient();

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));

      SCResponse scResponse = await networkClient.call<Animal, List<Animal>>(
          networkApi.syncGetAnimalList(user.userid),
          callable: Animal());

//      if (scResponse.status == Status.SUCCESS) {
//        List<Animal> animalList = scResponse.data;
//        animalDao = await sqFliteDatabase.animalDao();
//
//        for (Animal animalServer in animalList) {
//          animalServerId = animalServer.animalId;
//          Logger.v("ServerId " + animalServerId.toString());
//          Animal animal = await animalDao.findByAnimalId(animalServerId);
//
//          if (animal == null) {
//            animalDao.delete(animalServer);
//            animalDao.persist(new Animal.make(
//                animalServer.aId,
//                animalServer.animalId,
//                animalServer.fId,
//                animalServer.userId,
//                animalServer.speciesId,
//                animalServer.speciesName,
//                animalServer.breedId,
//                animalServer.breedName,
//                animalServer.speciesCode));
//          } else {
//            if (animal.aId == animalServer.aId) {
//              animalDao.update(Animal.make(
//                  animalServer.aId,
//                  animalServer.animalId,
//                  animalServer.fId,
//                  animalServer.userId,
//                  animalServer.speciesId,
//                  animalServer.speciesName,
//                  animalServer.breedId,
//                  animalServer.breedName,
//                  animalServer.speciesCode));
//            }
//          }
//        }
//
//
//        sSink.add(SCResponse.success("Animal Sync success"));
//      }

      if (scResponse.status == Status.SUCCESS) {
        List<Animal> animalLists = scResponse.data;
        animalDao = await sqFliteDatabase.animalDao();

        for (Animal animalServer in animalLists) {
          animalServerId = animalServer.animalId;
          Logger.v("ServerId " + animalServerId.toString());
          Animal animal = await animalDao.findByAnimalId(animalServerId);
          if (animal == null) {
            var formattedCreatedDate = animalServer.createdDate == null
                ? ""
                : animalServer.createdDate.split("T")[0];
            var formattedUpdatedDate = animalServer.updatedDate == null
                ? ""
                : animalServer.updatedDate.split("T")[0];
            animalDao.persist(new Animal.make(
                    animalServer.aId,
                    animalServer.animalId,
                    animalServer.mfarmerId,
                    animalServer.speciesId,
                    animalServer.speciesName,
                    animalServer.breedId,
                    animalServer.breedName,
                    animalServer.speciesCode,
                    animalServer.mobileNumber,
                    formattedCreatedDate,
                    formattedUpdatedDate,
                    animalServer.createdBy,
                    animalServer.updatedBy,
                    animalServer.fId,
                    animalServer.userId,
                    animalServer.farmerName,
                    animalServer.gender,
                    animalServer.syncDate,
                    status: animalServer.status,
                    syncToServer: true)

//            Animal.make(
//                    animalServer.aId,
//                    animalServer.animalId,
//                    animalServer.fId,
//                    animalServer.speciesId,
//                    animalServer.speciesName,
//                    animalServer.breedId,
//                    animalServer.breedName,
//                    animalServer.speciesCode,
//                    animalServer.mobileNumber,
//                    formattedCreatedDate,
//                    formattedUpdatedDate,
//                    animalServer.createdBy,
//                    animalServer.updatedBy,
//                    syncToServer: true)

                );
          } else {
            animalDao.updateAnimalSyncData(animalServer);
//            animalDao.updateByServerAnimalId(new Animal.make(
//                animalServer.aId,
//                animalServer.animalId,
//                animalServer.fId,
////                animalServer.userId,
//                animalServer.speciesId,
//                animalServer.speciesName,
//                animalServer.breedId,
//                animalServer.breedName,
//                animalServer.speciesCode,
//                syncToServer: true));
          }
        }

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Animal Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  @override
  void delete(Animal entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<Animal> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  Animal findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Animal findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(Animal entity) {
    // TODO: implement persist
  }

  @override
  void update(Animal entity) {
    // TODO: implement update
  }
}
