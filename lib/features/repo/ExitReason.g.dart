// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ExitReason.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExitReason _$ExitReasonFromJson(Map<String, dynamic> json) {
  return ExitReason()
    ..eId = json['eId'] as int
    ..ids = json['ExitId'] as int
    ..name = json['ExitReasonName'] as String;
}

Map<String, dynamic> _$ExitReasonToJson(ExitReason instance) =>
    <String, dynamic>{
      'eId': instance.eId,
      'ExitId': instance.ids,
      'ExitReasonName': instance.name
    };
