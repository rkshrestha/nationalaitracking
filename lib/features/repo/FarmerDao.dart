import 'package:flutter/material.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/repo/SyncModel.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';

class FarmerDao implements BaseDao<Farmer> {
  static FarmerDao _instance;
  FarmerBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  FarmerDao._(this.bean);

  static FarmerDao get(FarmerBean bean) {
    if (_instance == null) {
      _instance = FarmerDao._(bean);
      Logger.v("Inside farmerDao farmer");
    }
    return _instance;
  }

  getFarmerByMobileNumber(String mblNumber) async {
    Find findMobileNumber = Find(bean.tableName);

    findMobileNumber.where(bean.mobileNumber.eq(mblNumber));

    return bean.findOne(findMobileNumber);
  }


  //get farmer update
  getFarmerById(String farmerId) async {
    Find findfId = Find(bean.tableName);
Logger.v("Ids:"+farmerId.toString());
Logger.v("Idssssss:"+bean.fId.toString());
    findfId.where(bean.fId.eq(farmerId));

    return bean.findOne(findfId);
  }



  //get mobile number and farmer details
  getFarmerMobileNumberById(String fId)async  {
    Logger.v("testId:" +fId.toString() );
    Find findMobileNumberFarmer = Find(bean.tableName);
    findMobileNumberFarmer.where(bean.fId.eq(fId));
     return  bean.findOne(findMobileNumberFarmer);
  }


  findByFarmerId(int farmerId)async {
    Find findByFarmerId = Find(bean.tableName);

    findByFarmerId.where(bean.farmerId.eq(farmerId));

    return bean.findOne(findByFarmerId);
  }
//
//deleteById(int ids){
//  Find findByFarmerId = Find(bean.tableName);
//  findByFarmerId.where(bean.fId.eq(ids));
//  return bean.remove(findByFarmerId);
//}

  @override
  Future<void> delete(Farmer farmer) {
    Logger.v('farmerId:${farmer.fId}');
    return bean.remove(farmer.fId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<Farmer>> findAll() {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findFarmer = Find(bean.tableName);
    findFarmer.where(bean.userId.eq(user.userid));

    return bean.findMany(findFarmer);
  }

  @override
  Future<Farmer> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<Farmer> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(Farmer farmer) async {
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table farmer");
    return await bean.insert(farmer);
  }

  @override
  Future<int> update(Farmer farmer) {
 return bean.update(farmer);
  }


  Future<List<Farmer>> findAllBySyncStatus() {
    Find findFarmer = Find(bean.tableName);
    findFarmer.where(bean.syncToServer.eq(false));
    return bean.findMany(findFarmer);
  }


  Future<int> updateByMobileNumber(Farmer farmer) async {

    DateTime todayDate = DateTime.now();
    final Update update = bean.updater
        .where(bean.mobileNumber.eq(farmer.mobileNumber));
    update.set(bean.farmerId, farmer.farmerId);
    update.set(bean.syncDate, farmer.syncDate);

    return await bean.adapter.update(update);
  }



  Future<int> updateByMobileNumbers(SyncModel syncModel) async {

    for( Farmer farmer in syncModel.farmers) {
      DateTime todayDate = DateTime.now();
      final Update update = bean.updater
          .where(bean.mobileNumber.eq(farmer.mobileNumber));
      update.set(bean.farmerId, farmer.farmerId);
      update.set(bean.syncDate, farmer.syncDate);

      return await bean.adapter.update(update);
    }
  }


  Future<int> updateByServerFarmerId(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.farmerId.eq(farmer.farmerId));

    return await bean.adapter.update(update);
  }



  Future<int> updateByMobileNumberSync(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mobileNumber.eq(farmer.mobileNumber)).set(bean.syncToServer ,true);

    return await bean.adapter.update(update);
  }


  //single model
  Future<int> updateByMobileNumberSyncs(SyncModel syncModel) async {

    for( Farmer farmers in syncModel.farmers){
      final Update update = bean.updater
          .where(bean.mobileNumber.eq(farmers.mobileNumber)).set(bean.syncToServer ,true);

      return await bean.adapter.update(update);
    }

  }

  //test for update from server
  Future<int> updateByAllSync(Farmer farmer) async {

    var formattedCreatedDate = farmer.createdDate==null?"":farmer.createdDate.split("T")[0];
    var formattedUpdatedDate = farmer.updatedDate==null?"":farmer.updatedDate.split("T")[0];
    final Update update = bean.updater
        .where(bean.mobileNumber.eq(farmer.mobileNumber));

    update.set(bean.mobileNumber, farmer.mobileNumber);
    update.set(bean.farmerName, farmer.farmerName);
    update.set(bean.province, farmer.province);
    update.set(bean.provinceId, farmer.provinceId);
    update.set(bean.district, farmer.district);
    update.set(bean.districtId, farmer.districtId);
    update.set(bean.municipality, farmer.municipality);
    update.set(bean.municipalityId, farmer.municipalityId);
    update.set(bean.ward, farmer.ward);
    update.set(bean.village, farmer.village);
    update.set(bean.syncToServer, true);
    update.set(bean.fId, farmer.fId);
    update.set(bean.farmerId, farmer.farmerId);
    update.set(bean.createdDate, formattedCreatedDate);
    update.set(bean.createdBy, farmer.createdBy);
    update.set(bean.updatedDate, formattedUpdatedDate);
    update.set(bean.userId, farmer.userId);
    update.set(bean.syncDate, farmer.syncDate);
    return await bean.adapter.update(update);
  }


  findBySameMobileNumber(Farmer farmer)async {
    Find findByMbl = Find(bean.tableName);
    findByMbl.where(bean.mobileNumber.eq(farmer.mobileNumber));

    return bean.findOne(findByMbl);
  }


  findBySameMobileNumberUpdate(Farmer farmer)async {
    Find findByMbl = Find(bean.tableName);
    findByMbl.where(bean.mobileNumber.eq(farmer.mobileNumber).and(bean.fId.isNot(farmer.fId)));

    return bean.findOne(findByMbl);
  }


  Future<List<Farmer>> findAllSyncData() {
    Find findFarmer = Find(bean.tableName);
    findFarmer.where(bean.syncToServer.eq(true));
    return bean.findMany(findFarmer);
  }

  Future<List<Farmer>> findAllUnSyncData() {
    Find findFarmer = Find(bean.tableName);
    findFarmer.where(bean.syncToServer.eq(false));
    return bean.findMany(findFarmer);
  }



}
