import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'Species.g.dart';

part 'Species.jorm.dart';

@JsonSerializable()
class Species implements JsonCallable<Species> {
  @PrimaryKey(auto: true)
  int sId;
  @Column(isNullable: true)
  @JsonKey(name: 'AnimalId')
  int ids;
  @Column(isNullable: true)
  @JsonKey(name: 'AnimalName')
  String name;

  Species();


  Species.make(this.ids, this.name);

  factory Species.fromJson(Map<String, dynamic> json) =>
      _$SpeciesFromJson(json);

  Map<String, dynamic> toJson() => _$SpeciesToJson(this);

  @override
  Species fromJson(Map<String, dynamic> json) {
    return Species.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class SpeciesBean extends Bean<Species> with _SpeciesBean {
  SpeciesBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'species';
}
