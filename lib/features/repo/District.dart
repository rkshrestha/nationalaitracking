
import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'District.g.dart';
part 'District.jorm.dart';

@JsonSerializable()
class District implements JsonCallable<District>{
  @PrimaryKey(auto: true)
  int dId;
  @JsonKey(name: 'DistrictId')
  int districtid;
  @JsonKey(name: 'DCode')
  String dcode;
  @JsonKey(name: 'SCode')
  int provinceid;
  @JsonKey(name: 'DistrictName')
  String name;

  District();

  District.make(this.districtid, this.dcode, this.provinceid, this.name);

  factory District.fromJson(Map<String, dynamic> json) =>
      _$DistrictFromJson(json);

  Map<String, dynamic> toJson() => _$DistrictToJson(this);

  @override
  District fromJson(Map<String, dynamic> json) {
    return District.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }


}


@GenBean()
class DistrictBean extends Bean<District> with _DistrictBean {
  DistrictBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'district';
}
