// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'LiquidNitrogen.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LiquidNitrogen _$LiquidNitrogenFromJson(Map<String, dynamic> json) {
  return LiquidNitrogen()
    ..lnId = json['MLiquidNitrogenId'] as String
    ..slnId = json['LiquidNitrogenId'] as int
    ..receivedDate = json['ReceivedDate'] as String
    ..quantity = (json['Quantity'] as num)?.toDouble()
    ..userId = json['UserId'] as int
    ..createdDate = json['CreatedDate'] as String
    ..updatedDate = json['UpdatedDate'] as String
    ..createdBy = json['CreatedBy'] as int
    ..updatedBy = json['UpdatedBy'] as int
    ..englishReceivedDate = json['EnglishReceivedDate'] as String
    ..syncDate = json['SyncDate'] as String
    ..syncToServer = json['SyncToServer'] as bool;
}

Map<String, dynamic> _$LiquidNitrogenToJson(LiquidNitrogen instance) =>
    <String, dynamic>{
      'MLiquidNitrogenId': instance.lnId,
      'LiquidNitrogenId': instance.slnId,
      'ReceivedDate': instance.receivedDate,
      'Quantity': instance.quantity,
      'UserId': instance.userId,
      'CreatedDate': instance.createdDate,
      'UpdatedDate': instance.updatedDate,
      'CreatedBy': instance.createdBy,
      'UpdatedBy': instance.updatedBy,
      'EnglishReceivedDate': instance.englishReceivedDate,
      'SyncDate': instance.syncDate,
      'SyncToServer': instance.syncToServer
    };
