import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:nbis/features/repo/AIDao.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalDao.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Breed.dart';
import 'package:nbis/features/repo/BreedDao.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/CalvingDao.dart';
import 'package:nbis/features/repo/District.dart';
import 'package:nbis/features/repo/DistrictDao.dart';
import 'package:nbis/features/repo/ExitReason.dart';
import 'package:nbis/features/repo/ExitReasonDao.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/ExitRecordDao.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerDao.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/LiquidNitrogenDao.dart';
import 'package:nbis/features/repo/Municipality.dart';
import 'package:nbis/features/repo/MunicipalityDao.dart';
import 'package:nbis/features/repo/NotificationDao.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';
import 'package:nbis/features/repo/PDDao.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Province.dart';
import 'package:nbis/features/repo/ProvinceDao.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/repo/SemenDao.dart';
import 'package:nbis/features/repo/Species.dart';
import 'package:nbis/features/repo/SpeciesDao.dart';
import 'package:nbis/s/Logger.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';

class SQFliteDatabase {
  static SQFliteDatabase _instance;
  static SqfliteAdapter _adapter;
  static const String DBNAME = "DB_NBIS.db";

  SQFliteDatabase._();

  static SQFliteDatabase getDB() {
    if (_instance == null) {
      _instance = SQFliteDatabase._();
    }
    return _instance;
  }

  connect() async {
    var dbPath = await getDatabasesPath();
    Logger.v('Database' + dbPath.toString());

    String realPath = path.join(dbPath.toString(), DBNAME);
    _adapter = new SqfliteAdapter(realPath);
    await _adapter.connect();

    Logger.v("connect");
  }



  Future<String> getDBPath() async {
    var dbPath = await getDatabasesPath();
    return dbPath;
  }

  close() async {
    Logger.v("database close called");
    await _adapter.close();

  }

//farmer

  Future<FarmerDao> farmerDao() async {
//    await getDB().connect();

    Logger.v('farmerDao');
    FarmerBean farmerBean = FarmerBean(_adapter);
    Logger.v('test');
    await getDB().connect();
    await farmerBean.createTable(ifNotExists: true);
    return FarmerDao.get(farmerBean);
  }

  Future<AnimalDao> animalDao() async {
    Logger.v('animalDao');
//    await getDB().connect();
    AnimalBean animalBean = AnimalBean(_adapter);
    await getDB().connect();
    await animalBean.createTable(ifNotExists: true);
    return AnimalDao.get(animalBean);
  }



  Future<AIDao> aiDao() async {
    Logger.v('aiDao');
//    await getDB().connect();

    ArtificialInseminationBean aiBean = ArtificialInseminationBean(_adapter);
    Logger.v('aiDao1');
    await getDB().connect();
    await aiBean.createTable(ifNotExists: true);
    return AIDao.get(aiBean);
  }

  Future<PDDao> pdDao() async {
    Logger.v('pdDao');
//    await getDB().connect();
    PregnancyDiagnosisBean pdBean = PregnancyDiagnosisBean(_adapter);
    await getDB().connect();
    await pdBean.createTable(ifNotExists: true);
    return PDDao.get(pdBean);
  }

  Future<CalvingDao> calvingDao() async {
    Logger.v('calvingDao');
//    await getDB().connect();
    CalvingBean calvingBean = CalvingBean(_adapter);
    await getDB().connect();
    await calvingBean.createTable(ifNotExists: true);
    return CalvingDao.get(calvingBean);
  }

  Future<ExitRecordDao> exitRecordDao() async {
    Logger.v('exitDao');
//    await getDB().connect();
    ExitRecordBean exitRecordBean = ExitRecordBean(_adapter);
    await getDB().connect();
    await exitRecordBean.createTable(ifNotExists: true);
    return ExitRecordDao.get(exitRecordBean);
  }



  Future<SemenDao> semenDao() async {
    Logger.v("semenDao");
//    await getDB().connect();
    SemenBean semenBean = SemenBean(_adapter);
    Logger.v('after semen bean' + semenBean.toString());
    await getDB().connect();
    Logger.v("show: " + _adapter.toString());

    await semenBean.createTable(ifNotExists: true);

    return SemenDao.get(semenBean);
  }

  Future<LiquidNitrogenDao> liquidNitrogenDao() async {
    Logger.v("liquidNitrogenDao");
//    await getDB().connect();
    LiquidNitrogenBean bean = LiquidNitrogenBean(_adapter);
    Logger.v('after nitrogen bean' + bean.toString());
    Logger.v("show" + _adapter.toString());
    await getDB().connect();

    await bean.createTable(ifNotExists: true);

    return LiquidNitrogenDao.get(bean);
  }


  Future<SpeciesDao> speciesDao() async {
    Logger.v('speciesDao');
    await getDB().connect();
    SpeciesBean speciesBean = SpeciesBean(_adapter);
    await speciesBean.createTable(ifNotExists: true);
    return SpeciesDao.get(speciesBean);
  }

  Future<BreedDao> breedDao() async {
    Logger.v('breedDao');
    await getDB().connect();
    BreedBean breedBean = BreedBean(_adapter);
    await breedBean.createTable(ifNotExists: true);
    return BreedDao.get(breedBean);
  }

  Future<ProvinceDao> provinceDao() async {
    Logger.v('provinceDao');
    await getDB().connect();
    ProvinceBean provinceBean = ProvinceBean(_adapter);
    await provinceBean.createTable(ifNotExists: true);
    return ProvinceDao.get(provinceBean);
  }

  Future<DistrictDao> districtDao() async {
    Logger.v('districtDao');
    await getDB().connect();
    DistrictBean districtBean = DistrictBean(_adapter);
    await districtBean.createTable(ifNotExists: true);
    return DistrictDao.get(districtBean);
  }


  Future<MunicipalityDao> municipalityDao() async {
    Logger.v('MunicipalityDao');
    await getDB().connect();
    MunicipalityBean municipalityBean = MunicipalityBean(_adapter);
    await municipalityBean.createTable(ifNotExists: true);
    return MunicipalityDao.get(municipalityBean);
  }


  Future<ExitReasonDao> exitReasonDao() async {
    Logger.v('exitReason Dao');
    await getDB().connect();
    ExitReasonBean exitBean = ExitReasonBean(_adapter);
    await exitBean.createTable(ifNotExists: true);
    Logger.v('after exit create');
    return ExitReasonDao.get(exitBean);
  }


  notificationRequestDao() async {
    await getDB().connect();
    NotificationRequestBean bean = NotificationRequestBean(_adapter);
    await getDB().connect();
    await bean.createTable(ifNotExists: true);
    return NotificationDao.get(bean);
  }

}
