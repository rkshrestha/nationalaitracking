// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Calving.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Calving _$CalvingFromJson(Map<String, dynamic> json) {
  return Calving()
    ..cId = json['MCalvingId'] as String
    ..calvingId = json['CalvingId'] as int
    ..farmerId = json['FarmerId'] as int
    ..animalId = json['AnimalId'] as int
    ..speciesId = json['SpeciesId'] as int
    ..breedId = json['BreedId'] as int
    ..calvingDate = json['CalvingDate'] as String
    ..calfSex = json['CalfSex'] as String
    ..vetAssist = json['VetAssist'] as bool
    ..calfCode = json['CalfId'] as String
    ..remarks = json['Remarks'] as String
    ..createdDate = json['CreatedDate'] as String
    ..updatedDate = json['UpdatedDate'] as String
    ..mobileNumber = json['MobileNo'] as String
    ..speciesCode = json['SpeciesCode'] as String
    ..createdBy = json['CreatedBy'] as int
    ..updatedBy = json['UpdatedBy'] as int
    ..mFarmerId = json['MFarmerId'] as String
    ..mAnimalId = json['MAnimalId'] as String
    ..userId = json['UserId'] as int
    ..farmerName = json['FarmerName'] as String
    ..englishCalvingDate = json['EnglishCalvingDate'] as String
    ..syncDate = json['SyncDate'] as String
    ..speciesName = json['SpeciesName'] as String
    ..calfAnimalId = json['CalfAnimalId'] as int
    ..syncToServer = json['SyncToServer'] as bool;
}

Map<String, dynamic> _$CalvingToJson(Calving instance) => <String, dynamic>{
      'MCalvingId': instance.cId,
      'CalvingId': instance.calvingId,
      'FarmerId': instance.farmerId,
      'AnimalId': instance.animalId,
      'SpeciesId': instance.speciesId,
      'BreedId': instance.breedId,
      'CalvingDate': instance.calvingDate,
      'CalfSex': instance.calfSex,
      'VetAssist': instance.vetAssist,
      'CalfId': instance.calfCode,
      'Remarks': instance.remarks,
      'CreatedDate': instance.createdDate,
      'UpdatedDate': instance.updatedDate,
      'MobileNo': instance.mobileNumber,
      'SpeciesCode': instance.speciesCode,
      'CreatedBy': instance.createdBy,
      'UpdatedBy': instance.updatedBy,
      'MFarmerId': instance.mFarmerId,
      'MAnimalId': instance.mAnimalId,
      'UserId': instance.userId,
      'FarmerName': instance.farmerName,
      'EnglishCalvingDate': instance.englishCalvingDate,
      'SyncDate': instance.syncDate,
      'SpeciesName': instance.speciesName,
      'CalfAnimalId': instance.calfAnimalId,
      'SyncToServer': instance.syncToServer
    };
