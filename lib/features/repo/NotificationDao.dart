
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';

class NotificationDao  implements BaseDao<NotificationRequest> {

  static NotificationDao _instance;
  NotificationRequestBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  NotificationDao._(this.bean);

  static NotificationDao get(NotificationRequestBean bean) {
    if (_instance == null) {
      _instance = NotificationDao._(bean);
    }
    return _instance;
  }

  findNotification(){
    DateTime today = new DateTime.now();
//    DateTime todayDate =
//    new DateTime(today.year + 56, today.month + 8, today.day + 17);
    String todayDate = DateFormat('yyyy-MM-dd').format(today);
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findNotification =  Find(bean.tableName);

    findNotification.where(bean.notificationDate.eq(todayDate).and(bean.userId.eq(user.userid)));

    return bean.findMany(findNotification);
  }

  @override
  Future<void> delete(NotificationRequest entity) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<void> deleteAll() {
    return bean.removeAll();
  }

  @override
  Future<List<NotificationRequest>> findAll() {
    Find findNotification = Find(bean.tableName);
    return bean.findMany(findNotification);
  }

  @override
  Future<NotificationRequest> findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Future<NotificationRequest> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(NotificationRequest nr) async{
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table ai");
    return await bean.insert(nr);
  }

  @override
  Future<void> update(NotificationRequest entity) {
    // TODO: implement update
    return null;
  }


}