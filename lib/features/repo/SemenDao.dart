import 'package:flutter/material.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';

class SemenDao implements BaseDao<Semen> {
  static SemenDao _instance;
  SemenBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  SemenDao._(this.bean);


  static SemenDao get(SemenBean bean) {
    if (_instance == null) {
      _instance = SemenDao._(bean);
    }
    return _instance;
  }


  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
  }
  findBySemenId(int semenId)async {
    Find findBySemenId = Find(bean.tableName);
     Logger.v("sid"+ semenId.toString());
     Logger.v("ssId"+bean.semenId.toString());
    findBySemenId.where(bean.semenId.eq(semenId));

    return bean.findOne(findBySemenId);
  }


  @override
  Future<void> delete(Semen semen) {
    Logger.v('semenId:  ${semen.msId}');

    return bean.remove(semen.msId);
  }

  @override
  Future<void> deleteAll() {
    return bean.removeAll();
  }

  @override
  Future<List<Semen>> findAll() async{
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findSemen = Find(bean.tableName);
    findSemen.where(bean.userId.eq(user.userid));
     return await bean.findMany(findSemen);
  }

  @override
  Future<Semen> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<Semen> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(Semen semen) async {
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table semen");
    return await bean.insert(semen);
  }

  @override
  Future<int> update(Semen semen) async {
    return await bean.update(semen);
  }


  Future<int> updateByServerSemenId(Semen semen) async {
    var formattedDate = semen.receivedDate==null?"":semen.receivedDate.split("T")[0];
    var formattedCreatedDate = semen.createdDate==null?"":semen.createdDate.split("T")[0];
    var formattedUpdatedDate = semen.updatedDate==null?"":semen.updatedDate.split("T")[0];
    var formattedEnglishDate = semen.englishReceivedDate==null?"":semen.englishReceivedDate.split("T")[0];
    final Update update = bean.updater
        .where(bean.semenId.eq(semen.semenId));

    update.set(bean.murrah, semen.murrah);
    update.set(bean.jersey, semen.jersey);
    update.set(bean.holsteinFriesian,semen.holsteinFriesian);
    update.set(bean.msId,semen.msId);
    update.set(bean.receivedDate,formattedDate);
    update.set(bean.semenId,semen.semenId);
    update.set(bean.createdDate,formattedCreatedDate);
    update.set(bean.updatedDate,formattedUpdatedDate);
    update.set(bean.createdBy,semen.createdBy);
    update.set(bean.updatedBy,semen.updatedBy);
    update.set(bean.boar,semen.boar);
    update.set(bean.saanen,semen.saanen);
    update.set(bean.yorkshire,semen.yorkshire);
    update.set(bean.landrace,semen.landrace);
    update.set(bean.duroc,semen.duroc);
    update.set(bean.hampshire,semen.hampshire);
    update.set(bean.userId,semen.userId);
    update.set(bean.syncDate,semen.syncDate);
    update.set(bean.englishReceivedDate, formattedEnglishDate);
    update.set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

//
  Future<int> updateByDate(Semen semen) async {
    var formattedDate=semen.receivedDate.split("T")[0];
    DateTime todayDate = DateTime.now();
    final Update update = bean.updater
        .where(bean.receivedDate.eq(formattedDate));
        update.set(bean.semenId, semen.semenId);
        update.set(bean.syncDate, semen.syncDate);

    Logger.v("bean date: "+semen.receivedDate );
    return await bean.adapter.update(update);
  }



  Future<int> updateByDatesSync(Semen syncedSemen) async {
    var formattedDate=syncedSemen.receivedDate.split("T")[0];
    final Update update = bean.updater
        .where(bean.receivedDate.eq(formattedDate)).set(bean.syncToServer ,true);

    Logger.v("bean date: "+syncedSemen.receivedDate );
    return await bean.adapter.update(update);
  }


  Future<List<Semen>> findAllBySyncStatus() {
    Find findSemen = Find(bean.tableName);
    findSemen.where(bean.syncToServer.eq(false));
    return bean.findMany(findSemen);
  }


  findBySameDate(Semen semen)async {
    Find findBySemenId = Find(bean.tableName);
    Logger.v("sid"+ semen.receivedDate);
    Logger.v("ssId"+bean.receivedDate.toString());
    findBySemenId.where(bean.receivedDate.eq(semen.receivedDate));

    return bean.findOne(findBySemenId);
  }

  findBySameDatess(Semen semen)async {
    Find findBySemenId = Find(bean.tableName);
    Logger.v("sid"+ semen.receivedDate);
    Logger.v("ssId"+bean.receivedDate.toString());
    findBySemenId.where(bean.receivedDate.eq(semen.receivedDate).and(bean.msId.isNot(semen.msId)));

    return bean.findOne(findBySemenId);
  }

  Future<List<Semen>> findAllSyncData() {
    Find findSemen = Find(bean.tableName);
    findSemen.where(bean.syncToServer.eq(true));
    return bean.findMany(findSemen);
  }

  Future<List<Semen>> findAllUnSyncData() {
    Find findSemen = Find(bean.tableName);
    findSemen.where(bean.syncToServer.eq(false));
    return bean.findMany(findSemen);
  }
}
