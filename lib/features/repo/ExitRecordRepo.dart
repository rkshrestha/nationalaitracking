import 'dart:async';

import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalDao.dart';
import 'package:nbis/features/repo/ExitReason.dart';
import 'package:nbis/features/repo/ExitReasonDao.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/ExitRecordDao.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:uuid/uuid.dart';

class ExitRecordRepo implements Repository<ExitRecord> {
  SQFliteDatabase sqFliteDatabase;
  ExitRecordDao exitRecordDao;
  ExitReasonDao exitReasonDao;
  AnimalDao animalDao;
  List<ExitRecord> exitList;
  List<ExitReason> exitReasonList;
  SessionManager sessionManager;
  BuildContext context;
  int exitRId;

  ExitRecordRepo(this.context) {
    Logger.v("inside exitRepo");
    init();
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
  }

  void saveExit(context, ExitRecord exit, Farmer farmer, Animal animal,
      ExitReason exitReason, StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try saveExit');

      exitRecordDao = await sqFliteDatabase.exitRecordDao();
      animalDao = await sqFliteDatabase.animalDao();
      Logger.v('check exit add ');

      var uuid = new Uuid();
      var uid =  uuid.v1();


      var user = this.sessionManager.findOne().user;
      ExitRecord exits =
          await exitRecordDao.findBySameDateAndAnimal(exit, animal);
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);
      DateTime today = new DateTime.now();
      DateTime todayNepaliDate =
      new DateTime(today.year + 56, today.month + 8, today.day + 17, today.hour,
          today.minute, today.second);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);

      NepaliDateTime nepaliExitDate = NepaliDateTime.tryParse(exit.dateOfExit);
      DateTime englishAIDate = DateConverter.toAD(nepaliExitDate);
      var formattedEnglishDate = englishAIDate.toIso8601String().split("T")[0];
      if (exits == null) {
        int exitId = await exitRecordDao.persist(new ExitRecord.make(
                uid,
//                exit.eId,
                null,
                farmer.farmerId,
                animal.animalId,
                animal.speciesId,
                animal.breedId,
                exit.dateOfExit,
                exitReason.name,
                exit.mobileNumber,
                exit.speciesCode,
            today.toIso8601String(),
                null,
                user.userid,
                null,
                farmer.fId,
                animal.aId,
                user.userid,
                exit.farmerName,
            formattedEnglishDate,
                null,
                syncToServer: false)

//        ExitRecord.make(
//            exit.eId,
//            null,
//            farmer.fId,
//            animal.aId,
////            4,
////          user.userid,
//            animal.speciesId,
//            animal.breedId,
//            exit.dateOfExit,
//            exitReason.name
////        "Sold"
//        )

            );

        await animalDao.updateForAnimalStatus(animal.speciesCode);
        Navigator.pop(context, true);
//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("exit_add_success"),
//        ));
        sSink.add(SCResponse.success("Exit Animal has been successfully added"));
      } else {
//        sSink.add(SCResponse.error(
//          AppTranslations.of(context).text("exit_exist"),
//        ));
        sSink.add(SCResponse.error("Same animal can't be exit twice"));
      }
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("exit_add_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't insert exit record"));
      Logger.v(ex.toString());
    }
  }

  void updateExitRecord(context, ExitRecord exit, Farmer farmer, Animal animal,
      ExitReason exitReason, StreamSink<SCResponse<String>> sSink) async {
    try {
      exitRecordDao = await sqFliteDatabase.exitRecordDao();
      animalDao = await sqFliteDatabase.animalDao();
      var user = this.sessionManager.findOne().user;
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);
      DateTime today = new DateTime.now();
      DateTime todayNepaliDate =
      new DateTime(today.year + 56, today.month + 8, today.day + 17, today.hour,
          today.minute, today.second);

      NepaliDateTime nepaliExitDate = NepaliDateTime.tryParse(exit.dateOfExit);
      DateTime englishAIDate = DateConverter.toAD(nepaliExitDate);
      var formattedEnglishDate = englishAIDate.toIso8601String().split("T")[0];
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
      int updateExitRecord = await exitRecordDao.update(new ExitRecord.make(
          exit.eId,
          exit.erId,
          farmer.farmerId,
          animal.animalId,
          animal.speciesId,
          animal.breedId,
          exit.dateOfExit,
          exitReason.name,
          exit.mobileNumber,
          exit.speciesCode,
//          todayNepaliDate.toIso8601String(),
           exit.createdDate,
          today.toIso8601String(),
          user.userid,
          user.userid,
          farmer.fId,
          animal.aId,
          user.userid,
          exit.farmerName,
          formattedEnglishDate,
          exit.syncDate,
          syncToServer: false));
      Logger.v("Code"+ animal.speciesCode);

      await animalDao.updateForAnimalStatus(animal.speciesCode);
      Navigator.pop(context, true);
//      sSink.add(SCResponse.success(
//        AppTranslations.of(context).text("exit_update_success"),
//      ));
      sSink.add(SCResponse.success("Exit Record has been successfully updated"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("exit_update_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't update exit Record"));
      Logger.v(ex.toString());
    }
  }

  void getExitRecordList(
      StreamSink<SCResponse<List<ExitRecord>>> stSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      exitRecordDao = await sqFliteDatabase.exitRecordDao();

      exitList = await exitRecordDao.findAll();

      for (ExitRecord exit in exitList) {
        Logger.v(exit.dateOfExit);
      }

      stSink.add(SCResponse.success(exitList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

//sync list
  void getSyncExitRecordList(
      StreamSink<SCResponse<List<ExitRecord>>> stSink) async {
    try {
      exitRecordDao = await sqFliteDatabase.exitRecordDao();

      exitList = await exitRecordDao.findAllSyncData();

      for (ExitRecord exit in exitList) {
        Logger.v(exit.dateOfExit);
      }

      stSink.add(SCResponse.success(exitList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //unsync list
  void getUnSyncExitRecordList(
      StreamSink<SCResponse<List<ExitRecord>>> stSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      exitRecordDao = await sqFliteDatabase.exitRecordDao();

      exitList = await exitRecordDao.findAllUnSyncData();

      for (ExitRecord exit in exitList) {
        Logger.v(exit.dateOfExit);
      }

      stSink.add(SCResponse.success(exitList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

//  void syncExitReasonList(StreamSink<SCResponse<String>> sSink) async {
//    try {
//      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
//
//      if (!isNetworkAvailable) {
//        sSink.add(SCResponse.noInternetAvailable());
//        return;
//      }
//
//      NetworkClient networkClient = NetworkClient();
//      NetworkApi networkApi = NetworkApi.create(
//          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      SCResponse scResponse = await networkClient
//          .call<ExitReason, List<ExitReason>>(networkApi.syncExitReasonList(),
//              callable: ExitReason());
//
//      if (scResponse.status == Status.SUCCESS) {
//        List<ExitReason> exitReasonList = scResponse.data;
//        exitReasonDao = await sqFliteDatabase.exitReasonDao();
//
//        exitReasonDao.deleteAll();
//        for (ExitReason exitReason in exitReasonList) {
//          exitReasonDao
//              .persist(new ExitReason.make(exitReason.ids, exitReason.name));
//        }
//
//        Logger.v(exitReasonList[0].name);
//
//        sSink.add(SCResponse.success("Exit Reason Sync success"));
//      } else if (scResponse.status == Status.ERROR) {
//        Logger.v(scResponse.errorMessage);
//        sSink.add(scResponse);
//      }
//    } catch (ex) {
//      Logger.v(ex.toString());
//    }
//  }

  void syncExitReasonList(
      StreamSink<SCResponse<List<ExitReason>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient
          .call<ExitReason, List<ExitReason>>(networkApi.syncExitReasonList(),
              callable: ExitReason());

      if (scResponse.status == Status.SUCCESS) {
        List<ExitReason> exitReasonList = scResponse.data;
        exitReasonDao = await sqFliteDatabase.exitReasonDao();

        exitReasonDao.deleteAll();
        for (ExitReason exitReason in exitReasonList) {
          exitReasonDao
              .persist(new ExitReason.make(exitReason.ids, exitReason.name));
        }

        Logger.v(exitReasonList[0].name);

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Exit Reason Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getExitReasonList(
      StreamSink<SCResponse<List<ExitReason>>> stSink) async {
    try {
      exitReasonDao = await sqFliteDatabase.exitReasonDao();

      exitReasonList = await exitReasonDao.findAll();

      for (ExitReason exitReason in exitReasonList) {
        Logger.v(exitReason.name);
      }

      stSink.add(SCResponse.success(exitReasonList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //delete
  deleteExitRecord(context, ExitRecord exitRecord,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      exitRecordDao = await sqFliteDatabase.exitRecordDao();

      await exitRecordDao.delete(exitRecord);

//      sSink.add(SCResponse.success(
//        AppTranslations.of(context).text("exit_delete_success"),
//      ));
      sSink.add(SCResponse.success("Exit Record deleted Successfully"));
    } catch (ex) {
      sSink.add(SCResponse.error("Couldn't delete exit Record"));
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("exit_delete_fail"),
//      ));
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local start


  void updateExitRecordServerIdAndStatus(List<ExitRecord> exitRecordList,
      StreamSink<SCResponse<List<ExitRecord>>> sSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();

    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      exitRecordDao = await sqFliteDatabase.exitRecordDao();
      var user = this.sessionManager.findOne().user;

      for (ExitRecord syncedExit in exitRecordList) {
        await exitRecordDao.updateByDateAndAnimalIdSync(syncedExit);

        await exitRecordDao.updateByDateAndAnimalId(syncedExit);
      }

    }catch(ex){Logger.v(ex.toString());}
  }
  //update Server Id in local end





  //exit record send to server
  void sendExitRecordData(List<ExitRecord> exitRecordList,
      StreamSink<SCResponse<List<ExitRecord>>> sSink) async {
//
//    sqFliteDatabase = SQFliteDatabase.getDB();
//    await sqFliteDatabase.connect();

    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));

//      List<ExitRecord> unsyncedExitRecordList =
//          await exitRecordDao.findAllBySyncExitStatus();

      exitRecordDao = await sqFliteDatabase.exitRecordDao();
      var user = this.sessionManager.findOne().user;
      SCResponse scResponse = await networkClient.call<ExitRecord,
              List<ExitRecord>>(
          networkApi.sendExitRecordData(exitRecordList, user.userid),
//          networkApi.sendExitRecordData(unsyncedExitRecordList, user.userid),
          callable: ExitRecord());

      if (scResponse.status == Status.SUCCESS) {
        List<ExitRecord> syncedExitList = scResponse.data;

        for (ExitRecord syncedExit in syncedExitList) {
          await exitRecordDao.updateByDateAndAnimalIdSync(syncedExit);

          await exitRecordDao.updateByDateAndAnimalId(syncedExit);
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }

      sSink.add(scResponse);
    } catch (ex) {
      Logger.v(ex.toString());
    }
//    finally{
//      sqFliteDatabase.close();
//    }
  }

  //get exit record data
  void syncGetExitRecordData(StreamSink<SCResponse<List<ExitRecord>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager.findOne().user;
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse =
          await networkClient.call<ExitRecord, List<ExitRecord>>(
              networkApi.syncGetExitRecordList(user.userid),
              callable: ExitRecord());

//      if (scResponse.status == Status.SUCCESS) {
//        List<ExitRecord> exitRecordList = scResponse.data;
//        exitRecordDao = await sqFliteDatabase.exitRecordDao();
//
//        for (ExitRecord exitServer in exitRecordList) {
//          exitRId = exitServer.erId;
//          Logger.v("ServerId " + exitRId.toString());
//          ExitRecord exit = await exitRecordDao.findByExitRecordId(exitRId);
//
//          if (exit == null) {
//            exitRecordDao.delete(exitServer);
//            exitRecordDao.persist(new ExitRecord.make(
//                exitServer.eId,
//                exitServer.erId,
//                exitServer.farmerId,
//                exitServer.animalId,
//                exitServer.userId,
//                exitServer.speciesId,
//                exitServer.breedId,
//                exitServer.dateOfExit,
//                exitServer.exitReason));
//          } else {
//            if (exit.eId == exitServer.eId) {
//              exitRecordDao.update(ExitRecord.make(
//                  exitServer.eId,
//                  exitServer.erId,
//                  exitServer.farmerId,
//                  exitServer.animalId,
//                  exitServer.userId,
//                  exitServer.speciesId,
//                  exitServer.breedId,
//                  exitServer.dateOfExit,
//                  exitServer.exitReason));
//            }
//          }
//        }
//
//
//        sSink.add(SCResponse.success("Exit Record Sync success"));
//      }
//

      if (scResponse.status == Status.SUCCESS) {
        List<ExitRecord> exitRecordLists = scResponse.data;
        exitRecordDao = await sqFliteDatabase.exitRecordDao();
//
        for (ExitRecord exitRecordServer in exitRecordLists) {
          exitRId = exitRecordServer.erId;
          Logger.v("ServerId " + exitRId.toString());
          ExitRecord exitRecord =
              await exitRecordDao.findByExitRecordId(exitRId);
          if (exitRecord == null) {
            var formattedDate = exitRecordServer.dateOfExit==null?"":exitRecordServer.dateOfExit.split("T")[0];
            var formattedCreatedDate =
                exitRecordServer.createdDate==null?"":exitRecordServer.createdDate.split("T")[0];
            var formattedUpdatedDate =
                exitRecordServer.updatedDate==null?"":exitRecordServer.updatedDate.split("T")[0];
            var formattedEnglishDate = exitRecordServer.englishExitDate==null?"":exitRecordServer.englishExitDate.split("T")[0];
            exitRecordDao.persist(new ExitRecord.make(
                exitRecordServer.eId,
                exitRecordServer.erId,
                exitRecordServer.farmerId,
                exitRecordServer.animalId,
                exitRecordServer.speciesId,
                exitRecordServer.breedId,
                formattedDate,
                exitRecordServer.exitReason,
                exitRecordServer.mobileNumber,
                exitRecordServer.speciesCode,
                formattedCreatedDate,
                formattedUpdatedDate,
                exitRecordServer.createdBy,
                exitRecordServer.updatedBy,
                exitRecordServer.mFarmerId,
                exitRecordServer.mAnimalId,
                exitRecordServer.userId,
                exitRecordServer.farmerName,
                formattedEnglishDate,
                exitRecordServer.syncDate,
                syncToServer: true));
          } else {
//            var formattedDate = exitRecordServer.dateOfExit.split("T")[0];
            exitRecordDao.updateByServerAIId(exitRecordServer);
//            exitRecordDao.updateByServerAIId(new ExitRecord.make(
//                exitRecordServer.eId,
//                exitRecordServer.erId,
//                exitRecordServer.farmerId,
//                exitRecordServer.animalId,
////                exitRecordServer.userId,
//                exitRecordServer.speciesId,
//                exitRecordServer.breedId,
//                formattedDate,
//                exitRecordServer.exitReason,
//                syncToServer: true));
          }
        }

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Exit Record Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getExitRecordListByAnimal(
      Animal animal, StreamSink<SCResponse<List<ExitRecord>>> stSink) async {
    try {
      exitRecordDao = await sqFliteDatabase.exitRecordDao();

      exitList = await exitRecordDao.getExitRecordByAnimal(animal);

      for (ExitRecord exit in exitList) {
        Logger.v(exit.animalId.toString());
      }

      stSink.add(SCResponse.success(exitList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getUnSyncExitRecordListByAnimal(
      Animal animal, StreamSink<SCResponse<List<ExitRecord>>> stSink) async {
    try {
      exitRecordDao = await sqFliteDatabase.exitRecordDao();

      exitList = await exitRecordDao.getUnSyncExitRecordByAnimal(animal.aId);

      for (ExitRecord exit in exitList) {
        Logger.v(exit.animalId.toString());
      }

      stSink.add(SCResponse.success(exitList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSyncExitRecordListByAnimal(
      Animal animal, StreamSink<SCResponse<List<ExitRecord>>> stSink) async {
    try {
      exitRecordDao = await sqFliteDatabase.exitRecordDao();

      exitList = await exitRecordDao.getSyncExitRecordByAnimal(animal.aId);

      for (ExitRecord exit in exitList) {
        Logger.v(exit.animalId.toString());
      }

      stSink.add(SCResponse.success(exitList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  @override
  void delete(ExitRecord entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<ExitRecord> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  ExitRecord findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  ExitRecord findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(ExitRecord entity) {
    // TODO: implement persist
  }

  @override
  void update(ExitRecord entity) {
    // TODO: implement update
  }
}
