// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PregnancyDiagnosis.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PregnancyDiagnosis _$PregnancyDiagnosisFromJson(Map<String, dynamic> json) {
  return PregnancyDiagnosis()
    ..pdId = json['MPdId'] as String
    ..spdId = json['PdId'] as int
    ..animalId = json['AnimalId'] as int
    ..farmerId = json['FarmerId'] as int
    ..speciesId = json['SpeciesId'] as int
    ..breedId = json['BreedId'] as int
    ..pdDate = json['PdDate'] as String
    ..pdResult = json['PdResult'] as bool
    ..expectedDeliveryDate = json['ExpectedDeliveryDate'] as String
    ..remarks = json['Remarks'] as String
    ..createdDate = json['CreatedDate'] as String
    ..updatedDate = json['UpdatedDate'] as String
    ..mobileNumber = json['MobileNo'] as String
    ..speciesCode = json['SpeciesCode'] as String
    ..createdBy = json['CreatedBy'] as int
    ..updatedBy = json['UpdatedBy'] as int
    ..mAnimalId = json['MAnimalId'] as String
    ..mFarmerId = json['MFarmerId'] as String
    ..userId = json['UserId'] as int
    ..farmerName = json['FarmerName'] as String
    ..englishPdDate = json['EnglishPdDate'] as String
    ..englishExpectedDate = json['EnglishExpectedDeliveryDate'] as String
    ..syncDate = json['SyncDate'] as String
    ..syncToServer = json['SyncToServer'] as bool;
}

Map<String, dynamic> _$PregnancyDiagnosisToJson(PregnancyDiagnosis instance) =>
    <String, dynamic>{
      'MPdId': instance.pdId,
      'PdId': instance.spdId,
      'AnimalId': instance.animalId,
      'FarmerId': instance.farmerId,
      'SpeciesId': instance.speciesId,
      'BreedId': instance.breedId,
      'PdDate': instance.pdDate,
      'PdResult': instance.pdResult,
      'ExpectedDeliveryDate': instance.expectedDeliveryDate,
      'Remarks': instance.remarks,
      'CreatedDate': instance.createdDate,
      'UpdatedDate': instance.updatedDate,
      'MobileNo': instance.mobileNumber,
      'SpeciesCode': instance.speciesCode,
      'CreatedBy': instance.createdBy,
      'UpdatedBy': instance.updatedBy,
      'MAnimalId': instance.mAnimalId,
      'MFarmerId': instance.mFarmerId,
      'UserId': instance.userId,
      'FarmerName': instance.farmerName,
      'EnglishPdDate': instance.englishPdDate,
      'EnglishExpectedDeliveryDate': instance.englishExpectedDate,
      'SyncDate': instance.syncDate,
      'SyncToServer': instance.syncToServer
    };
