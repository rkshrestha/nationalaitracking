import 'package:flutter/material.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nepali_utils/nepali_utils.dart';

class CalvingDao implements BaseDao<Calving> {
  static CalvingDao _instance;
  CalvingBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  CalvingDao._(this.bean);

  static CalvingDao get(CalvingBean bean) {
    if (_instance == null) {
      _instance = CalvingDao._(bean);
    }
    return _instance;
  }




  getCalvingByAnimal(Animal animal) {
    Find findCalving = Find(bean.tableName);
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager
        .findOne()
        .user;

    findCalving.where(bean.mobileNumber.eq(animal.mobileNumber).and(bean.speciesCode.eq(animal.speciesCode)).and(bean.userId.eq(user.userid)));

    return bean.findMany(findCalving);
  }

  getUnSyncCalvingByAnimal(String aId) {
    Find findCalving = Find(bean.tableName);

    findCalving.where(bean.mAnimalId.eq(aId).and(bean.syncToServer.eq(false)));

    return bean.findMany(findCalving);
  }

  getSyncCalvingByAnimal(String aId) {
    Find findCalving = Find(bean.tableName);

    findCalving.where(bean.mAnimalId.eq(aId).and(bean.syncToServer.eq(true)));

    return bean.findMany(findCalving);
  }

  findByCalvingId(int calvingId) async {
    Find findByCalvingId = Find(bean.tableName);

    findByCalvingId.where(bean.calvingId.eq(calvingId));

    return bean.findOne(findByCalvingId);
  }

  findByAnimalIdCalf(String uid) async {
//  findByAnimalIdCalf(Calving mCalving) async {
//    Logger.v("acSex"+mCalving.calfSex);
    Find findByCalvingId = Find(bean.tableName);

    findByCalvingId.where(bean.cId.eq(uid));

    return bean.findOne(findByCalvingId);
  }


  @override
  Future<void> delete(Calving calving) {
    Logger.v('calvingId:${calving.cId}');
    return bean.remove(calving.cId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<Calving>> findAll() {

    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findCalving = Find(bean.tableName);
    findCalving.where(bean.userId.eq(user.userid));
    return bean.findMany(findCalving);
  }

  @override
  Future<Calving> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<Calving> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(Calving calving) async {
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table calving");
    return await bean.insert(calving);
  }

  @override
  Future<int> update(Calving calving) {
    return bean.update(calving);
  }

  Future<List<Calving>> findAllBySyncStatus() {
    Find findCalving = Find(bean.tableName);
    findCalving.where(bean.syncToServer.eq(false));
    return bean.findMany(findCalving);
  }

  Future<int> updateByDateAndAnimalId(Calving calving) async {
    var formattedDate = calving.calvingDate.split("T")[0];
    DateTime todayDate = DateTime.now();
    final Update update = bean.updater
        .where(bean.calvingDate.eq(formattedDate))
        .and(bean.mAnimalId.eq(calving.mAnimalId));

        update.set(bean.calvingId, calving.calvingId);
        update.set(bean.syncDate, calving.syncDate);
        update.set(bean.calfAnimalId, calving.calfAnimalId);

    return await bean.adapter.update(update);
  }

  Future<int> updateByDateAndAnimalIdSync(Calving syncedCalving) async {
    var formattedDate = syncedCalving.calvingDate.split("T")[0];
    final Update update = bean.updater
        .where(bean.calvingDate.eq(formattedDate))
        .and(bean.mAnimalId.eq(syncedCalving.mAnimalId))
        .set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  Future<int> updateByServerAIId(Calving calving) async {
    final Update update = bean.updater.where(bean.calvingDate
        .eq(calving.calvingDate)
        .and(bean.mAnimalId.eq(calving.mAnimalId)));
    var formattedDate = calving.calvingDate==null?"":calving.calvingDate.split("T")[0];
    var formattedCreatedDate = calving.createdDate==null?"":calving.createdDate.split("T")[0];
    var formattedUpdatedDate = calving.updatedDate==null?"":calving.updatedDate.split("T")[0];
    var formattedEnglishDate = calving.englishCalvingDate==null?"":calving.englishCalvingDate.split("T")[0];
    update.set(bean.calvingId, calving.calvingId);
    update.set(bean.cId, calving.cId);
    update.set(bean.farmerId, calving.farmerId);
    update.set(bean.animalId, calving.animalId);
    update.set(bean.speciesId, calving.speciesId);
    update.set(bean.breedId, calving.breedId);
    update.set(bean.calvingDate, formattedDate);
    update.set(bean.calfCode, calving.calfCode);
    update.set(bean.calfSex, calving.calfSex);
    update.set(bean.vetAssist, calving.vetAssist);
    update.set(bean.remarks, calving.remarks);
    update.set(bean.createdDate, formattedCreatedDate);
    update.set(bean.updatedDate, formattedUpdatedDate);
    update.set(bean.createdBy, calving.createdBy);
    update.set(bean.updatedBy, calving.updatedBy);
    update.set(bean.mobileNumber, calving.mobileNumber);
    update.set(bean.speciesCode, calving.speciesCode);
    update.set(bean.userId, calving.userId);
    update.set(bean.farmerName, calving.farmerName);
    update.set(bean.englishCalvingDate, formattedEnglishDate);
    update.set(bean.syncDate, calving.syncDate);
    update.set(bean.speciesName, calving.speciesName);
    update.set(bean.calfAnimalId, calving.calfAnimalId);
    update.set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  findBySameDateAndAnimal(Calving calving, Animal animal) async {
//    var formattedDate = calving.calvingDate.split("T")[0];
    NepaliDateTime nepaliCalvingDate =
    NepaliDateTime.tryParse(calving.calvingDate);
    DateTime englishDate = DateConverter.toAD(nepaliCalvingDate);
    Logger.v('after nepali date2');
    String formattedEnglishDate =
        englishDate.toIso8601String().split("T").first;
    Find findByDateAndAnimal = Find(bean.tableName);
    findByDateAndAnimal.where(
        bean.englishCalvingDate.eq(formattedEnglishDate).and(bean.speciesCode.eq(animal.speciesCode)).and(bean.mobileNumber.eq(calving.mobileNumber)));

    return bean.findOne(findByDateAndAnimal);
  }

  Future<List<Calving>> findAllSyncData() {
    Find findCalving = Find(bean.tableName);
    findCalving.where(bean.syncToServer.eq(true));
    return bean.findMany(findCalving);
  }

  Future<List<Calving>> findAllUnSyncData() {
    Find findCalving = Find(bean.tableName);
    findCalving.where(bean.syncToServer.eq(false));
    return bean.findMany(findCalving);
  }


  //update farmer server id
  Future<int> updateFarmerIdInCalving(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mobileNumber.eq(farmer.mobileNumber))
        .set(bean.farmerId, farmer.farmerId);

    return await bean.adapter.update(update);
  }

  //update animal server id
  Future<int> updateAnimalIdInCalving(Animal animal) async {
    final Update update = bean.updater
        .where(bean.mAnimalId.eq(animal.aId));
    update.set(bean.animalId, animal.animalId);
//    update.set(bean.calfAnimalId, animal.animalId);

    return await bean.adapter.update(update);
  }



  //update farmer mobile number on edit case
  Future<int> updateFarmerMblNumber(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mFarmerId.eq(farmer.fId));

        update.set(bean.mobileNumber, farmer.mobileNumber);
        update.set(bean.farmerName, farmer.farmerName);
        update.set(bean.syncToServer, false);
//        update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }


  updateMAnimalId(Animal animal)async{
    final Update update = bean.updater
        .where(bean.speciesCode.eq(animal.speciesCode));
    update.set(bean.mAnimalId, animal.aId);


    return await bean.adapter.update(update);
  }


  //update animal species code on edit case
  Future<int> updateAnimalSpeciesCode(Animal animal) async {
    Logger.v("scode"+animal.speciesCode);
    Logger.v("scode"+animal.aId.toString());
    Logger.v("scode"+animal.animalId.toString());
    final Update update = bean.updater
        .where(bean.mAnimalId.eq(animal.aId));
        update.set(bean.speciesCode, animal.speciesCode);
    update.set(bean.speciesId,animal.speciesId);
    update.set(bean.breedId,animal.breedId);
//    update.set(bean.calfCode, animal.speciesCode);
        update.set(bean.syncToServer, false);
//        update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }


  Future<int> updateAnimalToCalving(Animal animal) async {
    Logger.v("scode"+animal.speciesCode);
    Logger.v("scode"+animal.aId.toString());
    Logger.v("scode"+animal.animalId.toString());
    final Update update = bean.updater
        .where(bean.cId.eq(animal.aId));
    update.set(bean.calfCode, animal.speciesCode);
    update.set(bean.speciesId,animal.speciesId);
    update.set(bean.breedId,animal.breedId);
//    update.set(bean.speciesCode, animal.speciesCode);
    update.set(bean.syncToServer, false);
//    update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }



}
