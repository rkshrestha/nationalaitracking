// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Animal.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _AnimalBean implements Bean<Animal> {
  final aId = StrField('a_id');
  final animalId = IntField('animal_id');
  final mfarmerId = StrField('mfarmer_id');
  final speciesId = IntField('species_id');
  final speciesName = StrField('species_name');
  final breedId = IntField('breed_id');
  final breedName = StrField('breed_name');
  final speciesCode = StrField('species_code');
  final mobileNumber = StrField('mobile_number');
  final createdDate = StrField('created_date');
  final updatedDate = StrField('updated_date');
  final createdBy = IntField('created_by');
  final updatedBy = IntField('updated_by');
  final fId = IntField('f_id');
  final userId = IntField('user_id');
  final farmerName = StrField('farmer_name');
  final gender = StrField('gender');
  final status = BoolField('status');
  final syncDate = StrField('sync_date');
  final syncToServer = BoolField('sync_to_server');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        aId.name: aId,
        animalId.name: animalId,
        mfarmerId.name: mfarmerId,
        speciesId.name: speciesId,
        speciesName.name: speciesName,
        breedId.name: breedId,
        breedName.name: breedName,
        speciesCode.name: speciesCode,
        mobileNumber.name: mobileNumber,
        createdDate.name: createdDate,
        updatedDate.name: updatedDate,
        createdBy.name: createdBy,
        updatedBy.name: updatedBy,
        fId.name: fId,
        userId.name: userId,
        farmerName.name: farmerName,
        gender.name: gender,
        status.name: status,
        syncDate.name: syncDate,
        syncToServer.name: syncToServer,
      };
  Animal fromMap(Map map) {
    Animal model = Animal();
    model.aId = adapter.parseValue(map['a_id']);
    model.animalId = adapter.parseValue(map['animal_id']);
    model.mfarmerId = adapter.parseValue(map['mfarmer_id']);
    model.speciesId = adapter.parseValue(map['species_id']);
    model.speciesName = adapter.parseValue(map['species_name']);
    model.breedId = adapter.parseValue(map['breed_id']);
    model.breedName = adapter.parseValue(map['breed_name']);
    model.speciesCode = adapter.parseValue(map['species_code']);
    model.mobileNumber = adapter.parseValue(map['mobile_number']);
    model.createdDate = adapter.parseValue(map['created_date']);
    model.updatedDate = adapter.parseValue(map['updated_date']);
    model.createdBy = adapter.parseValue(map['created_by']);
    model.updatedBy = adapter.parseValue(map['updated_by']);
    model.fId = adapter.parseValue(map['f_id']);
    model.userId = adapter.parseValue(map['user_id']);
    model.farmerName = adapter.parseValue(map['farmer_name']);
    model.gender = adapter.parseValue(map['gender']);
    model.status = adapter.parseValue(map['status']);
    model.syncDate = adapter.parseValue(map['sync_date']);
    model.syncToServer = adapter.parseValue(map['sync_to_server']);

    return model;
  }

  List<SetColumn> toSetColumns(Animal model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(aId.set(model.aId));
      ret.add(animalId.set(model.animalId));
      ret.add(mfarmerId.set(model.mfarmerId));
      ret.add(speciesId.set(model.speciesId));
      ret.add(speciesName.set(model.speciesName));
      ret.add(breedId.set(model.breedId));
      ret.add(breedName.set(model.breedName));
      ret.add(speciesCode.set(model.speciesCode));
      ret.add(mobileNumber.set(model.mobileNumber));
      ret.add(createdDate.set(model.createdDate));
      ret.add(updatedDate.set(model.updatedDate));
      ret.add(createdBy.set(model.createdBy));
      ret.add(updatedBy.set(model.updatedBy));
      ret.add(fId.set(model.fId));
      ret.add(userId.set(model.userId));
      ret.add(farmerName.set(model.farmerName));
      ret.add(gender.set(model.gender));
      ret.add(status.set(model.status));
      ret.add(syncDate.set(model.syncDate));
      ret.add(syncToServer.set(model.syncToServer));
    } else if (only != null) {
      if (only.contains(aId.name)) ret.add(aId.set(model.aId));
      if (only.contains(animalId.name)) ret.add(animalId.set(model.animalId));
      if (only.contains(mfarmerId.name))
        ret.add(mfarmerId.set(model.mfarmerId));
      if (only.contains(speciesId.name))
        ret.add(speciesId.set(model.speciesId));
      if (only.contains(speciesName.name))
        ret.add(speciesName.set(model.speciesName));
      if (only.contains(breedId.name)) ret.add(breedId.set(model.breedId));
      if (only.contains(breedName.name))
        ret.add(breedName.set(model.breedName));
      if (only.contains(speciesCode.name))
        ret.add(speciesCode.set(model.speciesCode));
      if (only.contains(mobileNumber.name))
        ret.add(mobileNumber.set(model.mobileNumber));
      if (only.contains(createdDate.name))
        ret.add(createdDate.set(model.createdDate));
      if (only.contains(updatedDate.name))
        ret.add(updatedDate.set(model.updatedDate));
      if (only.contains(createdBy.name))
        ret.add(createdBy.set(model.createdBy));
      if (only.contains(updatedBy.name))
        ret.add(updatedBy.set(model.updatedBy));
      if (only.contains(fId.name)) ret.add(fId.set(model.fId));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(farmerName.name))
        ret.add(farmerName.set(model.farmerName));
      if (only.contains(gender.name)) ret.add(gender.set(model.gender));
      if (only.contains(status.name)) ret.add(status.set(model.status));
      if (only.contains(syncDate.name)) ret.add(syncDate.set(model.syncDate));
      if (only.contains(syncToServer.name))
        ret.add(syncToServer.set(model.syncToServer));
    } else /* if (onlyNonNull) */ {
      if (model.aId != null) {
        ret.add(aId.set(model.aId));
      }
      if (model.animalId != null) {
        ret.add(animalId.set(model.animalId));
      }
      if (model.mfarmerId != null) {
        ret.add(mfarmerId.set(model.mfarmerId));
      }
      if (model.speciesId != null) {
        ret.add(speciesId.set(model.speciesId));
      }
      if (model.speciesName != null) {
        ret.add(speciesName.set(model.speciesName));
      }
      if (model.breedId != null) {
        ret.add(breedId.set(model.breedId));
      }
      if (model.breedName != null) {
        ret.add(breedName.set(model.breedName));
      }
      if (model.speciesCode != null) {
        ret.add(speciesCode.set(model.speciesCode));
      }
      if (model.mobileNumber != null) {
        ret.add(mobileNumber.set(model.mobileNumber));
      }
      if (model.createdDate != null) {
        ret.add(createdDate.set(model.createdDate));
      }
      if (model.updatedDate != null) {
        ret.add(updatedDate.set(model.updatedDate));
      }
      if (model.createdBy != null) {
        ret.add(createdBy.set(model.createdBy));
      }
      if (model.updatedBy != null) {
        ret.add(updatedBy.set(model.updatedBy));
      }
      if (model.fId != null) {
        ret.add(fId.set(model.fId));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.farmerName != null) {
        ret.add(farmerName.set(model.farmerName));
      }
      if (model.gender != null) {
        ret.add(gender.set(model.gender));
      }
      if (model.status != null) {
        ret.add(status.set(model.status));
      }
      if (model.syncDate != null) {
        ret.add(syncDate.set(model.syncDate));
      }
      if (model.syncToServer != null) {
        ret.add(syncToServer.set(model.syncToServer));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addStr(aId.name, primary: true, isNullable: false);
    st.addInt(animalId.name, isNullable: true);
    st.addStr(mfarmerId.name, isNullable: true);
    st.addInt(speciesId.name, isNullable: true);
    st.addStr(speciesName.name, isNullable: true);
    st.addInt(breedId.name, isNullable: true);
    st.addStr(breedName.name, isNullable: true);
    st.addStr(speciesCode.name, isNullable: true);
    st.addStr(mobileNumber.name, isNullable: true);
    st.addStr(createdDate.name, isNullable: true);
    st.addStr(updatedDate.name, isNullable: true);
    st.addInt(createdBy.name, isNullable: true);
    st.addInt(updatedBy.name, isNullable: true);
    st.addInt(fId.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addStr(farmerName.name, isNullable: true);
    st.addStr(gender.name, isNullable: true);
    st.addBool(status.name, isNullable: true);
    st.addStr(syncDate.name, isNullable: true);
    st.addBool(syncToServer.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Animal model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<Animal> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Animal model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<Animal> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Animal model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.aId.eq(model.aId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Animal> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.aId.eq(model.aId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Animal> find(String aId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.aId.eq(aId));
    return await findOne(find);
  }

  Future<int> remove(String aId) async {
    final Remove remove = remover.where(this.aId.eq(aId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Animal> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.aId.eq(model.aId));
    }
    return adapter.remove(remove);
  }
}
