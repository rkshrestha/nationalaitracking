// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NotificationRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationRequest _$NotificationRequestFromJson(Map<String, dynamic> json) {
  return NotificationRequest()
    ..nId = json['MnId'] as int
    ..title = json['Title'] as String
    ..body = json['Body'] as String
    ..notificationDate = json['Date'] as String
    ..userId = json['UserId'] as int;
}

Map<String, dynamic> _$NotificationRequestToJson(
        NotificationRequest instance) =>
    <String, dynamic>{
      'MnId': instance.nId,
      'Title': instance.title,
      'Body': instance.body,
      'Date': instance.notificationDate,
      'UserId': instance.userId
    };
