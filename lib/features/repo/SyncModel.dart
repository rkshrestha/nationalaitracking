import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Semen.dart';

import 'JsonCallable.dart';

part 'SyncModel.g.dart';

@JsonSerializable()
class SyncModel  implements JsonCallable<SyncModel> {
	@JsonKey(name: 'Farmers')
	List<Farmer> farmers;
	@JsonKey(name: 'Animals')
	List<Animal> animals;
	@JsonKey(name: 'ArtificialInseminations')
	List<ArtificialInsemination> artificialInseminations;
	@JsonKey(name: 'PregnancyDiagnoses')
	List<PregnancyDiagnosis> pregnancyDiagnoses;
	@JsonKey(name: 'Calvings')
	List<Calving> calvings;
	@JsonKey(name: 'ExitRecords')
	List<ExitRecord> exitRecords;
	@JsonKey(name: 'LiquidNitrogens')
	List<LiquidNitrogen> liquidNitrogens;
	@JsonKey(name: 'Semen')
	List<Semen> semen;
	@JsonKey(name:"ErrorMessage")
	String errorMsg;


	SyncModel();
	SyncModel.make(
			{this.farmers,
				this.animals,
				this.artificialInseminations,
				this.pregnancyDiagnoses,
				this.calvings,
				this.exitRecords,
				this.liquidNitrogens,
				this.semen,this.errorMsg});

	factory SyncModel.fromJson(Map<String, dynamic> json) => _$SyncModelFromJson(json);

	Map<String, dynamic> toJson() => _$SyncModelToJson(this);

	@override
	SyncModel fromJson(Map<String, dynamic> json) {
		return SyncModel.fromJson(json);
	}

	@override
	String toJsonString() {
		return jsonEncode(toJson());
	}


//	SyncModel.fromJson(Map<String, dynamic> json) {
//		if (json['Farmers'] != null) {
//			farmers = new List<Null>();
//			json['Farmers'].forEach((v) {
//				farmers.add(new Farmer.fromJson(v));
//			});
//		}
//		if (json['Animals'] != null) {
//			animals = new List<Null>();
//			json['Animals'].forEach((v) {
//				animals.add(new Animal.fromJson(v));
//			});
//		}
//		if (json['ArtificialInseminations'] != null) {
//			artificialInseminations = new List<Null>();
//			json['ArtificialInseminations'].forEach((v) {
//				artificialInseminations.add(new ArtificialInsemination.fromJson(v));
//			});
//		}
//		if (json['PregnancyDiagnoses'] != null) {
//			pregnancyDiagnoses = new List<Null>();
//			json['PregnancyDiagnoses'].forEach((v) {
//				pregnancyDiagnoses.add(new PregnancyDiagnosis.fromJson(v));
//			});
//		}
//		if (json['Calvings'] != null) {
//			calvings = new List<Null>();
//			json['Calvings'].forEach((v) {
//				calvings.add(new Calving.fromJson(v));
//			});
//		}
//		if (json['ExitRecords'] != null) {
//			exitRecords = new List<Null>();
//			json['ExitRecords'].forEach((v) {
//				exitRecords.add(new ExitRecord.fromJson(v));
//			});
//		}
//		if (json['LiquidNitrogens'] != null) {
//			liquidNitrogens = new List<Null>();
//			json['LiquidNitrogens'].forEach((v) {
//				liquidNitrogens.add(new LiquidNitrogen.fromJson(v));
//			});
//		}
//		if (json['Semen'] != null) {
//			semen = new List<Null>();
//			json['Semen'].forEach((v) {
//				semen.add(new Semen.fromJson(v));
//			});
//		}
//	}
//
//	Map<String, dynamic> toJson() {
//		final Map<String, dynamic> data = new Map<String, dynamic>();
//		if (this.farmers != null) {
//			data['Farmers'] = this.farmers.map((v) => v.toJson()).toList();
//		}
//		if (this.animals != null) {
//			data['Animals'] = this.animals.map((v) => v.toJson()).toList();
//		}
//		if (this.artificialInseminations != null) {
//			data['ArtificialInseminations'] =
//					this.artificialInseminations.map((v) => v.toJson()).toList();
//		}
//		if (this.pregnancyDiagnoses != null) {
//			data['PregnancyDiagnoses'] =
//					this.pregnancyDiagnoses.map((v) => v.toJson()).toList();
//		}
//		if (this.calvings != null) {
//			data['Calvings'] = this.calvings.map((v) => v.toJson()).toList();
//		}
//		if (this.exitRecords != null) {
//			data['ExitRecords'] = this.exitRecords.map((v) => v.toJson()).toList();
//		}
//		if (this.liquidNitrogens != null) {
//			data['LiquidNitrogens'] =
//					this.liquidNitrogens.map((v) => v.toJson()).toList();
//		}
//		if (this.semen != null) {
//			data['Semen'] = this.semen.map((v) => v.toJson()).toList();
//		}
//		return data;
//	}
//
//  @override
//  SyncModel fromJson(Map<String, > json) {
//    // TODO: implement fromJson
//    return null;
//  }
//
//  @override
//  String toJsonString() {
//    // TODO: implement toJsonString
//    return null;
//  }
}

//
//@JsonSerializable()
//class SyncModel {
////	@JsonKey(name: 'Farmers')
////	List<Farmer> farmers;
////	@JsonKey(name: 'Animals')
////	List<Animal> animals;
////
////	SyncModel.make({this.farmers, this.animals});
////	SyncModel();
////
////	SyncModel.fromJson(Map<String, dynamic> json) {
////		if (json['Farmers'] != null) {
////			farmers = new List<Farmer>();
////			json['Farmers'].forEach((v) {
////				farmers.add(new Farmer.fromJson(v));
////			});
////		}
////		if (json['Animals'] != null) {
////			animals = new List<Animal>();
////			json['Animals'].forEach((v) {
////				animals.add(new Animal.fromJson(v));
////			});
////		}
////	}
////
////	Map<String, dynamic> toJson() {
////		final Map<String, dynamic> data = new Map<String, dynamic>();
////		if (this.farmers != null) {
////			data['Farmers'] = this.farmers.map((v) => v.toJson()).toList();
////		}
////		if (this.animals != null) {
////			data['Animals'] = this.animals.map((v) => v.toJson()).toList();
////		}
////		return data;
////	}
//
//
////
//	@JsonKey(name: 'Farmers')
//	List<PregnancyDiagnosis> pregnancyDiagnoses;
//	@JsonKey(name: 'Farmers')
//	List<LiquidNitrogen> liquidNitrogens;
//	@JsonKey(name: 'Farmers')
//	List<Farmer> farmers;
//	@JsonKey(name: 'Farmers')
//	List<Animal> animals;
//	@JsonKey(name: 'Farmers')
//	List<Semen> semen;
//	@JsonKey(name: 'Farmers')
//	List<ArtificialInsemination> artificialInseminations;
//	@JsonKey(name: 'Farmers')
//	List<ExitRecord> exitRecords;
//	@JsonKey(name: 'Farmers')
//	List<Calving> calvings;
//
//	SyncModel();
//
//	SyncModel.make({this.pregnancyDiagnoses, this.liquidNitrogens, this.farmers, this.animals, this.semen, this.artificialInseminations, this.exitRecords, this.calvings});
//
//	SyncModel.fromJson(Map<String, dynamic> json) {
//		if (json['PregnancyDiagnoses'] != null) {
//			pregnancyDiagnoses = new List<PregnancyDiagnosis>();
//		}
//		if (json['LiquidNitrogens'] != null) {
//			liquidNitrogens = new List<LiquidNitrogen>();
//		}
//		if (json['Farmers'] != null) {
//			farmers = new List<Farmer>();
//		}
//		if (json['Animals'] != null) {
//			animals = new List<Animal>();
//		}
//		if (json['Semen'] != null) {
//			semen = new List<Semen>();
//		}
//		if (json['ArtificialInseminations'] != null) {
//			artificialInseminations = new List<ArtificialInsemination>();
//		}
//		if (json['ExitRecords'] != null) {
//			exitRecords = new List<ExitRecord>();
//		}
//		if (json['Calvings'] != null) {
//			calvings = new List<Calving>();
//		}
//	}
//
//	Map<String, dynamic> toJson() {
//		final Map<String, dynamic> data = new Map<String, dynamic>();
//		if (this.pregnancyDiagnoses != null) {
//      data['PregnancyDiagnoses'] =  [];
//    }
//		if (this.liquidNitrogens != null) {
//      data['LiquidNitrogens'] =  [];
//    }
//		if (this.farmers != null) {
//      data['Farmers'] =  [];
//    }
//		if (this.animals != null) {
//      data['Animals'] =  [];
//    }
//		if (this.semen != null) {
//      data['Semen'] =  [];
//    }
//		if (this.artificialInseminations != null) {
//      data['ArtificialInseminations'] =  [];
//    }
//		if (this.exitRecords != null) {
//      data['ExitRecords'] =  [];
//    }
//		if (this.calvings != null) {
//      data['Calvings'] =  [];
//    }
//		return data;
//	}
//}
