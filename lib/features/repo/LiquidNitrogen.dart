import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'LiquidNitrogen.g.dart';

part 'LiquidNitrogen.jorm.dart';

@JsonSerializable()
class LiquidNitrogen implements JsonCallable<LiquidNitrogen> {
//  @PrimaryKey(auto: true)
//  @JsonKey(name: 'MLiquidNitrogenId')
//  int lnId;
  @PrimaryKey()
  @JsonKey(name: 'MLiquidNitrogenId')
  String lnId;
  @JsonKey(name: 'LiquidNitrogenId')
  @Column(isNullable: true)
  int slnId;
  @JsonKey(name: 'ReceivedDate')
  @Column(isNullable: true)
  String receivedDate;
  @JsonKey(name: 'Quantity')
  @Column(isNullable: true)
  double quantity;
  @JsonKey(name: 'UserId')
  @Column(isNullable: true)
  int userId;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedDate')
  String createdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedDate')
  String updatedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedBy')
  int createdBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedBy')
  int updatedBy;
  @JsonKey(name: 'EnglishReceivedDate')
  @Column(isNullable: true)
  String englishReceivedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'SyncDate')
  String syncDate;
  @JsonKey(name: "SyncToServer")
  bool syncToServer = false;

  LiquidNitrogen();


  LiquidNitrogen.make(this.lnId, this.slnId, this.receivedDate, this.quantity,
      this.userId, this.createdDate, this.updatedDate, this.createdBy,
      this.updatedBy,this.englishReceivedDate,this.syncDate, {this.syncToServer =false});

  factory LiquidNitrogen.fromJson(Map<String, dynamic> json) =>
      _$LiquidNitrogenFromJson(json);

  Map<String, dynamic> toJson() => _$LiquidNitrogenToJson(this);

  @override
  LiquidNitrogen fromJson(Map<String, dynamic> json) {
    return LiquidNitrogen.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class LiquidNitrogenBean extends Bean<LiquidNitrogen> with _LiquidNitrogenBean {
  LiquidNitrogenBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'liquid_nitrogens';
}
