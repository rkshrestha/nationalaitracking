import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'Calving.g.dart';

part 'Calving.jorm.dart';

@JsonSerializable()
class Calving implements JsonCallable<Calving> {
  @PrimaryKey()
  @JsonKey(name: 'MCalvingId')
  String cId;
//  @PrimaryKey(auto: true)
//  @JsonKey(name: 'MCalvingId')
//  int cId;
  @JsonKey(name: 'CalvingId')
  @Column(isNullable: true)
  int calvingId;
  @JsonKey(name: 'FarmerId')
  @Column(isNullable: true)
  int farmerId;
  @JsonKey(name: 'AnimalId')
  @Column(isNullable: true)
  int animalId;
  @JsonKey(name: 'SpeciesId')
  @Column(isNullable: true)
  int speciesId;
  @JsonKey(name: 'BreedId')
  @Column(isNullable: true)
  int breedId;
  @JsonKey(name: 'CalvingDate')
  @Column(isNullable: true)
  String calvingDate;
  @JsonKey(name: 'CalfSex')
  @Column(isNullable: true)
  String calfSex;
  @JsonKey(name: 'VetAssist')
  @Column(isNullable: true)
  bool vetAssist;
  @JsonKey(name: 'CalfId')
  @Column(isNullable: true)
  String calfCode;
  @JsonKey(name: 'Remarks')
  @Column(isNullable: true)
  String remarks;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedDate')
  String createdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedDate')
  String updatedDate;
  @JsonKey(name: 'MobileNo')
  String mobileNumber;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesCode')
  String speciesCode;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedBy')
  int createdBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedBy')
  int updatedBy;
  @Column(isNullable: true)
  @JsonKey(name: 'MFarmerId')
  String mFarmerId;
  @Column(isNullable: true)
  @JsonKey(name: 'MAnimalId')
  String mAnimalId;
  @Column(isNullable: true)
  @JsonKey(name: 'UserId')
  int userId;
  @Column(isNullable: true)
  @JsonKey(name: 'FarmerName')
  String farmerName;
  @Column(isNullable: true)
  @JsonKey(name: 'EnglishCalvingDate')
  String englishCalvingDate;
  @Column(isNullable: true)
  @JsonKey(name: 'SyncDate')
  String syncDate;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesName')
  String speciesName;
  @Column(isNullable: true)
  @JsonKey(name: 'CalfAnimalId')
  int calfAnimalId;
  @JsonKey(name: "SyncToServer")
  bool syncToServer = false;

  Calving();

  Calving.make(
      this.cId,
      this.calvingId,
      this.farmerId,
      this.animalId,
      this.speciesId,
      this.breedId,
      this.calvingDate,
      this.calfSex,
      this.vetAssist,
      this.calfCode,
      this.remarks,
      this.createdDate,
      this.updatedDate,
      this.mobileNumber,
      this.speciesCode,
      this.createdBy,
      this.updatedBy,
      this.mFarmerId,
      this.mAnimalId,
      this.userId,
      this.farmerName,
      this.englishCalvingDate,
      this.syncDate,
      this.speciesName,
      this.calfAnimalId,
      {this.syncToServer = false});

  factory Calving.fromJson(Map<String, dynamic> json) =>
      _$CalvingFromJson(json);

  Map<String, dynamic> toJson() => _$CalvingToJson(this);

  @override
  Calving fromJson(Map<String, dynamic> json) {
    return Calving.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class CalvingBean extends Bean<Calving> with _CalvingBean {
  CalvingBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'calvings';
}
