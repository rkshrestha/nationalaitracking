
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Species.dart';
import 'package:nbis/s/Logger.dart';

class SpeciesDao implements BaseDao<Species>{

  static SpeciesDao _instance;
  SpeciesBean bean;

  SpeciesDao._(this.bean);

  static SpeciesDao get(SpeciesBean bean) {
    if (_instance == null) {
      _instance = SpeciesDao._(bean);
    }
    return _instance;
  }



  @override
  Future<void> delete(Species entity) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<void> deleteAll() {
return bean.removeAll();
  }

  @override
  Future<List<Species>> findAll() {
    Find findSpecies = Find(bean.tableName);
    return bean.findMany(findSpecies);
  }

  @override
  Future<Species> findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Future<Species> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<void> persist(Species species) {
    Logger.v("inserted");
    return bean.insert(species);

  }

  @override
  Future<void> update(Species entity) {
    // TODO: implement update
    return null;
  }
}