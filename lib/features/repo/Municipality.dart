import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'Municipality.jorm.dart';
part 'Municipality.g.dart';

@JsonSerializable()
class Municipality implements JsonCallable<Municipality> {
  @PrimaryKey(auto: true)
  int mId;

  @JsonKey(name: 'MunicipalityId')
  int municipalityid;
  @JsonKey(name: 'MCode')
  int mcode;
  @JsonKey(name: 'DCode')
  int dcode;
  @JsonKey(name: 'SCode')
  int scode;
  @JsonKey(name: 'MunicipalityName')
  String name;
  @JsonKey(name: 'Type')
  String type;

  Municipality();

  Municipality.make(this.municipalityid, this.mcode, this.dcode, this.scode,
      this.name, this.type);

  factory Municipality.fromJson(Map<String, dynamic> json) =>
      _$MunicipalityFromJson(json);

  Map<String, dynamic> toJson() => _$MunicipalityToJson(this);

  @override
  Municipality fromJson(Map<String, dynamic> json) {
    return Municipality.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class MunicipalityBean extends Bean<Municipality> with _MunicipalityBean {
  MunicipalityBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'municipality';
}
