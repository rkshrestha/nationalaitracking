// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NotificationRequest.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _NotificationRequestBean implements Bean<NotificationRequest> {
  final nId = IntField('n_id');
  final title = StrField('title');
  final body = StrField('body');
  final notificationDate = StrField('notification_date');
  final userId = IntField('user_id');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        nId.name: nId,
        title.name: title,
        body.name: body,
        notificationDate.name: notificationDate,
        userId.name: userId,
      };
  NotificationRequest fromMap(Map map) {
    NotificationRequest model = NotificationRequest();
    model.nId = adapter.parseValue(map['n_id']);
    model.title = adapter.parseValue(map['title']);
    model.body = adapter.parseValue(map['body']);
    model.notificationDate = adapter.parseValue(map['notification_date']);
    model.userId = adapter.parseValue(map['user_id']);

    return model;
  }

  List<SetColumn> toSetColumns(NotificationRequest model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.nId != null) {
        ret.add(nId.set(model.nId));
      }
      ret.add(title.set(model.title));
      ret.add(body.set(model.body));
      ret.add(notificationDate.set(model.notificationDate));
      ret.add(userId.set(model.userId));
    } else if (only != null) {
      if (model.nId != null) {
        if (only.contains(nId.name)) ret.add(nId.set(model.nId));
      }
      if (only.contains(title.name)) ret.add(title.set(model.title));
      if (only.contains(body.name)) ret.add(body.set(model.body));
      if (only.contains(notificationDate.name))
        ret.add(notificationDate.set(model.notificationDate));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
    } else /* if (onlyNonNull) */ {
      if (model.nId != null) {
        ret.add(nId.set(model.nId));
      }
      if (model.title != null) {
        ret.add(title.set(model.title));
      }
      if (model.body != null) {
        ret.add(body.set(model.body));
      }
      if (model.notificationDate != null) {
        ret.add(notificationDate.set(model.notificationDate));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(nId.name, primary: true, autoIncrement: true, isNullable: false);
    st.addStr(title.name, isNullable: false);
    st.addStr(body.name, isNullable: false);
    st.addStr(notificationDate.name, isNullable: false);
    st.addInt(userId.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(NotificationRequest model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(nId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      NotificationRequest newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<NotificationRequest> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(NotificationRequest model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(nId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      NotificationRequest newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<NotificationRequest> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(NotificationRequest model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.nId.eq(model.nId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<NotificationRequest> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.nId.eq(model.nId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<NotificationRequest> find(int nId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.nId.eq(nId));
    return await findOne(find);
  }

  Future<int> remove(int nId) async {
    final Remove remove = remover.where(this.nId.eq(nId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<NotificationRequest> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.nId.eq(model.nId));
    }
    return adapter.remove(remove);
  }
}
