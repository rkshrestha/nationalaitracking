import 'package:flutter/material.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';

class ExitRecordDao implements BaseDao<ExitRecord> {
  static ExitRecordDao _instance;
  ExitRecordBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  ExitRecordDao._(this.bean);

  static ExitRecordDao get(ExitRecordBean bean) {
    if (_instance == null) {
      _instance = ExitRecordDao._(bean);
    }
    return _instance;
  }

  findByExitRecordId(int erId) async {
    Find findByExitId = Find(bean.tableName);

    findByExitId.where(bean.erId.eq(erId));

    return bean.findOne(findByExitId);
  }

  getExitRecordByAnimal(Animal animal) {
    Find findExit = Find(bean.tableName);

    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager
        .findOne()
        .user;
    findExit.where(bean.mobileNumber.eq(animal.mobileNumber).and(bean.speciesCode.eq(animal.speciesCode)).and(bean.userId.eq(user.userid)));

    return bean.findMany(findExit);
  }

  getUnSyncExitRecordByAnimal(String aId) {
    Find findExit = Find(bean.tableName);

    findExit.where(bean.mAnimalId.eq(aId).and(bean.syncToServer.eq(false)));

    return bean.findMany(findExit);
  }

  getSyncExitRecordByAnimal(String aId) {
    Find findExit = Find(bean.tableName);

    findExit.where(bean.mAnimalId.eq(aId).and(bean.syncToServer.eq(true)));

    return bean.findMany(findExit);
  }

  @override
  Future<void> delete(ExitRecord exit) {
    Logger.v('exitId:${exit.eId}');
    return bean.remove(exit.eId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<ExitRecord>> findAll() {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findExit = Find(bean.tableName);
    findExit.where(bean.userId.eq(user.userid));

    return bean.findMany(findExit);
  }

  @override
  Future<ExitRecord> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<ExitRecord> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(ExitRecord exit) async {
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table exit");
    return await bean.insert(exit);
  }

  @override
  Future<int> update(ExitRecord exit) {
    return bean.update(exit);
  }

  //
  Future<int> updateByDateAndAnimalId(ExitRecord exit) async {
    var formattedDate = exit.dateOfExit.split("T")[0];
    DateTime todayDate = DateTime.now();
    final Update update = bean.updater
        .where(bean.dateOfExit.eq(formattedDate))
        .and(bean.mAnimalId.eq(exit.mAnimalId));

       update.set(bean.erId, exit.erId);
       update.set(bean.syncDate, exit.syncDate);

    return await bean.adapter.update(update);
  }

  Future<int> updateByDateAndAnimalIdSync(ExitRecord syncedExit) async {
    var formattedDate = syncedExit.dateOfExit.split("T")[0];
    final Update update = bean.updater
        .where(bean.dateOfExit.eq(formattedDate))
        .and(bean.mAnimalId.eq(syncedExit.mAnimalId))
        .set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  Future<int> updateByServerAIId(ExitRecord exit) async {
    final Update update = bean.updater.where(bean.dateOfExit
        .eq(exit.dateOfExit)
        .and(bean.mAnimalId.eq(exit.mAnimalId)));
    var formattedDate = exit.dateOfExit==null?"":exit.dateOfExit.split("T")[0];
    var formattedCreatedDate = exit.createdDate==null?"":exit.createdDate.split("T")[0];
    var formattedUpdatedDate = exit.updatedDate==null?"":exit.updatedDate.split("T")[0];
    var formattedEnglishDate = exit.englishExitDate==null?"":exit.englishExitDate.split("T")[0];
    update.set(bean.eId, exit.eId);
    update.set(bean.erId, exit.erId);
    update.set(bean.farmerId, exit.farmerId);
    update.set(bean.animalId, exit.animalId);
    update.set(bean.speciesId, exit.speciesId);
    update.set(bean.breedId, exit.breedId);
    update.set(bean.dateOfExit, formattedDate);
    update.set(bean.exitReason, exit.exitReason);
    update.set(bean.mobileNumber, exit.mobileNumber);
    update.set(bean.speciesCode, exit.speciesCode);
    update.set(bean.createdDate, formattedCreatedDate);
    update.set(bean.updatedDate, formattedUpdatedDate);
    update.set(bean.createdBy, exit.createdBy);
    update.set(bean.updatedBy, exit.updatedBy);
    update.set(bean.userId, exit.userId);
    update.set(bean.farmerName, exit.farmerName);
    update.set(bean.syncDate, exit.syncDate);
    update.set(bean.englishExitDate, formattedEnglishDate);
    update.set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  Future<List<ExitRecord>> findAllBySyncExitStatus() {
    Find findExitRecord = Find(bean.tableName);
    findExitRecord.where(bean.syncToServer.eq(false));
    return bean.findMany(findExitRecord);
  }

  findBySameDateAndAnimal(ExitRecord exit, Animal animal) async {
    var formattedDate = exit.dateOfExit.split("T")[0];
    Find findByDateAndAnimal = Find(bean.tableName);
    findByDateAndAnimal.where(
        bean.dateOfExit.eq(formattedDate).and(bean.mAnimalId.eq(animal.aId)).and(bean.mobileNumber.eq(exit.mobileNumber)));

    return bean.findOne(findByDateAndAnimal);
  }

  Future<List<ExitRecord>> findAllSyncData() {
    Find findExitRecord = Find(bean.tableName);
    findExitRecord.where(bean.syncToServer.eq(true));
    return bean.findMany(findExitRecord);
  }

  Future<List<ExitRecord>> findAllUnSyncData() {
    Find findExitRecord = Find(bean.tableName);
    findExitRecord.where(bean.syncToServer.eq(false));
    return bean.findMany(findExitRecord);
  }


  //update farmer server id
  Future<int> updateFarmerIdInExit(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mobileNumber.eq(farmer.mobileNumber))
        .set(bean.farmerId, farmer.farmerId);

    return await bean.adapter.update(update);
  }

  //update animal server id
  Future<int> updateAnimalIdInExit(Animal animal) async {
    final Update update = bean.updater
        .where(bean.mAnimalId.eq(animal.aId))
        .set(bean.animalId, animal.animalId);

    return await bean.adapter.update(update);
  }



  //update farmer mobile number on edit case
  Future<int> updateFarmerMblNumber(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mFarmerId.eq(farmer.fId));
        update.set(bean.mobileNumber, farmer.mobileNumber);
        update.set(bean.farmerName, farmer.farmerName);
        update.set(bean.syncToServer, false);
//        update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }

  //update animal species code on edit case
  Future<int> updateAnimalSpeciesCode(Animal animal) async {
    final Update update = bean.updater
        .where(bean.mAnimalId.eq(animal.aId));
        update.set(bean.speciesCode, animal.speciesCode);
        update.set(bean.syncToServer, false);
//        update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }


}
