import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/LoginRequest.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/sharedPreferences/Session.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/features/sharedPreferences/User.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';

class UserRepo implements Repository<LoginRequest> {

  BuildContext context;
  UserRepo(this.context);

  void sendLoginRequest(LoginRequest loginRequest, StreamSink<SCResponse<User>> sSink) async {
    try {
      Logger.v("Test " + DsonUtils.toJsonString(loginRequest));

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();
//      NetworkApi networkApi = NetworkApi.create(
//          networkClient.client(newBaseUrl: "http://192.168.10.228:809/"));
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));

      SCResponse scResponse = await networkClient.call<User, User>(
          networkApi.sendLoginRequest(loginRequest),
          callable: User());

      Logger.v(scResponse.status.toString());
      sSink.add(scResponse);

    } catch (ex) {
      Logger.v(ex.toString());
    }
  }





  void sendForgotPasswordRequest(String technicianId, StreamSink<SCResponse<String>> sSink) async {
    try {

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));

      SCResponse scResponse = await networkClient.call<String, String>(
          networkApi.sendForgotPasswordRequest(technicianId),
          disableCallable: true);

      Logger.v(scResponse.status.toString());
      sSink.add(scResponse);

    } catch (ex) {
      Logger.v(ex.toString());
    }
  }




  Session getSession(){
    var sessionManager = SessionManager.getInstance(this.context);
    return  sessionManager.findOne();

    //session.user=it.data;
  }

  @override
  void delete(LoginRequest entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<LoginRequest> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  LoginRequest findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  LoginRequest findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(LoginRequest entity) {
    // TODO: implement persist
  }

  @override
  void update(LoginRequest entity) {
    // TODO: implement update
  }
}
