// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Semen.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Semen _$SemenFromJson(Map<String, dynamic> json) {
  return Semen()
    ..msId = json['MsId'] as String
    ..semenId = json['SId'] as int
    ..userId = json['UserId'] as int
    ..receivedDate = json['ReceivedDate'] as String
    ..createdDate = json['CreatedDate'] as String
    ..updatedDate = json['UpdatedDate'] as String
    ..createdBy = json['CreatedBy'] as int
    ..updatedBy = json['UpdatedBy'] as int
    ..jersey = (json['Jersey'] as num)?.toDouble()
    ..boar = (json['Boar'] as num)?.toDouble()
    ..saanen = (json['Saanen'] as num)?.toDouble()
    ..yorkshire = (json['Yorkshire'] as num)?.toDouble()
    ..landrace = (json['Landrace'] as num)?.toDouble()
    ..duroc = (json['Duroc'] as num)?.toDouble()
    ..hampshire = (json['Hampshire'] as num)?.toDouble()
    ..holsteinFriesian = (json['HolsteinFriesian'] as num)?.toDouble()
    ..murrah = (json['Murrah'] as num)?.toDouble()
    ..englishReceivedDate = json['EnglishReceivedDate'] as String
    ..syncDate = json['SyncDate'] as String
    ..syncToServer = json['SyncToServer'] as bool;
}

Map<String, dynamic> _$SemenToJson(Semen instance) => <String, dynamic>{
      'MsId': instance.msId,
      'SId': instance.semenId,
      'UserId': instance.userId,
      'ReceivedDate': instance.receivedDate,
      'CreatedDate': instance.createdDate,
      'UpdatedDate': instance.updatedDate,
      'CreatedBy': instance.createdBy,
      'UpdatedBy': instance.updatedBy,
      'Jersey': instance.jersey,
      'Boar': instance.boar,
      'Saanen': instance.saanen,
      'Yorkshire': instance.yorkshire,
      'Landrace': instance.landrace,
      'Duroc': instance.duroc,
      'Hampshire': instance.hampshire,
      'HolsteinFriesian': instance.holsteinFriesian,
      'Murrah': instance.murrah,
      'EnglishReceivedDate': instance.englishReceivedDate,
      'SyncDate': instance.syncDate,
      'SyncToServer': instance.syncToServer
    };
