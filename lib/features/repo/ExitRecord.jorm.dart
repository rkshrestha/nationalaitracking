// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ExitRecord.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _ExitRecordBean implements Bean<ExitRecord> {
  final eId = StrField('e_id');
  final erId = IntField('er_id');
  final farmerId = IntField('farmer_id');
  final animalId = IntField('animal_id');
  final speciesId = IntField('species_id');
  final breedId = IntField('breed_id');
  final dateOfExit = StrField('date_of_exit');
  final exitReason = StrField('exit_reason');
  final mobileNumber = StrField('mobile_number');
  final speciesCode = StrField('species_code');
  final createdDate = StrField('created_date');
  final updatedDate = StrField('updated_date');
  final createdBy = IntField('created_by');
  final updatedBy = IntField('updated_by');
  final mFarmerId = StrField('m_farmer_id');
  final mAnimalId = StrField('m_animal_id');
  final userId = IntField('user_id');
  final farmerName = StrField('farmer_name');
  final englishExitDate = StrField('english_exit_date');
  final syncDate = StrField('sync_date');
  final syncToServer = BoolField('sync_to_server');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        eId.name: eId,
        erId.name: erId,
        farmerId.name: farmerId,
        animalId.name: animalId,
        speciesId.name: speciesId,
        breedId.name: breedId,
        dateOfExit.name: dateOfExit,
        exitReason.name: exitReason,
        mobileNumber.name: mobileNumber,
        speciesCode.name: speciesCode,
        createdDate.name: createdDate,
        updatedDate.name: updatedDate,
        createdBy.name: createdBy,
        updatedBy.name: updatedBy,
        mFarmerId.name: mFarmerId,
        mAnimalId.name: mAnimalId,
        userId.name: userId,
        farmerName.name: farmerName,
        englishExitDate.name: englishExitDate,
        syncDate.name: syncDate,
        syncToServer.name: syncToServer,
      };
  ExitRecord fromMap(Map map) {
    ExitRecord model = ExitRecord();
    model.eId = adapter.parseValue(map['e_id']);
    model.erId = adapter.parseValue(map['er_id']);
    model.farmerId = adapter.parseValue(map['farmer_id']);
    model.animalId = adapter.parseValue(map['animal_id']);
    model.speciesId = adapter.parseValue(map['species_id']);
    model.breedId = adapter.parseValue(map['breed_id']);
    model.dateOfExit = adapter.parseValue(map['date_of_exit']);
    model.exitReason = adapter.parseValue(map['exit_reason']);
    model.mobileNumber = adapter.parseValue(map['mobile_number']);
    model.speciesCode = adapter.parseValue(map['species_code']);
    model.createdDate = adapter.parseValue(map['created_date']);
    model.updatedDate = adapter.parseValue(map['updated_date']);
    model.createdBy = adapter.parseValue(map['created_by']);
    model.updatedBy = adapter.parseValue(map['updated_by']);
    model.mFarmerId = adapter.parseValue(map['m_farmer_id']);
    model.mAnimalId = adapter.parseValue(map['m_animal_id']);
    model.userId = adapter.parseValue(map['user_id']);
    model.farmerName = adapter.parseValue(map['farmer_name']);
    model.englishExitDate = adapter.parseValue(map['english_exit_date']);
    model.syncDate = adapter.parseValue(map['sync_date']);
    model.syncToServer = adapter.parseValue(map['sync_to_server']);

    return model;
  }

  List<SetColumn> toSetColumns(ExitRecord model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(eId.set(model.eId));
      ret.add(erId.set(model.erId));
      ret.add(farmerId.set(model.farmerId));
      ret.add(animalId.set(model.animalId));
      ret.add(speciesId.set(model.speciesId));
      ret.add(breedId.set(model.breedId));
      ret.add(dateOfExit.set(model.dateOfExit));
      ret.add(exitReason.set(model.exitReason));
      ret.add(mobileNumber.set(model.mobileNumber));
      ret.add(speciesCode.set(model.speciesCode));
      ret.add(createdDate.set(model.createdDate));
      ret.add(updatedDate.set(model.updatedDate));
      ret.add(createdBy.set(model.createdBy));
      ret.add(updatedBy.set(model.updatedBy));
      ret.add(mFarmerId.set(model.mFarmerId));
      ret.add(mAnimalId.set(model.mAnimalId));
      ret.add(userId.set(model.userId));
      ret.add(farmerName.set(model.farmerName));
      ret.add(englishExitDate.set(model.englishExitDate));
      ret.add(syncDate.set(model.syncDate));
      ret.add(syncToServer.set(model.syncToServer));
    } else if (only != null) {
      if (only.contains(eId.name)) ret.add(eId.set(model.eId));
      if (only.contains(erId.name)) ret.add(erId.set(model.erId));
      if (only.contains(farmerId.name)) ret.add(farmerId.set(model.farmerId));
      if (only.contains(animalId.name)) ret.add(animalId.set(model.animalId));
      if (only.contains(speciesId.name))
        ret.add(speciesId.set(model.speciesId));
      if (only.contains(breedId.name)) ret.add(breedId.set(model.breedId));
      if (only.contains(dateOfExit.name))
        ret.add(dateOfExit.set(model.dateOfExit));
      if (only.contains(exitReason.name))
        ret.add(exitReason.set(model.exitReason));
      if (only.contains(mobileNumber.name))
        ret.add(mobileNumber.set(model.mobileNumber));
      if (only.contains(speciesCode.name))
        ret.add(speciesCode.set(model.speciesCode));
      if (only.contains(createdDate.name))
        ret.add(createdDate.set(model.createdDate));
      if (only.contains(updatedDate.name))
        ret.add(updatedDate.set(model.updatedDate));
      if (only.contains(createdBy.name))
        ret.add(createdBy.set(model.createdBy));
      if (only.contains(updatedBy.name))
        ret.add(updatedBy.set(model.updatedBy));
      if (only.contains(mFarmerId.name))
        ret.add(mFarmerId.set(model.mFarmerId));
      if (only.contains(mAnimalId.name))
        ret.add(mAnimalId.set(model.mAnimalId));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(farmerName.name))
        ret.add(farmerName.set(model.farmerName));
      if (only.contains(englishExitDate.name))
        ret.add(englishExitDate.set(model.englishExitDate));
      if (only.contains(syncDate.name)) ret.add(syncDate.set(model.syncDate));
      if (only.contains(syncToServer.name))
        ret.add(syncToServer.set(model.syncToServer));
    } else /* if (onlyNonNull) */ {
      if (model.eId != null) {
        ret.add(eId.set(model.eId));
      }
      if (model.erId != null) {
        ret.add(erId.set(model.erId));
      }
      if (model.farmerId != null) {
        ret.add(farmerId.set(model.farmerId));
      }
      if (model.animalId != null) {
        ret.add(animalId.set(model.animalId));
      }
      if (model.speciesId != null) {
        ret.add(speciesId.set(model.speciesId));
      }
      if (model.breedId != null) {
        ret.add(breedId.set(model.breedId));
      }
      if (model.dateOfExit != null) {
        ret.add(dateOfExit.set(model.dateOfExit));
      }
      if (model.exitReason != null) {
        ret.add(exitReason.set(model.exitReason));
      }
      if (model.mobileNumber != null) {
        ret.add(mobileNumber.set(model.mobileNumber));
      }
      if (model.speciesCode != null) {
        ret.add(speciesCode.set(model.speciesCode));
      }
      if (model.createdDate != null) {
        ret.add(createdDate.set(model.createdDate));
      }
      if (model.updatedDate != null) {
        ret.add(updatedDate.set(model.updatedDate));
      }
      if (model.createdBy != null) {
        ret.add(createdBy.set(model.createdBy));
      }
      if (model.updatedBy != null) {
        ret.add(updatedBy.set(model.updatedBy));
      }
      if (model.mFarmerId != null) {
        ret.add(mFarmerId.set(model.mFarmerId));
      }
      if (model.mAnimalId != null) {
        ret.add(mAnimalId.set(model.mAnimalId));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.farmerName != null) {
        ret.add(farmerName.set(model.farmerName));
      }
      if (model.englishExitDate != null) {
        ret.add(englishExitDate.set(model.englishExitDate));
      }
      if (model.syncDate != null) {
        ret.add(syncDate.set(model.syncDate));
      }
      if (model.syncToServer != null) {
        ret.add(syncToServer.set(model.syncToServer));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addStr(eId.name, primary: true, isNullable: false);
    st.addInt(erId.name, isNullable: true);
    st.addInt(farmerId.name, isNullable: true);
    st.addInt(animalId.name, isNullable: true);
    st.addInt(speciesId.name, isNullable: true);
    st.addInt(breedId.name, isNullable: true);
    st.addStr(dateOfExit.name, isNullable: true);
    st.addStr(exitReason.name, isNullable: true);
    st.addStr(mobileNumber.name, isNullable: false);
    st.addStr(speciesCode.name, isNullable: true);
    st.addStr(createdDate.name, isNullable: true);
    st.addStr(updatedDate.name, isNullable: true);
    st.addInt(createdBy.name, isNullable: true);
    st.addInt(updatedBy.name, isNullable: true);
    st.addStr(mFarmerId.name, isNullable: true);
    st.addStr(mAnimalId.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addStr(farmerName.name, isNullable: true);
    st.addStr(englishExitDate.name, isNullable: true);
    st.addStr(syncDate.name, isNullable: true);
    st.addBool(syncToServer.name, isNullable: true);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(ExitRecord model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<ExitRecord> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(ExitRecord model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<ExitRecord> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(ExitRecord model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.eId.eq(model.eId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<ExitRecord> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.eId.eq(model.eId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<ExitRecord> find(String eId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.eId.eq(eId));
    return await findOne(find);
  }

  Future<int> remove(String eId) async {
    final Remove remove = remover.where(this.eId.eq(eId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<ExitRecord> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.eId.eq(model.eId));
    }
    return adapter.remove(remove);
  }
}
