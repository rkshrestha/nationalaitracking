// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Calving.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _CalvingBean implements Bean<Calving> {
  final cId = StrField('c_id');
  final calvingId = IntField('calving_id');
  final farmerId = IntField('farmer_id');
  final animalId = IntField('animal_id');
  final speciesId = IntField('species_id');
  final breedId = IntField('breed_id');
  final calvingDate = StrField('calving_date');
  final calfSex = StrField('calf_sex');
  final vetAssist = BoolField('vet_assist');
  final calfCode = StrField('calf_code');
  final remarks = StrField('remarks');
  final createdDate = StrField('created_date');
  final updatedDate = StrField('updated_date');
  final mobileNumber = StrField('mobile_number');
  final speciesCode = StrField('species_code');
  final createdBy = IntField('created_by');
  final updatedBy = IntField('updated_by');
  final mFarmerId = StrField('m_farmer_id');
  final mAnimalId = StrField('m_animal_id');
  final userId = IntField('user_id');
  final farmerName = StrField('farmer_name');
  final englishCalvingDate = StrField('english_calving_date');
  final syncDate = StrField('sync_date');
  final speciesName = StrField('species_name');
  final calfAnimalId = IntField('calf_animal_id');
  final syncToServer = BoolField('sync_to_server');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        cId.name: cId,
        calvingId.name: calvingId,
        farmerId.name: farmerId,
        animalId.name: animalId,
        speciesId.name: speciesId,
        breedId.name: breedId,
        calvingDate.name: calvingDate,
        calfSex.name: calfSex,
        vetAssist.name: vetAssist,
        calfCode.name: calfCode,
        remarks.name: remarks,
        createdDate.name: createdDate,
        updatedDate.name: updatedDate,
        mobileNumber.name: mobileNumber,
        speciesCode.name: speciesCode,
        createdBy.name: createdBy,
        updatedBy.name: updatedBy,
        mFarmerId.name: mFarmerId,
        mAnimalId.name: mAnimalId,
        userId.name: userId,
        farmerName.name: farmerName,
        englishCalvingDate.name: englishCalvingDate,
        syncDate.name: syncDate,
        speciesName.name: speciesName,
        calfAnimalId.name: calfAnimalId,
        syncToServer.name: syncToServer,
      };
  Calving fromMap(Map map) {
    Calving model = Calving();
    model.cId = adapter.parseValue(map['c_id']);
    model.calvingId = adapter.parseValue(map['calving_id']);
    model.farmerId = adapter.parseValue(map['farmer_id']);
    model.animalId = adapter.parseValue(map['animal_id']);
    model.speciesId = adapter.parseValue(map['species_id']);
    model.breedId = adapter.parseValue(map['breed_id']);
    model.calvingDate = adapter.parseValue(map['calving_date']);
    model.calfSex = adapter.parseValue(map['calf_sex']);
    model.vetAssist = adapter.parseValue(map['vet_assist']);
    model.calfCode = adapter.parseValue(map['calf_code']);
    model.remarks = adapter.parseValue(map['remarks']);
    model.createdDate = adapter.parseValue(map['created_date']);
    model.updatedDate = adapter.parseValue(map['updated_date']);
    model.mobileNumber = adapter.parseValue(map['mobile_number']);
    model.speciesCode = adapter.parseValue(map['species_code']);
    model.createdBy = adapter.parseValue(map['created_by']);
    model.updatedBy = adapter.parseValue(map['updated_by']);
    model.mFarmerId = adapter.parseValue(map['m_farmer_id']);
    model.mAnimalId = adapter.parseValue(map['m_animal_id']);
    model.userId = adapter.parseValue(map['user_id']);
    model.farmerName = adapter.parseValue(map['farmer_name']);
    model.englishCalvingDate = adapter.parseValue(map['english_calving_date']);
    model.syncDate = adapter.parseValue(map['sync_date']);
    model.speciesName = adapter.parseValue(map['species_name']);
    model.calfAnimalId = adapter.parseValue(map['calf_animal_id']);
    model.syncToServer = adapter.parseValue(map['sync_to_server']);

    return model;
  }

  List<SetColumn> toSetColumns(Calving model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(cId.set(model.cId));
      ret.add(calvingId.set(model.calvingId));
      ret.add(farmerId.set(model.farmerId));
      ret.add(animalId.set(model.animalId));
      ret.add(speciesId.set(model.speciesId));
      ret.add(breedId.set(model.breedId));
      ret.add(calvingDate.set(model.calvingDate));
      ret.add(calfSex.set(model.calfSex));
      ret.add(vetAssist.set(model.vetAssist));
      ret.add(calfCode.set(model.calfCode));
      ret.add(remarks.set(model.remarks));
      ret.add(createdDate.set(model.createdDate));
      ret.add(updatedDate.set(model.updatedDate));
      ret.add(mobileNumber.set(model.mobileNumber));
      ret.add(speciesCode.set(model.speciesCode));
      ret.add(createdBy.set(model.createdBy));
      ret.add(updatedBy.set(model.updatedBy));
      ret.add(mFarmerId.set(model.mFarmerId));
      ret.add(mAnimalId.set(model.mAnimalId));
      ret.add(userId.set(model.userId));
      ret.add(farmerName.set(model.farmerName));
      ret.add(englishCalvingDate.set(model.englishCalvingDate));
      ret.add(syncDate.set(model.syncDate));
      ret.add(speciesName.set(model.speciesName));
      ret.add(calfAnimalId.set(model.calfAnimalId));
      ret.add(syncToServer.set(model.syncToServer));
    } else if (only != null) {
      if (only.contains(cId.name)) ret.add(cId.set(model.cId));
      if (only.contains(calvingId.name))
        ret.add(calvingId.set(model.calvingId));
      if (only.contains(farmerId.name)) ret.add(farmerId.set(model.farmerId));
      if (only.contains(animalId.name)) ret.add(animalId.set(model.animalId));
      if (only.contains(speciesId.name))
        ret.add(speciesId.set(model.speciesId));
      if (only.contains(breedId.name)) ret.add(breedId.set(model.breedId));
      if (only.contains(calvingDate.name))
        ret.add(calvingDate.set(model.calvingDate));
      if (only.contains(calfSex.name)) ret.add(calfSex.set(model.calfSex));
      if (only.contains(vetAssist.name))
        ret.add(vetAssist.set(model.vetAssist));
      if (only.contains(calfCode.name)) ret.add(calfCode.set(model.calfCode));
      if (only.contains(remarks.name)) ret.add(remarks.set(model.remarks));
      if (only.contains(createdDate.name))
        ret.add(createdDate.set(model.createdDate));
      if (only.contains(updatedDate.name))
        ret.add(updatedDate.set(model.updatedDate));
      if (only.contains(mobileNumber.name))
        ret.add(mobileNumber.set(model.mobileNumber));
      if (only.contains(speciesCode.name))
        ret.add(speciesCode.set(model.speciesCode));
      if (only.contains(createdBy.name))
        ret.add(createdBy.set(model.createdBy));
      if (only.contains(updatedBy.name))
        ret.add(updatedBy.set(model.updatedBy));
      if (only.contains(mFarmerId.name))
        ret.add(mFarmerId.set(model.mFarmerId));
      if (only.contains(mAnimalId.name))
        ret.add(mAnimalId.set(model.mAnimalId));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(farmerName.name))
        ret.add(farmerName.set(model.farmerName));
      if (only.contains(englishCalvingDate.name))
        ret.add(englishCalvingDate.set(model.englishCalvingDate));
      if (only.contains(syncDate.name)) ret.add(syncDate.set(model.syncDate));
      if (only.contains(speciesName.name))
        ret.add(speciesName.set(model.speciesName));
      if (only.contains(calfAnimalId.name))
        ret.add(calfAnimalId.set(model.calfAnimalId));
      if (only.contains(syncToServer.name))
        ret.add(syncToServer.set(model.syncToServer));
    } else /* if (onlyNonNull) */ {
      if (model.cId != null) {
        ret.add(cId.set(model.cId));
      }
      if (model.calvingId != null) {
        ret.add(calvingId.set(model.calvingId));
      }
      if (model.farmerId != null) {
        ret.add(farmerId.set(model.farmerId));
      }
      if (model.animalId != null) {
        ret.add(animalId.set(model.animalId));
      }
      if (model.speciesId != null) {
        ret.add(speciesId.set(model.speciesId));
      }
      if (model.breedId != null) {
        ret.add(breedId.set(model.breedId));
      }
      if (model.calvingDate != null) {
        ret.add(calvingDate.set(model.calvingDate));
      }
      if (model.calfSex != null) {
        ret.add(calfSex.set(model.calfSex));
      }
      if (model.vetAssist != null) {
        ret.add(vetAssist.set(model.vetAssist));
      }
      if (model.calfCode != null) {
        ret.add(calfCode.set(model.calfCode));
      }
      if (model.remarks != null) {
        ret.add(remarks.set(model.remarks));
      }
      if (model.createdDate != null) {
        ret.add(createdDate.set(model.createdDate));
      }
      if (model.updatedDate != null) {
        ret.add(updatedDate.set(model.updatedDate));
      }
      if (model.mobileNumber != null) {
        ret.add(mobileNumber.set(model.mobileNumber));
      }
      if (model.speciesCode != null) {
        ret.add(speciesCode.set(model.speciesCode));
      }
      if (model.createdBy != null) {
        ret.add(createdBy.set(model.createdBy));
      }
      if (model.updatedBy != null) {
        ret.add(updatedBy.set(model.updatedBy));
      }
      if (model.mFarmerId != null) {
        ret.add(mFarmerId.set(model.mFarmerId));
      }
      if (model.mAnimalId != null) {
        ret.add(mAnimalId.set(model.mAnimalId));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.farmerName != null) {
        ret.add(farmerName.set(model.farmerName));
      }
      if (model.englishCalvingDate != null) {
        ret.add(englishCalvingDate.set(model.englishCalvingDate));
      }
      if (model.syncDate != null) {
        ret.add(syncDate.set(model.syncDate));
      }
      if (model.speciesName != null) {
        ret.add(speciesName.set(model.speciesName));
      }
      if (model.calfAnimalId != null) {
        ret.add(calfAnimalId.set(model.calfAnimalId));
      }
      if (model.syncToServer != null) {
        ret.add(syncToServer.set(model.syncToServer));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addStr(cId.name, primary: true, isNullable: false);
    st.addInt(calvingId.name, isNullable: true);
    st.addInt(farmerId.name, isNullable: true);
    st.addInt(animalId.name, isNullable: true);
    st.addInt(speciesId.name, isNullable: true);
    st.addInt(breedId.name, isNullable: true);
    st.addStr(calvingDate.name, isNullable: true);
    st.addStr(calfSex.name, isNullable: true);
    st.addBool(vetAssist.name, isNullable: true);
    st.addStr(calfCode.name, isNullable: true);
    st.addStr(remarks.name, isNullable: true);
    st.addStr(createdDate.name, isNullable: true);
    st.addStr(updatedDate.name, isNullable: true);
    st.addStr(mobileNumber.name, isNullable: false);
    st.addStr(speciesCode.name, isNullable: true);
    st.addInt(createdBy.name, isNullable: true);
    st.addInt(updatedBy.name, isNullable: true);
    st.addStr(mFarmerId.name, isNullable: true);
    st.addStr(mAnimalId.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addStr(farmerName.name, isNullable: true);
    st.addStr(englishCalvingDate.name, isNullable: true);
    st.addStr(syncDate.name, isNullable: true);
    st.addStr(speciesName.name, isNullable: true);
    st.addInt(calfAnimalId.name, isNullable: true);
    st.addBool(syncToServer.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Calving model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<Calving> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Calving model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<Calving> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Calving model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.cId.eq(model.cId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Calving> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.cId.eq(model.cId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Calving> find(String cId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.cId.eq(cId));
    return await findOne(find);
  }

  Future<int> remove(String cId) async {
    final Remove remove = remover.where(this.cId.eq(cId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Calving> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.cId.eq(model.cId));
    }
    return adapter.remove(remove);
  }
}
