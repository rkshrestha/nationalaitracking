// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Animal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Animal _$AnimalFromJson(Map<String, dynamic> json) {
  return Animal()
    ..aId = json['MId'] as String
    ..animalId = json['AnimalId'] as int
    ..mfarmerId = json['MFarmerId'] as String
    ..speciesId = json['SpeciesId'] as int
    ..speciesName = json['SpeciesName'] as String
    ..breedId = json['BreedId'] as int
    ..breedName = json['BreedName'] as String
    ..speciesCode = json['SpeciesCode'] as String
    ..mobileNumber = json['MobileNo'] as String
    ..createdDate = json['CreatedDate'] as String
    ..updatedDate = json['UpdatedDate'] as String
    ..createdBy = json['CreatedBy'] as int
    ..updatedBy = json['UpdatedBy'] as int
    ..fId = json['FarmerId'] as int
    ..userId = json['UserId'] as int
    ..farmerName = json['FarmerName'] as String
    ..gender = json['Gender'] as String
    ..status = json['Status'] as bool
    ..syncDate = json['SyncDate'] as String
    ..syncToServer = json['SyncToServer'] as bool;
}

Map<String, dynamic> _$AnimalToJson(Animal instance) => <String, dynamic>{
      'MId': instance.aId,
      'AnimalId': instance.animalId,
      'MFarmerId': instance.mfarmerId,
      'SpeciesId': instance.speciesId,
      'SpeciesName': instance.speciesName,
      'BreedId': instance.breedId,
      'BreedName': instance.breedName,
      'SpeciesCode': instance.speciesCode,
      'MobileNo': instance.mobileNumber,
      'CreatedDate': instance.createdDate,
      'UpdatedDate': instance.updatedDate,
      'CreatedBy': instance.createdBy,
      'UpdatedBy': instance.updatedBy,
      'FarmerId': instance.fId,
      'UserId': instance.userId,
      'FarmerName': instance.farmerName,
      'Gender': instance.gender,
      'Status': instance.status,
      'SyncDate': instance.syncDate,
      'SyncToServer': instance.syncToServer
    };
