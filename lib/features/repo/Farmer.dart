import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'Farmer.g.dart';

part 'Farmer.jorm.dart';

@JsonSerializable()
class Farmer implements JsonCallable<Farmer> {
//  @PrimaryKey(auto: true)
//  @JsonKey(name: 'MId')
//  int fId;
  @PrimaryKey()
  @JsonKey(name: 'MId')
  String fId;
  @JsonKey(name: 'FarmerId')
  @Column(isNullable: true)
  int farmerId;
  @Column(isNullable: true)
  @JsonKey(name: 'MobileNo')
  String mobileNumber;
  @Column(isNullable: true)
  @JsonKey(name: 'FarmerName')
  String farmerName;
  @Column(isNullable: true)
  @JsonKey(name: 'ProvinceId')
  int provinceId;
  @Column(isNullable: true)
  @JsonKey(name: 'Province')
  String province;
  @Column(isNullable: true)
  @JsonKey(name: 'DistrictId')
  int districtId;
  @Column(isNullable: true)
  @JsonKey(name: 'District')
  String district;
  @Column(isNullable: true)
  @JsonKey(name: 'MunicipalityId')
  int municipalityId;
  @Column(isNullable: true)
  @JsonKey(name: 'Municipality')
  String municipality;
  @Column(isNullable: true)
  @JsonKey(name: 'Ward')
  int ward;
  @Column(isNullable: true)
  @JsonKey(name: 'Village')
  String village;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedDate')
  String createdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedDate')
  String updatedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedBy')
  int createdBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedBy')
  int updatedBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UserId')
  int userId;
  @Column(isNullable: true)
  @JsonKey(name: 'SyncDate')
  String syncDate;
  @JsonKey(name: "SyncToServer")
  bool syncToServer = false;

  Farmer();

  Farmer.make(
      this.fId,
      this.farmerId,
      this.mobileNumber,
      this.farmerName,
      this.provinceId,
      this.province,
      this.districtId,
      this.district,
      this.municipalityId,
      this.municipality,
      this.ward,
      this.village,
      this.createdDate,
      this.updatedDate,
      this.createdBy,
      this.updatedBy,
      this.userId,
      this.syncDate,
      {this.syncToServer = false});

  factory Farmer.fromJson(Map<String, dynamic> json) => _$FarmerFromJson(json);

  Map<String, dynamic> toJson() => _$FarmerToJson(this);

  @override
  Farmer fromJson(Map<String, dynamic> json) {
    return Farmer.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class FarmerBean extends Bean<Farmer> with _FarmerBean {
  FarmerBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'farmers';
}
