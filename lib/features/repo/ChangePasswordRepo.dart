
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/ChangePasswordModel.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/sharedPreferences/User.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';

class ChangePasswordRepo implements Repository<ChangePasswordModel>{
  BuildContext context;
  ChangePasswordRepo(this.context);

  void sendChangePasswordRequest(ChangePasswordModel change, StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v("Test " + DsonUtils.toJsonString(change));

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));

      SCResponse scResponse = await networkClient.call<String, String>(
          networkApi.sendChangePasswordRequest(change),
          disableCallable: true);

      Logger.v("Report:"+scResponse.status.toString());
          sSink.add(scResponse);


    } catch (ex) {
      Logger.v(ex.toString());
    }
  }


  @override
  void delete(ChangePasswordModel entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<ChangePasswordModel> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  ChangePasswordModel findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  ChangePasswordModel findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(ChangePasswordModel entity) {
    // TODO: implement persist
  }

  @override
  void update(ChangePasswordModel entity) {
    // TODO: implement update
  }
}