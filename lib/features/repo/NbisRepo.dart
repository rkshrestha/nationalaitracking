import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/AIDao.dart';
//import 'package:nbis/features/repo/AIRepo.dart';
//import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalDao.dart';
//import 'package:nbis/features/repo/AnimalRepo.dart';
//import 'package:nbis/features/repo/ArtificialInsemination.dart';
//import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/CalvingDao.dart';
import 'package:nbis/features/repo/District.dart';
import 'package:nbis/features/repo/DistrictDao.dart';
//import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/ExitRecordDao.dart';
//import 'package:nbis/features/repo/ExitRecordRepo.dart';
import 'package:nbis/features/repo/FarmerDao.dart';
//import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/features/repo/LiquidNitrogenDao.dart';
import 'package:nbis/features/repo/Municipality.dart';
import 'package:nbis/features/repo/MunicipalityDao.dart';
import 'package:nbis/features/repo/PDDao.dart';
//import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Province.dart';
import 'package:nbis/features/repo/ProvinceDao.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/repo/SemenDao.dart';
import 'package:nbis/features/repo/SyncModel.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';
//import 'package:nbis/features/repo/CalvingRepo.dart';
//import 'package:nbis/features/repo/LiquidNitrogenRepo.dart';
//import 'package:nbis/features/repo/PDRepo.dart';
//import 'package:nbis/features/repo/SemenRepo.dart';

class NbisRepo {
  SQFliteDatabase sqFliteDatabase;
  ProvinceDao provinceDao;
  DistrictDao districtDao;
  MunicipalityDao municipalityDao;
  List<Province> provinceList;
  List<District> districtList;
  List<Municipality> municipalityList;
  BuildContext context;
  SessionManager sessionManager;

  //test for send//
  FarmerDao farmerDao;
  AnimalDao animalDao;
  AIDao aiDao;
  PDDao pdDao;
  CalvingDao calvingDao;
  ExitRecordDao exitRecordDao;
  LiquidNitrogenDao liquidNitrogenDao;
  SemenDao semenDao;
//  List<SyncModel> allList;

  //end test

  NbisRepo(this.context) {
    //  TAG_CLAZZ = this.runtimeType.toString();
    //Logger.log(TAG_CLAZZ, "inside animalRepo");
    init();
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
  }




//  void syncProvinceList(StreamSink<SCResponse<String>> sSink) async {
//    try {
//      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
//
//      if (!isNetworkAvailable) {
//        sSink.add(SCResponse.noInternetAvailable());
//        return;
//      }
//      NetworkClient networkClient = NetworkClient();
//      NetworkApi networkApi = NetworkApi.create(
//          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      SCResponse scResponse = await networkClient
//          .call<Province, List<Province>>(networkApi.syncProvinceList(),
//              callable: Province());
//
//      if (scResponse.status == Status.SUCCESS) {
//        List<Province> provinceList = scResponse.data;
//        provinceDao = await sqFliteDatabase.provinceDao();
//
//        //if(provinceDao.){}
//        provinceDao.deleteAll();
//
//        for (Province province in provinceList) {
//          provinceDao
//              .persist(new Province.make(province.provinceid, province.state));
//        }
//
//        Logger.v(provinceList[0].state);
//        //   sSink.add(scResponse);
//        sSink.add(SCResponse.success("Province Sync success"));
//      } else if (scResponse.status == Status.ERROR) {
//        Logger.v(scResponse.errorMessage);
//        sSink.add(scResponse);
//      }
//    } catch (ex) {
//      Logger.v(ex.toString());
//    }
//  }


  void syncProvinceList(StreamSink<SCResponse<List<Province>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient
          .call<Province, List<Province>>(networkApi.syncProvinceList(),
          callable: Province());

      if (scResponse.status == Status.SUCCESS) {
        List<Province> provinceList = scResponse.data;
        provinceDao = await sqFliteDatabase.provinceDao();

        //if(provinceDao.){}
        provinceDao.deleteAll();

        for (Province province in provinceList) {
          provinceDao
              .persist(new Province.make(province.provinceid, province.state));
        }

        Logger.v(provinceList[0].state);
           sSink.add(scResponse);
//        sSink.add(SCResponse.success("Province Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getProvinceList(StreamSink<SCResponse<List<Province>>> stSink) async {
    try {
      provinceDao = await sqFliteDatabase.provinceDao();

      provinceList = await provinceDao.findAll();

      for (Province province in provinceList) {
        Logger.v(province.state);
      }

      stSink.add(SCResponse.success(provinceList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //get
//  void getProvince(
//      int provinceId, StreamController<SCResponse<Province>> stSink) async {
//    try {
//      provinceDao = await sqFliteDatabase.provinceDao();
//      var province = await provinceDao.getProvince(provinceId);
//
//      if (province != null) {
//        //   Log.v('printing one id: ' + farmer.fId.toString());
//        stSink.add(SCResponse.success(province));
//      }
//    } catch (ex) {
//      Logger.v(ex.toString());
//    }
//  }

  getProvince(int provinceId) async {
    try {
      provinceDao = await sqFliteDatabase.provinceDao();
      var province = await provinceDao.getProvince(provinceId);
      return province;
    } catch (ex) {
      Logger.v(ex.toString());
    }
    return null;
  }

  void getDistrict(
      int districtId, StreamController<SCResponse<District>> stSink) async {
    try {
      districtDao = await sqFliteDatabase.districtDao();
      var district = await districtDao.getDistrict(districtId);

      if (district != null) {
        //   Log.v('printing one id: ' + farmer.fId.toString());
        stSink.add(SCResponse.success(district));
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getMunicipality(int municipalityId,
      StreamController<SCResponse<Municipality>> stSink) async {
    try {
      municipalityDao = await sqFliteDatabase.municipalityDao();
      var municipality = await municipalityDao.getMunicipality(municipalityId);

      if (municipality != null) {
        //   Log.v('printing one id: ' + farmer.fId.toString());
        stSink.add(SCResponse.success(municipality));
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }


//  void syncDistrictList(StreamSink<SCResponse<String>> sSink) async {
//    try {
//      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
//
//      if (!isNetworkAvailable) {
//        sSink.add(SCResponse.noInternetAvailable());
//        return;
//      }
//
//      NetworkClient networkClient = NetworkClient();
//      NetworkApi networkApi = NetworkApi.create(
//          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      SCResponse scResponse = await networkClient
//          .call<District, List<District>>(networkApi.syncDistrictList(),
//              callable: District());
//
//      if (scResponse.status == Status.SUCCESS) {
//        List<District> districtList = scResponse.data;
//        districtDao = await sqFliteDatabase.districtDao();
//
//        districtDao.deleteAll();
//
//        for (District district in districtList) {
//          districtDao.persist(new District.make(district.districtid,
//              district.dcode, district.provinceid, district.name));
//        }
//
//        Logger.v(districtList[0].name);
//        //   sSink.add(scResponse);
//        sSink.add(SCResponse.success("District Sync success"));
//      } else if (scResponse.status == Status.ERROR) {
//        Logger.v(scResponse.errorMessage);
//        sSink.add(scResponse);
//      }
//    } catch (ex) {
//      Logger.v(ex.toString());
//    }
//  }

  //district list


  void syncDistrictList(StreamSink<SCResponse<List<District>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient
          .call<District, List<District>>(networkApi.syncDistrictList(),
          callable: District());

      if (scResponse.status == Status.SUCCESS) {
        List<District> districtList = scResponse.data;
        districtDao = await sqFliteDatabase.districtDao();

        districtDao.deleteAll();

        for (District district in districtList) {
          districtDao.persist(new District.make(district.districtid,
              district.dcode, district.provinceid, district.name));
        }

        Logger.v(districtList[0].name);
           sSink.add(scResponse);
//        sSink.add(SCResponse.success("District Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }



  void getDistrictList(int provinceId, StreamSink<SCResponse<List<District>>> stSink) async {
    try {
      districtDao = await sqFliteDatabase.districtDao();

//      districtList = await districtDao.findAll();
      districtList = await districtDao.getDistrictByProvinceId(provinceId);

      for (District district in districtList) {
        Logger.v(district.name);
      }

      stSink.add(SCResponse.success(districtList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

//municipality sync



//  void syncMunicipalityList(StreamSink<SCResponse<String>> sSink) async {
//    try {
//      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
//
//      if (!isNetworkAvailable) {
//        sSink.add(SCResponse.noInternetAvailable());
//        return;
//      }
//      NetworkClient networkClient = NetworkClient();
//      NetworkApi networkApi = NetworkApi.create(
//          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      SCResponse scResponse =
//          await networkClient.call<Municipality, List<Municipality>>(
//              networkApi.syncMunicipalityList(),
//              callable: Municipality());
//
//      if (scResponse.status == Status.SUCCESS) {
//        List<Municipality> municipalityList = scResponse.data;
//        municipalityDao = await sqFliteDatabase.municipalityDao();
//
//        municipalityDao.deleteAll();
//
//        for (Municipality municipality in municipalityList) {
//          municipalityDao.persist(new Municipality.make(
//              municipality.municipalityid,
//              municipality.mcode,
//              municipality.dcode,
//              municipality.scode,
//              municipality.name,
//              municipality.type));
//        }
//
//        Logger.v(municipalityList[0].name);
//
//        sSink.add(SCResponse.success("Municipality Sync success"));
//      } else if (scResponse.status == Status.ERROR) {
//        Logger.v(scResponse.errorMessage);
//        sSink.add(scResponse);
//      }
//    } catch (ex) {
//      Logger.v(ex.toString());
//    }
//  }

  //municipality List

  void syncMunicipalityList(StreamSink<SCResponse<List<Municipality>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse =
      await networkClient.call<Municipality, List<Municipality>>(
          networkApi.syncMunicipalityList(),
          callable: Municipality());

      if (scResponse.status == Status.SUCCESS) {
        List<Municipality> municipalityList = scResponse.data;
        municipalityDao = await sqFliteDatabase.municipalityDao();

        municipalityDao.deleteAll();

        for (Municipality municipality in municipalityList) {
          municipalityDao.persist(new Municipality.make(
              municipality.municipalityid,
              municipality.mcode,
              municipality.dcode,
              municipality.scode,
              municipality.name,
              municipality.type));
        }

        Logger.v(municipalityList[0].name);

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Municipality Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }



  void getMunicipalityList(int districtId, StreamSink<SCResponse<List<Municipality>>> stSink) async {
    try {
      municipalityDao = await sqFliteDatabase.municipalityDao();

      municipalityList =
          await municipalityDao.getMunicipalityByDistrictId(districtId);

      for (Municipality municipality in municipalityList) {
        Logger.v(municipality.name);
      }

      stSink.add(SCResponse.success(municipalityList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }



  //sync all data

  void sendAllData(SyncModel allList,
      StreamSink<SCResponse<SyncModel>> sSink) async {
//    void sendAllData(List<SyncModel> allList,
//      StreamSink<SCResponse<List<SyncModel>>> sSink) async {

    Logger.v("Test  :" + DsonUtils.toJsonString(allList));
    try {

//      sqFliteDatabase = SQFliteDatabase.getDB();
//      await sqFliteDatabase.connect();

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();
      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();
      farmerDao = await sqFliteDatabase.farmerDao();
      animalDao = await sqFliteDatabase.animalDao();
      aiDao = await sqFliteDatabase.aiDao();
      pdDao = await sqFliteDatabase.pdDao();
      calvingDao = await sqFliteDatabase.calvingDao();
      exitRecordDao = await sqFliteDatabase.exitRecordDao();
      var user = this.sessionManager.findOne().user;

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      List<Farmer> unsyncedFarmerList = await farmerDao.findAllBySyncStatus();

//      List<Animal> unsyncedAnimalList = await animalDao.findAllUnSyncData();
//      List<ArtificialInsemination> unsyncedAIList =
//      await aiDao.findAllUnSyncData();
//      List<PregnancyDiagnosis> unsyncedPDList = await pdDao.findAllUnSyncData();
//      List<Calving> unsyncedCalvingList = await calvingDao.findAllUnSyncData();
//      List<ExitRecord> unsyncedExitList = await exitRecordDao.findAllUnSyncData();


      SCResponse scResponse = await networkClient.call<SyncModel, SyncModel>(
          networkApi.sendAllData(allList, user.userid),
        callable: SyncModel()
      );

      if (scResponse.status == Status.SUCCESS) {
        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }

      sSink.add(scResponse);
    } catch (ex) {
      Logger.v(ex.toString());
    }
//    finally{
//      sqFliteDatabase.close();
//    }
  }



}
