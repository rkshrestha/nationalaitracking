import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/repo/SemenDao.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:uuid/uuid.dart';

class SemenRepo implements Repository<Semen> {
  SQFliteDatabase sqFliteDatabase;
  SemenDao semenDao;
  List<Semen> semenUnSyncList;
  List<Semen> semenSyncList;
  SessionManager sessionManager;
  BuildContext context;
  int serverSemenId;

  SemenRepo(context) {
    Logger.v("inside semenRepo");

    init();
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
  }

  void saveSemen(
      context, Semen semen, StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try saveSemen' + semen.receivedDate);

      semenDao = await sqFliteDatabase.semenDao();
      Logger.v('check semen add ');
      var user = this.sessionManager.findOne().user;
//      Logger.v(user.userid.toString());
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);
      DateTime today = new DateTime.now();
      DateTime todayNepaliDate = new DateTime(today.year + 56, today.month + 8,
          today.day + 17, today.hour, today.minute, today.second);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);

      NepaliDateTime nepaliSemenDate =
          NepaliDateTime.tryParse(semen.receivedDate);
      DateTime englishSemenDate = DateConverter.toAD(nepaliSemenDate);
      String englishReceivedSemenDate =
          englishSemenDate.toIso8601String().split("T").first;
      Semen semens = await semenDao.findBySameDate(semen);
      var uuid = new Uuid();
      var uid= uuid.v1();
//      Logger.v('UUID:'+uid);


      if (semens == null) {
        int semenId = await semenDao.persist(new Semen.make(
                uid,
//                semen.msId,
                null,
                user.userid,
                semen.receivedDate,
                today.toIso8601String(),
                null,
                user.userid,
                null,
                semen.jersey,
                semen.boar,
                semen.saanen,
                semen.yorkshire,
                semen.landrace,
                semen.duroc,
                semen.hampshire,
                semen.holsteinFriesian,
                semen.murrah,
                englishReceivedSemenDate,
                null,
                syncToServer: false)

//        Semen.make(
//            semen.msId,
//            null,
////            4,
//            user.userid,
//            semen.receivedDate,
//            semen.jersey,
//            semen.holsteinFriesian,
//            semen.murrah,
//        syncToServer: false)

            );

        Navigator.pop(context, true);
//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("semen_add_success"),
//        ));
        sSink.add(SCResponse.success("Semen has been successfully added"));
      } else {
//        sSink.add(SCResponse.error(
//          AppTranslations.of(context).text("semen_exist"),
//        ));
        sSink.add(SCResponse.error("Duplicate received date.."));
      }
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("semen_add_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't insert semen"));
      Logger.v(ex.toString());
    }
  }

  void getSemenUnSyncList(StreamSink<SCResponse<List<Semen>>> stSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      semenDao = await sqFliteDatabase.semenDao();

      semenUnSyncList = await semenDao.findAllUnSyncData();

      for (Semen semen in semenUnSyncList) {
        Logger.v(semen.receivedDate);
      }

      stSink.add(SCResponse.success(semenUnSyncList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSemenSyncList(StreamSink<SCResponse<List<Semen>>> stSink) async {
    try {
      semenDao = await sqFliteDatabase.semenDao();

      semenSyncList = await semenDao.findAllSyncData();

      for (Semen semen in semenSyncList) {
        Logger.v(semen.receivedDate);
      }

      stSink.add(SCResponse.success(semenSyncList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSemenList(StreamSink<SCResponse<List<Semen>>> stSink) async {
    try {
      semenDao = await sqFliteDatabase.semenDao();

      semenSyncList = await semenDao.findAll();

      for (Semen semen in semenSyncList) {
        Logger.v(semen.receivedDate);
      }

      stSink.add(SCResponse.success(semenSyncList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //update
  void updateSemen(
      context, Semen semen, StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try updateSemen' + semen.receivedDate);

      semenDao = await sqFliteDatabase.semenDao();
      var user = this.sessionManager.findOne().user;
//      Logger.v(user.userid.toString());
//      Logger.v("getSemenId" + semen.semenId.toString());
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);
      DateTime today = new DateTime.now();
      DateTime todayNepaliDate = new DateTime(today.year + 56, today.month + 8,
          today.day + 17, today.hour, today.minute, today.second);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
      NepaliDateTime nepaliSemenDate =
          NepaliDateTime.tryParse(semen.receivedDate);
      DateTime englishSemenDate = DateConverter.toAD(nepaliSemenDate);
      String englishReceivedSemenDate =
          englishSemenDate.toIso8601String().split("T").first;

//      Semen semens = await semenDao.findBySameDatess(semen);

//      if (semens == null) {
        int updateSemen = await semenDao.update(new Semen.make(
            semen.msId,
            semen.semenId,
            user.userid,
            semen.receivedDate,
//          todayNepaliDate.toIso8601String(),
            semen.createdDate,
            today.toIso8601String(),
            user.userid,
            user.userid,
            semen.jersey,
            semen.boar,
            semen.saanen,
            semen.yorkshire,
            semen.landrace,
            semen.duroc,
            semen.hampshire,
            semen.holsteinFriesian,
            semen.murrah,
            englishReceivedSemenDate,
            semen.syncDate,
            syncToServer: false));

        Navigator.pop(context, true);
        sSink.add(SCResponse.success(
          "Semen has been successfully updated!!",
//          AppTranslations.of(context).text("semen_update_success"),
        ));
//      } else {
////        sSink.add(SCResponse.error(
////          AppTranslations.of(context).text("semen_exist"),
////        ));
//        sSink.add(SCResponse.error("Duplicate received date.."));
//      }

//      sSink.add(SCResponse.success("Semen has been updated successfully"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("semen_update_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't update semen"));
      Logger.v(ex.toString());
    }
  }

  deleteSemen(
      context, Semen semen, StreamSink<SCResponse<String>> sSink) async {
    try {
      semenDao = await sqFliteDatabase.semenDao();

      await semenDao.delete(semen);

//      sSink.add(SCResponse.success(
//        AppTranslations.of(context).text("semen_delete_success"),
//      ));
      sSink.add(SCResponse.success("Semen deleted Successfully"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("semen_delete_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't delete semen"));
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local start
  void updateSemenServerIdAndStatus(
      List<Semen> semenList, StreamSink<SCResponse<List<Semen>>> sSink) async {
//    sqFliteDatabase = SQFliteDatabase.getDB();
//    await sqFliteDatabase.connect();
    try {
      Logger.v("Test " + DsonUtils.toJsonString(semenList));
      //  Logger.v('Testing: '+ loginRequest.toString());

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      semenDao = await sqFliteDatabase.semenDao();
      var user = this.sessionManager.findOne().user;

      for (Semen syncedSemen in semenList) {
        Logger.v("synced" + syncedSemen.syncToServer.toString());
        await semenDao.updateByDatesSync(syncedSemen);

        await semenDao.updateByDate(syncedSemen);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local end

  //sync to server
  //semen send
  void sendSemenData(
      List<Semen> semenList, StreamSink<SCResponse<List<Semen>>> sSink) async {
//    sqFliteDatabase = SQFliteDatabase.getDB();
//    await sqFliteDatabase.connect();
    try {
      Logger.v("Test " + DsonUtils.toJsonString(semenList));
      //  Logger.v('Testing: '+ loginRequest.toString());

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));

//      List<Semen> unsyncedSemenList = await semenDao.findAllBySyncStatus();
//      Logger.v("Lists of unsynced data:" + unsyncedSemenList.length.toString());
      Logger.v("Lists of unsynced data:" + semenList.length.toString());

      SCResponse scResponse = await networkClient.call<Semen, List<Semen>>(
          networkApi.sendSemenData(semenList),
//          networkApi.sendSemenData(unsyncedSemenList),
          callable: Semen());

      Logger.v(scResponse.status.toString());

      if (scResponse.status == Status.SUCCESS) {
        List<Semen> syncedSemenList = scResponse.data;

        for (Semen syncedSemen in syncedSemenList) {
          Logger.v("synced" + syncedSemen.syncToServer.toString());
//          if (syncedSemen.syncToServer = false) {
          await semenDao.updateByDatesSync(syncedSemen);
//          }

          await semenDao.updateByDate(syncedSemen);
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }

      sSink.add(scResponse);

//      if (scResponse.status == Status.SUCCESS) {
//        List<LoginRequest> RequestList = scResponse.data;
//        Log.v(RequestList[0].userName);
//        sSink.add(scResponse);
//      } else if (scResponse.status == Status.ERROR) {
//        Log.v(scResponse.errorMessage);
//        sSink.add(scResponse);
//      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
//    finally{
//      sqFliteDatabase.close();
//    }
  }

  void syncGetSemenList(StreamSink<SCResponse<List<Semen>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager.findOne().user;
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient.call<Semen, List<Semen>>(
          networkApi.syncGetSemenList(user.userid),
          callable: Semen());

      if (scResponse.status == Status.SUCCESS) {
        List<Semen> semenLists = scResponse.data;
        semenDao = await sqFliteDatabase.semenDao();
//        List<Semen> semenListLocal = await semenDao.findAll();
//

        for (Semen semenServer in semenLists) {
          serverSemenId = semenServer.semenId;
          Logger.v("ServerSemenId " + serverSemenId.toString());
          Semen semen = await semenDao.findBySemenId(serverSemenId);
          Logger.v("Semen " + semen.toString());
          if (semen == null) {
            //  semenDao.delete(semenServer);
            var formattedDate = semenServer.receivedDate == null
                ? ""
                : semenServer.receivedDate.split("T")[0];
            var formattedCreatedDate = semenServer.createdDate == null
                ? ""
                : semenServer.createdDate.split("T")[0];
            var formattedUpdatedDate = semenServer.updatedDate == null
                ? ""
                : semenServer.updatedDate.split("T")[0];
            var formattedEnglishDate = semenServer.englishReceivedDate == null
                ? ""
                : semenServer.englishReceivedDate.split("T")[0];
            semenDao.persist(new Semen.make(
                    semenServer.msId,
                    semenServer.semenId,
                    semenServer.userId,
                    formattedDate,
                    formattedCreatedDate,
                    formattedUpdatedDate,
                    semenServer.createdBy,
                    semenServer.updatedBy,
                    semenServer.jersey,
                    semenServer.boar,
                    semenServer.saanen,
                    semenServer.yorkshire,
                    semenServer.landrace,
                    semenServer.duroc,
                    semenServer.hampshire,
                    semenServer.holsteinFriesian,
                    semenServer.murrah,
                    formattedEnglishDate,
                    semenServer.syncDate,
                    syncToServer: true)
//            Semen.make(
//                semenServer.msId,
//                semenServer.semenId,
//                semenServer.userId,
//                formattedDate,
//                semenServer.jersey,
//                semenServer.holsteinFriesian,
//                semenServer.murrah,
//                syncToServer: true)
                );
          } else {
            semenDao.updateByServerSemenId(semenServer);
//            semenDao.updateByServerSemenId(new Semen.make(
//                semenServer.msId,
//                semenServer.semenId,
//                semenServer.userId,
//                semenServer.receivedDate,
//                semenServer.jersey,
//                semenServer.holsteinFriesian,
//                semenServer.murrah,
//                syncToServer: true));
          }
        }

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Semen Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

//

  @override
  void delete(Semen entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<Semen> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  Semen findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Semen findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(Semen entity) {
    // TODO: implement persist
  }

  @override
  void update(Semen entity) {
    // TODO: implement update
  }
}
