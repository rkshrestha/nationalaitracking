import 'package:flutter/material.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';

class LiquidNitrogenDao implements BaseDao<LiquidNitrogen> {
  static LiquidNitrogenDao _instance;
  LiquidNitrogenBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  LiquidNitrogenDao._(this.bean);

  static LiquidNitrogenDao get(LiquidNitrogenBean bean) {
    if (_instance == null) {
      _instance = LiquidNitrogenDao._(bean);
    }
    return _instance;
  }

  findByNitrogenId(int slnId) async {
    Find findByNitrogenId = Find(bean.tableName);

    findByNitrogenId.where(bean.slnId.eq(slnId));

    return bean.findOne(findByNitrogenId);
  }

  @override
  Future<void> delete(LiquidNitrogen liquidNitrogen) {
    Logger.v('lnId:${liquidNitrogen.lnId}');
    int lnIds = int.tryParse(liquidNitrogen.lnId);
//    return bean.remove(lnIds);
    return bean.remove(liquidNitrogen.lnId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<LiquidNitrogen>> findAll() {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findLn = Find(bean.tableName);
    findLn.where(bean.userId.eq(user.userid));

    return bean.findMany(findLn);
  }

  @override
  Future<LiquidNitrogen> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<LiquidNitrogen> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(LiquidNitrogen liquidNitrogen) async {
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table Liquid Nitrogen");
    return await bean.insert(liquidNitrogen);
  }

  @override
  Future<int> update(LiquidNitrogen nitrogen) {
    return bean.update(nitrogen);
  }

  Future<List<LiquidNitrogen>> findAllNitrogenBySyncStatus() {
    Find findNitrogen = Find(bean.tableName);
    findNitrogen.where(bean.syncToServer.eq(false));
    return bean.findMany(findNitrogen);
  }

  Future<int> updateByDate(LiquidNitrogen nitrogen) async {
    var formattedDate = nitrogen.receivedDate.split("T")[0];
    Logger.v("Datesofreceived: "+ formattedDate);
    DateTime todaysDate = DateTime.now();
    final Update update = bean.updater
        .where(bean.receivedDate.eq(formattedDate));

        update.set(bean.slnId, nitrogen.slnId);
        update.set(bean.syncDate, nitrogen.syncDate);

    return await bean.adapter.update(update);
  }

  Future<int> updateByDatesSync(LiquidNitrogen nitrogen) async {
    var formattedDate = nitrogen.receivedDate.split("T")[0];
    final Update update = bean.updater
        .where(bean.receivedDate.eq(formattedDate))
        .set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  Future<int> updateByServerslnId(LiquidNitrogen nitrogen) async {
    var formattedNitrogenDate = nitrogen.receivedDate==null?"":nitrogen.receivedDate.split("T")[0];
    var formattedCreatedDate = nitrogen.createdDate==null?"":nitrogen.createdDate.split("T")[0];
    var formattedUpdatedDate = nitrogen.updatedDate==null?"":nitrogen.updatedDate.split("T")[0];
    var formattedEnglishDate = nitrogen.englishReceivedDate==null?"":nitrogen.englishReceivedDate.split("T")[0];
    final Update update = bean.updater.where(bean.slnId.eq(nitrogen.slnId));
    update.set(bean.slnId, nitrogen.slnId);
    update.set(bean.lnId, nitrogen.lnId);
    update.set(bean.receivedDate, formattedNitrogenDate);
    update.set(bean.quantity, nitrogen.quantity);
    update.set(bean.userId, nitrogen.userId);
    update.set(bean.createdBy, nitrogen.createdBy);
    update.set(bean.updatedBy, nitrogen.updatedBy);
    update.set(bean.createdDate, formattedCreatedDate);
    update.set(bean.updatedDate, formattedUpdatedDate);
    update.set(bean.englishReceivedDate, formattedEnglishDate);
    update.set(bean.syncDate, nitrogen.syncDate);

    update.set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  findBySameDate(LiquidNitrogen liquidNitrogen) async {
    Find findByDate = Find(bean.tableName);
    findByDate.where(bean.receivedDate.eq(liquidNitrogen.receivedDate));

    return bean.findOne(findByDate);
  }


  findBySameDateUpdate(LiquidNitrogen liquidNitrogen) async {
    Find findByDate = Find(bean.tableName);
//    findByDate.where(bean.receivedDate.eq(liquidNitrogen.receivedDate));
    int lnIds = int.tryParse(liquidNitrogen.lnId);
//    findByDate.where(bean.receivedDate.eq(liquidNitrogen.receivedDate).and(bean.lnId.isNot(lnIds)));
    findByDate.where(bean.receivedDate.eq(liquidNitrogen.receivedDate).and(bean.lnId.isNot(liquidNitrogen.lnId)));

    return bean.findOne(findByDate);
  }
  Future<List<LiquidNitrogen>> findAllSyncData() {
    Find findNitrogens = Find(bean.tableName);
    findNitrogens.where(bean.syncToServer.eq(true));
    return bean.findMany(findNitrogens);
  }

  Future<List<LiquidNitrogen>> findAllUnSyncData() {
    Find findNitrogens = Find(bean.tableName);
    findNitrogens.where(bean.syncToServer.eq(false));
    return bean.findMany(findNitrogens);
  }
}
