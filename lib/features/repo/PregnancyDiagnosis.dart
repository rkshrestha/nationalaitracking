import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'PregnancyDiagnosis.g.dart';

part 'PregnancyDiagnosis.jorm.dart';

@JsonSerializable()
class PregnancyDiagnosis implements JsonCallable<PregnancyDiagnosis> {
  @PrimaryKey()
  @JsonKey(name: 'MPdId')
  String pdId;
//  @PrimaryKey(auto: true)
//  @JsonKey(name: 'MPdId')
//  int pdId;
  @JsonKey(name: 'PdId')
  @Column(isNullable: true)
  int spdId;
  @JsonKey(name: 'AnimalId')
  @Column(isNullable: true)
  int animalId;
  @JsonKey(name: 'FarmerId')
  @Column(isNullable: true)
  int farmerId;
  @JsonKey(name: 'SpeciesId')
  @Column(isNullable: true)
  int speciesId;
  @JsonKey(name: 'BreedId')
  @Column(isNullable: true)
  int breedId;
  @JsonKey(name: 'PdDate')
  @Column(isNullable: true)
  String pdDate;
  @JsonKey(name: 'PdResult')
  @Column(isNullable: true)
  bool pdResult;
  @JsonKey(name: 'ExpectedDeliveryDate')
  @Column(isNullable: true)
  String expectedDeliveryDate;
  @JsonKey(name: 'Remarks')
  @Column(isNullable: true)
  String remarks;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedDate')
  String createdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedDate')
  String updatedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'MobileNo')
  String mobileNumber;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesCode')
  String speciesCode;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedBy')
  int createdBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedBy')
  int updatedBy;
  @JsonKey(name: 'MAnimalId')
  @Column(isNullable: true)
  String mAnimalId;
  @JsonKey(name: 'MFarmerId')
  @Column(isNullable: true)
  String mFarmerId;
  @Column(isNullable: true)
  @JsonKey(name: 'UserId')
  int userId;
  @Column(isNullable: true)
  @JsonKey(name: 'FarmerName')
  String farmerName;
  @Column(isNullable: true)
  @JsonKey(name: 'EnglishPdDate')
  String englishPdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'EnglishExpectedDeliveryDate')
  String englishExpectedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'SyncDate')
  String syncDate;
  @JsonKey (name: "SyncToServer")
  bool syncToServer = false;

  PregnancyDiagnosis();


  PregnancyDiagnosis.make(this.pdId, this.spdId, this.animalId, this.farmerId,
      this.speciesId, this.breedId, this.pdDate, this.pdResult,
      this.expectedDeliveryDate, this.remarks, this.createdDate,
      this.updatedDate, this.mobileNumber, this.speciesCode, this.createdBy,
      this.updatedBy, this.mAnimalId, this.mFarmerId, this.userId,
      this.farmerName, this.englishPdDate, this.englishExpectedDate,this.syncDate,
      {this.syncToServer = false});

  factory PregnancyDiagnosis.fromJson(Map<String, dynamic> json) =>
      _$PregnancyDiagnosisFromJson(json);

  Map<String, dynamic> toJson() => _$PregnancyDiagnosisToJson(this);

  @override
  PregnancyDiagnosis fromJson(Map<String, dynamic> json) {
    return PregnancyDiagnosis.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }

}

@GenBean()
class PregnancyDiagnosisBean extends Bean<PregnancyDiagnosis>
    with _PregnancyDiagnosisBean {
  PregnancyDiagnosisBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'pregnancy_diagnosis';
}
