// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PregnancyDiagnosis.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _PregnancyDiagnosisBean implements Bean<PregnancyDiagnosis> {
  final pdId = StrField('pd_id');
  final spdId = IntField('spd_id');
  final animalId = IntField('animal_id');
  final farmerId = IntField('farmer_id');
  final speciesId = IntField('species_id');
  final breedId = IntField('breed_id');
  final pdDate = StrField('pd_date');
  final pdResult = BoolField('pd_result');
  final expectedDeliveryDate = StrField('expected_delivery_date');
  final remarks = StrField('remarks');
  final createdDate = StrField('created_date');
  final updatedDate = StrField('updated_date');
  final mobileNumber = StrField('mobile_number');
  final speciesCode = StrField('species_code');
  final createdBy = IntField('created_by');
  final updatedBy = IntField('updated_by');
  final mAnimalId = StrField('m_animal_id');
  final mFarmerId = StrField('m_farmer_id');
  final userId = IntField('user_id');
  final farmerName = StrField('farmer_name');
  final englishPdDate = StrField('english_pd_date');
  final englishExpectedDate = StrField('english_expected_date');
  final syncDate = StrField('sync_date');
  final syncToServer = BoolField('sync_to_server');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        pdId.name: pdId,
        spdId.name: spdId,
        animalId.name: animalId,
        farmerId.name: farmerId,
        speciesId.name: speciesId,
        breedId.name: breedId,
        pdDate.name: pdDate,
        pdResult.name: pdResult,
        expectedDeliveryDate.name: expectedDeliveryDate,
        remarks.name: remarks,
        createdDate.name: createdDate,
        updatedDate.name: updatedDate,
        mobileNumber.name: mobileNumber,
        speciesCode.name: speciesCode,
        createdBy.name: createdBy,
        updatedBy.name: updatedBy,
        mAnimalId.name: mAnimalId,
        mFarmerId.name: mFarmerId,
        userId.name: userId,
        farmerName.name: farmerName,
        englishPdDate.name: englishPdDate,
        englishExpectedDate.name: englishExpectedDate,
        syncDate.name: syncDate,
        syncToServer.name: syncToServer,
      };
  PregnancyDiagnosis fromMap(Map map) {
    PregnancyDiagnosis model = PregnancyDiagnosis();
    model.pdId = adapter.parseValue(map['pd_id']);
    model.spdId = adapter.parseValue(map['spd_id']);
    model.animalId = adapter.parseValue(map['animal_id']);
    model.farmerId = adapter.parseValue(map['farmer_id']);
    model.speciesId = adapter.parseValue(map['species_id']);
    model.breedId = adapter.parseValue(map['breed_id']);
    model.pdDate = adapter.parseValue(map['pd_date']);
    model.pdResult = adapter.parseValue(map['pd_result']);
    model.expectedDeliveryDate =
        adapter.parseValue(map['expected_delivery_date']);
    model.remarks = adapter.parseValue(map['remarks']);
    model.createdDate = adapter.parseValue(map['created_date']);
    model.updatedDate = adapter.parseValue(map['updated_date']);
    model.mobileNumber = adapter.parseValue(map['mobile_number']);
    model.speciesCode = adapter.parseValue(map['species_code']);
    model.createdBy = adapter.parseValue(map['created_by']);
    model.updatedBy = adapter.parseValue(map['updated_by']);
    model.mAnimalId = adapter.parseValue(map['m_animal_id']);
    model.mFarmerId = adapter.parseValue(map['m_farmer_id']);
    model.userId = adapter.parseValue(map['user_id']);
    model.farmerName = adapter.parseValue(map['farmer_name']);
    model.englishPdDate = adapter.parseValue(map['english_pd_date']);
    model.englishExpectedDate =
        adapter.parseValue(map['english_expected_date']);
    model.syncDate = adapter.parseValue(map['sync_date']);
    model.syncToServer = adapter.parseValue(map['sync_to_server']);

    return model;
  }

  List<SetColumn> toSetColumns(PregnancyDiagnosis model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      ret.add(pdId.set(model.pdId));
      ret.add(spdId.set(model.spdId));
      ret.add(animalId.set(model.animalId));
      ret.add(farmerId.set(model.farmerId));
      ret.add(speciesId.set(model.speciesId));
      ret.add(breedId.set(model.breedId));
      ret.add(pdDate.set(model.pdDate));
      ret.add(pdResult.set(model.pdResult));
      ret.add(expectedDeliveryDate.set(model.expectedDeliveryDate));
      ret.add(remarks.set(model.remarks));
      ret.add(createdDate.set(model.createdDate));
      ret.add(updatedDate.set(model.updatedDate));
      ret.add(mobileNumber.set(model.mobileNumber));
      ret.add(speciesCode.set(model.speciesCode));
      ret.add(createdBy.set(model.createdBy));
      ret.add(updatedBy.set(model.updatedBy));
      ret.add(mAnimalId.set(model.mAnimalId));
      ret.add(mFarmerId.set(model.mFarmerId));
      ret.add(userId.set(model.userId));
      ret.add(farmerName.set(model.farmerName));
      ret.add(englishPdDate.set(model.englishPdDate));
      ret.add(englishExpectedDate.set(model.englishExpectedDate));
      ret.add(syncDate.set(model.syncDate));
      ret.add(syncToServer.set(model.syncToServer));
    } else if (only != null) {
      if (only.contains(pdId.name)) ret.add(pdId.set(model.pdId));
      if (only.contains(spdId.name)) ret.add(spdId.set(model.spdId));
      if (only.contains(animalId.name)) ret.add(animalId.set(model.animalId));
      if (only.contains(farmerId.name)) ret.add(farmerId.set(model.farmerId));
      if (only.contains(speciesId.name))
        ret.add(speciesId.set(model.speciesId));
      if (only.contains(breedId.name)) ret.add(breedId.set(model.breedId));
      if (only.contains(pdDate.name)) ret.add(pdDate.set(model.pdDate));
      if (only.contains(pdResult.name)) ret.add(pdResult.set(model.pdResult));
      if (only.contains(expectedDeliveryDate.name))
        ret.add(expectedDeliveryDate.set(model.expectedDeliveryDate));
      if (only.contains(remarks.name)) ret.add(remarks.set(model.remarks));
      if (only.contains(createdDate.name))
        ret.add(createdDate.set(model.createdDate));
      if (only.contains(updatedDate.name))
        ret.add(updatedDate.set(model.updatedDate));
      if (only.contains(mobileNumber.name))
        ret.add(mobileNumber.set(model.mobileNumber));
      if (only.contains(speciesCode.name))
        ret.add(speciesCode.set(model.speciesCode));
      if (only.contains(createdBy.name))
        ret.add(createdBy.set(model.createdBy));
      if (only.contains(updatedBy.name))
        ret.add(updatedBy.set(model.updatedBy));
      if (only.contains(mAnimalId.name))
        ret.add(mAnimalId.set(model.mAnimalId));
      if (only.contains(mFarmerId.name))
        ret.add(mFarmerId.set(model.mFarmerId));
      if (only.contains(userId.name)) ret.add(userId.set(model.userId));
      if (only.contains(farmerName.name))
        ret.add(farmerName.set(model.farmerName));
      if (only.contains(englishPdDate.name))
        ret.add(englishPdDate.set(model.englishPdDate));
      if (only.contains(englishExpectedDate.name))
        ret.add(englishExpectedDate.set(model.englishExpectedDate));
      if (only.contains(syncDate.name)) ret.add(syncDate.set(model.syncDate));
      if (only.contains(syncToServer.name))
        ret.add(syncToServer.set(model.syncToServer));
    } else /* if (onlyNonNull) */ {
      if (model.pdId != null) {
        ret.add(pdId.set(model.pdId));
      }
      if (model.spdId != null) {
        ret.add(spdId.set(model.spdId));
      }
      if (model.animalId != null) {
        ret.add(animalId.set(model.animalId));
      }
      if (model.farmerId != null) {
        ret.add(farmerId.set(model.farmerId));
      }
      if (model.speciesId != null) {
        ret.add(speciesId.set(model.speciesId));
      }
      if (model.breedId != null) {
        ret.add(breedId.set(model.breedId));
      }
      if (model.pdDate != null) {
        ret.add(pdDate.set(model.pdDate));
      }
      if (model.pdResult != null) {
        ret.add(pdResult.set(model.pdResult));
      }
      if (model.expectedDeliveryDate != null) {
        ret.add(expectedDeliveryDate.set(model.expectedDeliveryDate));
      }
      if (model.remarks != null) {
        ret.add(remarks.set(model.remarks));
      }
      if (model.createdDate != null) {
        ret.add(createdDate.set(model.createdDate));
      }
      if (model.updatedDate != null) {
        ret.add(updatedDate.set(model.updatedDate));
      }
      if (model.mobileNumber != null) {
        ret.add(mobileNumber.set(model.mobileNumber));
      }
      if (model.speciesCode != null) {
        ret.add(speciesCode.set(model.speciesCode));
      }
      if (model.createdBy != null) {
        ret.add(createdBy.set(model.createdBy));
      }
      if (model.updatedBy != null) {
        ret.add(updatedBy.set(model.updatedBy));
      }
      if (model.mAnimalId != null) {
        ret.add(mAnimalId.set(model.mAnimalId));
      }
      if (model.mFarmerId != null) {
        ret.add(mFarmerId.set(model.mFarmerId));
      }
      if (model.userId != null) {
        ret.add(userId.set(model.userId));
      }
      if (model.farmerName != null) {
        ret.add(farmerName.set(model.farmerName));
      }
      if (model.englishPdDate != null) {
        ret.add(englishPdDate.set(model.englishPdDate));
      }
      if (model.englishExpectedDate != null) {
        ret.add(englishExpectedDate.set(model.englishExpectedDate));
      }
      if (model.syncDate != null) {
        ret.add(syncDate.set(model.syncDate));
      }
      if (model.syncToServer != null) {
        ret.add(syncToServer.set(model.syncToServer));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addStr(pdId.name, primary: true, isNullable: false);
    st.addInt(spdId.name, isNullable: true);
    st.addInt(animalId.name, isNullable: true);
    st.addInt(farmerId.name, isNullable: true);
    st.addInt(speciesId.name, isNullable: true);
    st.addInt(breedId.name, isNullable: true);
    st.addStr(pdDate.name, isNullable: true);
    st.addBool(pdResult.name, isNullable: true);
    st.addStr(expectedDeliveryDate.name, isNullable: true);
    st.addStr(remarks.name, isNullable: true);
    st.addStr(createdDate.name, isNullable: true);
    st.addStr(updatedDate.name, isNullable: true);
    st.addStr(mobileNumber.name, isNullable: true);
    st.addStr(speciesCode.name, isNullable: true);
    st.addInt(createdBy.name, isNullable: true);
    st.addInt(updatedBy.name, isNullable: true);
    st.addStr(mAnimalId.name, isNullable: true);
    st.addStr(mFarmerId.name, isNullable: true);
    st.addInt(userId.name, isNullable: true);
    st.addStr(farmerName.name, isNullable: true);
    st.addStr(englishPdDate.name, isNullable: true);
    st.addStr(englishExpectedDate.name, isNullable: true);
    st.addStr(syncDate.name, isNullable: true);
    st.addBool(syncToServer.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(PregnancyDiagnosis model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.insert(insert);
  }

  Future<void> insertMany(List<PregnancyDiagnosis> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(PregnancyDiagnosis model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.upsert(upsert);
  }

  Future<void> upsertMany(List<PregnancyDiagnosis> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(PregnancyDiagnosis model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.pdId.eq(model.pdId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<PregnancyDiagnosis> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.pdId.eq(model.pdId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<PregnancyDiagnosis> find(String pdId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.pdId.eq(pdId));
    return await findOne(find);
  }

  Future<int> remove(String pdId) async {
    final Remove remove = remover.where(this.pdId.eq(pdId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<PregnancyDiagnosis> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.pdId.eq(model.pdId));
    }
    return adapter.remove(remove);
  }
}
