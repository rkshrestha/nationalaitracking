// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'District.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

District _$DistrictFromJson(Map<String, dynamic> json) {
  return District()
    ..dId = json['dId'] as int
    ..districtid = json['DistrictId'] as int
    ..dcode = json['DCode'] as String
    ..provinceid = json['SCode'] as int
    ..name = json['DistrictName'] as String;
}

Map<String, dynamic> _$DistrictToJson(District instance) => <String, dynamic>{
      'dId': instance.dId,
      'DistrictId': instance.districtid,
      'DCode': instance.dcode,
      'SCode': instance.provinceid,
      'DistrictName': instance.name
    };
