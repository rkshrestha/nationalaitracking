import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/AIDao.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalDao.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/CalvingDao.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/PDDao.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:uuid/uuid.dart';

class CalvingRepo implements Repository<Calving> {
  SQFliteDatabase sqFliteDatabase;
  CalvingDao calvingDao;
  AnimalDao animalDao;
  AIBloc aiBloc;
  AIDao aiDao;
  PDDao pdDao;
  List<ArtificialInsemination> aiListData;
  List<Calving> calvingData;
  List<PregnancyDiagnosis> pdListData;
  SessionManager sessionManager;
  BuildContext context;
  List<Calving> calvingList;
  int sCalvingId;
  String latestDate = "";
  String codeFromList = "";
  String mobileFromList = "";
  String englishExpectedDates = "";

  CalvingRepo(context) {
    Logger.v("inside calvingRepo");
    init();
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
  }

  void saveCalving(context, Calving calving, Farmer farmer, Animal animal,
      int speciesId, int breedId, StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try saveCalving');

      calvingDao = await sqFliteDatabase.calvingDao();
      animalDao = await sqFliteDatabase.animalDao();
      var user = this.sessionManager.findOne().user;
      Logger.v('check calving add ');
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);

      aiDao = await sqFliteDatabase.aiDao();
      if (calving.speciesName == "Buffalo" || calving.speciesName == "Cattle") {
        aiListData = await aiDao.findForAddingCalving(calving, animal);
      } else if (calving.speciesName == "Goat" ||
          calving.speciesName == "Pig") {
        aiListData = await aiDao.findForAddingCalvingGoatPig(calving, animal);
        Logger.v("testAI"+DsonUtils.toJsonString(aiListData));
      }

      pdDao = await sqFliteDatabase.pdDao();
      pdListData = await pdDao.findForAddingCalfFromPDReport(calving, animal);

      DateTime todays = new DateTime.now();
      String today = DateFormat('MM-dd-yyyy').format(todays);
      Calving cal = await calvingDao.findBySameDateAndAnimal(calving, animal);
      Animal animals =
          await animalDao.findBySameSpeciesCodeForCalving(calving.calfCode);

      var uuid = new Uuid();
      var uid = uuid.v1();

      Logger.v('after date');
//list more than 0

      var aUuid = uuid.v1();
      if (aiListData.length > 0) {
        Logger.v("counrAILIst" + aiListData.length.toString());
        codeFromList = aiListData[0].speciesCode;
        mobileFromList = aiListData[0].mobileNumber;
        latestDate = aiListData[0].englishAiDate;
        Logger.v("lastest Date: " + latestDate.toString());
        String test = aiListData[0].aiDate;
//        Logger.v('check  lastest ');
//        Logger.v("code:" + codeFromList);
//        Logger.v("mobileFromList:" + mobileFromList);
//        Logger.v("latestDate" + latestDate);
//        Logger.v("latestNepaliDate" + test);

        NepaliDateTime nepaliCalvingDate =
            NepaliDateTime.tryParse(calving.calvingDate);
//        Logger.v('after nepali date');
        DateTime englishDate = DateConverter.toAD(nepaliCalvingDate);
//        Logger.v('after nepali datecounrAILIst2');
        String formattedEnglishDate =
            englishDate.toIso8601String().split("T").first;

        DateTime aiDbDate = DateTime.tryParse(latestDate);

//        NepaliDateTime aiDbDatess = NepaliDateTime.tryParse(test);

        Logger.v(
            "getdifer" + englishDate.difference(aiDbDate).inDays.toString());

//        Logger.v("getdifernepali" +
//            nepaliCalvingDate.difference(aiDbDatess).inDays.toString());

        if (animals != null) {
          sSink.add(SCResponse.error(
            "Calf Code should not be same as of Animal code!!",
//            AppTranslations.of(context).text("animal_exist_for_calf"),
          ));
        } else if (cal != null) {
          sSink.add(SCResponse.error(
            "Calving with same date and animal already exist!!",
//            AppTranslations.of(context).text("calving_exist"),
          ));
        } else if (calving.speciesName == "Cattle" &&
            (englishDate.difference(aiDbDate).inDays) < 285) {
          sSink.add(SCResponse.error(
//            AppTranslations.of(context).text("calving_exist"),
              "Cattle cannot be added, the difference of ai date and calving date must be 285 days"));
        } else if (calving.speciesName == "Buffalo" &&
            (englishDate.difference(aiDbDate).inDays) < 300) {
          sSink.add(SCResponse.error(
//            AppTranslations.of(context).text("calving_exist"),
              "Buffalo cannot be added, the difference of ai date and calving date must be 300 days"));
        } else if (calving.speciesName == "Goat" &&
            (englishDate.difference(aiDbDate).inDays) < 145) {
          sSink.add(SCResponse.error(
//            AppTranslations.of(context).text("calving_exist"),
              "Goat cannot be added, the difference of ai date and calving date must be 145 days"));
        } else if (calving.speciesName == "Pig" &&
            (englishDate.difference(aiDbDate).inDays) < 118) {
          sSink.add(SCResponse.error(
//            AppTranslations.of(context).text("calving_exist"),
              "Pig cannot be added, the difference of ai date and calving date must be 118 days"));
        } else {
          int calvingId = await calvingDao.persist(new Calving.make(
              uid,
//              calving.cId,
              null,
              farmer.farmerId,
              animal.animalId,
              animal.speciesId,
              animal.breedId,
              calving.calvingDate,
              calving.calfSex,
              calving.vetAssist,
              calving.calfCode,
              calving.remarks,
              today,
              null,
              calving.mobileNumber,
              calving.speciesCode,
              user.userid,
              null,
              farmer.fId,
              animal.aId,
              user.userid,
              calving.farmerName,
              formattedEnglishDate,
              null,
              calving.speciesName,
              0,
              syncToServer: false));

//          if (calving.calfSex == "Female") {
//            Logger.v("test for add animal");
//          Logger.v("FarmerId:" + farmer.fId.toString());
          Logger.v("uuidsCalving"+uid);

          var animalCid = await calvingDao.findByAnimalIdCalf(uid);
          if(animalCid !=null){
            Logger.v("id:"+animalCid.cId);
            await animalDao.persist(new Animal.make(
                animalCid.cId,
                0,
                farmer.fId,
                animal.speciesId,
                animal.speciesName,
                animal.breedId,
                animal.breedName,
                calving.calfCode,
                calving.mobileNumber,
                today,
                null,
                user.userid,
                null,
                farmer.farmerId,
                user.userid,
                calving.farmerName,
                calving.calfSex,
                null,
                status: true,
                syncToServer: false));
          }

//          await animalDao.persist(new Animal.make(
//              aUuid,
//              0,
//              farmer.fId,
//              animal.speciesId,
//              animal.speciesName,
//              animal.breedId,
//              animal.breedName,
//              calving.calfCode,
//              calving.mobileNumber,
//              today,
//              null,
//              user.userid,
//              null,
//              farmer.farmerId,
//              user.userid,
//              calving.farmerName,
//              calving.calfSex,
//              null,
//              status: true,
//              syncToServer: false));
//          } else {
//            sSink.add(SCResponse.error(
//              "Calf Code should not be same as of Animal code!!",
////              AppTranslations.of(context).text("animal_exist_for_calf"),
//            ));
//          }
          Navigator.pop(context, true);
          sSink.add(SCResponse.success(
            "Calving added successfully !!",
//            AppTranslations.of(context).text("calving_add_success"),
          ));
        }

        //for list == 0
      } else {
        NepaliDateTime nepaliCalvingDate =
            NepaliDateTime.tryParse(calving.calvingDate);
        Logger.v('after nepali date');
        DateTime englishDate = DateConverter.toAD(nepaliCalvingDate);
        Logger.v('after nepali date2');
        String formattedEnglishDate =
            englishDate.toIso8601String().split("T").first;

        if (animals != null) {
          sSink.add(SCResponse.error(
            "Calf Code should not be same as of Animal code!!",
//            AppTranslations.of(context).text("animal_exist_for_calf"),
          ));
        } else if (cal != null) {
          sSink.add(SCResponse.error(
            "Calving with same date and animal already exist!!",
//            AppTranslations.of(context).text("calving_exist"),
          ));
        } else {
          int calvingId = await calvingDao.persist(new Calving.make(
              uid,
//              calving.cId,
              null,
              farmer.farmerId,
              animal.animalId,
              animal.speciesId,
              animal.breedId,
              calving.calvingDate,
              calving.calfSex,
              calving.vetAssist,
              calving.calfCode,
              calving.remarks,
              today,
              null,
              calving.mobileNumber,
              calving.speciesCode,
              user.userid,
              null,
              farmer.fId,
              animal.aId,
              user.userid,
              calving.farmerName,
              formattedEnglishDate,
              null,
              calving.speciesName,
              0,
              syncToServer: false));

//          if (calving.calfSex == "Female") {
//            Logger.v("test for add animal");

          var animalCid = await calvingDao.findByAnimalIdCalf(uid);
          if(animalCid !=null){
            Logger.v("cid:"+animalCid.cId);

            await animalDao.persist(new Animal.make(
                animalCid.cId,
                0,
                farmer.fId,
                animal.speciesId,
                animal.speciesName,
                animal.breedId,
                animal.breedName,
                calving.calfCode,
                calving.mobileNumber,
                today,
                null,
                user.userid,
                null,
                farmer.farmerId,
                user.userid,
                calving.farmerName,
                calving.calfSex,
                null,
                status: true,
                syncToServer: false));
          }


//          await animalDao.persist(new Animal.make(
//              aUuid,
//              0,
//              farmer.fId,
//              animal.speciesId,
//              animal.speciesName,
//              animal.breedId,
//              animal.breedName,
//              calving.calfCode,
//              calving.mobileNumber,
//              today,
//              null,
//              user.userid,
//              null,
//              farmer.farmerId,
//              user.userid,
//              calving.farmerName,
//              calving.calfSex,
//              null,
//              status: true,
//              syncToServer: false));
//          } else if (calving.calfSex == "Male") {
//          } else {
//            sSink.add(SCResponse.error(
//              "Calf Code should not be same as of Animal code!!",
//              AppTranslations.of(context).text("animal_exist_for_calf"),
//            ));
//          }
          Navigator.pop(context, true);
          sSink.add(SCResponse.success(
            "Calving added successfully !!",
//            AppTranslations.of(context).text("calving_add_success"),
          ));
        }
      }

//
//      if (animals != null) {
//        sSink.add(SCResponse.error(
//          AppTranslations.of(context).text("animal_exist_for_calf"),
//        ));
//      } else if (cal == null) {
//        int calvingId = await calvingDao.persist(new Calving.make(
//            calving.cId,
//            null,
//            farmer.farmerId,
//            animal.animalId,
//            animal.speciesId,
//            animal.breedId,
//            calving.calvingDate,
//            calving.calfSex,
//            calving.vetAssist,
//            calving.calfCode,
//            calving.remarks,
//            today.toIso8601String(),
//            null,
//            calving.mobileNumber,
//            calving.speciesCode,
//            user.userid,
//            null,
//            farmer.fId,
//            animal.aId,
//            user.userid,
//            calving.farmerName,
//            formattedEnglishDate,
//            null,
//            syncToServer: false));
//
//        if (calving.calfSex == "Female") {
//          Logger.v("test for add animal");
////          Logger.v("FarmerId:" + farmer.fId.toString());
//          await animalDao.persist(new Animal.addCalving(
//              0,
//              farmer.fId,
//              animal.speciesId,
//              animal.speciesName,
//              animal.breedId,
//              animal.breedName,
//              calving.calfCode,
//              calving.mobileNumber,
//              today.toIso8601String(),
//              null,
//              user.userid,
//              null,
//              farmer.farmerId,
//              user.userid,
//              calving.farmerName,
//              null,
//              status: true,
//              syncToServer: false));
//        } else {
//          sSink.add(SCResponse.error(
//            AppTranslations.of(context).text("animal_exist_for_calf"),
//          ));
//        }
//        Navigator.pop(context, true);
//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("calving_add_success"),
//        ));
//      } else {
//        sSink.add(SCResponse.error(
//          AppTranslations.of(context).text("calving_exist"),
//        ));
//      }

    } catch (ex) {
      sSink.add(SCResponse.error(
        "Couldn't add Calving!!",
//        AppTranslations.of(context).text("calving_add_fail"),
      ));
//      sSink.add(SCResponse.error("Couldn't add calving"));
      Logger.v(ex.toString());
    }
  }

  void getCalvingList(StreamSink<SCResponse<List<Calving>>> stSink) async {
    try {
      calvingDao = await sqFliteDatabase.calvingDao();

      calvingList = await calvingDao.findAll();

      for (Calving calving in calvingList) {
        Logger.v(calving.calvingDate);
      }

      stSink.add(SCResponse.success(calvingList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getUnSyncCalvingList(
      StreamSink<SCResponse<List<Calving>>> stSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      calvingDao = await sqFliteDatabase.calvingDao();

      calvingList = await calvingDao.findAllUnSyncData();

      for (Calving calving in calvingList) {
        Logger.v(calving.calvingDate);
      }

      stSink.add(SCResponse.success(calvingList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSyncCalvingList(StreamSink<SCResponse<List<Calving>>> stSink) async {
    try {
      calvingDao = await sqFliteDatabase.calvingDao();

      calvingList = await calvingDao.findAllSyncData();

      for (Calving calving in calvingList) {
        Logger.v(calving.calvingDate);
      }

      stSink.add(SCResponse.success(calvingList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void updateCalving(context, Calving calving, Farmer farmer, Animal animal,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try updateCalving' + animal.breedName);

      calvingDao = await sqFliteDatabase.calvingDao();
      animalDao = await sqFliteDatabase.animalDao();
      var user = this.sessionManager.findOne().user;

      DateTime today = new DateTime.now();
      DateTime todayNepaliDate = new DateTime(today.year + 56, today.month + 8,
          today.day + 17, today.hour, today.minute, today.second);
      NepaliDateTime nepaliCalvingDate =
          NepaliDateTime.tryParse(calving.calvingDate);
      DateTime englishDate = DateConverter.toAD(nepaliCalvingDate);
      String formattedEnglishDate =
          englishDate.toIso8601String().split("T").first;
      Logger.v("formattedEnglishDate" + formattedEnglishDate);
      Logger.v('try updateCalvingsssss' + animal.breedName);

      int aiUpdate = await calvingDao.update(new Calving.make(
          calving.cId,
          calving.calvingId,
          farmer.farmerId,
          animal.animalId,
          animal.speciesId,
          animal.breedId,
          calving.calvingDate,
          calving.calfSex,
          calving.vetAssist,
          calving.calfCode,
          calving.remarks,
//          todayNepaliDate.toIso8601String(),
          calving.createdDate,
          today.toIso8601String(),
          calving.mobileNumber,
          calving.speciesCode,
          user.userid,
          user.userid,
          farmer.fId,
          animal.aId,
          user.userid,
          calving.farmerName,
          formattedEnglishDate,
          calving.syncDate,
          calving.speciesName,
          calving.calfAnimalId,
          syncToServer: false));

//      var animalss = await animalDao.getAnimalId(calving.calfCode);

      var animalss = await animalDao.findByAnimalIdCalf(calving);
//      var animalss = await calvingDao.findByAnimalIdCalf(calving);
      if (animalss != null) {
        Logger.v("animal Updated");
        await animalDao.updateAnimalFromCalving(farmer, animal, calving);
      }

//      if (animalss != null) {
////        var animalDetails = await animalDao.findByAnimalIdCalf(calving.mAnimalId);
////      if((animalss.calfSex = calving.calfSex) == "Female"){
//        Logger.v("animalssSex"+animalss.calfSex);
//        Logger.v("acSex"+calving.calfSex);
//        if ((animalss.calfSex = calving.calfSex) == "Male") {
//        } else if ((animalss.calfSex != calving.calfSex)) {
//          if (calving.calfSex == "Female") {
//            DateTime todaysDate = new DateTime.now();
//            String todayDate = DateFormat('MM-dd-yyyy').format(todaysDate);
//            var uuid = new Uuid();
//            var auid = uuid.v1();
//
//            await animalDao.persist(new Animal.make(
//                auid,
//                0,
//                farmer.fId,
//                animal.speciesId,
//                animal.speciesName,
//                animal.breedId,
//                animal.breedName,
//                calving.calfCode,
//                calving.mobileNumber,
//                todayDate,
//                null,
//                user.userid,
//                null,
//                farmer.farmerId,
//                user.userid,
//                calving.farmerName,
//                null,
//                status: true,
//                syncToServer: false));
//          } else if (animalss.calfSex == "Female" && calving.calfSex == "Female") {
//            await animalDao.updateAnimalFromCalving(farmer, animal, calving);
//          } else {
//            sSink.add(SCResponse.error(
//              "Calf Code should not be same as of Animal code!!",
////          AppTranslations.of(context).text("animal_exist_for_calf"),
//            ));
//          }
//
//          if (calving.calfSex == "Male") {
//            await animalDao.updateAnimalHavingMale(farmer, animal, calving);
//          }
//        }

//        if (animalss.calfSex == "Female" ) {
////          if (calving.calfSex == "Female") {
//            DateTime todaysDate = new DateTime.now();
//            String todayDate = DateFormat('MM-dd-yyyy').format(todaysDate);
//            var uuid = new Uuid();
//            var auid = uuid.v1();
//
//            await animalDao.persist(new Animal.make(
//                auid,
//                0,
//                farmer.fId,
//                animal.speciesId,
//                animal.speciesName,
//                animal.breedId,
//                animal.breedName,
//                calving.calfCode,
//                calving.mobileNumber,
//                todayDate,
//                null,
//                user.userid,
//                null,
//                farmer.farmerId,
//                user.userid,
//                calving.farmerName,
//                null,
//                status: true,
//                syncToServer: false));
////          } else {
////            sSink.add(SCResponse.error(
////              "Calf Code should not be same as of Animal code!!",
//////              AppTranslations.of(context).text("animal_exist_for_calf"),
////            ));
////          }
//
//
//
//
//
//        } else if (calving.calfSex == "Male") {
//          await animalDao.updateAnimalFromCalving(farmer, animal, calving);
//
//          await animalDao.updateAnimalHavingMale(farmer, animal, calving);
//        } else {
//          sSink.add(SCResponse.error(
//            "Calf Code should not be same as of Animal code!!",
////          AppTranslations.of(context).text("animal_exist_for_calf"),
//          ));
//        }
//      } else {}

      Navigator.pop(context, true);
      sSink.add(SCResponse.success(
        "Calving has been successfully updated!!",
//        AppTranslations.of(context).text("calving_update_success"),
      ));
//      sSink.add(SCResponse.success("Calving has been successfully updated"));
    } catch (ex) {
      sSink.add(SCResponse.error(
        "Couldn't update Calving!!",
//        AppTranslations.of(context).text("calving_update_fail"),
      ));
//      sSink.add(SCResponse.error("Couldn't update calving"));
      Logger.v(ex.toString());
    }
  }

  //delete calving
  deleteCalving(
      context, Calving calving, StreamSink<SCResponse<String>> sSink) async {
    try {
      calvingDao = await sqFliteDatabase.calvingDao();

      await calvingDao.delete(calving);

      sSink.add(SCResponse.success(
        "Calving deleted Successfully!!",
//        AppTranslations.of(context).text("calving_delete_success"),
      ));
//      sSink.add(SCResponse.success("Calving deleted Successfully"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("calving_delete_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't delete calving"));
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local start
  void updateCalvingServerIdAndStatus(List<Calving> calvingList,
      StreamSink<SCResponse<List<Calving>>> sSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      Logger.v("Test " + DsonUtils.toJsonString(calvingList));
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      calvingDao = await sqFliteDatabase.calvingDao();
      var user = this.sessionManager.findOne().user;

      for (Calving syncedCalving in calvingList) {
        await calvingDao.updateByDateAndAnimalIdSync(syncedCalving);

        await calvingDao.updateByDateAndAnimalId(syncedCalving);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local end

  //calving send to server
  void sendCalvingData(List<Calving> calvingList,
      StreamSink<SCResponse<List<Calving>>> sSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      Logger.v("Test " + DsonUtils.toJsonString(calvingList));
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));

//      List<Calving> unsyncedCalvingList =
//          await calvingDao.findAllBySyncStatus();
      calvingDao = await sqFliteDatabase.calvingDao();
      var user = this.sessionManager.findOne().user;
      SCResponse scResponse = await networkClient.call<Calving, List<Calving>>(
          networkApi.sendCalvingData(calvingList, user.userid),
//          networkApi.sendCalvingData(unsyncedCalvingList, user.userid),
          callable: Calving());

      if (scResponse.status == Status.SUCCESS) {
        List<Calving> syncedCalvingList = scResponse.data;

        for (Calving syncedCalving in syncedCalvingList) {
          await calvingDao.updateByDateAndAnimalIdSync(syncedCalving);

          await calvingDao.updateByDateAndAnimalId(syncedCalving);
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }

      sSink.add(scResponse);
    } catch (ex) {
      Logger.v(ex.toString());
    }
//    finally{
//      sqFliteDatabase.close();
//    }
  }

  //get calving data from server
  void syncGetCalvingData(StreamSink<SCResponse<List<Calving>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager.findOne().user;
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient.call<Calving, List<Calving>>(
          networkApi.syncGetCalvingList(user.userid),
          callable: Calving());

//      if (scResponse.status == Status.SUCCESS) {
//        List<Calving> calvingList = scResponse.data;
//        calvingDao = await sqFliteDatabase.calvingDao();
//
//        for (Calving calvingServer in calvingList) {
//          sCalvingId = calvingServer.farmerId;
//          Logger.v("ServerId " + sCalvingId.toString());
//          Calving calving = await calvingDao.findByCalvingId(sCalvingId);
//
//          if (calving == null) {
//            calvingDao.delete(calvingServer);
//            calvingDao.persist(new Calving.make(
//              calvingServer.cId,
//                calvingServer.calvingId,
//                calvingServer.farmerId,
//                calvingServer.animalId,
//                calvingServer.speciesId,
//                calvingServer.breedId,
//                calvingServer.calvingDate,
//                calvingServer.calfSex,
//                calvingServer.vetAssist,
//                calvingServer.calfCode,
//                calvingServer.userId,
//                calvingServer.remarks));
//          } else {
//            if (calving.cId == calvingServer.cId) {
//              calvingDao.update(Calving.make(
//                  calvingServer.cId,
//                  calvingServer.calvingId,
//                  calvingServer.farmerId,
//                  calvingServer.animalId,
//                  calvingServer.speciesId,
//                  calvingServer.breedId,
//                  calvingServer.calvingDate,
//                  calvingServer.calfSex,
//                  calvingServer.vetAssist,
//                  calvingServer.calfCode,
//                  calvingServer.userId,
//                  calvingServer.remarks));
//            }
//          }
//        }
//
//        sSink.add(SCResponse.success("Calving Sync success"));
//      }
//
      if (scResponse.status == Status.SUCCESS) {
        List<Calving> calvingLists = scResponse.data;
        calvingDao = await sqFliteDatabase.calvingDao();
//
        for (Calving calvingServer in calvingLists) {
          sCalvingId = calvingServer.calvingId;
//          Logger.v("ServerId " + sCalvingId.toString());
          Calving calving = await calvingDao.findByCalvingId(sCalvingId);
          if (calving == null) {
            var formattedDate = calvingServer.calvingDate == null
                ? ""
                : calvingServer.calvingDate.split("T")[0];
            var formattedCreatedDate = calvingServer.createdDate == null
                ? ""
                : calvingServer.createdDate.split("T")[0];
            var formattedUpdatedDate = calvingServer.updatedDate == null
                ? ""
                : calvingServer.updatedDate.split("T")[0];
            var formattedEnglishDate = calvingServer.englishCalvingDate == null
                ? ""
                : calvingServer.englishCalvingDate.split("T")[0];

            calvingDao.persist(new Calving.make(
                calvingServer.cId,
                calvingServer.calvingId,
                calvingServer.farmerId,
                calvingServer.animalId,
                calvingServer.speciesId,
                calvingServer.breedId,
                formattedDate,
                calvingServer.calfSex,
                calvingServer.vetAssist,
                calvingServer.calfCode,
                calvingServer.remarks,
                formattedCreatedDate,
                formattedUpdatedDate,
                calvingServer.mobileNumber,
                calvingServer.speciesCode,
                calvingServer.createdBy,
                calvingServer.updatedBy,
                calvingServer.mFarmerId,
                calvingServer.mAnimalId,
                calvingServer.userId,
                calvingServer.farmerName,
                formattedEnglishDate,
                calvingServer.syncDate,
                calvingServer.speciesName,
                calvingServer.calfAnimalId,
                syncToServer: true));
          } else {
//            var formattedDate = calvingServer.calvingDate.split("T")[0];
            calvingDao.updateByServerAIId(calvingServer);
//            calvingDao.updateByServerAIId(new Calving.make(
//                calvingServer.cId,
//                calvingServer.calvingId,
//                calvingServer.farmerId,
//                calvingServer.animalId,
//                calvingServer.speciesId,
//                calvingServer.breedId,
//                formattedDate,
//                calvingServer.calfSex,
//                calvingServer.vetAssist,
//                calvingServer.calfCode,
////                calvingServer.userId,
//                calvingServer.remarks,
//                syncToServer: true));
          }
        }

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Calving Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //get calving by animal
  void getCalvingListByAnimal(
      Animal animal, StreamSink<SCResponse<List<Calving>>> stSink) async {
    try {
      calvingDao = await sqFliteDatabase.calvingDao();

      calvingList = await calvingDao.getCalvingByAnimal(animal);

      for (Calving calving in calvingList) {
        Logger.v(calving.animalId.toString());
      }

      stSink.add(SCResponse.success(calvingList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //unsync list
  void getUnSyncCalvingListByAnimal(
      Animal animal, StreamSink<SCResponse<List<Calving>>> stSink) async {
    try {
      calvingDao = await sqFliteDatabase.calvingDao();

      calvingList = await calvingDao.getUnSyncCalvingByAnimal(animal.aId);

      for (Calving calving in calvingList) {
        Logger.v(calving.animalId.toString());
      }

      stSink.add(SCResponse.success(calvingList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //sync list
  void getSyncCalvingListByAnimal(
      Animal animal, StreamSink<SCResponse<List<Calving>>> stSink) async {
    try {
      calvingDao = await sqFliteDatabase.calvingDao();

      calvingList = await calvingDao.getSyncCalvingByAnimal(animal.aId);

      for (Calving calving in calvingList) {
        Logger.v(calving.animalId.toString());
      }

      stSink.add(SCResponse.success(calvingList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  @override
  void delete(Calving entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<Calving> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  Calving findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Calving findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(Calving entity) {
    // TODO: implement persist
  }

  @override
  void update(Calving entity) {
    // TODO: implement update
  }
}
