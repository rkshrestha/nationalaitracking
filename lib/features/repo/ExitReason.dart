import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'ExitReason.g.dart';

part 'ExitReason.jorm.dart';
@JsonSerializable()
class ExitReason implements JsonCallable<ExitReason>{
  @PrimaryKey(auto: true)
  int eId;
  @JsonKey(name: 'ExitId')
  int ids;
  @JsonKey(name: 'ExitReasonName')
  String name;


  ExitReason();


  ExitReason.make(this.ids, this.name);

  factory ExitReason.fromJson(Map<String, dynamic> json) =>
      _$ExitReasonFromJson(json);

  Map<String, dynamic> toJson() => _$ExitReasonToJson(this);

  @override
  ExitReason fromJson(Map<String, dynamic > json) {
    return ExitReason.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }

}

@GenBean()
class ExitReasonBean extends Bean<ExitReason> with _ExitReasonBean {
  ExitReasonBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'ExitReason';
}
