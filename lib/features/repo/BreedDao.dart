import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Breed.dart';
import 'package:nbis/s/Logger.dart';

class BreedDao implements BaseDao<Breed> {
  static BreedDao _instance;
  BreedBean bean;

  BreedDao._(this.bean);

  static BreedDao get(BreedBean bean) {
    if (_instance == null) {
      _instance = BreedDao._(bean);
    }
    return _instance;
  }


  getBreedBySpeciesId(int speciesId) async {
    Find findBreed = Find(bean.tableName);

    findBreed.where(bean.aids.eq(speciesId));

    return bean.findMany(findBreed);
  }


  @override
  Future<void> delete(Breed entity) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<void> deleteAll() {
  return bean.removeAll();

  }

  @override
  Future<List<Breed>> findAll() {
  Find findBreed = Find(bean.tableName);
  return bean.findMany(findBreed);
  }

  @override
  Future<Breed> findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Future<Breed> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<void> persist(Breed breed) {
    Logger.v("Breed inserted");
    return bean.insert(breed);

  }

  @override
  Future<void> update(Breed entity) {
    // TODO: implement update
    return null;
  }
}
