import 'package:flutter/material.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
//import 'package:nbis/features/repo/SyncModel.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';

class AnimalDao implements BaseDao<Animal> {
  static AnimalDao _instance;
  AnimalBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  AnimalDao._(this.bean);

  static AnimalDao get(AnimalBean bean) {
    if (_instance == null) {
      _instance = AnimalDao._(bean);
    }
    return _instance;

  }

  getAnimalId(String calfCode){
    Find findSpeciesCode = Find(bean.tableName);
    Logger.v("calfCode:"+calfCode);
    findSpeciesCode.where(bean.speciesCode.eq(calfCode));

    return bean.findOne(findSpeciesCode);
  }


  updateAnimalHavingMale(Farmer farmer, Animal animal, Calving calving)async{
    DateTime today = new DateTime.now();
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    final Update update = bean.updater
        .where(bean.aId.eq(calving.mAnimalId));

//    update.set(bean.animalId, calving.calfAnimalId);
//    update.set(bean.fId, farmer.farmerId);
//    update.set(bean.speciesId, animal.speciesId);
//    update.set(bean.speciesName, animal.speciesName);
//    update.set(bean.breedId, animal.breedId);
//    update.set(bean.breedName, animal.breedName);
//    update.set(bean.speciesCode, calving.calfCode);
//    update.set(bean.mobileNumber, calving.mobileNumber);
//    update.set(bean.createdDate, calving.createdDate);
//    update.set(bean.updatedDate, today.toIso8601String(),);
//    update.set(bean.createdBy, user.userid);
//    update.set(bean.updatedBy, user.userid);
//    update.set(bean.farmerName, calving.farmerName);
//    update.set(bean.syncDate, calving.syncDate);
    update.set(bean.status, false);
    update .set(bean.syncToServer, false);

    return await bean.adapter.update(update);

  }


  Future<int> updateAnimalFromCalving(Farmer farmer, Animal animal, Calving calving) async {
    DateTime today = new DateTime.now();
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    final Update update = bean.updater
        .where(bean.aId.eq(calving.cId));
//        .where(bean.mobileNumber.eq(calving.mobileNumber).and(bean.speciesCode.eq(calving.calfCode)));

   update.set(bean.animalId, calving.calfAnimalId);
   update.set(bean.fId, farmer.farmerId);
   update.set(bean.speciesId, animal.speciesId);
   update.set(bean.speciesName, animal.speciesName);
   update.set(bean.breedId, animal.breedId);
   update.set(bean.breedName, animal.breedName);
   update.set(bean.speciesCode, calving.calfCode);
   update.set(bean.mobileNumber, calving.mobileNumber);
   update.set(bean.createdDate, calving.createdDate);
   update.set(bean.updatedDate, today.toIso8601String(),);
   update.set(bean.createdBy, user.userid);
   update.set(bean.updatedBy, user.userid);
   update.set(bean.farmerName, calving.farmerName);
    update.set(bean.gender, calving.calfSex);
    update.set(bean.syncDate, calving.syncDate);
   update.set(bean.status, true);
    update .set(bean.syncToServer, false);

    return await bean.adapter.update(update);
  }

  getAnimalBySpeciesCode(String speciesCode) {
    Find findSpeciesCode = Find(bean.tableName);

    findSpeciesCode.where(bean.speciesCode.eq(speciesCode));

    return bean.findOne(findSpeciesCode);
  }

  getAnimalByFarmer(String mobileNumber) {
    Find findFarmer = Find(bean.tableName);
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;

    findFarmer.where(bean.mobileNumber.eq(mobileNumber).and(bean.status.eq(true))
//        .and(bean.userId.eq(user.userid)));
        .and(bean.gender.eq("Female")).and(bean.userId.eq(user.userid)));

    return bean.findMany(findFarmer);
  }

  getUnSyncAnimalByFarmer(String fId) {
    Find findFarmer = Find(bean.tableName);

    findFarmer.where(bean.mfarmerId.eq(fId).and(bean.syncToServer.eq(false)));

    return bean.findMany(findFarmer);
  }

  getSyncAnimalByFarmer(String fId) {
    Find findFarmer = Find(bean.tableName);

    findFarmer.where(bean.mfarmerId.eq(fId).and(bean.syncToServer.eq(true)));

    return bean.findMany(findFarmer);
  }

  findByAnimalId(int animalId) async {
    Find findByAnimalId = Find(bean.tableName);

    findByAnimalId.where(bean.animalId.eq(animalId));

    return bean.findOne(findByAnimalId);
  }

  findByAnimalIdCalf(Calving calving) async {
    Find findByAnimalId = Find(bean.tableName);

    findByAnimalId.where(bean.aId.eq(calving.cId));

    return bean.findOne(findByAnimalId);
  }
  
//
  

//get animal by id update
  findByaId(String animalId) async {
    Find findaId = Find(bean.tableName);

    findaId.where(bean.aId.eq(animalId));

    return bean.findOne(findaId);
  }

  getAnimalById(String animalId) async {
    Find findaId = Find(bean.tableName);

    findaId.where(bean.aId.eq(animalId));

    return bean.findMany(findaId);
  }



  @override
  Future<void> delete(Animal animal) {
    Logger.v('animalId:${animal.aId}');
    return bean.remove(animal.aId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<Animal>> findAll() {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findAnimal = Find(bean.tableName);
//    findAnimal.where(bean.status.eq(true).and(bean.userId.eq(user.userid)));
    findAnimal.where(bean.status.eq(true).and(bean.userId.eq(user.userid)).and(bean.gender.eq("Female")));
    return bean.findMany(findAnimal);
  }

  @override
  Future<Animal> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<Animal> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(Animal animal) async {
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table animal");
    return await bean.insert(animal);
  }

  @override
  Future<int> update(Animal animal) {
    return bean.update(animal);
  }

  Future<List<Animal>> findAllBySyncStatus() {
    Find findAnimal = Find(bean.tableName);
    findAnimal.where(bean.syncToServer.eq(false));
    return bean.findMany(findAnimal);
  }

//
  Future<int> updateBySpeciesCode(Animal animal) async {
//    DateTime todayDate = DateTime.now();
    final Update update = bean.updater
        .where(bean.speciesCode.eq(animal.speciesCode).and(bean.mobileNumber.eq(animal.mobileNumber)));
        update.set(bean.animalId, animal.animalId);
        update.set(bean.syncDate, animal.syncDate);

    return await bean.adapter.update(update);
  }

//  Future<int> updateFarmerId(Farmer farmer) async {
//    final Update update = bean.updater
//        .where(bean.fId.eq(farmer.fId))
//        .set(bean.fId, farmer.farmerId);
//
//    return await bean.adapter.update(update);
//  }

  Future<int> updateFarmerId(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mobileNumber.eq(farmer.mobileNumber))
        .set(bean.fId, farmer.farmerId);

    return await bean.adapter.update(update);
  }



  Future<int> updateAnimalSyncData(Animal animal) async {
    var formattedCreatedDate = animal.createdDate==null?"":animal.createdDate.split("T")[0];
    var formattedUpdatedDate = animal.updatedDate==null?"":animal.updatedDate.split("T")[0];
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
//    var user = this.sessionManager.findOne().user;
    final Update update =
        bean.updater.where(bean.speciesCode.eq(animal.speciesCode));

    update.set(bean.speciesCode, animal.speciesCode);
    update.set(bean.farmerName, animal.farmerName);
    update.set(bean.aId, animal.aId);
    update.set(bean.animalId, animal.animalId);
    update.set(bean.fId, animal.fId);
    update.set(bean.mfarmerId, animal.mfarmerId);
    update.set(bean.speciesId, animal.speciesId);
    update.set(bean.speciesName, animal.speciesName);
    update.set(bean.breedId, animal.breedId);
    update.set(bean.breedName, animal.breedName);
    update.set(bean.mobileNumber, animal.mobileNumber);
    update.set(bean.createdBy, animal.createdBy);
    update.set(bean.createdDate, formattedCreatedDate);
    update.set(bean.updatedDate, formattedUpdatedDate);
    update.set(bean.updatedBy, animal.updatedBy);
    update.set(bean.userId, animal.userId);
    update.set(bean.gender, animal.gender);
    update.set(bean.syncToServer, true);
    update.set(bean.status, animal.status);
    update.set(bean.syncDate, animal.syncDate);

    return await bean.adapter.update(update);
  }

  Future<int> updateBySpeciesCodeSync(Animal animal) async {
    Logger.v("animalListDetails:"+ DsonUtils.toJsonString(animal));
    final Update update = bean.updater
        .where(bean.speciesCode.eq(animal.speciesCode).and(bean.mobileNumber.eq(animal.mobileNumber)))
        .set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  Future<int> updateByServerAnimalId(Animal animal) async {
    final Update update = bean.updater.where(bean.animalId.eq(animal.animalId));

    return await bean.adapter.update(update);
  }

  findBySameSpeciesCode(Animal animal) async {
    Find findByCode = Find(bean.tableName);
    findByCode.where(bean.mobileNumber.eq(animal.mobileNumber).and(bean.speciesCode.eq(animal.speciesCode)));

    return bean.findOne(findByCode);
  }




  findBySameSpeciesCodeUpdate(Animal animal) async {
    Find findByCode = Find(bean.tableName);
    findByCode.where(bean.mobileNumber.eq(animal.mobileNumber).and(bean.speciesCode.eq(animal.speciesCode)).and(bean.aId.isNot(animal.aId)));

    return bean.findOne(findByCode);
  }


  findBySameSpeciesCodeForCalving(String calfCode) async {
    Find findByCode = Find(bean.tableName);
    findByCode.where(bean.speciesCode.eq(calfCode));

    return bean.findOne(findByCode);
  }


  Future<List<Animal>> findAllSyncData() {
    Find findAnimal = Find(bean.tableName);
    findAnimal.where(bean.syncToServer.eq(true));
    return bean.findMany(findAnimal);
  }

  Future<List<Animal>> findAllUnSyncData() async {
    Logger.v("findSyncData");
//    await bean.createTable(ifNotExists: true);
    Find findAnimal = Find(bean.tableName);
    findAnimal.where(bean.syncToServer.eq(false));
    return await bean.findMany(findAnimal);
  }



  //exit record disable animal
  Future<int> updateForAnimalStatus(String code) async {
    Logger.v("Code"+ code);
    final Update update = bean.updater
        .where(bean.speciesCode.eq(code));
       update.set(bean.status, false);
       update.set(bean.syncToServer, false);

    return await bean.adapter.update(update);
  }

  //update farmer mobile number on edit case
  Future<int> updateFarmerMblNumber(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mfarmerId.eq(farmer.fId));

        update.set(bean.mobileNumber, farmer.mobileNumber);
        update.set(bean.farmerName, farmer.farmerName);
        update.set(bean.syncToServer, false);
//        update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }

}
