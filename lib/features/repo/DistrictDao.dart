import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/District.dart';

class DistrictDao implements BaseDao<District> {
  static DistrictDao _instance;
  DistrictBean bean;

  DistrictDao._(this.bean);

  static DistrictDao get(DistrictBean bean) {
    if (_instance == null) {
      _instance = DistrictDao._(bean);
    }
    return _instance;
  }

  getDistrictByProvinceId(int provinceId) async {
    Find findDistricts = Find(bean.tableName);

    findDistricts.where(bean.provinceid.eq(provinceId));

    return bean.findMany(findDistricts);
  }


  getDistrict(int districtId) async {
    Find findDistrict = Find(bean.tableName);

    findDistrict.where(bean.districtid.eq(districtId));

    return bean.findOne(findDistrict);
  }


  @override
  Future<void> delete(District entity) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<void> deleteAll() {
    return bean.removeAll();
  }

  @override
  Future<List<District>> findAll() {
    Find findDistrict = Find(bean.tableName);
    return bean.findMany(findDistrict);
  }

  @override
  Future<District> findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  Future<District> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<void> persist(District district) {
    return bean.insert(district);
  }

  @override
  Future<void> update(District entity) {
    // TODO: implement update
    return null;
  }
}
