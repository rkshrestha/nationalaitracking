import 'package:flutter/material.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';

class AIDao implements BaseDao<ArtificialInsemination> {
  static AIDao _instance;
  ArtificialInseminationBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  AIDao._(this.bean);

  static AIDao get(ArtificialInseminationBean bean) {
    if (_instance == null) {
      _instance = AIDao._(bean);
    }
    return _instance;
  }

  getAIByAnimal(Animal animal) {
    Find findAnimal = Find(bean.tableName);
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;

    findAnimal.where(bean.mobileNumber.eq(animal.mobileNumber).and(bean
        .speciesCode
        .eq(animal.speciesCode)
        .and(bean.userId.eq(user.userid))));
//    findAnimal.where(bean.aId.eq(animal.aId));
//    findAnimal.where(bean.aId.eq(animal.aId).and(bean.mfarmerId.eq(animal.mfarmerId)));

    return bean.findMany(findAnimal);
  }

//  getAIByAnimal(int aId) {
//    Find findAnimal = Find(bean.tableName);
//
//    findAnimal.where(bean.aId.eq(aId));
//
//    return bean.findMany(findAnimal);
//  }
  getUnSyncAIByAnimal(String aId) {
    Find findAnimal = Find(bean.tableName);

    findAnimal.where(bean.aId.eq(aId).and(bean.syncToServer.eq(false)));

    return bean.findMany(findAnimal);
  }

  getSyncAIByAnimal(String aId) {
    Find findAnimal = Find(bean.tableName);

    findAnimal.where(bean.aId.eq(aId).and(bean.syncToServer.eq(true)));

    return bean.findMany(findAnimal);
  }

  findByAiId(int ainId) async {
    Find findByAiId = Find(bean.tableName);

    findByAiId.where(bean.ainId.eq(ainId));

    return bean.findOne(findByAiId);
  }

  findByAiSerial(ArtificialInsemination ai, Animal animal) {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findAI = Find(bean.tableName);
    findAI
        .where(bean.userId.eq(user.userid).and(bean.mobileNumber
            .eq(ai.mobileNumber)
            .and(bean.speciesCode.eq(ai.speciesCode))))
        .orderBy("ai_serial", false);

    return bean.findMany(findAI);
  }

  findForAddingPd(PregnancyDiagnosis pd, Animal animal) {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findAI = Find(bean.tableName);
    findAI
        .where(bean.userId.eq(user.userid).and(bean.mobileNumber
            .eq(pd.mobileNumber)
            .and(bean.speciesCode.eq(pd.speciesCode))))
        .orderBy("ai_serial", false);

    return bean.findMany(findAI);
  }

  findForAddingCalving(Calving calving, Animal animal) {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findAI = Find(bean.tableName);
//    if (calving.speciesName == "Buffalo" || calving.speciesName == "Cattle") {
      findAI
          .where(bean.userId.eq(user.userid).and(bean.mobileNumber
              .eq(calving.mobileNumber)
              .and(bean.speciesCode.eq(calving.speciesCode))))
          .orderBy("ai_serial", false);
//    } else {
//      findAI.where(bean.userId.eq(user.userid).and(bean.mobileNumber
//          .eq(calving.mobileNumber)
//          .and(bean.speciesCode.eq(calving.speciesCode))));
//    }

    return bean.findMany(findAI);
  }


  //for goat and pig
  findForAddingCalvingGoatPig(Calving calving, Animal animal) {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findAI = Find(bean.tableName);

      findAI.where(bean.userId.eq(user.userid).and(bean.mobileNumber
          .eq(calving.mobileNumber)
          .and(bean.speciesCode.eq(calving.speciesCode)))).orderBy("ai_id", false);


    return bean.findMany(findAI);
  }




  findByAiSerialForPD(PregnancyDiagnosis pd, Animal animal) {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findAI = Find(bean.tableName);
    findAI
        .where(bean.userId.eq(user.userid).and(bean.mobileNumber
            .eq(pd.mobileNumber)
            .and(bean.speciesCode.eq(pd.speciesCode))))
        .orderBy("ai_serial", false);

    return bean.findMany(findAI);
  }

  @override
  Future<void> delete(ArtificialInsemination ai) {
    Logger.v('aiId:${ai.aiId}');
    return bean.remove(ai.aiId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<ArtificialInsemination>> findAll() {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findAI = Find(bean.tableName);
    findAI.where(bean.userId.eq(user.userid));

    return bean.findMany(findAI);
  }

  @override
  Future<ArtificialInsemination> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<ArtificialInsemination> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(ArtificialInsemination ai) async {
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table ai");
    return await bean.insert(ai);
  }

  @override
  Future<int> update(ArtificialInsemination ai) {
    return bean.update(ai);
  }

  Future<List<ArtificialInsemination>> findAllBySyncStatus() {
    Find findAI = Find(bean.tableName);
    findAI.where(bean.syncToServer.eq(false));
    return bean.findMany(findAI);
  }

  //
  Future<int> updateByDateAndAnimalId(ArtificialInsemination ai) async {
//    var formattedDate = ai.aiDate.split(" ")[0];
    var formattedDate = ai.aiDate.split("T")[0];
//    DateTime todayDate = DateTime.now();
//    Logger.v("aId:" + ai.aId.toString() + bean.aId.toString());
//    Logger.v("aId:" + formattedDate + bean.aiDate.toString());
    final Update update = bean.updater
        .where(bean.aiDate.eq(formattedDate))
        .and(bean.aId.eq(ai.aId));

    update.set(bean.ainId, ai.ainId);
    update.set(bean.syncDate, ai.syncDate);

    return await bean.adapter.update(update);
  }

  Future<int> updateByDateAndAnimalIdSync(
      ArtificialInsemination syncedAI) async {
    var formattedDate = syncedAI.aiDate.split("T")[0];
//    var formattedDate = syncedAI.aiDate.split(" ")[0];
    Logger.v("dateTime:" + formattedDate);
    final Update update = bean.updater
        .where(bean.aiDate.eq(formattedDate))
        .and(bean.aId.eq(syncedAI.aId))
        .set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  Future<int> updateByServerAIId(ArtificialInsemination ai) async {
    var formattedCreatedDate =
        ai.createdDate == null ? "" : ai.createdDate.split("T")[0];
    var formattedUpdatedDate =
        ai.updatedDate == null ? "" : ai.updatedDate.split("T")[0];
    final Update update =
        bean.updater.where(bean.aiDate.eq(ai.aiDate).and(bean.aId.eq(ai.aId)));
    var formattedDate = ai.aiDate.split("T")[0];
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
//    var user = this.sessionManager.findOne().user;
    update.set(bean.aId, ai.aId);
    update.set(bean.sAnimalId, ai.sAnimalId);
    update.set(bean.mfarmerId, ai.mfarmerId);
    update.set(bean.sfarmerId, ai.sfarmerId);
    update.set(bean.aiDate, formattedDate);
    update.set(bean.mobileNumber, ai.mobileNumber);
    update.set(bean.speciesCode, ai.speciesCode);
    update.set(bean.currentParity, ai.currentParity);
    update.set(bean.aiSerial, ai.aiSerial);
    update.set(bean.remarks, ai.remarks);
    update.set(bean.speciesId, ai.speciesId);
    update.set(bean.breedId, ai.breedId);
    update.set(bean.ainId, ai.ainId);
    update.set(bean.aiId, ai.aiId);
    update.set(bean.createdDate, formattedCreatedDate);
    update.set(bean.updatedDate, formattedUpdatedDate);
    update.set(bean.createdBy, ai.createdBy);
    update.set(bean.updatedBy, ai.updatedBy);
    update.set(bean.shireId, ai.shireId);
    update.set(bean.userId, ai.userId);
    update.set(bean.userId, ai.userId);
    update.set(bean.englishAiDate, ai.englishAiDate);
    update.set(bean.englishTentativePDDate, ai.englishTentativePDDate);
    update.set(bean.tentativePDDate, ai.tentativePDDate);
    update.set(bean.farmerName, ai.farmerName);
    update.set(bean.syncDate, ai.syncDate);
    update.set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  findBySameDateAndAnimal(ArtificialInsemination ai, Animal animal) async {
    var formattedDate = ai.aiDate.split("T")[0];
    Find findByDateAndAnimal = Find(bean.tableName);

    findByDateAndAnimal.where(bean.aiDate
        .eq(formattedDate)
        .and(bean.speciesCode.eq(ai.speciesCode))
        .and(bean.mobileNumber.eq(ai.mobileNumber)));

    return bean.findOne(findByDateAndAnimal);
  }

  findByDifferenceParityAndSerial(
      ArtificialInsemination ai, Animal animal) async {
    Find findByDateAndAnimal = Find(bean.tableName);

    findByDateAndAnimal.where((bean.speciesCode.eq(animal.speciesCode))
        .and(bean.aiSerial.eq(ai.aiSerial))
        .and(bean.mobileNumber.eq(ai.mobileNumber)));

    return bean.findOne(findByDateAndAnimal);
  }

  findBySerialIncrease(ArtificialInsemination ai, Animal animal) async {
//    var formattedDate = ai.aiDate.split("T")[0];
    Find checkByserial = Find(bean.tableName);

    checkByserial.where(
        (bean.aId.eq(animal.aId)).and(bean.aiSerial.eq(ai.aiSerial - 1)));

    return bean.findOne(checkByserial);
  }

//  findByDateDifference(
//      ArtificialInsemination ai, Animal animal) async {
//    var formattedDate = ai.aiDate.split("T")[0];
//    Find findByDateAndAnimal = Find(bean.tableName);
//
//    DateTime dateCheck = DateTime.tryParse(formattedDate);
//    DateTime date2 = DateTime.tryParse(bean.aiDate.toString());
//    final difference = date2.difference(dateCheck).inDays !=20;
//
//    findByDateAndAnimal
//        .where(difference.and(bean.aId.eq(ai.aId)));
//
//    return bean.findOne(findByDateAndAnimal);
//  }

  Future<List<ArtificialInsemination>> findAllSyncData() {
    Find findArtificialInsemination = Find(bean.tableName);
    findArtificialInsemination.where(bean.syncToServer.eq(true));
    return bean.findMany(findArtificialInsemination);
  }

  Future<List<ArtificialInsemination>> findAllUnSyncData() {
    Find findArtificialInsemination = Find(bean.tableName);
    findArtificialInsemination.where(bean.syncToServer.eq(false));
    return bean.findMany(findArtificialInsemination);
  }

  //update farmer server id
  Future<int> updateFarmerIdInAI(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mobileNumber.eq(farmer.mobileNumber))
        .set(bean.sfarmerId, farmer.farmerId);

    return await bean.adapter.update(update);
  }

  //update animal server id
  Future<int> updateAnimalIdInAI(Animal animal) async {
    final Update update = bean.updater
        .where(bean.aId.eq(animal.aId))
        .set(bean.sAnimalId, animal.animalId);

    return await bean.adapter.update(update);
  }

  //update farmer mobile number on edit case
  Future<int> updateFarmerMblNumber(Farmer farmer) async {
    Update update = bean.updater.where(bean.mfarmerId.eq(farmer.fId));

    update.set(bean.mobileNumber, farmer.mobileNumber);
    update.set(bean.farmerName, farmer.farmerName);
    update.set(bean.syncToServer, false);
//    update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }

  //update animal species code on edit case
  Future<int> updateAnimalSpeciesCode(Animal animal) async {
    final Update update = bean.updater.where(bean.aId.eq(animal.aId));

    update.set(bean.speciesCode, animal.speciesCode);
    update.set(bean.speciesId,animal.speciesId);
    update.set(bean.breedId,animal.breedId);
    update.set(bean.syncToServer, false);
//    update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }
}
