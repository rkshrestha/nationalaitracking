class JsonCallable<T>{
  T fromJson(Map<String, dynamic> json){}
  Map<String, dynamic> toJson(){}
  String toJsonString(){}
}