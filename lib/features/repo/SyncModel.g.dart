// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SyncModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SyncModel _$SyncModelFromJson(Map<String, dynamic> json) {
  return SyncModel()
    ..farmers = (json['Farmers'] as List)
        ?.map((e) =>
            e == null ? null : Farmer.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..animals = (json['Animals'] as List)
        ?.map((e) =>
            e == null ? null : Animal.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..artificialInseminations = (json['ArtificialInseminations'] as List)
        ?.map((e) => e == null
            ? null
            : ArtificialInsemination.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..pregnancyDiagnoses = (json['PregnancyDiagnoses'] as List)
        ?.map((e) => e == null
            ? null
            : PregnancyDiagnosis.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..calvings = (json['Calvings'] as List)
        ?.map((e) =>
            e == null ? null : Calving.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..exitRecords = (json['ExitRecords'] as List)
        ?.map((e) =>
            e == null ? null : ExitRecord.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..liquidNitrogens = (json['LiquidNitrogens'] as List)
        ?.map((e) => e == null
            ? null
            : LiquidNitrogen.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..semen = (json['Semen'] as List)
        ?.map(
            (e) => e == null ? null : Semen.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..errorMsg = json['ErrorMessage'] as String;
}

Map<String, dynamic> _$SyncModelToJson(SyncModel instance) => <String, dynamic>{
      'Farmers': instance.farmers,
      'Animals': instance.animals,
      'ArtificialInseminations': instance.artificialInseminations,
      'PregnancyDiagnoses': instance.pregnancyDiagnoses,
      'Calvings': instance.calvings,
      'ExitRecords': instance.exitRecords,
      'LiquidNitrogens': instance.liquidNitrogens,
      'Semen': instance.semen,
      'ErrorMessage': instance.errorMsg
    };
