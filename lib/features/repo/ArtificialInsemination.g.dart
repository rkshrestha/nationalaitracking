// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ArtificialInsemination.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ArtificialInsemination _$ArtificialInseminationFromJson(
    Map<String, dynamic> json) {
  return ArtificialInsemination()
    ..aiId = json['MId'] as String
    ..ainId = json['AiId'] as int
    ..sfarmerId = json['FarmerId'] as int
    ..mobileNumber = json['MobileNo'] as String
    ..speciesCode = json['SpeciesCode'] as String
    ..speciesId = json['SpeciesId'] as int
    ..breedId = json['BreedId'] as int
    ..sAnimalId = json['AnimalId'] as int
    ..aiDate = json['AiDate'] as String
    ..currentParity = json['CurrentParity'] as int
    ..aiSerial = json['AiSerial'] as int
    ..remarks = json['Remarks'] as String
    ..createdDate = json['CreatedDate'] as String
    ..updatedDate = json['UpdatedDate'] as String
    ..createdBy = json['CreatedBy'] as int
    ..updatedBy = json['UpdatedBy'] as int
    ..shireId = json['SireId'] as String
    ..mfarmerId = json['MFarmerId'] as String
    ..aId = json['MAnimalId'] as String
    ..userId = json['UserId'] as int
    ..tentativePDDate = json['TentativePdDate'] as String
    ..farmerName = json['FarmerName'] as String
    ..englishAiDate = json['EnglishAiDate'] as String
    ..englishTentativePDDate = json['EnglishTentativePdDate'] as String
    ..syncDate = json['SyncDate'] as String
    ..syncToServer = json['SyncToServer'] as bool;
}

Map<String, dynamic> _$ArtificialInseminationToJson(
        ArtificialInsemination instance) =>
    <String, dynamic>{
      'MId': instance.aiId,
      'AiId': instance.ainId,
      'FarmerId': instance.sfarmerId,
      'MobileNo': instance.mobileNumber,
      'SpeciesCode': instance.speciesCode,
      'SpeciesId': instance.speciesId,
      'BreedId': instance.breedId,
      'AnimalId': instance.sAnimalId,
      'AiDate': instance.aiDate,
      'CurrentParity': instance.currentParity,
      'AiSerial': instance.aiSerial,
      'Remarks': instance.remarks,
      'CreatedDate': instance.createdDate,
      'UpdatedDate': instance.updatedDate,
      'CreatedBy': instance.createdBy,
      'UpdatedBy': instance.updatedBy,
      'SireId': instance.shireId,
      'MFarmerId': instance.mfarmerId,
      'MAnimalId': instance.aId,
      'UserId': instance.userId,
      'TentativePdDate': instance.tentativePDDate,
      'FarmerName': instance.farmerName,
      'EnglishAiDate': instance.englishAiDate,
      'EnglishTentativePdDate': instance.englishTentativePDDate,
      'SyncDate': instance.syncDate,
      'SyncToServer': instance.syncToServer
    };
