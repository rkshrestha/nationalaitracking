import 'package:flutter/material.dart';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/BaseDao.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';

import 'Farmer.dart';

class PDDao implements BaseDao<PregnancyDiagnosis> {
  static PDDao _instance;
  PregnancyDiagnosisBean bean;
  SessionManager sessionManager;
  SQFliteDatabase sqFliteDatabase;
  BuildContext context;

  PDDao._(this.bean);

  static PDDao get(PregnancyDiagnosisBean bean) {
    if (_instance == null) {
      _instance = PDDao._(bean);
    }
    return _instance;
  }

  getPDByAnimal(Animal animal) {
    Find findPD = Find(bean.tableName);
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager
        .findOne()
        .user;
    Logger.v("getIDs:"+ animal.aId.toString());
    findPD.where(bean.mobileNumber.eq(animal.mobileNumber).and(bean.speciesCode.eq(animal.speciesCode)).and(bean.userId.eq(user.userid)));
//    findPD.where(bean.mAnimalId.eq(animal.aId).and(bean.mobileNumber.eq(animal.mobileNumber)));


    return bean.findMany(findPD);
  }

  getUnSyncPDByAnimal(String aId) {
    Find findPD = Find(bean.tableName);

    findPD.where(bean.mAnimalId.eq(aId).and(bean.syncToServer.eq(false)));

    return bean.findMany(findPD);
  }



  findForAddingCalfFromPDReport(Calving calving, Animal animal) {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager
        .findOne()
        .user;
    Find findPD = Find(bean.tableName);
    findPD.where(bean.userId.eq(user.userid).and(
        bean.mobileNumber.eq(calving.mobileNumber).and(
            bean.speciesCode.eq(calving.speciesCode)))).orderBy("pd_result", false);

    return bean.findMany(findPD);
  }




  getSyncPDByAnimal(String aId) {
    Find findPD = Find(bean.tableName);

    findPD.where(bean.mAnimalId.eq(aId).and(bean.syncToServer.eq(true)));

    return bean.findMany(findPD);
  }

  findByPdId(int spdId) async {
    Find findByPdId = Find(bean.tableName);

    findByPdId.where(bean.spdId.eq(spdId));

    return bean.findOne(findByPdId);
  }

  @override
  Future<void> delete(PregnancyDiagnosis pd) {
    Logger.v('pdId:${pd.pdId}');
    return bean.remove(pd.pdId);
  }

  @override
  Future<void> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<List<PregnancyDiagnosis>> findAll() {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    var user = this.sessionManager.findOne().user;
    Find findPd = Find(bean.tableName);
    findPd.where(bean.userId.eq(user.userid));

    return bean.findMany(findPd);
  }

  @override
  Future<PregnancyDiagnosis> findById(Object id) {
    return bean.find(id);
  }

  @override
  Future<PregnancyDiagnosis> findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  Future<int> persist(PregnancyDiagnosis pd) async {
    await bean.createTable(ifNotExists: true);
    Logger.v("create Table pd");
    return await bean.insert(pd);
  }

  @override
  Future<int> update(PregnancyDiagnosis pd) {
    return bean.update(pd);
  }

  Future<List<PregnancyDiagnosis>> findAllBySyncStatus() {
    Find findPD = Find(bean.tableName);
    findPD.where(bean.syncToServer.eq(false));
    return bean.findMany(findPD);
  }

  Future<int> updateByDateAndAnimalId(PregnancyDiagnosis pd) async {
    var formattedDate = pd.pdDate.split("T")[0];
    Logger.v("dates:"+pd.pdDate);
    DateTime todayDate = DateTime.now();
    final Update update = bean.updater
        .where(bean.pdDate.eq(formattedDate))
        .and(bean.mAnimalId.eq(pd.mAnimalId));
        update.set(bean.spdId, pd.spdId);
        update.set(bean.syncDate, pd.syncDate);

    return await bean.adapter.update(update);
  }

  Future<int> updateByDateAndAnimalIdSync(PregnancyDiagnosis syncedPD) async {
    var formattedDate = syncedPD.pdDate.split("T")[0];
    final Update update = bean.updater
        .where(bean.pdDate.eq(formattedDate))
        .and(bean.mAnimalId.eq(syncedPD.mAnimalId))
        .set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  Future<int> updateByServerPDId(PregnancyDiagnosis pd) async {
    final Update update = bean.updater
        .where(bean.pdDate.eq(pd.pdDate).and(bean.mAnimalId.eq(pd.mAnimalId)));
    var formattedDate = pd.pdDate==null?"": pd.pdDate.split("T")[0];
    var formattedCreatedDate = pd.createdDate==null?"":pd.createdDate.split("T")[0];
    var formattedDDate = pd.expectedDeliveryDate==null?"":pd.expectedDeliveryDate.split("T")[0];
    var formattedUpdatedDDate = pd.updatedDate==null?"":pd.updatedDate.split("T")[0];

    var formattedEnglishDate = pd.englishPdDate==null?"":pd.englishPdDate.split("T").first;
    var formattedExpectedEnglishDate =
    pd.englishExpectedDate==null?"":pd.englishExpectedDate.split("T").first;
    update.set(bean.pdId, pd.pdId);
    update.set(bean.spdId, pd.spdId);
    update.set(bean.farmerId, pd.farmerId);
    update.set(bean.mFarmerId, pd.mFarmerId);
    update.set(bean.animalId, pd.animalId);
    update.set(bean.mAnimalId, pd.mAnimalId);
    update.set(bean.speciesId, pd.speciesId);
    update.set(bean.pdDate, formattedDate);
    update.set(bean.pdResult, formattedDDate);
    update.set(bean.expectedDeliveryDate, pd.expectedDeliveryDate);
    update.set(bean.remarks, pd.remarks);
    update.set(bean.createdDate, formattedCreatedDate);
    update.set(bean.updatedDate, formattedUpdatedDDate);
    update.set(bean.createdBy, pd.createdBy);
    update.set(bean.updatedBy, pd.updatedBy);
    update.set(bean.mobileNumber, pd.mobileNumber);
    update.set(bean.speciesCode, pd.speciesCode);
    update.set(bean.farmerName, pd.farmerName);
    update.set(bean.englishPdDate, formattedEnglishDate);
    update.set(bean.englishExpectedDate, formattedExpectedEnglishDate);
    update.set(bean.userId, pd.userId);
    update.set(bean.syncDate, pd.syncDate);
    update.set(bean.syncToServer, true);

    return await bean.adapter.update(update);
  }

  findBySameDateAndAnimal(PregnancyDiagnosis pd, Animal animal) async {
    var formattedDate = pd.pdDate.split("T")[0];
    Find findByDateAndAnimal = Find(bean.tableName);
    findByDateAndAnimal
        .where(bean.pdDate.eq(formattedDate).and(bean.mAnimalId.eq(animal.aId)).and(bean.mobileNumber.eq(pd.mobileNumber)));

    return bean.findOne(findByDateAndAnimal);
  }

  Future<List<PregnancyDiagnosis>> findAllSyncData() {
    Find findPregnancyDiagnosis = Find(bean.tableName);
    findPregnancyDiagnosis.where(bean.syncToServer.eq(true));
    return bean.findMany(findPregnancyDiagnosis);
  }

  Future<List<PregnancyDiagnosis>> findAllUnSyncData() {
    Find findPregnancyDiagnosis = Find(bean.tableName);
    findPregnancyDiagnosis.where(bean.syncToServer.eq(false));
    return bean.findMany(findPregnancyDiagnosis);
  }

  //update farmer server id
  Future<int> updateFarmerIdInPD(Farmer farmer) async {
    final Update update = bean.updater
        .where(bean.mobileNumber.eq(farmer.mobileNumber))
        .set(bean.farmerId, farmer.farmerId);

    return await bean.adapter.update(update);
  }

  //update animal server id
  Future<int> updateAnimalIdInPD(Animal animal) async {
    final Update update = bean.updater
        .where(bean.mAnimalId.eq(animal.aId))
        .set(bean.animalId, animal.animalId);

    return await bean.adapter.update(update);
  }

  //update farmer mobile number on edit case
  Future<int> updateFarmerMblNumber(Farmer farmer) async {
    final Update update = bean.updater.where(bean.mFarmerId.eq(farmer.fId));
    update.set(bean.mobileNumber, farmer.mobileNumber);
    update.set(bean.farmerName, farmer.farmerName);
    update.set(bean.syncToServer, false);
//    update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }

  //update animal species code on edit case
  Future<int> updateAnimalSpeciesCode(Animal animal) async {
    final Update update = bean.updater.where(bean.mAnimalId.eq(animal.aId));

    update.set(bean.speciesCode, animal.speciesCode);
    update.set(bean.speciesId,animal.speciesId);
    update.set(bean.breedId,animal.breedId);
    update.set(bean.syncToServer, false);
//    update.set(bean.syncDate, null);

    return await bean.adapter.update(update);
  }
}
