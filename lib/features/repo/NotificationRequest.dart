
import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';


part 'NotificationRequest.g.dart';
part 'NotificationRequest.jorm.dart';


@JsonSerializable()
class NotificationRequest implements JsonCallable<NotificationRequest>{
//  @JsonKey(name: 'title')
//  String title;
//  @JsonKey(name: 'body')
//  String body;
//
//  NotificationRequest({this.title, this.body});

//  @JsonKey(name: 'AnimalId')
//  int animalId;
//  @JsonKey(name: 'Message')
//  String message;
//
//  NotificationRequest({this.animalId, this.message});
//
//  NotificationRequest.fromJson(Map<String, dynamic> json) {
//    animalId = json['AnimalId'];
//    message = json['Message'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['AnimalId'] = this.animalId;
//    data['Message'] = this.message;
//    return data;
//  }
  @PrimaryKey(auto: true)
  @JsonKey(name: 'MnId')
  int nId;
  @JsonKey(name: 'Title')
  String title;
  @JsonKey(name: 'Body')
  String body;
  @JsonKey(name: 'Date')
  String notificationDate;
  @JsonKey(name: 'UserId')
  int userId;


  NotificationRequest();


  NotificationRequest.make({this.nId,this.title, this.body,this.notificationDate});
  NotificationRequest.save(this.title, this.body,this.notificationDate,this.userId);

  factory NotificationRequest.fromJson(Map<String, dynamic> json) =>
      _$NotificationRequestFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationRequestToJson(this);

  @override
  NotificationRequest fromJson(Map<String, dynamic> json) {
    return NotificationRequest.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class NotificationRequestBean extends Bean<NotificationRequest> with _NotificationRequestBean {
  NotificationRequestBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'notificationRequest';
}