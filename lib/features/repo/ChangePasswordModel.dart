
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ChangePasswordModel {


  @JsonKey(name: 'UserId')
  int userId;
  @JsonKey(name: 'Password')
  String password;
  @JsonKey(name: 'NewPassword')
  String newPassword;

  ChangePasswordModel({this.userId, this.password, this.newPassword});


  ChangePasswordModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    password = json['Password'];
    newPassword = json['NewPassword'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Password'] = this.password;
    data['NewPassword'] = this.newPassword;
    return data;
  }
}