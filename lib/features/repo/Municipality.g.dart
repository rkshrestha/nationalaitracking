// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Municipality.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Municipality _$MunicipalityFromJson(Map<String, dynamic> json) {
  return Municipality()
    ..mId = json['mId'] as int
    ..municipalityid = json['MunicipalityId'] as int
    ..mcode = json['MCode'] as int
    ..dcode = json['DCode'] as int
    ..scode = json['SCode'] as int
    ..name = json['MunicipalityName'] as String
    ..type = json['Type'] as String;
}

Map<String, dynamic> _$MunicipalityToJson(Municipality instance) =>
    <String, dynamic>{
      'mId': instance.mId,
      'MunicipalityId': instance.municipalityid,
      'MCode': instance.mcode,
      'DCode': instance.dcode,
      'SCode': instance.scode,
      'MunicipalityName': instance.name,
      'Type': instance.type
    };
