// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Breed.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Breed _$BreedFromJson(Map<String, dynamic> json) {
  return Breed()
    ..bId = json['bId'] as int
    ..bids = json['BreedId'] as int
    ..aids = json['AnimalId'] as int
    ..name = json['BreedName'] as String;
}

Map<String, dynamic> _$BreedToJson(Breed instance) => <String, dynamic>{
      'bId': instance.bId,
      'BreedId': instance.bids,
      'AnimalId': instance.aids,
      'BreedName': instance.name
    };
