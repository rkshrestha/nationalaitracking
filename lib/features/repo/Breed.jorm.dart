// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Breed.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _BreedBean implements Bean<Breed> {
  final bId = IntField('b_id');
  final bids = IntField('bids');
  final aids = IntField('aids');
  final name = StrField('name');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        bId.name: bId,
        bids.name: bids,
        aids.name: aids,
        name.name: name,
      };
  Breed fromMap(Map map) {
    Breed model = Breed();
    model.bId = adapter.parseValue(map['b_id']);
    model.bids = adapter.parseValue(map['bids']);
    model.aids = adapter.parseValue(map['aids']);
    model.name = adapter.parseValue(map['name']);

    return model;
  }

  List<SetColumn> toSetColumns(Breed model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.bId != null) {
        ret.add(bId.set(model.bId));
      }
      ret.add(bids.set(model.bids));
      ret.add(aids.set(model.aids));
      ret.add(name.set(model.name));
    } else if (only != null) {
      if (model.bId != null) {
        if (only.contains(bId.name)) ret.add(bId.set(model.bId));
      }
      if (only.contains(bids.name)) ret.add(bids.set(model.bids));
      if (only.contains(aids.name)) ret.add(aids.set(model.aids));
      if (only.contains(name.name)) ret.add(name.set(model.name));
    } else /* if (onlyNonNull) */ {
      if (model.bId != null) {
        ret.add(bId.set(model.bId));
      }
      if (model.bids != null) {
        ret.add(bids.set(model.bids));
      }
      if (model.aids != null) {
        ret.add(aids.set(model.aids));
      }
      if (model.name != null) {
        ret.add(name.set(model.name));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(bId.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(bids.name, isNullable: false);
    st.addInt(aids.name, isNullable: false);
    st.addStr(name.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Breed model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(bId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Breed newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Breed> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Breed model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(bId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Breed newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Breed> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Breed model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.bId.eq(model.bId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Breed> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.bId.eq(model.bId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Breed> find(int bId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.bId.eq(bId));
    return await findOne(find);
  }

  Future<int> remove(int bId) async {
    final Remove remove = remover.where(this.bId.eq(bId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Breed> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.bId.eq(model.bId));
    }
    return adapter.remove(remove);
  }
}
