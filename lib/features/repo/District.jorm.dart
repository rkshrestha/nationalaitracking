// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'District.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _DistrictBean implements Bean<District> {
  final dId = IntField('d_id');
  final districtid = IntField('districtid');
  final dcode = StrField('dcode');
  final provinceid = IntField('provinceid');
  final name = StrField('name');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        dId.name: dId,
        districtid.name: districtid,
        dcode.name: dcode,
        provinceid.name: provinceid,
        name.name: name,
      };
  District fromMap(Map map) {
    District model = District();
    model.dId = adapter.parseValue(map['d_id']);
    model.districtid = adapter.parseValue(map['districtid']);
    model.dcode = adapter.parseValue(map['dcode']);
    model.provinceid = adapter.parseValue(map['provinceid']);
    model.name = adapter.parseValue(map['name']);

    return model;
  }

  List<SetColumn> toSetColumns(District model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.dId != null) {
        ret.add(dId.set(model.dId));
      }
      ret.add(districtid.set(model.districtid));
      ret.add(dcode.set(model.dcode));
      ret.add(provinceid.set(model.provinceid));
      ret.add(name.set(model.name));
    } else if (only != null) {
      if (model.dId != null) {
        if (only.contains(dId.name)) ret.add(dId.set(model.dId));
      }
      if (only.contains(districtid.name))
        ret.add(districtid.set(model.districtid));
      if (only.contains(dcode.name)) ret.add(dcode.set(model.dcode));
      if (only.contains(provinceid.name))
        ret.add(provinceid.set(model.provinceid));
      if (only.contains(name.name)) ret.add(name.set(model.name));
    } else /* if (onlyNonNull) */ {
      if (model.dId != null) {
        ret.add(dId.set(model.dId));
      }
      if (model.districtid != null) {
        ret.add(districtid.set(model.districtid));
      }
      if (model.dcode != null) {
        ret.add(dcode.set(model.dcode));
      }
      if (model.provinceid != null) {
        ret.add(provinceid.set(model.provinceid));
      }
      if (model.name != null) {
        ret.add(name.set(model.name));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(dId.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(districtid.name, isNullable: false);
    st.addStr(dcode.name, isNullable: false);
    st.addInt(provinceid.name, isNullable: false);
    st.addStr(name.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(District model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(dId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      District newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<District> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(District model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(dId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      District newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<District> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(District model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.dId.eq(model.dId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<District> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.dId.eq(model.dId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<District> find(int dId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.dId.eq(dId));
    return await findOne(find);
  }

  Future<int> remove(int dId) async {
    final Remove remove = remover.where(this.dId.eq(dId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<District> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.dId.eq(model.dId));
    }
    return adapter.remove(remove);
  }
}
