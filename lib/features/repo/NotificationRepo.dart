import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/NotificationDao.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';

class NotificationRepo implements Repository<NotificationRequest> {
  SQFliteDatabase sqFliteDatabase;
  NotificationDao notificationDao;
  SessionManager sessionManager;
  List<NotificationRequest> notificationList;
  BuildContext context;
  int slnId;

  NotificationRepo(context) {
    Logger.v("inside notificationRepo");
    init();
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
  }

  void getNotificationList(
      StreamSink<SCResponse<List<NotificationRequest>>> stSink) async {
    try {
      notificationDao = await sqFliteDatabase.notificationRequestDao();

      notificationList = await notificationDao.findNotification();

      for (NotificationRequest notification in notificationList) {
        Logger.v("getNotification" + notification.title);
      }

      stSink.add(SCResponse.success(notificationList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void saveNotificationData(
      StreamSink<SCResponse<List<NotificationRequest>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager.findOne().user;
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient
          .call<NotificationRequest, List<NotificationRequest>>(
              networkApi.getNotification(user.userid),
              callable: NotificationRequest());

      if (scResponse.status == Status.SUCCESS) {
        List<NotificationRequest> notificationList = scResponse.data;
        notificationDao = await sqFliteDatabase.notificationRequestDao();

        for (NotificationRequest notificationRequestServer
            in notificationList) {
          DateTime today = new DateTime.now();
//          DateTime todayDate =
//              new DateTime(today.year + 56, today.month + 8, today.day + 17);
          String todayDate = DateFormat('yyyy-MM-dd').format(today);
//          if (notificationRequestServer == null) {
//          var formattedDate =
//              notificationRequestServer.notificationDate.split("T")[0];
//          notificationDao.deleteAll();

          notificationDao.persist(new NotificationRequest.save(
              notificationRequestServer.title,
              notificationRequestServer.body,
              todayDate,
              user.userid));
//          }
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void saveNotificationPDData(
      StreamSink<SCResponse<List<NotificationRequest>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager.findOne().user;
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient
          .call<NotificationRequest, List<NotificationRequest>>(
              networkApi.getPDNotification(user.userid),
              callable: NotificationRequest());

      if (scResponse.status == Status.SUCCESS) {
        List<NotificationRequest> notificationPDList = scResponse.data;
        notificationDao = await sqFliteDatabase.notificationRequestDao();

        for (NotificationRequest notificationPDRequestServer
            in notificationPDList) {
          DateTime today = new DateTime.now();
//          DateTime todayDate =
//              new DateTime(today.year + 56, today.month + 8, today.day + 17);
          String todayDate = DateFormat('yyyy-MM-dd').format(today);
//          if (notificationRequestServer == null) {
//          var formattedDate =
//              notificationRequestServer.notificationDate.split("T")[0];
          Logger.v("get Date for notification:"+todayDate);
          notificationDao.deleteAll();

          notificationDao.persist(new NotificationRequest.save(
              notificationPDRequestServer.title,
              notificationPDRequestServer.body,
              todayDate,
              user.userid));
//          }
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  @override
  void delete(NotificationRequest entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<NotificationRequest> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  NotificationRequest findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  NotificationRequest findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(NotificationRequest entity) {
    // TODO: implement persist
  }

  @override
  void update(NotificationRequest entity) {
    // TODO: implement update
  }
}
