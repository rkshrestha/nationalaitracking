class Repository<T>{

  void persist(T entity){}


  void update(T entity){}


  T findById(Object id){}

  T findOne(){}


  void delete(T entity){}


  List<T> findAll(){}


  void deleteAll(){}

}