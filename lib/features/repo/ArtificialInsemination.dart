import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'ArtificialInsemination.g.dart';

part 'ArtificialInsemination.jorm.dart';

@JsonSerializable()
class ArtificialInsemination implements JsonCallable<ArtificialInsemination> {
  @PrimaryKey()
  @JsonKey(name: 'MId')
  String aiId;
//  @PrimaryKey(auto: true)
//  @JsonKey(name: 'MId')
//  int aiId;
  @Column(isNullable: true)
  @JsonKey(name: 'AiId')
  int ainId;
  @Column(isNullable: true)
  @JsonKey(name: 'FarmerId')
  int sfarmerId;
  @Column(isNullable: true)
  @JsonKey(name: 'MobileNo')
  String mobileNumber;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesCode')
  String speciesCode;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesId')
  int speciesId;
  @Column(isNullable: true)
  @JsonKey(name: 'BreedId')
  int breedId;
  @Column(isNullable: true)
  @JsonKey(name: 'AnimalId')
  int sAnimalId;
  @Column(isNullable: true)
  @JsonKey(name: 'AiDate')
  String aiDate;
  @Column(isNullable: true)
  @JsonKey(name: 'CurrentParity')
  int currentParity;
  @Column(isNullable: true)
  @JsonKey(name: 'AiSerial')
  int aiSerial;
  @Column(isNullable: true)
  @JsonKey(name: 'Remarks')
  String remarks;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedDate')
  String createdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedDate')
  String updatedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedBy')
  int createdBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedBy')
  int updatedBy;
  @Column(isNullable: true)
  @JsonKey(name: 'SireId')
  String shireId;
  @Column(isNullable: true)
  @JsonKey(name: 'MFarmerId')
  String mfarmerId;
  @Column(isNullable: true)
  @JsonKey(name: 'MAnimalId')
  String aId;
  @Column(isNullable: true)
  @JsonKey(name: 'UserId')
  int userId;
  @Column(isNullable: true)
  @JsonKey(name: 'TentativePdDate')
  String tentativePDDate;
  @Column(isNullable: true)
  @JsonKey(name: 'FarmerName')
  String farmerName;
  @Column(isNullable: true)
  @JsonKey(name: 'EnglishAiDate')
  String englishAiDate;
  @Column(isNullable: true)
  @JsonKey(name: 'EnglishTentativePdDate')
  String englishTentativePDDate;
  @Column(isNullable: true)
  @JsonKey(name: 'SyncDate')
  String syncDate;
  @JsonKey(name: "SyncToServer")
  bool syncToServer = false;

  ArtificialInsemination();

  ArtificialInsemination.make(
      this.aiId,
      this.ainId,
      this.mfarmerId,
      this.mobileNumber,
      this.speciesCode,
      this.speciesId,
      this.breedId,
      this.sAnimalId,
      this.aiDate,
      this.currentParity,
      this.aiSerial,
      this.remarks,
      this.createdDate,
      this.updatedDate,
      this.createdBy,
      this.updatedBy,
      this.shireId,
      this.sfarmerId,
      this.aId,
      this.userId,
      this.tentativePDDate,
      this.farmerName,
      this.englishAiDate,
      this.englishTentativePDDate,
      this.syncDate,
      {this.syncToServer = false});


  factory ArtificialInsemination.fromJson(Map<String, dynamic> json) =>
      _$ArtificialInseminationFromJson(json);

  Map<String, dynamic> toJson() => _$ArtificialInseminationToJson(this);

  @override
  ArtificialInsemination fromJson(Map<String, dynamic> json) {
    return ArtificialInsemination.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class ArtificialInseminationBean extends Bean<ArtificialInsemination>
    with _ArtificialInseminationBean {
  ArtificialInseminationBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'artificial_insemination';
}
