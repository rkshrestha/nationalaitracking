// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Province.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _ProvinceBean implements Bean<Province> {
  final pId = IntField('p_id');
  final provinceid = IntField('provinceid');
  final state = StrField('state');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        pId.name: pId,
        provinceid.name: provinceid,
        state.name: state,
      };
  Province fromMap(Map map) {
    Province model = Province();
    model.pId = adapter.parseValue(map['p_id']);
    model.provinceid = adapter.parseValue(map['provinceid']);
    model.state = adapter.parseValue(map['state']);

    return model;
  }

  List<SetColumn> toSetColumns(Province model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.pId != null) {
        ret.add(pId.set(model.pId));
      }
      ret.add(provinceid.set(model.provinceid));
      ret.add(state.set(model.state));
    } else if (only != null) {
      if (model.pId != null) {
        if (only.contains(pId.name)) ret.add(pId.set(model.pId));
      }
      if (only.contains(provinceid.name))
        ret.add(provinceid.set(model.provinceid));
      if (only.contains(state.name)) ret.add(state.set(model.state));
    } else /* if (onlyNonNull) */ {
      if (model.pId != null) {
        ret.add(pId.set(model.pId));
      }
      if (model.provinceid != null) {
        ret.add(provinceid.set(model.provinceid));
      }
      if (model.state != null) {
        ret.add(state.set(model.state));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(pId.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(provinceid.name, isNullable: false);
    st.addStr(state.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Province model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(pId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Province newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Province> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Province model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(pId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Province newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Province> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Province model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.pId.eq(model.pId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Province> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.pId.eq(model.pId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Province> find(int pId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.pId.eq(pId));
    return await findOne(find);
  }

  Future<int> remove(int pId) async {
    final Remove remove = remover.where(this.pId.eq(pId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Province> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.pId.eq(model.pId));
    }
    return adapter.remove(remove);
  }
}
