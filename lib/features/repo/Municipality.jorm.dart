// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Municipality.dart';

// **************************************************************************
// BeanGenerator
// **************************************************************************

abstract class _MunicipalityBean implements Bean<Municipality> {
  final mId = IntField('m_id');
  final municipalityid = IntField('municipalityid');
  final mcode = IntField('mcode');
  final dcode = IntField('dcode');
  final scode = IntField('scode');
  final name = StrField('name');
  final type = StrField('type');
  Map<String, Field> _fields;
  Map<String, Field> get fields => _fields ??= {
        mId.name: mId,
        municipalityid.name: municipalityid,
        mcode.name: mcode,
        dcode.name: dcode,
        scode.name: scode,
        name.name: name,
        type.name: type,
      };
  Municipality fromMap(Map map) {
    Municipality model = Municipality();
    model.mId = adapter.parseValue(map['m_id']);
    model.municipalityid = adapter.parseValue(map['municipalityid']);
    model.mcode = adapter.parseValue(map['mcode']);
    model.dcode = adapter.parseValue(map['dcode']);
    model.scode = adapter.parseValue(map['scode']);
    model.name = adapter.parseValue(map['name']);
    model.type = adapter.parseValue(map['type']);

    return model;
  }

  List<SetColumn> toSetColumns(Municipality model,
      {bool update = false, Set<String> only, bool onlyNonNull = false}) {
    List<SetColumn> ret = [];

    if (only == null && !onlyNonNull) {
      if (model.mId != null) {
        ret.add(mId.set(model.mId));
      }
      ret.add(municipalityid.set(model.municipalityid));
      ret.add(mcode.set(model.mcode));
      ret.add(dcode.set(model.dcode));
      ret.add(scode.set(model.scode));
      ret.add(name.set(model.name));
      ret.add(type.set(model.type));
    } else if (only != null) {
      if (model.mId != null) {
        if (only.contains(mId.name)) ret.add(mId.set(model.mId));
      }
      if (only.contains(municipalityid.name))
        ret.add(municipalityid.set(model.municipalityid));
      if (only.contains(mcode.name)) ret.add(mcode.set(model.mcode));
      if (only.contains(dcode.name)) ret.add(dcode.set(model.dcode));
      if (only.contains(scode.name)) ret.add(scode.set(model.scode));
      if (only.contains(name.name)) ret.add(name.set(model.name));
      if (only.contains(type.name)) ret.add(type.set(model.type));
    } else /* if (onlyNonNull) */ {
      if (model.mId != null) {
        ret.add(mId.set(model.mId));
      }
      if (model.municipalityid != null) {
        ret.add(municipalityid.set(model.municipalityid));
      }
      if (model.mcode != null) {
        ret.add(mcode.set(model.mcode));
      }
      if (model.dcode != null) {
        ret.add(dcode.set(model.dcode));
      }
      if (model.scode != null) {
        ret.add(scode.set(model.scode));
      }
      if (model.name != null) {
        ret.add(name.set(model.name));
      }
      if (model.type != null) {
        ret.add(type.set(model.type));
      }
    }

    return ret;
  }

  Future<void> createTable({bool ifNotExists = false}) async {
    final st = Sql.create(tableName, ifNotExists: ifNotExists);
    st.addInt(mId.name, primary: true, autoIncrement: true, isNullable: false);
    st.addInt(municipalityid.name, isNullable: false);
    st.addInt(mcode.name, isNullable: false);
    st.addInt(dcode.name, isNullable: false);
    st.addInt(scode.name, isNullable: false);
    st.addStr(name.name, isNullable: false);
    st.addStr(type.name, isNullable: false);
    return adapter.createTable(st);
  }

  Future<dynamic> insert(Municipality model,
      {bool cascade = false,
      bool onlyNonNull = false,
      Set<String> only}) async {
    final Insert insert = inserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(mId.name);
    var retId = await adapter.insert(insert);
    if (cascade) {
      Municipality newModel;
    }
    return retId;
  }

  Future<void> insertMany(List<Municipality> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = models
        .map((model) =>
            toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .toList();
    final InsertMany insert = inserters.addAll(data);
    await adapter.insertMany(insert);
    return;
  }

  Future<dynamic> upsert(Municipality model,
      {bool cascade = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Upsert upsert = upserter
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull))
        .id(mId.name);
    var retId = await adapter.upsert(upsert);
    if (cascade) {
      Municipality newModel;
    }
    return retId;
  }

  Future<void> upsertMany(List<Municipality> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
    }
    final UpsertMany upsert = upserters.addAll(data);
    await adapter.upsertMany(upsert);
    return;
  }

  Future<int> update(Municipality model,
      {bool cascade = false,
      bool associate = false,
      Set<String> only,
      bool onlyNonNull = false}) async {
    final Update update = updater
        .where(this.mId.eq(model.mId))
        .setMany(toSetColumns(model, only: only, onlyNonNull: onlyNonNull));
    return adapter.update(update);
  }

  Future<void> updateMany(List<Municipality> models,
      {bool onlyNonNull = false, Set<String> only}) async {
    final List<List<SetColumn>> data = [];
    final List<Expression> where = [];
    for (var i = 0; i < models.length; ++i) {
      var model = models[i];
      data.add(
          toSetColumns(model, only: only, onlyNonNull: onlyNonNull).toList());
      where.add(this.mId.eq(model.mId));
    }
    final UpdateMany update = updaters.addAll(data, where);
    await adapter.updateMany(update);
    return;
  }

  Future<Municipality> find(int mId,
      {bool preload = false, bool cascade = false}) async {
    final Find find = finder.where(this.mId.eq(mId));
    return await findOne(find);
  }

  Future<int> remove(int mId) async {
    final Remove remove = remover.where(this.mId.eq(mId));
    return adapter.remove(remove);
  }

  Future<int> removeMany(List<Municipality> models) async {
// Return if models is empty. If this is not done, all records will be removed!
    if (models == null || models.isEmpty) return 0;
    final Remove remove = remover;
    for (final model in models) {
      remove.or(this.mId.eq(model.mId));
    }
    return adapter.remove(remove);
  }
}
