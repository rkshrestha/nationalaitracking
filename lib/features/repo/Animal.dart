import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'Animal.g.dart';

part 'Animal.jorm.dart';

@JsonSerializable()
class Animal implements JsonCallable<Animal> {
//  @PrimaryKey(auto: true)
//  @JsonKey(name: 'MId')
//  int aId;
  @PrimaryKey()
  @JsonKey(name: 'MId')
  String aId;
  @JsonKey(name: 'AnimalId')
  @Column(isNullable: true)
  int animalId;
  @Column(isNullable: true)
  @JsonKey(name: 'MFarmerId')
  String mfarmerId;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesId')
  int speciesId;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesName')
  String speciesName;
  @Column(isNullable: true)
  @JsonKey(name: 'BreedId')
  int breedId;
  @Column(isNullable: true)
  @JsonKey(name: 'BreedName')
  String breedName;
  @Column(isNullable: true)
  @JsonKey(name: 'SpeciesCode')
  String speciesCode;
  @Column(isNullable: true)
  @JsonKey(name: 'MobileNo')
  String mobileNumber;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedDate')
  String createdDate;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedDate')
  String updatedDate;
  @Column(isNullable: true)
  @JsonKey(name: 'CreatedBy')
  int createdBy;
  @Column(isNullable: true)
  @JsonKey(name: 'UpdatedBy')
  int updatedBy;
  @Column(isNullable: true)
  @JsonKey(name: 'FarmerId')
  int fId;
  @Column(isNullable: true)
  @JsonKey(name: 'UserId')
  int userId;
  @Column(isNullable: true)
  @JsonKey(name: 'FarmerName')
  String farmerName;
  @Column(isNullable: true)
  @JsonKey(name: 'Gender')
  String gender;
  @Column(isNullable: true)
  @JsonKey(name: 'Status')
  bool status;
  @Column(isNullable: true)
  @JsonKey(name: 'SyncDate')
  String syncDate;
  @JsonKey(name: "SyncToServer")
  bool syncToServer = false;

  Animal();

  Animal.make(
      this.aId,
      this.animalId,
      this.mfarmerId,
      this.speciesId,
      this.speciesName,
      this.breedId,
      this.breedName,
      this.speciesCode,
      this.mobileNumber,
      this.createdDate,
      this.updatedDate,
      this.createdBy,
      this.updatedBy,
      this.fId,
      this.userId,
      this.farmerName,
      this.gender,
      this.syncDate,
      {this.status = true,
        this.syncToServer =
        false});


//  Animal.addCalving(this.animalId, this.mfarmerId, this.speciesId, this.speciesName,
//      this.breedId, this.breedName, this.speciesCode, this.mobileNumber,
//      this.createdDate, this.updatedDate, this.createdBy, this.updatedBy,
//      this.fId,this.userId,this.farmerName,this.syncDate, {this.status=true,
//        this.syncToServer=false});
//  Animal.make(this.aId, this.animalId, this.mfarmerId, this.speciesId,
//      this.speciesName, this.breedId, this.breedName, this.speciesCode,
//      this.mobileNumber, this.createdDate, this.updatedDate, this.createdBy,
//      this.updatedBy, {this.syncToServer=false});

  factory Animal.fromJson(Map<String, dynamic> json) => _$AnimalFromJson(json);

  Map<String, dynamic> toJson() => _$AnimalToJson(this);

  @override
  Animal fromJson(Map<String, dynamic> json) {
    return Animal.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }
}

@GenBean()
class AnimalBean extends Bean<Animal> with _AnimalBean {
  AnimalBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'animals';
}
