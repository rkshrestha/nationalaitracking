import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/AIDao.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/PDDao.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:uuid/uuid.dart';

class PDRepo implements Repository<PregnancyDiagnosis> {
  SQFliteDatabase sqFliteDatabase;
  PDDao pdDao;
  AIDao aiDao;
  List<ArtificialInsemination> aiListData;
  List<PregnancyDiagnosis> pdList;
  SessionManager sessionManager;
  BuildContext context;
  int spdId;
  String latestDate = "";
  String codeFromList = "";
  String mobileFromList = "";
  String englishExpectedDates;

  //for adding check pd (90 days differernce)

  PDRepo(context) {
    Logger.v("inside PDRepo");
    init();
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
  }

  void savePD(context, PregnancyDiagnosis pd, Farmer farmer, Animal animal,
      int speciesId, int breedId, StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try savePd');

      pdDao = await sqFliteDatabase.pdDao();
      Logger.v('check pd add ');
      var user = this.sessionManager.findOne().user;

      DateTime today = new DateTime.now();
      PregnancyDiagnosis pregnancyDiagnosises =
          await pdDao.findBySameDateAndAnimal(pd, animal);

//      test for ai pd difference more than 85 days....
      aiDao = await sqFliteDatabase.aiDao();
      aiListData = await aiDao.findForAddingPd(pd, animal);


      var uuid = new Uuid();
      var uid =  uuid.v1();

      if (aiListData.length > 0) {
        Logger.v("counrAILIst" + aiListData.length.toString());
        codeFromList = aiListData[0].speciesCode;
        mobileFromList = aiListData[0].mobileNumber;
        latestDate = aiListData[0].englishAiDate;
        String test = aiListData[0].aiDate;
        Logger.v('check pd lastest ');
        Logger.v("code:" + codeFromList);
        Logger.v("mobileFromList:" + mobileFromList);
        Logger.v("latestDate" + latestDate);
        Logger.v("latestDate" + test);

        NepaliDateTime nepalipdDate = NepaliDateTime.tryParse(pd.pdDate);
        Logger.v("getPdDate: " + pd.pdDate);
        DateTime englishPdDate = DateConverter.toAD(nepalipdDate);
        String englishPdDates =
            englishPdDate.toIso8601String().split("T").first;

        DateTime aiDbDate = DateTime.tryParse(latestDate);
        Logger.v("latestDate" + today.toString());

        Logger.v(
            "getdifer" + englishPdDate.difference(aiDbDate).inDays.toString());
        //
        Logger.v("pregnancyDiagnosises list case");
        if (pregnancyDiagnosises != null) {
          sSink.add(SCResponse.error(
            "PD with same date and animal already exist!!",

//            AppTranslations.of(context).text("pd_exist"),
          ));
        } else if ((englishPdDate.difference(aiDbDate).inDays) < 90) {
          sSink.add(SCResponse.error(
            "AI date difference for this animal is less than 90 days is not valid!!",
//            AppTranslations.of(context).text("pd_add_before90"),
          ));
        } else {
          pdDao = await sqFliteDatabase.pdDao();

          if (pd?.expectedDeliveryDate?.isEmpty ?? true) {
            englishExpectedDates = "";

            Logger.v("getEnglish:" + englishExpectedDates);
          } else if (pd.expectedDeliveryDate == null) {
            englishExpectedDates = "";
          } else {
            NepaliDateTime nepaliExpectedDate =
                NepaliDateTime.tryParse(pd.expectedDeliveryDate);
            DateTime englishExpectedDate =
                DateConverter.toAD(nepaliExpectedDate);
            englishExpectedDates =
                englishExpectedDate.toIso8601String().split("T").first;
            Logger.v("getEnglish:" + englishExpectedDates);

//            NepaliDateTime nepaliExpectedDate =
//            NepaliDateTime.tryParse(pd.expectedDeliveryDate);
//            if (nepaliExpectedDate == null) {
//              englishExpectedDates = "";
//            } else {
//              DateTime englishExpectedDate =
//              DateConverter.toAD(nepaliExpectedDate);
//
//              if (englishExpectedDate == null) {
//                englishExpectedDates = "";
//              } else {
//                englishExpectedDates =
//                    englishExpectedDate.toIso8601String().split("T").first;
//                Logger.v("getEnglish:" + englishExpectedDates);
//              }
//            }

          }

          int spdId = await pdDao.persist(new PregnancyDiagnosis.make(
//              pd.pdId,d
          uid,
              0,
              animal.animalId,
              farmer.farmerId,
              animal.speciesId,
              animal.breedId,
              pd.pdDate,
              pd.pdResult,
              pd.expectedDeliveryDate,
              pd.remarks,
              today.toIso8601String(),
              null,
              pd.mobileNumber,
              pd.speciesCode,
              user.userid,
              null,
              animal.aId,
              farmer.fId,
              user.userid,
              pd.farmerName,
              englishPdDates,
              englishExpectedDates,
              null,
              syncToServer: false));

          Navigator.pop(context, true);
          sSink.add(SCResponse.success(
            "PD added successfully !!",
//            AppTranslations.of(context).text("pd_add_success"),
          ));
        }
//        else {
//          Logger.v("testForNoexpectedDate");
//          NepaliDateTime nepalipdDate = NepaliDateTime.tryParse(pd.pdDate);
//          DateTime englishPdDate = DateConverter.toAD(nepalipdDate);
//          String englishPdDates =
//              englishPdDate.toIso8601String().split("T").first;
//
//          int spdId = await pdDao.persist(new PregnancyDiagnosis.make(
//              pd.pdId,
//              0,
//              animal.animalId,
//              farmer.farmerId,
//              animal.speciesId,
//              animal.breedId,
//              pd.pdDate,
//              pd.pdResult,
//              pd.expectedDeliveryDate,
//              pd.remarks,
//              today.toIso8601String(),
//              null,
//              pd.mobileNumber,
//              pd.speciesCode,
//              user.userid,
//              null,
//              animal.aId,
//              farmer.fId,
//              user.userid,
//              pd.farmerName,
//              englishPdDates,
//              "",
//              null,
//              syncToServer: false));
//
//          Navigator.pop(context, true);
//          sSink.add(SCResponse.success(
//            AppTranslations.of(context).text("pd_add_success"),
//          ));
//        }
      } else {
        Logger.v("counrAILIst" + aiListData.length.toString());
        pdDao = await sqFliteDatabase.pdDao();
//        aiListData = await aiDao.findForAddingPd(pd, animal);

        if (pregnancyDiagnosises != null) {
          sSink.add(SCResponse.error(
            "PD with same date and animal already exist!!",
//            AppTranslations.of(context).text("pd_exist"),
          ));
          Logger.v("pregnancyDiagnosises list 0 case");
        } else {
          pdDao = await sqFliteDatabase.pdDao();

          NepaliDateTime nepalipdDate = NepaliDateTime.tryParse(pd.pdDate);
          Logger.v("getPdDatessss: " + pd.pdDate);
          DateTime englishPdDate = DateConverter.toAD(nepalipdDate);

          String englishPdDates =
              englishPdDate.toIso8601String().split("T").first;
          Logger.v("englishPd" + englishPdDate.toIso8601String());

//          if (pd?.expectedDeliveryDate?.isEmpty ?? true) {
//            englishExpectedDates = "";
//
//            Logger.v("getEnglish:" + englishExpectedDates);
//          } else if (pd.expectedDeliveryDate == null) {
//            englishExpectedDates = "";
//            Logger.v("getEnglish:" + englishExpectedDates);
//          } else {
          if (pd.expectedDeliveryDate == null) {
            pd.expectedDeliveryDate = "";
          } else {
            NepaliDateTime nepaliExpectedDate =
                NepaliDateTime.tryParse(pd.expectedDeliveryDate);

            if (nepaliExpectedDate == null) {
              englishExpectedDates = "";
            } else {
              DateTime englishExpectedDate =
                  DateConverter.toAD(nepaliExpectedDate);

              if (englishExpectedDate == null) {
                englishExpectedDates = "";
              } else {
                englishExpectedDates =
                    englishExpectedDate.toIso8601String().split("T").first;
                Logger.v("getEnglish:" + englishExpectedDates);
              }
            }
          }

//          }

          int spdId = await pdDao.persist(new PregnancyDiagnosis.make(
              uid,
//              pd.pdId,
              0,
              animal.animalId,
              farmer.farmerId,
              animal.speciesId,
              animal.breedId,
              pd.pdDate,
              pd.pdResult,
              pd.expectedDeliveryDate,
              pd.remarks,
              today.toIso8601String(),
              null,
              pd.mobileNumber,
              pd.speciesCode,
              user.userid,
              null,
              animal.aId,
              farmer.fId,
              user.userid,
              pd.farmerName,
              englishPdDates,
              englishExpectedDates,
              null,
              syncToServer: false));

          Navigator.pop(context, true);
          sSink.add(SCResponse.success(
            "PD added successfully !!",
//            AppTranslations.of(context).text("pd_add_success"),
          ));
        }
      }

//      if (pregnancyDiagnosises == null) {
//        if (pd.expectedDeliveryDate.isNotEmpty ||
//            pd.expectedDeliveryDate != null) {
//          Logger.v("pdDates:" + pd.expectedDeliveryDate);
//          NepaliDateTime nepalipdDate = NepaliDateTime.tryParse(pd.pdDate);
//          DateTime englishPdDate = DateConverter.toAD(nepalipdDate);
//          String englishPdDates =
//              englishPdDate.toIso8601String().split("T").first;
//          Logger.v("englishDate:" + englishPdDates);
//
//          NepaliDateTime nepaliExpectedDate =
//              NepaliDateTime.tryParse(pd.expectedDeliveryDate);
//          DateTime englishExpectedDate = DateConverter.toAD(nepaliExpectedDate);
//          String englishExpectedDates =
//              englishExpectedDate.toIso8601String().split("T").first;
////          Logger.v("englishDatessss:" + englishExpectedDates);
//
//          int spdId = await pdDao.persist(new PregnancyDiagnosis.make(
//              pd.pdId,
//              null,
//              animal.animalId,
//              farmer.farmerId,
//              animal.speciesId,
//              animal.breedId,
//              pd.pdDate,
//              pd.pdResult,
//              pd.expectedDeliveryDate,
//              pd.remarks,
//              today.toIso8601String(),
//              null,
//              pd.mobileNumber,
//              pd.speciesCode,
//              user.userid,
//              null,
//              animal.aId,
//              farmer.fId,
//              user.userid,
//              pd.farmerName,
//              englishPdDates,
//              englishExpectedDates,
//              null,
//              syncToServer: false));
//
//          Navigator.pop(context, true);
//          sSink.add(SCResponse.success(
//            AppTranslations.of(context).text("pd_add_success"),
//          ));
//        } else {
//          NepaliDateTime nepalipdDate = NepaliDateTime.tryParse(pd.pdDate);
//          DateTime englishPdDate = DateConverter.toAD(nepalipdDate);
//          String englishPdDates =
//              englishPdDate.toIso8601String().split("T").first;
//
//          int spdId = await pdDao.persist(new PregnancyDiagnosis.make(
//              pd.pdId,
//              null,
//              animal.animalId,
//              farmer.farmerId,
//              animal.speciesId,
//              animal.breedId,
//              pd.pdDate,
//              pd.pdResult,
//              pd.expectedDeliveryDate,
//              pd.remarks,
//              today.toIso8601String(),
//              null,
//              pd.mobileNumber,
//              pd.speciesCode,
//              user.userid,
//              null,
//              animal.aId,
//              farmer.fId,
//              user.userid,
//              pd.farmerName,
//              englishPdDates,
//              "",
//              null,
//              syncToServer: false));
//
//          Navigator.pop(context, true);
//          sSink.add(SCResponse.success(
//            AppTranslations.of(context).text("pd_add_success"),
//          ));
//        }
//      } else {
//        sSink.add(SCResponse.error(
//          AppTranslations.of(context).text("pd_exist"),
//        ));
////        sSink.add(SCResponse.error("PD with same date and animal already exist"));
//      }
    } catch (ex) {
      sSink.add(SCResponse.error(
        "Couldn't add PD!!",
//        AppTranslations.of(context).text("pd_add_fail"),
      ));

      Logger.v(ex.toString());
    }
  }

  void updatePD(context, PregnancyDiagnosis pd, Farmer farmer, Animal animal,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try updatePD' + animal.breedName);

      pdDao = await sqFliteDatabase.pdDao();
      var user = this.sessionManager.findOne().user;
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);

      DateTime today = new DateTime.now();
      String todays = DateFormat('MM-dd-yyyy').format(today);
      Logger.v("todays"+todays);
//      DateTime todayNepaliDate = new DateTime(today.year + 56, today.month + 8,
//          today.day + 17, today.hour, today.minute, today.second);

      NepaliDateTime nepaliAiDate = NepaliDateTime.tryParse(pd.pdDate);
      DateTime englishPdDate = DateConverter.toAD(nepaliAiDate);
      String englishPdDates = englishPdDate.toIso8601String().split("T").first;

//      NepaliDateTime nepaliPDTentativeDate =
//      NepaliDateTime.tryParse(pd.expectedDeliveryDate);
//      DateTime englishExpectedDate = DateConverter.toAD(nepaliPDTentativeDate);
//      String englishExpectedDates =
//          englishExpectedDate.toIso8601String().split("T").first;

//      if (pd?.expectedDeliveryDate?.isEmpty ?? true) {
//        englishExpectedDates = "";
//
//        Logger.v("getEnglish:" + englishExpectedDates);
//      } else if (pd.expectedDeliveryDate == null) {
//        englishExpectedDates = "";
//      } else {
//        NepaliDateTime nepaliExpectedDate =
//            NepaliDateTime.tryParse(pd.expectedDeliveryDate);
//        DateTime englishExpectedDate = DateConverter.toAD(nepaliExpectedDate);
//        englishExpectedDates =
//            englishExpectedDate.toIso8601String().split("T").first;
//        Logger.v("getEnglish:" + englishExpectedDates);
//      }

      NepaliDateTime nepaliExpectedDate =
          NepaliDateTime.tryParse(pd.expectedDeliveryDate);
      if (nepaliExpectedDate == null) {
        englishExpectedDates = "";
      } else {
        DateTime englishExpectedDate = DateConverter.toAD(nepaliExpectedDate);

        if (englishExpectedDate == null) {
          englishExpectedDates = "";
        } else {
          englishExpectedDates =
              englishExpectedDate.toIso8601String().split("T").first;
          Logger.v("getEnglish:" + englishExpectedDates);
        }
      }

      int aiUpdate = await pdDao.update(new PregnancyDiagnosis.make(
              pd.pdId,
              pd.spdId,
              animal.animalId,
              farmer.farmerId,
              animal.speciesId,
              animal.breedId,
              pd.pdDate,
              pd.pdResult,
              pd.expectedDeliveryDate,
              pd.remarks,
//              todayNepaliDate.toIso8601String(),
              pd.createdDate,
              today.toIso8601String(),
              pd.mobileNumber,
              pd.speciesCode,
              user.userid,
              user.userid,
              animal.aId,
              farmer.fId,
              user.userid,
              pd.farmerName,
              englishPdDates,
              englishExpectedDates,
              pd.syncDate,
              syncToServer: false)

//      PregnancyDiagnosis.make(
//          pd.pdId,
//          pd.spdId,
////          user.userid,
//          animal.aId,
//          farmer.fId,
//          animal.speciesId,
//          animal.breedId,
//          pd.pdDate,
//          pd.pdResult,
//          pd.expectedDeliveryDate,
//          pd.remarks,
//          syncToServer: false)
          );

      Navigator.pop(context, true);
//      sSink.add(SCResponse.success(
//        AppTranslations.of(context).text("pd_update_success"),
//      ));
      sSink.add(SCResponse.success("PD has been successfully updated"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("pd_update_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't update pd"));
      Logger.v(ex.toString());
    }
  }

  void getPdList(
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> stSink) async {
//    sqFliteDatabase = SQFliteDatabase.getDB();
//    await sqFliteDatabase.connect();
    try {
      pdDao = await sqFliteDatabase.pdDao();

      pdList = await pdDao.findAll();

      for (PregnancyDiagnosis pd in pdList) {
        Logger.v(pd.pdDate);
      }

      stSink.add(SCResponse.success(pdList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getUnSyncPdList(
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> stSink) async {
    try {
      sqFliteDatabase = SQFliteDatabase.getDB();
      await sqFliteDatabase.connect();

      pdDao = await sqFliteDatabase.pdDao();

      pdList = await pdDao.findAllUnSyncData();

      for (PregnancyDiagnosis pd in pdList) {
        Logger.v(pd.pdDate);
      }

      stSink.add(SCResponse.success(pdList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSyncPdList(
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> stSink) async {
    try {
      pdDao = await sqFliteDatabase.pdDao();

      pdList = await pdDao.findAllSyncData();

      for (PregnancyDiagnosis pd in pdList) {
        Logger.v(pd.pdDate);
      }

      stSink.add(SCResponse.success(pdList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  deletePD(context, PregnancyDiagnosis pd,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      pdDao = await sqFliteDatabase.pdDao();

      await pdDao.delete(pd);

//      sSink.add(SCResponse.success(
//        AppTranslations.of(context).text("pd_delete_success"),
//      ));
      sSink.add(SCResponse.success("Pregnancy Diagnosis deleted Successfully"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("pd_delete_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't delete pregnancy diagnosis"));
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local start

  void updatePdServerIdAndStatus(List<PregnancyDiagnosis> pdList,
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> sSink) async {
    try {
      sqFliteDatabase = SQFliteDatabase.getDB();
      await sqFliteDatabase.connect();

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      pdDao = await sqFliteDatabase.pdDao();
      var user = this.sessionManager.findOne().user;

      for (PregnancyDiagnosis syncedPD in pdList) {
        await pdDao.updateByDateAndAnimalIdSync(syncedPD);

        await pdDao.updateByDateAndAnimalId(syncedPD);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local end

  //pd send to server
  void sendPdData(List<PregnancyDiagnosis> pdList,
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> sSink) async {
    try {
      sqFliteDatabase = SQFliteDatabase.getDB();
      await sqFliteDatabase.connect();

      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));

//      List<PregnancyDiagnosis> unsyncedPDList =
//          await pdDao.findAllBySyncStatus();
      Logger.v("UnsyncedData:" + DsonUtils.toJsonString(pdList));
//      Logger.v("UnsyncedData:" + DsonUtils.toJsonString(unsyncedPDList));

      pdDao = await sqFliteDatabase.pdDao();
      var user = this.sessionManager.findOne().user;
      SCResponse scResponse = await networkClient
          .call<PregnancyDiagnosis, List<PregnancyDiagnosis>>(
              networkApi.sendPDData(pdList, user.userid),
//              networkApi.sendPDData(unsyncedPDList, user.userid),
              callable: PregnancyDiagnosis());

      if (scResponse.status == Status.SUCCESS) {
        List<PregnancyDiagnosis> syncedPDList = scResponse.data;

        for (PregnancyDiagnosis syncedPD in syncedPDList) {
          await pdDao.updateByDateAndAnimalIdSync(syncedPD);

          await pdDao.updateByDateAndAnimalId(syncedPD);
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }

      sSink.add(scResponse);
    } catch (ex) {
      Logger.v(ex.toString());
    }
//    finally{
//      sqFliteDatabase.close();
//    }
  }

  //get pd data from server
  void syncGetPdData(
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager.findOne().user;
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse = await networkClient
          .call<PregnancyDiagnosis, List<PregnancyDiagnosis>>(
              //farmerId: null and animal : null
              networkApi.syncGetPDList(user.userid, null, null),
              callable: PregnancyDiagnosis());

//      if (scResponse.status == Status.SUCCESS) {
//        List<PregnancyDiagnosis> pdList = scResponse.data;
//        pdDao = await sqFliteDatabase.pdDao();
//
//        for (PregnancyDiagnosis pdServer in pdList) {
//          spdId = pdServer.farmerId;
//          Logger.v("ServerId " + spdId.toString());
//          PregnancyDiagnosis pd = await pdDao.findByPdId(spdId);
//
//          if (pd == null) {
//            pdDao.delete(pdServer);
//            pdDao.persist(new PregnancyDiagnosis.make(
//                pdServer.pdId,
//                pdServer.spdId,
//                pdServer.userId,
//                pdServer.animalId,
//                pdServer.farmerId,
//                pdServer.speciesId,
//                pdServer.breedId,
//                pdServer.pdDate,
//                pdServer.pdResult,
//                pdServer.expectedDeliveryDate,
//                pdServer.remarks));
//          } else {
//            if (pd.pdId == pdServer.pdId) {
//              pdDao.update(PregnancyDiagnosis.make(
//                  pdServer.pdId,
//                  pdServer.spdId,
//                  pdServer.userId,
//                  pdServer.animalId,
//                  pdServer.farmerId,
//                  pdServer.speciesId,
//                  pdServer.breedId,
//                  pdServer.pdDate,
//                  pdServer.pdResult,
//                  pdServer.expectedDeliveryDate,
//                  pdServer.remarks));
//            }
//          }
//        }
//
//
//        sSink.add(SCResponse.success("Pregnancy Diagnosis Sync success"));
//      }
//
      if (scResponse.status == Status.SUCCESS) {
        List<PregnancyDiagnosis> pdLists = scResponse.data;
        pdDao = await sqFliteDatabase.pdDao();
//
        for (PregnancyDiagnosis pdServer in pdLists) {
          spdId = pdServer.spdId;
          Logger.v("ServerId " + spdId.toString());
          PregnancyDiagnosis pd = await pdDao.findByPdId(spdId);
          if (pd == null) {
            var formattedDate =
                pdServer.pdDate == null ? "" : pdServer.pdDate.split("T").first;
            var formattedCreatedDate = pdServer.createdDate == null
                ? ""
                : pdServer.createdDate.split("T")[0];
            var formattedDDate = pdServer.expectedDeliveryDate == null
                ? ""
                : pdServer.expectedDeliveryDate.split("T").first;
            var formattedUpdatedDDate = pdServer.updatedDate == null
                ? ""
                : pdServer.updatedDate.split("T")[0];

            var formattedEnglishDate = pdServer.englishPdDate == null
                ? ""
                : pdServer.englishPdDate.split("T")[0];
            var formattedExpectedEnglishDate =
                pdServer.englishExpectedDate == null
                    ? ""
                    : pdServer.englishExpectedDate.split("T")[0];
            pdDao.persist(new PregnancyDiagnosis.make(
                    pdServer.pdId,
                    pdServer.spdId,
                    pdServer.animalId,
                    pdServer.farmerId,
                    pdServer.speciesId,
                    pdServer.breedId,
                    formattedDate,
                    pdServer.pdResult,
                    formattedDDate,
                    pdServer.remarks,
                    formattedCreatedDate,
                    formattedUpdatedDDate,
                    pdServer.mobileNumber,
                    pdServer.speciesCode,
                    pdServer.createdBy,
                    pdServer.updatedBy,
                    pdServer.mAnimalId,
                    pdServer.mFarmerId,
                    pdServer.userId,
                    pdServer.farmerName,
                    formattedEnglishDate,
                    formattedExpectedEnglishDate,
                    pdServer.syncDate,
                    syncToServer: true)
//            PregnancyDiagnosis.make(
//                pdServer.pdId,
//                pdServer.spdId,
////                pdServer.userId,
//                pdServer.animalId,
//                pdServer.farmerId,
//                pdServer.speciesId,
//                pdServer.breedId,
//                formattedDate,
//                pdServer.pdResult,
//                formattedDDate,
//                pdServer.remarks,
//                syncToServer: true)

                );
          } else {
//            var formattedDate = pdServer.pdDate.split("T")[0];
//            var formattedDDate = pdServer.expectedDeliveryDate.split("T")[0];
            pdDao.updateByServerPDId(pdServer);
//            pdDao.updateByServerAIId(new PregnancyDiagnosis.make(
//                pdServer.pdId,
//                pdServer.spdId,
////                pdServer.userId,
//                pdServer.animalId,
//                pdServer.farmerId,
//                pdServer.speciesId,
//                pdServer.breedId,
//                formattedDate,
//                pdServer.pdResult,
//                formattedDDate,
//                pdServer.remarks,
//                syncToServer: true));
          }
        }

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("PD Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //get pd by animal
  void getPDListByAnimal(Animal animal,
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> stSink) async {
    try {
      pdDao = await sqFliteDatabase.pdDao();

      pdList = await pdDao.getPDByAnimal(animal);
      Logger.v("getIT" + pdList.length.toString());
      for (PregnancyDiagnosis pd in pdList) {
        Logger.v("getIT" + pd.animalId.toString());
      }

      stSink.add(SCResponse.success(pdList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getUnSyncPDListByAnimal(Animal animal,
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> stSink) async {
    try {
      pdDao = await sqFliteDatabase.pdDao();

      pdList = await pdDao.getUnSyncPDByAnimal(animal.aId);

      for (PregnancyDiagnosis pd in pdList) {
        Logger.v(pd.animalId.toString());
      }

      stSink.add(SCResponse.success(pdList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSyncPDListByAnimal(Animal animal,
      StreamSink<SCResponse<List<PregnancyDiagnosis>>> stSink) async {
    try {
      pdDao = await sqFliteDatabase.pdDao();

      pdList = await pdDao.getSyncPDByAnimal(animal.aId);

      for (PregnancyDiagnosis pd in pdList) {
        Logger.v(pd.animalId.toString());
      }

      stSink.add(SCResponse.success(pdList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  @override
  void delete(PregnancyDiagnosis entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<PregnancyDiagnosis> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  PregnancyDiagnosis findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  PregnancyDiagnosis findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(PregnancyDiagnosis entity) {
    // TODO: implement persist
  }

  @override
  void update(PregnancyDiagnosis entity) {
    // TODO: implement update
  }
}
