import 'dart:convert';

import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:nbis/features/repo/JsonCallable.dart';

part 'Breed.g.dart';

part 'Breed.jorm.dart';
@JsonSerializable()
class Breed implements JsonCallable<Breed>{
  @PrimaryKey(auto: true)
  int bId;
  @JsonKey(name: 'BreedId')
  int bids;
  @JsonKey(name: 'AnimalId')
  int aids;
  @JsonKey(name: 'BreedName')
  String name;


  Breed();


  Breed.make(this.bids, this.aids, this.name);

  factory Breed.fromJson(Map<String, dynamic> json) =>
      _$BreedFromJson(json);

  Map<String, dynamic> toJson() => _$BreedToJson(this);

  @override
  Breed fromJson(Map<String, dynamic > json) {
  return Breed.fromJson(json);
  }

  @override
  String toJsonString() {
    return jsonEncode(toJson());
  }

}

@GenBean()
class BreedBean extends Bean<Breed> with _BreedBean {
  BreedBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => 'breed';
}
