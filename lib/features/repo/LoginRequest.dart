
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class LoginRequest {
////  int userId;
//  // String userName;
//  String mobileNumber;
//  String password;
//  String deviceType;
//
//  LoginRequest();
//
//  LoginRequest.make(this.mobileNumber, this.password, this.deviceType);
//
//// LoginRequest.make(this.userId, this.userName, this.mobileNumber, this.password);

  @JsonKey(name: 'MobileNo')
  String mobileno;
  @JsonKey(name: 'TechnicianId')
  String technicianId;
  @JsonKey(name: 'DeviceType')
  String devicetype;
  @JsonKey(name: 'Password')
  String password;
  @JsonKey(name: 'Token')
  String fcmToken;

  LoginRequest({this.mobileno,this.technicianId, this.devicetype, this.password,this.fcmToken});

  LoginRequest.fromJson(Map<String, dynamic> json) {
    mobileno = json['MobileNo'];
    technicianId = json['TechnicianId'];
    devicetype = json['DeviceType'];
    password = json['Password'];
    fcmToken = json['Token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNo'] = this.mobileno;
    data['TechnicianId'] = this.technicianId;
   // data['DeviceType'] = this.devicetype;
    data['DeviceType'] = "Mobile";
    data['Password'] = this.password;
    data['Token'] = this.fcmToken;
    return data;
  }
}
