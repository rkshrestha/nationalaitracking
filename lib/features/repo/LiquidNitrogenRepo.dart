import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/NetworkClient.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/LiquidNitrogenDao.dart';
import 'package:nbis/features/repo/Repository.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/NetworkUtils.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:uuid/uuid.dart';

class LiquidNitrogenRepo implements Repository<LiquidNitrogen> {
  SQFliteDatabase sqFliteDatabase;
  LiquidNitrogenDao liquidNitrogenDao;
  SessionManager sessionManager;
  List<LiquidNitrogen> liquidNitrogenList;
  BuildContext context;
  int slnId;

  LiquidNitrogenRepo(context) {
    Logger.v("inside LiquidNitrogenRepo");
    init();
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
  }

  void saveNitrogen(context, LiquidNitrogen liquidNitrogen,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      Logger.v('try saveNitrogen' + liquidNitrogen.lnId.toString());

      liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();
      Logger.v('check nitrogen add ');

      var uuid = new Uuid();
      var uid = uuid.v1();

//      Logger.v('UUID:'+uid.toString());

//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);

      DateTime today = new DateTime.now();
      DateTime todayNepaliDate = new DateTime(today.year + 56, today.month + 8,
          today.day + 17, today.hour, today.minute, today.second);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
      var user = this.sessionManager.findOne().user;
      Logger.v(user.userid.toString());
      LiquidNitrogen nitrogen =
          await liquidNitrogenDao.findBySameDate(liquidNitrogen);
      NepaliDateTime nepaliLnDate =
          NepaliDateTime.tryParse(liquidNitrogen.receivedDate);
      DateTime englishLnDate = DateConverter.toAD(nepaliLnDate);
      String englishLNitrogenDate =
          englishLnDate.toIso8601String().split("T").first;
      Logger.v('EnglishDate:' + englishLNitrogenDate);

      if (nitrogen == null) {
        int lnId = await liquidNitrogenDao.persist(
//            new LiquidNitrogen.make(
//                liquidNitrogen.lnId,
//                null,
//                liquidNitrogen.receivedDate,
//                liquidNitrogen.quantity,
//                user.userid,
//                today.toIso8601String(),
//                null,
//                user.userid,
//                null,
//                englishLNitrogenDate,
//                null,
//                syncToServer: false)

            new LiquidNitrogen.make(
                uid,
                null,
                liquidNitrogen.receivedDate,
                liquidNitrogen.quantity,
                user.userid,
                today.toIso8601String(),
                null,
                user.userid,
                null,
                englishLNitrogenDate,
                null,
                syncToServer: false));

        Logger.v("datas: " +
            liquidNitrogen.receivedDate +
            "\n" +
            liquidNitrogen.quantity.toString());

        Navigator.pop(context, true);
//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("nitrogen_add_success"),
//        ));
        sSink.add(
            SCResponse.success("Liquid Nitrogen has been successfully added"));
      } else {
//        sSink.add(SCResponse.error(
//          AppTranslations.of(context).text("nitrogen_exist"),
//        ));
        sSink.add(SCResponse.error("Duplicate received date"));
      }
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("nitrogen_add_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't add liquid Nitrogen"));
      Logger.v(ex.toString());
    }
  }

  void getLiquidNitrogenList(
      StreamSink<SCResponse<List<LiquidNitrogen>>> stSink) async {
    try {
      liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();

      liquidNitrogenList = await liquidNitrogenDao.findAll();

      for (LiquidNitrogen liquidNitrogen in liquidNitrogenList) {
        Logger.v(liquidNitrogen.receivedDate);
      }

      stSink.add(SCResponse.success(liquidNitrogenList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getSyncLiquidNitrogenList(
      StreamSink<SCResponse<List<LiquidNitrogen>>> stSink) async {
    try {
      liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();

      liquidNitrogenList = await liquidNitrogenDao.findAllSyncData();

      for (LiquidNitrogen liquidNitrogen in liquidNitrogenList) {
        Logger.v(liquidNitrogen.receivedDate);
      }

      stSink.add(SCResponse.success(liquidNitrogenList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void getUnSyncLiquidNitrogenList(
      StreamSink<SCResponse<List<LiquidNitrogen>>> stSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();

      liquidNitrogenList = await liquidNitrogenDao.findAllUnSyncData();

      for (LiquidNitrogen liquidNitrogen in liquidNitrogenList) {
        Logger.v(liquidNitrogen.receivedDate);
      }

      stSink.add(SCResponse.success(liquidNitrogenList));
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  void deleteNitrogen(context, LiquidNitrogen nitrogen,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();

      await liquidNitrogenDao.delete(nitrogen);

//      sSink.add(SCResponse.success(
//        AppTranslations.of(context).text("nitrogen_delete_success"),
//      ));
      sSink.add(SCResponse.success("Nitrogen deleted Successfully"));
    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("nitrogen_delete_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't delete nitrogen"));
      Logger.v(ex.toString());
    }
  }

  void updateNitrogen(context, LiquidNitrogen liquidNitrogen,
      StreamSink<SCResponse<String>> sSink) async {
    try {
      liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();
      var user = this.sessionManager.findOne().user;
//      DateTime dates = DateTime.now();
//      String today = DateFormat('yyyy-MM-dd').format(dates);

      DateTime today = new DateTime.now();
      DateTime todayNepaliDate = new DateTime(today.year + 56, today.month + 8,
          today.day + 17, today.hour, today.minute, today.second);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);

      NepaliDateTime nepaliLnDate =
          NepaliDateTime.tryParse(liquidNitrogen.receivedDate);
      DateTime englishLnDate = DateConverter.toAD(nepaliLnDate);
      String englishLNitrogenDate =
          englishLnDate.toIso8601String().split("T").first;

//      LiquidNitrogen nitrogen =
//      await liquidNitrogenDao.findBySameDateUpdate(liquidNitrogen);

//      if(nitrogen ==null){
      int updateNitrogen =
          await liquidNitrogenDao.update(new LiquidNitrogen.make(
                  liquidNitrogen.lnId,
                  liquidNitrogen.slnId,
                  liquidNitrogen.receivedDate,
                  liquidNitrogen.quantity,
                  user.userid,
//              todayNepaliDate.toIso8601String(),
                  liquidNitrogen.createdDate,
                  today.toIso8601String(),
                  user.userid,
                  user.userid,
                  englishLNitrogenDate,
                  liquidNitrogen.syncDate,
                  syncToServer: false)

//            new LiquidNitrogen.make(
//            liquidNitrogen.lnId,
//            liquidNitrogen.slnId,
//            liquidNitrogen.receivedDate,
//            liquidNitrogen.quantity,
//            user.userid,
////              todayNepaliDate.toIso8601String(),
//            liquidNitrogen.createdDate,
//            today.toIso8601String(),
//            user.userid,
//            user.userid,
//            englishLNitrogenDate,
//            liquidNitrogen.syncDate,
//            syncToServer: false)

              );

      Navigator.pop(context, true);
//        sSink.add(SCResponse.success(
//          AppTranslations.of(context).text("nitrogen_update_success"),
//        ));
      sSink.add(SCResponse.success("Nitrogen has been successfully updated"));
//      }else {
////        sSink.add(SCResponse.error(
////          AppTranslations.of(context).text("nitrogen_exist"),
////        ));
//        sSink.add(SCResponse.error("Duplicate received date.."));
//      }

    } catch (ex) {
//      sSink.add(SCResponse.error(
//        AppTranslations.of(context).text("nitrogen_update_fail"),
//      ));
      sSink.add(SCResponse.error("Couldn't update nitrogen"));
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local start
  void updateNitrogenServerIdAndStatus(List<LiquidNitrogen> nitrogenList,
      StreamSink<SCResponse<List<LiquidNitrogen>>> sSink) async {
    sqFliteDatabase = SQFliteDatabase.getDB();
    await sqFliteDatabase.connect();
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();
      var user = this.sessionManager.findOne().user;

      for (LiquidNitrogen syncedNitrogen in nitrogenList) {
        await liquidNitrogenDao.updateByDatesSync(syncedNitrogen);

        await liquidNitrogenDao.updateByDate(syncedNitrogen);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  //update Server Id in local end

  //sync to server

  //nitrogen send
  void sendNitrogenData(List<LiquidNitrogen> nitrogenList,
      StreamSink<SCResponse<List<LiquidNitrogen>>> sSink) async {
//    sqFliteDatabase = SQFliteDatabase.getDB();
//    await sqFliteDatabase.connect();
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }

      Logger.v("network available");
      NetworkClient networkClient = NetworkClient();

      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
//      List<LiquidNitrogen> unsyncedNitrogenList =
//          await liquidNitrogenDao.findAllNitrogenBySyncStatus();
      SCResponse scResponse =
          await networkClient.call<LiquidNitrogen, List<LiquidNitrogen>>(
              networkApi.sendNitrogenData(nitrogenList),
//              networkApi.sendNitrogenData(unsyncedNitrogenList),
              callable: LiquidNitrogen());

      Logger.v("testToSend: " + DsonUtils.toJsonString(nitrogenList));
//      Logger.v("testToSend: "+ DsonUtils.toJsonString(unsyncedNitrogenList));
      liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();
      var user = this.sessionManager.findOne().user;

      if (scResponse.status == Status.SUCCESS) {
        List<LiquidNitrogen> syncedNitrogenList = scResponse.data;

        for (LiquidNitrogen syncedNitrogen in syncedNitrogenList) {
          await liquidNitrogenDao.updateByDatesSync(syncedNitrogen);

          await liquidNitrogenDao.updateByDate(syncedNitrogen);
          Logger.v("syncedFromserver " + syncedNitrogen.receivedDate);
        }

        sSink.add(scResponse);
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }

      sSink.add(scResponse);
    } catch (ex) {
      Logger.v(ex.toString());
    }
//    finally{
//      sqFliteDatabase.close();
//    }
  }

  // get nitrogen from server
  void syncGetNitrogenList(
      StreamSink<SCResponse<List<LiquidNitrogen>>> sSink) async {
    try {
      bool isNetworkAvailable = await NetworkUtils().checkInternetConnection();

      if (!isNetworkAvailable) {
        sSink.add(SCResponse.noInternetAvailable());
        return;
      }
      var user = this.sessionManager.findOne().user;
      NetworkClient networkClient = NetworkClient();
      NetworkApi networkApi = NetworkApi.create(
          networkClient.client(newBaseUrl: NetworkClient.baseUrl));
      SCResponse scResponse =
          await networkClient.call<LiquidNitrogen, List<LiquidNitrogen>>(
              networkApi.syncGetLiquidNitrogenList(user.userid),
              callable: LiquidNitrogen());

//      if (scResponse.status == Status.SUCCESS) {
//        List<LiquidNitrogen> liquidNitrogenList = scResponse.data;
//        liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();
//
//        for (LiquidNitrogen lnServer in liquidNitrogenList) {
//          slnId = lnServer.slnId;
//          Logger.v("ServerId " + slnId.toString());
//          LiquidNitrogen nitrogen =
//          await liquidNitrogenDao.findByNitrogenId(slnId);
//
//          if (nitrogen == null) {
//            liquidNitrogenDao.delete(lnServer);
//            liquidNitrogenDao.persist(new LiquidNitrogen.make(
//                lnServer.lnId,
//                lnServer.slnId,
//                lnServer.receivedDate,
//                lnServer.quantity,
//                lnServer.userId));
//          } else {
//            if (nitrogen.lnId == lnServer.lnId) {
//              liquidNitrogenDao.update(LiquidNitrogen.make(
//                  lnServer.lnId,
//                  lnServer.slnId,
//                  lnServer.receivedDate,
//                  lnServer.quantity,
//                  lnServer.userId));
//            }
//          }
//        }
//
////        for (LiquidNitrogen nitrogen in liquidNitrogenList) {
////          liquidNitrogenDao.persist(new LiquidNitrogen.make(nitrogen.slnId,
////              nitrogen.receivedDate, nitrogen.quantity, nitrogen.userId));
////        }
//
//        //  Logger.v(liquidNitrogenList[0].lnId.toString());
//        //   sSink.add(scResponse);
//        sSink.add(SCResponse.success("Liquid Nitrogen Sync success"));
//      }
      if (scResponse.status == Status.SUCCESS) {
        List<LiquidNitrogen> liquidNitrogenLists = scResponse.data;
        liquidNitrogenDao = await sqFliteDatabase.liquidNitrogenDao();
        for (LiquidNitrogen nitrogenServer in liquidNitrogenLists) {
          slnId = nitrogenServer.slnId;
          LiquidNitrogen nitrogen =
              await liquidNitrogenDao.findByNitrogenId(slnId);
          if (nitrogen == null) {
            //  semenDao.delete(semenServer);
            var formattedNitrogenDate = nitrogenServer.receivedDate == null
                ? ""
                : nitrogenServer.receivedDate.split("T")[0];
            var formattedCreatedDate = nitrogenServer.createdDate == null
                ? ""
                : nitrogenServer.createdDate.split("T")[0];
            var formattedUpdatedDate = nitrogenServer.updatedDate == null
                ? ""
                : nitrogenServer.updatedDate.split("T")[0];
            var formattedEnglishDate =
                nitrogenServer.englishReceivedDate == null
                    ? ""
                    : nitrogenServer.englishReceivedDate.split("T")[0];
            liquidNitrogenDao.persist(new LiquidNitrogen.make(
                    nitrogenServer.lnId,
                    nitrogenServer.slnId,
                    formattedNitrogenDate,
                    nitrogenServer.quantity,
                    nitrogenServer.userId,
                    formattedCreatedDate,
                    formattedUpdatedDate,
                    nitrogenServer.createdBy,
                    nitrogenServer.updatedBy,
                    formattedEnglishDate,
                    nitrogenServer.syncDate,
                    syncToServer: true)

//            LiquidNitrogen.make(
//                nitrogenServer.lnId,
//                nitrogenServer.slnId,
//                formattedNitrogenDate,
//                nitrogenServer.quantity,
//                nitrogenServer.userId,
//                syncToServer: true)

                );
          } else {
            liquidNitrogenDao.updateByServerslnId(nitrogenServer);

//            liquidNitrogenDao.updateByServerslnId(new LiquidNitrogen.make(
//                nitrogenServer.lnId,
//                nitrogenServer.slnId,
//                nitrogenServer.receivedDate,
//                nitrogenServer.quantity,
//                nitrogenServer.userId,
//                syncToServer: true));
          }
        }

        sSink.add(scResponse);
//        sSink.add(SCResponse.success("Semen Sync success"));
      } else if (scResponse.status == Status.ERROR) {
        Logger.v(scResponse.errorMessage);
        sSink.add(scResponse);
      }
    } catch (ex) {
      Logger.v(ex.toString());
    }
  }

  @override
  void delete(LiquidNitrogen entity) {
    // TODO: implement delete
  }

  @override
  void deleteAll() {
    // TODO: implement deleteAll
  }

  @override
  List<LiquidNitrogen> findAll() {
    // TODO: implement findAll
    return null;
  }

  @override
  LiquidNitrogen findById(Object id) {
    // TODO: implement findById
    return null;
  }

  @override
  LiquidNitrogen findOne() {
    // TODO: implement findOne
    return null;
  }

  @override
  void persist(LiquidNitrogen entity) {
    // TODO: implement persist
  }

  @override
  void update(LiquidNitrogen entity) {
    // TODO: implement update
  }
}
