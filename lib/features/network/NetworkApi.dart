import "dart:async";

import 'package:chopper/chopper.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/ChangePasswordModel.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/LoginRequest.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/repo/SyncModel.dart';

part 'NetworkApi.chopper.dart';

// https://javiercbk.github.io/json_to_dart/
@ChopperApi()
abstract class NetworkApi extends ChopperService {
  static NetworkApi create([ChopperClient client]) => _$NetworkApi(client);

//  @Get(path:"getData.aspx")
//  Future<Response<dynamic>> sendLandListRequest(@Query("type") String type,@Query("pn") int pageNumber);
//
//
//  @Post(path: "account/sign-in")
//  Future<Response> sendLoginRequest(@Body() LoginRequest request);
  @Post(path: "api/user/login")
  Future<Response> sendLoginRequest(@Body() LoginRequest request);

  @Get(path: "api/forgot-password")
  Future<Response> sendForgotPasswordRequest(@Query("techId") String techId);

  @Post(path: "api/user/change_password")
  Future<Response> sendChangePasswordRequest(
      @Body() ChangePasswordModel changeRequest);

//animal sync
  @Get(path: "api/species/get_all")
  Future<Response> syncSpeciesList();

  @Get(path: "api/species_breed/get_all")
  Future<Response> syncBreedList();

  @Get(path: "api/provinces/get_all")
  Future<Response> syncProvinceList();

  @Get(path: "api/districts/get_all")
  Future<Response> syncDistrictList();

  @Get(path: "api/municipalities/get_all")
  Future<Response> syncMunicipalityList();

  @Get(path: "api/exit_reason/get_all")
  Future<Response> syncExitReasonList();

  //send data to server
  @Post(path: "api/semen/add")
  Future<Response> sendSemenData(@Body() List<Semen> semenList);

  @Post(path: "api/nitrogen/add")
  Future<Response> sendNitrogenData(
      @Body() List<LiquidNitrogen> liquidNitrogenList);

  @Post(path: "api/farmer/{userId}/add")
  Future<Response> sendFarmerData(@Body() List<Farmer> farmerList, @Path() int userId);

  @Post(path: "api/animal/{userId}/add")
  Future<Response> sendAnimalData(@Body() List<Animal> animalList, @Path() int userId);

  @Post(path: "api/artificial-insemination/{userId}/add")
  Future<Response> sendAIData(@Body() List<ArtificialInsemination> aiList, @Path() int userId);

  @Post(path: "api/calving/{userId}/add")
  Future<Response> sendCalvingData(@Body() List<Calving> calvingList, @Path() int userId);

  @Post(path: "api/exit-record/{userId}/add")
  Future<Response> sendExitRecordData(@Body() List<ExitRecord> exitRecordList,@Path() int userId);

  @Post(path: "api/pregnancy-diagnosis/{userId}/add")
  Future<Response> sendPDData(@Body() List<PregnancyDiagnosis> pdList,@Path() int userId);



  @Post(path: "api/add/{userId}/all")
  Future<Response> sendAllData(@Body() SyncModel allList,@Path() int userId);

//  @Post(path: "api/add/{userId}/all")
//  Future<Response> sendAllData(@Body() List<SyncModel> allList,@Path() int userId);


  //get Data from server

  @Get(path: "api/semen/semen-list")
  Future<Response> syncGetSemenList(@Query("UserId") int userId);

  @Get(path: "api/nitrogen/nitrogen-list")
  Future<Response> syncGetLiquidNitrogenList(@Query("UserId") int userId);

  @Get(path: "api/farmer/farmer-list")
  Future<Response> syncGetFarmerList(@Query("UserId") int userId);

  @Get(path: "api/animal/animal-list")
  Future<Response> syncGetAnimalList(@Query("UserId") int userId);

  @Get(path: "api/artificial-insemination/artificial-insemination-list")
  Future<Response> syncGetAIList(@Query("UserId") int userId);

  @Get(path: "api/pregnancy-diagnosis/pregnancy-diagnosis-list")
  Future<Response> syncGetPDList(@Query("UserId") int userId,
      @Query("FarmerId") int farmerId, @Query("AnimalId") int animalId);

  @Get(path: "api/calving/calving-list")
  Future<Response> syncGetCalvingList(@Query("UserId") int userId);

  @Get(path: "api/exit-record/exit-record-list")
  Future<Response> syncGetExitRecordList(@Query("UserId") int userId);

  @Post(path: "api/user/get_all")
  Future<Response> getAllDataFromServer(@Query("UserId") int userId);



  @Get(path: "api/notification/show-notification-list")
  Future<Response> getNotification(@Query("UserId") int userId);

  @Get(path: "api/notification/show-notification-pd-list")
  Future<Response> getPDNotification(@Query("UserId") int userId);

//
//  @Post(path: "api/user/login")
//  Future<Response> sendLoginRequest(@Body() LoginRequest request);
//
////animal sync
//  @Get(path: "api/species/get_all")
//  Future<Response> syncSpeciesList();
//
//  @Get(path: "api/species_breed/get_all")
//  Future<Response> syncBreedList();
//
//  @Get(path: "api/provinces/get_all")
//  Future<Response> syncProvinceList();
//
//  @Get(path: "api/districts/get_all")
//  Future<Response> syncDistrictList();
//
//  @Get(path: "api/municipalities/get_all")
//  Future<Response> syncMunicipalityList();
//
//  @Get(path: "api/exit_reason/get_all")
//  Future<Response> syncExitReasonList();
//
//  //send data to server
//  @Post(path: "api/semen/add")
//  Future<Response> sendSemenData(@Body() List<Semen> semenList);
//
//  @Post(path: "api/nitrogen/add")
//  Future<Response> sendNitrogenData(
//      @Body() List<LiquidNitrogen> liquidNitrogenList);
//
//  @Post(path: "api/farmer/add")
//  Future<Response> sendFarmerData(@Body() List<Farmer> farmerList);
//
//  @Post(path: "api/animal/add")
//  Future<Response> sendAnimalData(@Body() List<Animal> animalList);
//
//  @Post(path: "api/artificial-insemination/add")
//  Future<Response> sendAIData(@Body() List<ArtificialInsemination> aiList);
//
//  @Post(path: "api/calving/add")
//  Future<Response> sendCalvingData(@Body() List<Calving> calvingList);
//
//  @Post(path: "api/exit-record/add")
//  Future<Response> sendExitRecordData(@Body() List<ExitRecord> exitRecordList);
//
//  @Post(path: "api/pregnancy-diagnosis/add")
//  Future<Response> sendPDData(@Body() List<PregnancyDiagnosis> pdList);
//
//  //get Data from server
//
//  @Get(path: "api/semen/semen-list")
//  Future<Response> syncGetSemenList(@Query("UserId") int userId);
//
//  @Get(path: "api/nitrogen/nitrogen-list")
//  Future<Response> syncGetLiquidNitrogenList(@Query("UserId") int userId);
//
//  @Get(path: "api/farmer/farmer-list")
//  Future<Response> syncGetFarmerList(@Query("UserId") int userId);
//
//  @Get(path: "api/animal/animal-list")
//  Future<Response> syncGetAnimalList(
//      @Query("UserId") int userId);
//
//  @Get(path: "api/artificial-insemination/artificial-insemination-list")
//  Future<Response> syncGetAIList(@Query("UserId") int userId);
//
//
//  @Get(path: "api/pregnancy-diagnosis/pregnancy-diagnosis-list")
//  Future<Response> syncGetPDList(@Query("UserId") int userId,
//      @Query("FarmerId") int farmerId, @Query("AnimalId") int animalId);
//
//  @Get(path: "api/calving/calving-list")
//  Future<Response> syncGetCalvingList(@Query("UserId") int userId);
//
//  @Get(path: "api/exit-record/exit-record-list")
//  Future<Response> syncGetExitRecordList(@Query("UserId") int userId);
//

}
