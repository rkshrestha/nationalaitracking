import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:nbis/features/network/NetworkApi.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/JsonCallable.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:http/io_client.dart' as http;

class NetworkClient {
//  static const String baseUrl = "http://192.168.10.81:812";
  static const String baseUrl = "http://nbisapi.pathway.com.np";

  ChopperClient client(
      {String newBaseUrl, bool useFormUrlEncodedConverter = false}) {
    ChopperClient chopper = ChopperClient(
      baseUrl: (newBaseUrl != null)
          ? newBaseUrl
          : baseUrl,
      services: [
        // the generated service
        NetworkApi.create()
      ],
      converter: (useFormUrlEncodedConverter)
          ? FormUrlEncodedConverter()
          : JsonConverter(),
      interceptors: [
        (request) async => applyHeader(request, "Authorization", ""),
        AuthInterceptor(),
        LogInterceptor(),
      ],

      //Connection timeout
      client: http.IOClient(
        HttpClient()..connectionTimeout = const Duration(seconds: 30),
      ),


    );
    return chopper;
  }

  Future<SCResponse<R>> call<T, R>(Future<Response<dynamic>> future,
      {JsonCallable callable, bool disableCallable = false}) async {
    try {
      Response<dynamic> response = await future;
      if (disableCallable) {
        dynamic mappedResponse = response.body;
        return SCResponse.success(mappedResponse);
      } else {
        //String fakeData= '{"PropertyID":7385,"Title":"3BHK House On Rent at Vinayak Colony, Bhainsepati","TotalPrice":"Not Disclosed"}';
        Logger.v("before mapping actual server response");
        R mappedResponse = useDecoder<T, R>(response.body, callable);
        Logger.v("after mapping actual server response");
        Logger.v(jsonEncode(mappedResponse));
        Logger.v(
            "=================================================================================================");
        return SCResponse.success(mappedResponse);
      }
    } catch (e) {
      //  Log.v(e.toString());

      if (e == null) {
        return SCResponse.error("Server Error!");
      }

      Type dataType = e.runtimeType;
      Logger.v(dataType.toString());

      String errorMessage = "";

      if (e is SocketException) {
        errorMessage = "Unable to connect with server!";
        return SCResponse.error(errorMessage);
      }

      if (e is TimeoutException) {
        errorMessage = "Timeout!";
        return SCResponse.error(errorMessage);
      }

      if (e is Response) {
        errorMessage = e.body;
        if (e.statusCode == 403) {
          return SCResponse.tokenExpired(errorMessage);
        } else {
         // Map data = jsonDecode(jsonEncode(errorMessage));
        //  return SCResponse.error(data['Message'] as String);
          return SCResponse.error(errorMessage);
        }
      }
      return SCResponse.error("Server Error!");
    }
  }

  R useDecoder<T, R>(dynamic entity, JsonCallable callable) {
//      if (entity is String) {
//        Log.v("entity is string");
//        return entity as R;
//      }

    if (callable == null) {
      Logger.v(
          "Warning ! Callable required!!You may have forgot to pass the callable in your network client");
      Logger.v("Warning ! Couldn't map server response to your callable?");

      throw Exception("Parsing Error");
    }

    if (entity is Iterable) {
      Logger.v("entity is iterable ");
      return DsonUtils.toList<T>(jsonEncode(entity), callable) as R;
    }

//      if (entity is Map) {
//        Log.v('entity is  Map');
//        String data = jsonEncode(entity);
//        return jsonDecode(data) as Map<String, dynamic>  ;
//      }

    return DsonUtils.toObject(entity, callable) as R;
  }
}

class AuthInterceptor implements RequestInterceptor {
  @override
  FutureOr<Request> onRequest(Request request) {
    Logger.v("inside auth interceptor");
    request.headers.update("Authorization", (value) => "update value");
    request.headers.forEach((key, value) => Logger.v(key + "," + value));
    Logger.v("inside autha interceptor");
    return request;
  }
}

class LogInterceptor extends HttpLoggingInterceptor {
  @override
  FutureOr<Request> onRequest(Request request) async {
    setShowReminder();
    Logger.v("request url : " + jsonEncode(request.baseUrl + request.url));
    // return  super.onRequest(request);
    return request;
  }

  void setShowReminder() {
    // if (this.showReminder) {
    Logger.v(
        "=================================================================================================");
    Logger.v(
        'Tips : "Please use following command if you have update your api or changes on api file"');
    Logger.v(
        "> flutter packages pub run build_runner build --delete-conflicting-outputs or you may use watcher cmd!");
    Logger.v("> flutter packages pub run build_runner watch");
    Logger.v('"This will reflect changes of your api"');
    Logger.v(
        "=================================================================================================");
    //  }
  }

  @override
  FutureOr<Response> onResponse(Response response) {
    // Log.v("=================================================================================================");
    //Log.v("logging response from server... ");
    Logger.v("status code :  " + response.statusCode.toString());
    Logger.v("actual server response : " + jsonEncode(response.body));
    Logger.v(
        "=================================================================================================");
    return response;
  }
}

//class SCFormUrlEncodedConverter extends FormUrlEncodedConverter {
//
//  JsonCallable callable;
//
//  SCFormUrlEncodedConverter(this.callable);
//
//  dynamic _decode<T>(entity) {
//    if (entity is String) {
//      Log.v("entity is string");
//      return entity;
//    }
//
//    if (entity is Iterable) {
//      return DsonUtils.dynamicToList(entity, this.callable);
//    }
//
//    if (entity is Map) {
//      String data = jsonEncode(entity);
//      return jsonDecode(data) as Map<String, dynamic>;
//    }
//
//    return DsonUtils.toObject(entity, this.callable);
//  }
//}

//class SCJsonConveter extends JsonConverter {
//  JsonCallable callable;
//
//  SCJsonConveter(this.callable);
//
//  R mappedResponse = useDecoder<T,R>(response.body, callable);
//  Log.v("after mapping actual server response");
//
//  @override
//  Response convertResponse<T>(Response response) {
//    // use [JsonConverter] to decode json
//    final jsonRes = super.convertResponse<T>(response);
//    Log.v(
//        "=================================================================================================");
//    Log.v("actual server response : " + jsonEncode(jsonRes.body));
//    Log.v(
//        "=================================================================================================");
//
//    return jsonRes.replace<T>(
//      body: _decode<T>(jsonRes.body),
//    );
//  }
//
//  @override
//  // all objects should implements toJson method
//  Request convertRequest(Request request) => super.convertRequest(request);
//}
