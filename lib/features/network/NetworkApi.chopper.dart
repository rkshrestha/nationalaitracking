// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NetworkApi.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$NetworkApi extends NetworkApi {
  _$NetworkApi([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = NetworkApi;

  Future<Response> sendLoginRequest(LoginRequest request) {
    final $url = '/api/user/login';
    final $body = request;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendForgotPasswordRequest(String techId) {
    final $url = '/api/forgot-password';
    final Map<String, dynamic> $params = {'techId': techId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendChangePasswordRequest(
      ChangePasswordModel changeRequest) {
    final $url = '/api/user/change_password';
    final $body = changeRequest;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncSpeciesList() {
    final $url = '/api/species/get_all';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncBreedList() {
    final $url = '/api/species_breed/get_all';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncProvinceList() {
    final $url = '/api/provinces/get_all';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncDistrictList() {
    final $url = '/api/districts/get_all';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncMunicipalityList() {
    final $url = '/api/municipalities/get_all';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncExitReasonList() {
    final $url = '/api/exit_reason/get_all';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendSemenData(List<Semen> semenList) {
    final $url = '/api/semen/add';
    final $body = semenList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendNitrogenData(List<LiquidNitrogen> liquidNitrogenList) {
    final $url = '/api/nitrogen/add';
    final $body = liquidNitrogenList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendFarmerData(List<Farmer> farmerList, int userId) {
    final $url = '/api/farmer/${userId}/add';
    final $body = farmerList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendAnimalData(List<Animal> animalList, int userId) {
    final $url = '/api/animal/${userId}/add';
    final $body = animalList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendAIData(List<ArtificialInsemination> aiList, int userId) {
    final $url = '/api/artificial-insemination/${userId}/add';
    final $body = aiList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendCalvingData(List<Calving> calvingList, int userId) {
    final $url = '/api/calving/${userId}/add';
    final $body = calvingList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendExitRecordData(
      List<ExitRecord> exitRecordList, int userId) {
    final $url = '/api/exit-record/${userId}/add';
    final $body = exitRecordList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendPDData(List<PregnancyDiagnosis> pdList, int userId) {
    final $url = '/api/pregnancy-diagnosis/${userId}/add';
    final $body = pdList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> sendAllData(SyncModel allList, int userId) {
    final $url = '/api/add/${userId}/all';
    final $body = allList;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncGetSemenList(int userId) {
    final $url = '/api/semen/semen-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncGetLiquidNitrogenList(int userId) {
    final $url = '/api/nitrogen/nitrogen-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncGetFarmerList(int userId) {
    final $url = '/api/farmer/farmer-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncGetAnimalList(int userId) {
    final $url = '/api/animal/animal-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncGetAIList(int userId) {
    final $url = '/api/artificial-insemination/artificial-insemination-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncGetPDList(int userId, int farmerId, int animalId) {
    final $url = '/api/pregnancy-diagnosis/pregnancy-diagnosis-list';
    final Map<String, dynamic> $params = {
      'UserId': userId,
      'FarmerId': farmerId,
      'AnimalId': animalId
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncGetCalvingList(int userId) {
    final $url = '/api/calving/calving-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> syncGetExitRecordList(int userId) {
    final $url = '/api/exit-record/exit-record-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> getAllDataFromServer(int userId) {
    final $url = '/api/user/get_all';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('POST', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> getNotification(int userId) {
    final $url = '/api/notification/show-notification-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> getPDNotification(int userId) {
    final $url = '/api/notification/show-notification-pd-list';
    final Map<String, dynamic> $params = {'UserId': userId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }
}
