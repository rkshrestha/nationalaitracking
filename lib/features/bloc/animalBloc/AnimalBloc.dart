import 'dart:async';

//import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/Breed.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/Species.dart';

class AnimalBloc extends BaseBloc {
  AnimalRepo animalRepo;


  StreamController<SCResponse<String>> scSaveAnimal =
      StreamController.broadcast();

  StreamController<SCResponse<Animal>> scGetAnimal =
      StreamController.broadcast();
  StreamController<SCResponse<Animal>> scGetAnimalId =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scAnimalList =
      StreamController.broadcast();


  StreamController<SCResponse<List<Animal>>> scUnSyncAnimalList =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scSyncAnimalList =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scAnimalListByFarmer =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scAnimalListById =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scUnSyncAnimalListByFarmer =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scSyncAnimalListByFarmer =
  StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncSpeciesList =
//      StreamController.broadcast();

  StreamController<SCResponse<List<Species>>> scSyncSpeciesList =
  StreamController.broadcast();


//  StreamController<SCResponse<String>> scSyncBreedList =
//  StreamController.broadcast();

  StreamController<SCResponse<List<Breed>>> scSyncBreedList =
  StreamController.broadcast();

  StreamController<SCResponse<List<Species>>> scSpeciesList =
      StreamController.broadcast();

  StreamController<SCResponse<List<Breed>>> scBreedList =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scDeleteAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<String>> scUpdateAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scSendAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scSyncGetAnimalData =
  StreamController.broadcast();

  StreamController<SCResponse<List<Animal>>> scUpdateServerId =
  StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncGetAnimalData =
//  StreamController.broadcast();
  AnimalBloc(this.animalRepo);

  Stream<SCResponse<String>> saveAnimal(context,Animal animal, Farmer farmer, Species species, Breed breed) {
    this.animalRepo.saveAnimal(context,animal, farmer, species, breed, scSaveAnimal.sink);
    return scSaveAnimal.stream;
  }

  Stream<SCResponse<Animal>> getAnimalBySpeciesCode(context,String speciesCode) {
    this.animalRepo.getAnimalBySpeciesCode(context,speciesCode, scGetAnimal);
    return scGetAnimal.stream;
  }

  Stream<SCResponse<Animal>> getAnimalByaId(String animalId) {
    this.animalRepo.getAnimalByaId(animalId, scGetAnimalId);
    return scGetAnimalId.stream;
  }

  Stream<SCResponse<List<Animal>>> getAnimalListById(String animalId) {
    this.animalRepo.getAnimalListById(animalId,scAnimalListById.sink);
    return scAnimalListById.stream;
  }


  Stream<SCResponse<List<Animal>>> getAnimalListByFarmer(Farmer farmer) {
    this.animalRepo.getAnimalListByFarmer(farmer,scAnimalListByFarmer.sink);
    return scAnimalListByFarmer.stream;
  }

  Stream<SCResponse<List<Animal>>> getUnSyncAnimalListByFarmer(Farmer farmer) {
    this.animalRepo.getUnSyncAnimalListByFarmer(farmer,scUnSyncAnimalListByFarmer.sink);
    return scUnSyncAnimalListByFarmer.stream;
  }

  Stream<SCResponse<List<Animal>>> getSyncAnimalListByFarmer(Farmer farmer) {
    this.animalRepo.getSyncAnimalListByFarmer(farmer,scSyncAnimalListByFarmer.sink);
    return scSyncAnimalListByFarmer.stream;
  }

  Stream<SCResponse<List<Animal>>> getAnimalList() {
    this.animalRepo.getAnimalList(scAnimalList.sink);
    return scAnimalList.stream;
  }




  Stream<SCResponse<List<Animal>>> getUnSyncAnimalList()  {
    this.animalRepo.getUnSyncAnimalList(scUnSyncAnimalList.sink);
    return scUnSyncAnimalList.stream;
  }

  Stream<SCResponse<List<Animal>>> getSyncAnimalList() {
    this.animalRepo.getSyncAnimalList(scSyncAnimalList.sink);
    return scSyncAnimalList.stream;
  }

//  Stream<SCResponse<String>> syncSpeciesList() {
//    this.animalRepo.syncSpeciesList(scSyncSpeciesList.sink);
//    return scSyncSpeciesList.stream;
//  }

  Stream<SCResponse<List<Species>>> syncSpeciesList() {
    this.animalRepo.syncSpeciesList(scSyncSpeciesList.sink);
    return scSyncSpeciesList.stream;
  }


  Stream<SCResponse<List<Species>>> getSpeciesList() {
    this.animalRepo.getSpeciesList(scSpeciesList.sink);
    return scSpeciesList.stream;
  }


  Stream<SCResponse<List<Breed>>> getBreedList(int speciesId) {
    this.animalRepo.getBreedList(speciesId,scBreedList.sink);
    return scBreedList.stream;
  }


  Stream<SCResponse<String>> deleteAnimal(context,Animal animal) {
    this.animalRepo.deleteAnimal(context,animal, scDeleteAnimal.sink);
    return scDeleteAnimal.stream;
  }


//  Stream<SCResponse<String>> syncBreedList() {
//    this.animalRepo.syncBreedList(scSyncBreedList.sink);
//    return scSyncBreedList.stream;
//  }


  Stream<SCResponse<List<Breed>>> syncBreedList() {
    this.animalRepo.syncBreedList(scSyncBreedList.sink);
    return scSyncBreedList.stream;
  }


  Stream<SCResponse<String>> updateAnimal(context,Animal animal, Farmer farmer, Species species, Breed breed) {
    this.animalRepo.updateAnimal(context,animal, farmer, species, breed, scUpdateAnimal.sink);
    return scUpdateAnimal.stream;
  }

  Stream<SCResponse<List<Animal>>> sendAnimalData(List<Animal> animal) {
    this.animalRepo.sendAnimalData(animal, scSendAnimal.sink);
    return scSendAnimal.stream;
  }


//  Stream<SCResponse<String>> syncGetAnimalData() {
//    this.animalRepo.syncGetAnimalData( scSyncGetAnimalData.sink);
//    return scSyncGetAnimalData.stream;
//  }

  Stream<SCResponse<List<Animal>>> syncGetAnimalData() {
    this.animalRepo.syncGetAnimalData( scSyncGetAnimalData.sink);
    return scSyncGetAnimalData.stream;
  }


  Stream<SCResponse<List<Animal>>> updateAnimalServerIdAndStatus(List<Animal> animal) {
    this.animalRepo.updateAnimalServerIdAndStatus(animal, scUpdateServerId.sink);
    return scUpdateServerId.stream;
  }



  @override
  void onDispose() {
    scSaveAnimal.close();
    scGetAnimal.close();
    scAnimalList.close();
    scSyncSpeciesList.close();
    scSpeciesList.close();
    scBreedList.close();
    scDeleteAnimal.close();
    scSyncBreedList.close();
    scUpdateAnimal.close();
    scAnimalListByFarmer.close();
    scSendAnimal.close();
    scSyncGetAnimalData.close();
    scSyncAnimalList.close();
    scUnSyncAnimalList.close();
    scUnSyncAnimalListByFarmer.close();
    scSyncAnimalListByFarmer.close();
    scGetAnimalId.close();
    scAnimalListById.close();
    scUpdateServerId.close();
  }
}
