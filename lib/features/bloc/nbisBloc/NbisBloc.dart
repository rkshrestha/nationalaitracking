import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/District.dart';
import 'package:nbis/features/repo/Municipality.dart';
import 'package:nbis/features/repo/NbisRepo.dart';
import 'package:nbis/features/repo/Province.dart';
import 'package:nbis/features/repo/SyncModel.dart';

class NbisBloc extends BaseBloc {
  NbisRepo nbisRepo;

//  StreamController<SCResponse<String>> scSyncProvinceList =
//      StreamController.broadcast();

  StreamController<SCResponse<List<Province>>> scSyncProvinceList =
  StreamController.broadcast();

  StreamController<SCResponse<List<Province>>> scProvinceList =
      StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncDistrictList =
//      StreamController.broadcast();

  StreamController<SCResponse<List<District>>> scSyncDistrictList =
  StreamController.broadcast();


  StreamController<SCResponse<List<District>>> scDistrictList =
      StreamController.broadcast();



  StreamController<SCResponse<List<Municipality>>> scSyncMunicipalityList =
      StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncMunicipalityList =
//  StreamController.broadcast();


  StreamController<SCResponse<List<Municipality>>> scMunicipalityList =
      StreamController.broadcast();

  StreamController<SCResponse<Province>> scGetProvince =
      StreamController.broadcast();
  StreamController<SCResponse<District>> scGetDistrict =
      StreamController.broadcast();
  StreamController<SCResponse<Municipality>> scGetMunicipality =
      StreamController.broadcast();

  StreamController<SCResponse<SyncModel>> scSendAll =
  StreamController.broadcast();

//  StreamController<SCResponse<List<SyncModel>>> scSendAll =
//  StreamController.broadcast();

  NbisBloc(this.nbisRepo);

//  Stream<SCResponse<String>> syncProvinceList() {
//    this.nbisRepo.syncProvinceList(scSyncProvinceList.sink);
//    return scSyncProvinceList.stream;
//  }

  Stream<SCResponse<List<Province>>> syncProvinceList() {
    this.nbisRepo.syncProvinceList(scSyncProvinceList.sink);
    return scSyncProvinceList.stream;
  }

  Stream<SCResponse<List<Province>>> getProvinceList() {
    this.nbisRepo.getProvinceList(scProvinceList.sink);
    return scProvinceList.stream;
  }

//  Stream<SCResponse<String>> syncDistrictList() {
//    this.nbisRepo.syncDistrictList(scSyncDistrictList.sink);
//    return scSyncDistrictList.stream;
//  }

  Stream<SCResponse<List<District>>> syncDistrictList() {
    this.nbisRepo.syncDistrictList(scSyncDistrictList.sink);
    return scSyncDistrictList.stream;
  }

  Stream<SCResponse<List<District>>> getDistrictList(int provinceId) {
    this.nbisRepo.getDistrictList(provinceId, scDistrictList.sink);
    return scDistrictList.stream;
  }

//  Stream<SCResponse<String>> syncMunicipalityList() {
//    this.nbisRepo.syncMunicipalityList(scSyncMunicipalityList.sink);
//    return scSyncMunicipalityList.stream;
//  }

  Stream<SCResponse<List<Municipality>>> syncMunicipalityList() {
    this.nbisRepo.syncMunicipalityList(scSyncMunicipalityList.sink);
    return scSyncMunicipalityList.stream;
  }

  Stream<SCResponse<List<Municipality>>> getMunicipalityList(int districtId) {
    this.nbisRepo.getMunicipalityList(districtId, scMunicipalityList.sink);
    return scMunicipalityList.stream;
  }

//  Stream<SCResponse<Province>> getProvince(int provinceId) {
//    this.nbisRepo.getProvince(provinceId, scGetProvince);
//    return scGetProvince.stream;
//  }

   getProvince(int provinceId) {
   return this.nbisRepo.getProvince(provinceId);
  }


  Stream<SCResponse<District>> getDistrict(int districtId) {
    this.nbisRepo.getDistrict(districtId, scGetDistrict);
    return scGetDistrict.stream;
  }

  Stream<SCResponse<Municipality>> getMunicipality(int municipalityId) {
    this.nbisRepo.getMunicipality(municipalityId, scGetMunicipality);
    return scGetMunicipality.stream;
  }


  Stream<SCResponse<SyncModel>> sendAllData(SyncModel allList) {
    this.nbisRepo.sendAllData(allList, scSendAll.sink);
    return scSendAll.stream;
  }

//  Stream<SCResponse<List<SyncModel>>> sendAllData(List<SyncModel> allList) {
//    this.nbisRepo.sendAllData(allList, scSendAll.sink);
//    return scSendAll.stream;
//  }

  @override
  void onDispose() {
    scProvinceList.close();
    scSyncProvinceList.close();
    scSyncDistrictList.close();
    scDistrictList.close();
    scMunicipalityList.close();
    scSyncMunicipalityList.close();
    scGetMunicipality.close();
    scGetDistrict.close();
    scGetProvince.close();
    scSendAll.close();
  }
}
