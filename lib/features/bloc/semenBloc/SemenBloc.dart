import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/repo/SemenRepo.dart';

class SemenBloc extends BaseBloc {
  SemenRepo semenRepo;

  StreamController<SCResponse<String>> scSaveSemen =
      StreamController.broadcast();

  StreamController<SCResponse<List<Semen>>> scSemenUnSyncList =
      StreamController.broadcast();
  StreamController<SCResponse<List<Semen>>> scSemenSyncList =
  StreamController.broadcast();

  StreamController<SCResponse<List<Semen>>> scSemenList =
  StreamController.broadcast();

  StreamController<SCResponse<String>> scDeleteSemen =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scUpdateSemen =
      StreamController.broadcast();

  StreamController<SCResponse<List<Semen>>> scSendSemen =
      StreamController.broadcast();

  StreamController<SCResponse<List<Semen>>> scUpdateSemenId =
  StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncGetSemenList =
//  StreamController.broadcast();

  StreamController<SCResponse<List<Semen>>> scSyncGetSemenList =
  StreamController.broadcast();

  SemenBloc(this.semenRepo);

  Stream<SCResponse<String>> saveSemen(context,Semen semen) {
    this.semenRepo.saveSemen(context,semen, scSaveSemen.sink);
    return scSaveSemen.stream;
  }

  Stream<SCResponse<List<Semen>>> getSemenUnSyncList() {
    this.semenRepo.getSemenUnSyncList(scSemenUnSyncList.sink);
    return scSemenUnSyncList.stream;
  }

  Stream<SCResponse<List<Semen>>> getSemenSyncList() {
    this.semenRepo.getSemenSyncList(scSemenSyncList.sink);
    return scSemenSyncList.stream;
  }

  Stream<SCResponse<List<Semen>>> getSemenList() {
    this.semenRepo.getSemenList(scSemenList.sink);
    return scSemenList.stream;
  }

  Stream<SCResponse<String>> deleteSemen(context,Semen semen) {
    this.semenRepo.deleteSemen(context,semen, scDeleteSemen.sink);
    return scDeleteSemen.stream;
  }

  Stream<SCResponse<String>> updateSemen(context,Semen semen) {
    this.semenRepo.updateSemen(context,semen, scUpdateSemen.sink);
    return scUpdateSemen.stream;
  }

  Stream<SCResponse<List<Semen>>> sendSemenData(List<Semen> semenList) {
    this.semenRepo.sendSemenData(semenList, scSendSemen.sink);
    return scSendSemen.stream;
  }

  Stream<SCResponse<List<Semen>>> updateSemenServerIdAndStatus(List<Semen> semenList) {
    this.semenRepo.updateSemenServerIdAndStatus(semenList, scUpdateSemenId.sink);
    return scUpdateSemenId.stream;
  }


//  Stream<SCResponse<String>> syncGetSemenList() {
//    this.semenRepo.syncGetSemenList(scSyncGetSemenList.sink);
//    return scSyncGetSemenList.stream;
//  }
  Stream<SCResponse<List<Semen>>> syncGetSemenList() {
    this.semenRepo.syncGetSemenList(scSyncGetSemenList.sink);
    return scSyncGetSemenList.stream;
  }



  @override
  void onDispose() {
    scSaveSemen.close();
    scSemenUnSyncList.close();
    scDeleteSemen.close();
    scUpdateSemen.close();
    scSendSemen.close();
    scSyncGetSemenList.close();
    scSemenSyncList.close();
    scSemenList.close();
    scUpdateSemenId.close();
  }
}
