import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/LiquidNitrogenRepo.dart';

class NitrogenBloc extends BaseBloc {
  LiquidNitrogenRepo liquidNitrogenRepo;

  StreamController<SCResponse<String>> scSaveNitrogen =
      StreamController.broadcast();

  StreamController<SCResponse<List<LiquidNitrogen>>> scNitrogenList =
      StreamController.broadcast();

  StreamController<SCResponse<List<LiquidNitrogen>>> scSyncNitrogenList =
  StreamController.broadcast();

  StreamController<SCResponse<List<LiquidNitrogen>>> scUnSyncNitrogenList =
  StreamController.broadcast();
  
  StreamController<SCResponse<String>> scDeleteNitrogen =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scUpdateNitrogen =
      StreamController.broadcast();

  StreamController<SCResponse<List<LiquidNitrogen>>> scSendNitrogen =
  StreamController.broadcast();


  StreamController<SCResponse<List<LiquidNitrogen>>> scUpdateNitrogenId =
  StreamController.broadcast();


//  StreamController<SCResponse<String>> scSyncGetLiquidNitrogenList =
//  StreamController.broadcast();

  StreamController<SCResponse<List<LiquidNitrogen>>> scSyncGetLiquidNitrogenList =
  StreamController.broadcast();

  NitrogenBloc(this.liquidNitrogenRepo);

  Stream<SCResponse<String>> saveNitrogen(context,LiquidNitrogen liquidNitrogen) {
    this.liquidNitrogenRepo.saveNitrogen(context,liquidNitrogen, scSaveNitrogen.sink);
    return scSaveNitrogen.stream;
  }

  Stream<SCResponse<List<LiquidNitrogen>>> getLiquidNitrogenList() {
    this.liquidNitrogenRepo.getLiquidNitrogenList(scNitrogenList.sink);
    return scNitrogenList.stream;
  }

  Stream<SCResponse<List<LiquidNitrogen>>> getSyncLiquidNitrogenList() {
    this.liquidNitrogenRepo.getSyncLiquidNitrogenList(scSyncNitrogenList.sink);
    return scSyncNitrogenList.stream;
  }

  Stream<SCResponse<List<LiquidNitrogen>>> getUnSyncLiquidNitrogenList() {
    this.liquidNitrogenRepo.getUnSyncLiquidNitrogenList(scUnSyncNitrogenList.sink);
    return scUnSyncNitrogenList.stream;
  }

  Stream<SCResponse<String>> deleteNitrogen(context,LiquidNitrogen nitrogen) {
    this.liquidNitrogenRepo.deleteNitrogen(context,nitrogen, scDeleteNitrogen.sink);
    return scDeleteNitrogen.stream;
  }

  Stream<SCResponse<String>> updateNitrogen(context,LiquidNitrogen liquidNitrogen) {
    this
        .liquidNitrogenRepo
        .updateNitrogen(context,liquidNitrogen, scUpdateNitrogen.sink);
    return scUpdateNitrogen.stream;
  }


  Stream<SCResponse<List<LiquidNitrogen>>> sendNitrogenData(List<LiquidNitrogen> nitrogenList) {
    this.liquidNitrogenRepo.sendNitrogenData(nitrogenList, scSendNitrogen.sink);
    return scSendNitrogen.stream;
  }

  Stream<SCResponse<List<LiquidNitrogen>>> updateNitrogenServerIdAndStatus(List<LiquidNitrogen> nitrogenList) {
    this.liquidNitrogenRepo.updateNitrogenServerIdAndStatus(nitrogenList, scUpdateNitrogenId.sink);
    return scUpdateNitrogenId.stream;
  }


  Stream<SCResponse<List<LiquidNitrogen>>> syncGetLiquidNitrogenList() {
    this.liquidNitrogenRepo
        .syncGetNitrogenList(scSyncGetLiquidNitrogenList.sink);
    return scSyncGetLiquidNitrogenList.stream;
  }

//  Stream<SCResponse<String>> syncGetLiquidNitrogenList() {
//    this.liquidNitrogenRepo
//        .syncGetNitrogenList(scSyncGetLiquidNitrogenList.sink);
//    return scSyncGetLiquidNitrogenList.stream;
//  }

  @override
  void onDispose() {
    scSaveNitrogen.close();
    scNitrogenList.close();
    scDeleteNitrogen.close();
    scUpdateNitrogen.close();
    scSendNitrogen.close();
    scSyncGetLiquidNitrogenList.close();
    scSyncNitrogenList.close();
    scUnSyncNitrogenList.close();
    scUpdateNitrogenId.close();
  }
}
