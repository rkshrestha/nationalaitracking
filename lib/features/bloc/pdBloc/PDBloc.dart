import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/PDRepo.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';

class PDBloc extends BaseBloc {
  PDRepo pdRepo;

  StreamController<SCResponse<String>> scSavePD = StreamController.broadcast();

  StreamController<SCResponse<List<PregnancyDiagnosis>>> scPDList =
      StreamController.broadcast();

  StreamController<SCResponse<List<PregnancyDiagnosis>>> scUnSyncPDList =
  StreamController.broadcast();

  StreamController<SCResponse<List<PregnancyDiagnosis>>> scSyncPDList =
  StreamController.broadcast();

  StreamController<SCResponse<String>> scDeletePD =
      StreamController.broadcast();
  StreamController<SCResponse<List<PregnancyDiagnosis>>> scPDListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<PregnancyDiagnosis>>> scUnSyncPDListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<PregnancyDiagnosis>>> scSyncPDListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<String>> scUpdatePD =
      StreamController.broadcast();
  StreamController<SCResponse<List<PregnancyDiagnosis>>> scSendPD =
  StreamController.broadcast();


  StreamController<SCResponse<List<PregnancyDiagnosis>>> scUpdateServerId =
  StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncGetPdData =
//  StreamController.broadcast();

  StreamController<SCResponse<List<PregnancyDiagnosis>>> scSyncGetPdData =
  StreamController.broadcast();

  PDBloc(this.pdRepo);

  Stream<SCResponse<String>> savePD(context,PregnancyDiagnosis pd, Farmer farmer,
      Animal animal, int speciesId, int breedId) {
    this.pdRepo.savePD(context,pd, farmer, animal, speciesId, breedId, scSavePD.sink);
    return scSavePD.stream;
  }

  Stream<SCResponse<List<PregnancyDiagnosis>>> getPdList() {
    this.pdRepo.getPdList(scPDList.sink);
    return scPDList.stream;
  }

  Stream<SCResponse<List<PregnancyDiagnosis>>> getUnSyncPdList() {
    this.pdRepo.getUnSyncPdList(scUnSyncPDList.sink);
    return scUnSyncPDList.stream;
  }

  Stream<SCResponse<List<PregnancyDiagnosis>>> getSyncPdList() {
    this.pdRepo.getSyncPdList(scSyncPDList.sink);
    return scSyncPDList.stream;
  }

  Stream<SCResponse<String>> deletePD(context,PregnancyDiagnosis pd) {
    this.pdRepo.deletePD(context,pd, scDeletePD.sink);
    return scDeletePD.stream;
  }

  Stream<SCResponse<String>> updatePD(context,
      PregnancyDiagnosis pd, Farmer farmer, Animal animal) {
    this.pdRepo.updatePD(context,pd, farmer, animal, scUpdatePD.sink);
    return scUpdatePD.stream;
  }

  Stream<SCResponse<List<PregnancyDiagnosis>>> sendPDData(List<PregnancyDiagnosis> pdList) {
    this.pdRepo.sendPdData(pdList, scSendPD.sink);
    return scSendPD.stream;
  }

  Stream<SCResponse<List<PregnancyDiagnosis>>> updatePdServerIdAndStatus(List<PregnancyDiagnosis> pdList) {
    this.pdRepo.updatePdServerIdAndStatus(pdList, scUpdateServerId.sink);
    return scUpdateServerId.stream;
  }




  Stream<SCResponse<List<PregnancyDiagnosis>>> syncGetPdData() {
    this.pdRepo.syncGetPdData(scSyncGetPdData.sink);
    return scSyncGetPdData.stream;
  }

//  Stream<SCResponse<String>> syncGetPdData() {
//    this.pdRepo.syncGetPdData(scSyncGetPdData.sink);
//    return scSyncGetPdData.stream;
//  }

  Stream<SCResponse<List<PregnancyDiagnosis>>> getPDListByAnimal(Animal animal) {
    this.pdRepo.getPDListByAnimal(animal,scPDListByAnimal.sink);
    return scPDListByAnimal.stream;
  }

  Stream<SCResponse<List<PregnancyDiagnosis>>> getUnSyncPDListByAnimal(Animal animal) {
    this.pdRepo.getUnSyncPDListByAnimal(animal,scUnSyncPDListByAnimal.sink);
    return scUnSyncPDListByAnimal.stream;
  }

  Stream<SCResponse<List<PregnancyDiagnosis>>> getSyncPDListByAnimal(Animal animal) {
    this.pdRepo.getSyncPDListByAnimal(animal,scSyncPDListByAnimal.sink);
    return scSyncPDListByAnimal.stream;
  }

  @override
  void onDispose() {
    scSavePD.close();
    scPDList.close();
    scDeletePD.close();
    scUpdatePD.close();
    scSendPD.close();
    scSyncGetPdData.close();
    scUnSyncPDList.close();
    scSyncPDList.close();
    scPDListByAnimal.close();
    scUnSyncPDListByAnimal.close();
    scSyncPDListByAnimal.close();
    scUpdateServerId.close();
  }
}
