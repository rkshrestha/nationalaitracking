import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ExitReason.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/ExitRecordRepo.dart';
import 'package:nbis/features/repo/Farmer.dart';

class ExitRecordBloc extends BaseBloc {
  ExitRecordRepo exitRecordRepo;

  StreamController<SCResponse<String>> scSaveExit =
      StreamController.broadcast();

  StreamController<SCResponse<List<ExitRecord>>> scExitList =
      StreamController.broadcast();

  StreamController<SCResponse<List<ExitRecord>>> scSyncExitList =
  StreamController.broadcast();

  StreamController<SCResponse<List<ExitRecord>>> scUnSyncExitList =
  StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncExitReasonList =
//      StreamController.broadcast();

  StreamController<SCResponse<List<ExitReason>>> scSyncExitReasonList =
  StreamController.broadcast();

  StreamController<SCResponse<List<ExitReason>>> scExitReasonList =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scDeleteExitRecord =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scUpdateExitRecord =
      StreamController.broadcast();

  StreamController<SCResponse<List<ExitRecord>>> scSendExitRecord =
  StreamController.broadcast();


  StreamController<SCResponse<List<ExitRecord>>> scUpdateExitRecordId =
  StreamController.broadcast();

  StreamController<SCResponse<List<ExitRecord>>> scSyncGetExitRecordData =
  StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncGetExitRecordData =
//  StreamController.broadcast();

  StreamController<SCResponse<List<ExitRecord>>> scExitListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<ExitRecord>>> scUnSyncExitListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<ExitRecord>>> scSyncExitListByAnimal =
  StreamController.broadcast();

  ExitRecordBloc(this.exitRecordRepo);

  Stream<SCResponse<String>> saveExit(context,
      ExitRecord exit, Farmer farmer, Animal animal, ExitReason exitReason) {
    this
        .exitRecordRepo
        .saveExit(context,exit, farmer, animal, exitReason, scSaveExit.sink);
    return scSaveExit.stream;
  }

  Stream<SCResponse<List<ExitRecord>>> getExitRecordList() {
    this.exitRecordRepo.getExitRecordList(scExitList.sink);
    return scExitList.stream;
  }


  Stream<SCResponse<List<ExitRecord>>> getSyncExitRecordList() {
    this.exitRecordRepo.getSyncExitRecordList(scSyncExitList.sink);
    return scSyncExitList.stream;
  }

  Stream<SCResponse<List<ExitRecord>>> getUnSyncExitRecordList() {
    this.exitRecordRepo.getUnSyncExitRecordList(scUnSyncExitList.sink);
    return scUnSyncExitList.stream;
  }

//  Stream<SCResponse<String>> syncExitReasonList() {
//    this.exitRecordRepo.syncExitReasonList(scSyncExitReasonList.sink);
//    return scSyncExitReasonList.stream;
//  }


  Stream<SCResponse<List<ExitReason>>> syncExitReasonList() {
    this.exitRecordRepo.syncExitReasonList(scSyncExitReasonList.sink);
    return scSyncExitReasonList.stream;
  }


  Stream<SCResponse<List<ExitReason>>> getExitReasonList() {
    this.exitRecordRepo.getExitReasonList(scExitReasonList.sink);
    return scExitReasonList.stream;
  }

  Stream<SCResponse<String>> deleteExitRecord(context,ExitRecord exitRecord) {
    this.exitRecordRepo.deleteExitRecord(context,exitRecord, scDeleteExitRecord.sink);
    return scDeleteExitRecord.stream;
  }

  Stream<SCResponse<String>> updateExitRecord(context,
      ExitRecord exit, Farmer farmer, Animal animal, ExitReason exitReason) {
    this.exitRecordRepo.updateExitRecord(context,
        exit, farmer, animal, exitReason, scUpdateExitRecord.sink);
    return scUpdateExitRecord.stream;
  }

  Stream<SCResponse<List<ExitRecord>>>  sendExitRecordData(List<ExitRecord> exitRecordList) {
    this.exitRecordRepo.sendExitRecordData(exitRecordList, scSendExitRecord.sink);
    return scSendExitRecord.stream;
  }

  Stream<SCResponse<List<ExitRecord>>>  updateExitRecordServerIdAndStatus(List<ExitRecord> exitRecordList) {
    this.exitRecordRepo.updateExitRecordServerIdAndStatus(exitRecordList, scUpdateExitRecordId.sink);
    return scUpdateExitRecordId.stream;
  }



  Stream<SCResponse<List<ExitRecord>>> syncGetExitRecordData() {
    this.exitRecordRepo.syncGetExitRecordData(scSyncGetExitRecordData.sink);
    return scSyncGetExitRecordData.stream;
  }


//  Stream<SCResponse<String>> syncGetExitRecordData() {
//    this.exitRecordRepo.syncGetExitRecordData(scSyncGetExitRecordData.sink);
//    return scSyncGetExitRecordData.stream;
//  }

  Stream<SCResponse<List<ExitRecord>>> getExitRecordListByAnimal(Animal animal) {
    this.exitRecordRepo.getExitRecordListByAnimal(animal,scExitListByAnimal.sink);
    return scExitListByAnimal.stream;
  }

  Stream<SCResponse<List<ExitRecord>>> getUnSyncExitRecordListByAnimal(Animal animal) {
    this.exitRecordRepo.getUnSyncExitRecordListByAnimal(animal,scUnSyncExitListByAnimal.sink);
    return scUnSyncExitListByAnimal.stream;
  }


  Stream<SCResponse<List<ExitRecord>>> getSyncExitRecordListByAnimal(Animal animal) {
    this.exitRecordRepo.getSyncExitRecordListByAnimal(animal,scSyncExitListByAnimal.sink);
    return scSyncExitListByAnimal.stream;
  }


  @override
  void onDispose() {
    scExitList.close();
    scSaveExit.close();
    scExitReasonList.close();
    scSyncExitReasonList.close();
    scDeleteExitRecord.close();
    scUpdateExitRecord.close();
    scSendExitRecord.close();
    scSyncGetExitRecordData.close();
    scExitListByAnimal.close();
    scSyncExitList.close();
    scUnSyncExitList.close();
    scUnSyncExitListByAnimal.close();
    scSyncExitListByAnimal.close();
    scUpdateExitRecordId.close();
  }
}
