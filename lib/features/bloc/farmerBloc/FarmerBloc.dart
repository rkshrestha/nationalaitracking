import 'dart:async';

//import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/District.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/features/repo/Municipality.dart';
import 'package:nbis/features/repo/Province.dart';
//import 'package:nbis/s/Logger.dart';

class FarmerBloc extends BaseBloc {
  FarmerRepo farmerRepo;


  StreamController<SCResponse<String>> scSaveFarmer =
      StreamController.broadcast();

  StreamController<SCResponse<List<Farmer>>> scFarmerList =
      StreamController.broadcast();

  StreamController<SCResponse<List<Farmer>>> scUnSyncFarmerList =
  StreamController.broadcast();

  StreamController<SCResponse<List<Farmer>>> scSyncFarmerList =
  StreamController.broadcast();

  StreamController<SCResponse<Farmer>> scGetFarmer =
      StreamController.broadcast();

  StreamController<SCResponse<Farmer>> scGetFarmerById =
  StreamController.broadcast();

  StreamController<SCResponse<Farmer>> scGetNumber =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scDeleteFarmer =
      StreamController.broadcast();
  StreamController<SCResponse<String>> scUpdateFarmer =
      StreamController.broadcast();

  StreamController<SCResponse<List<Farmer>>> scSendFarmer =
      StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncGetFarmerData =
//      StreamController.broadcast();
  StreamController<SCResponse<List<Farmer>>> scSyncGetFarmerData =
  StreamController.broadcast();


  StreamController<SCResponse<List<Farmer>>> scUpdateServerId =
  StreamController.broadcast();


  FarmerBloc(this.farmerRepo);


  Stream<SCResponse<String>> saveFarmer(context,Farmer farmer, Province province,
      District district, Municipality municipality) {
    this.farmerRepo.saveFarmer(context,
        farmer, province, district, municipality, scSaveFarmer.sink);
    return scSaveFarmer.stream;
  }


  Stream<SCResponse<List<Farmer>>> getFarmerList() {
    this.farmerRepo.getFarmerList(scFarmerList.sink);
    return scFarmerList.stream;
  }

  Stream<SCResponse<List<Farmer>>> getUnSyncFarmerList() {
    this.farmerRepo.getUnSyncFarmerList(scUnSyncFarmerList.sink);
    return scUnSyncFarmerList.stream;
  }

  Stream<SCResponse<List<Farmer>>> getSyncFarmerList() {
    this.farmerRepo.getSyncFarmerList(scSyncFarmerList.sink);
    return scSyncFarmerList.stream;
  }

  Stream<SCResponse<Farmer>> getFarmerByFId(String farmerId) {
    this.farmerRepo.getFarmerByfId(farmerId, scGetFarmerById);
    return scGetFarmerById.stream;
  }



  Stream<SCResponse<Farmer>> getFarmerByMobileNumber(context,String mblNumber) {

    this.farmerRepo.getFarmerByMobileNumber(context,mblNumber, scGetFarmer);
    return scGetFarmer.stream;
  }

  //test
  Stream<SCResponse<Farmer>> getFarmerMobileNumberById(String fId) {
    this.farmerRepo.getFarmerMobileNumberById(fId, scGetNumber);
    return scGetNumber.stream;
  }

  Stream<SCResponse<String>> deleteFarmer(context,Farmer farmer) {
    this.farmerRepo.deleteFarmer(context,farmer, scDeleteFarmer.sink);
    return scDeleteFarmer.stream;
  }

  Stream<SCResponse<String>> updateFarmer(context,Farmer farmer, Province province,
      District district, Municipality municipality) {
    this.farmerRepo.updateFarmer( context,
        farmer, province, district, municipality, scUpdateFarmer.sink);
    return scUpdateFarmer.stream;
  }

  Stream<SCResponse<List<Farmer>>> sendFarmerData(List<Farmer> farmerList) {
    this.farmerRepo.sendFarmerData(farmerList, scSendFarmer.sink);
    return scSendFarmer.stream;
  }


  Stream<SCResponse<List<Farmer>>> updateServerIdAndStatus(List<Farmer> farmerList) {
    this.farmerRepo.updateServerIdAndStatus(farmerList, scUpdateServerId.sink);
    return scUpdateServerId.stream;
  }


//  Stream<SCResponse<String>> syncGetFarmerData() {
//    this.farmerRepo.syncGetFarmerData(scSyncGetFarmerData.sink);
//    return scSyncGetFarmerData.stream;
//  }


  Stream<SCResponse<List<Farmer>>> syncGetFarmerData() {
    this.farmerRepo.syncGetFarmerData(scSyncGetFarmerData.sink);
    return scSyncGetFarmerData.stream;
  }



  @override
  void onDispose() {
    scSaveFarmer.close();
    scGetFarmer.close();
    scFarmerList.close();
    scDeleteFarmer.close();
    scUpdateFarmer.close();
    scGetNumber.close();
    scSendFarmer.close();
    scSyncGetFarmerData.close();
    scSyncFarmerList.close();
    scUnSyncFarmerList.close();
    scGetFarmerById.close();
    scUpdateServerId.close();

  }
}
