import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/AIRepo.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';
import 'package:nbis/s/Logger.dart';

class AIBloc extends BaseBloc {
  AIRepo aiRepo;

  StreamController<SCResponse<String>> scSaveAI = StreamController.broadcast();

  StreamController<SCResponse<List<ArtificialInsemination>>> scAIList =
      StreamController.broadcast();

  StreamController<SCResponse<List<ArtificialInsemination>>> scUnSyncAIList =
  StreamController.broadcast();

  StreamController<SCResponse<List<ArtificialInsemination>>> scSyncAIList =
  StreamController.broadcast();

  StreamController<SCResponse<String>> scDeleteAI =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scUpdateAI =
      StreamController.broadcast();

  StreamController<SCResponse<List<ArtificialInsemination>>> scSendAI = StreamController.broadcast();
  StreamController<SCResponse<List<ArtificialInsemination>>> scUpdateAIServerId = StreamController.broadcast();

  StreamController<SCResponse<List<ArtificialInsemination>>> scSyncGetAiData =
      StreamController.broadcast();

//  StreamController<SCResponse<String>> scSyncGetAiData =
//  StreamController.broadcast();

//  StreamController<SCResponse<List<NotificationRequest>>> scGetNotification =
//  StreamController.broadcast();

  StreamController<SCResponse<List<NotificationRequest>>> scGetNotification =
  StreamController();

  StreamController<SCResponse<List<ArtificialInsemination>>> scAIListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<ArtificialInsemination>>> scUnSyncAIListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<ArtificialInsemination>>> scSyncAIListByAnimal =
  StreamController.broadcast();

  AIBloc(this.aiRepo);

  Stream<SCResponse<String>> saveAI(context,ArtificialInsemination ai, Animal animal,
      Farmer farmer, int species, int breed) {
    this.aiRepo.saveAI(context,ai, animal, farmer, species, breed, scSaveAI.sink);
    return scSaveAI.stream;
  }

  Stream<SCResponse<List<ArtificialInsemination>>> getAIList() {
    this.aiRepo.getAIList(scAIList.sink);
    return scAIList.stream;
  }

  Stream<SCResponse<List<ArtificialInsemination>>> getUnSyncAIList() {
    this.aiRepo.getUnSyncAIList(scUnSyncAIList.sink);
    return scUnSyncAIList.stream;
  }


  Stream<SCResponse<List<ArtificialInsemination>>> getSyncAIList() {
    this.aiRepo.getSyncAIList(scSyncAIList.sink);
    return scSyncAIList.stream;
  }


  Stream<SCResponse<String>> deleteAI(context,ArtificialInsemination ai) {
    this.aiRepo.deleteAI(context,ai, scDeleteAI.sink);
    return scDeleteAI.stream;
  }

  Stream<SCResponse<String>> updateAI(context,
      ArtificialInsemination ai, Animal animal, Farmer farmer, int speciesId, int breedId) {
    this.aiRepo.updateAI(context,ai, animal, farmer,speciesId,breedId, scUpdateAI.sink);
    return scUpdateAI.stream;
  }

  Stream<SCResponse<List<ArtificialInsemination>>> sendAIData(List<ArtificialInsemination> aiList) {
    Logger.v("test sendAI: ");
    this.aiRepo.sendAIData(aiList, scSendAI.sink);
    return scSendAI.stream;
  }

  Stream<SCResponse<List<ArtificialInsemination>>> syncGetAIData() {
    this.aiRepo.syncGetAIData(scSyncGetAiData.sink);
    return scSyncGetAiData.stream;
  }


//  Stream<SCResponse<String>> syncGetAIData() {
//    this.aiRepo.syncGetAIData(scSyncGetAiData.sink);
//    return scSyncGetAiData.stream;
//  }

  Stream<SCResponse<List<NotificationRequest>>> getNotification() {
    Logger.v("call notification");
    this.aiRepo.getNotification(scGetNotification);
    return scGetNotification.stream;
  }

  Stream<SCResponse<List<ArtificialInsemination>>> getAIListByAnimal(Animal animal) {
    this.aiRepo.getAIListByAnimal(animal,scAIListByAnimal.sink);
    return scAIListByAnimal.stream;
  }

  Stream<SCResponse<List<ArtificialInsemination>>> getUnSyncAIListByAnimal(Animal animal) {
    this.aiRepo.getUnSyncAIListByAnimal(animal,scUnSyncAIListByAnimal.sink);
    return scUnSyncAIListByAnimal.stream;
  }

  Stream<SCResponse<List<ArtificialInsemination>>> getSyncAIListByAnimal(Animal animal) {
    this.aiRepo.getSyncAIListByAnimal(animal,scSyncAIListByAnimal.sink);
    return scSyncAIListByAnimal.stream;
  }


  Stream<SCResponse<List<ArtificialInsemination>>> updateAIServerIdAndStatus(List<ArtificialInsemination> aiList) {
    this.aiRepo.updateAIServerIdAndStatus(aiList, scUpdateAIServerId.sink);
    return scUpdateAIServerId.stream;
  }
  @override
  void onDispose() {
    scAIList.close();
    scSaveAI.close();
    scDeleteAI.close();
    scUpdateAI.close();
    scSendAI.close();
    scSyncGetAiData.close();
    scAIListByAnimal.close();
    scUnSyncAIList.close();
    scSyncAIList.close();
    scUnSyncAIListByAnimal.close();
    scSyncAIListByAnimal.close();
    scGetNotification.close();
    scUpdateAIServerId.close();
  }
}
