
import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/NotificationRepo.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';

class NotificationBloc extends BaseBloc{
  NotificationRepo notificationRepo;
  StreamController<SCResponse<List<NotificationRequest>>> scGetNotificationList =
  StreamController.broadcast();

  StreamController<SCResponse<List<NotificationRequest>>> scSaveNotificationData =
  StreamController.broadcast();
  StreamController<SCResponse<List<NotificationRequest>>> scSavePDNotificationData =
  StreamController.broadcast();

//  StreamController<SCResponse<List<NotificationRequest>>> scSaveNotificationData =
//  StreamController();

  NotificationBloc(this.notificationRepo);

  Stream<SCResponse<List<NotificationRequest>>> getNotificationList() {
    this.notificationRepo
        .getNotificationList(scGetNotificationList.sink);
    return scGetNotificationList.stream;
  }

//
  Stream<SCResponse<List<NotificationRequest>>> saveNotificationData() {
    this.notificationRepo
        .saveNotificationData(scSaveNotificationData.sink);
    return scSaveNotificationData.stream;
  }

  Stream<SCResponse<List<NotificationRequest>>> saveNotificationPDData() {
    this.notificationRepo
        .saveNotificationPDData(scSavePDNotificationData.sink);
    return scSavePDNotificationData.stream;
  }

//  Stream<SCResponse<List<NotificationRequest>>> saveNotificationData() {
//    this.notificationRepo
//        .saveNotificationData(scSaveNotificationData);
//    return scSaveNotificationData.stream;
//  }

  @override
  void onDispose() {
    scGetNotificationList.close();
    scSaveNotificationData.close();
    scSavePDNotificationData.close();
  }
}