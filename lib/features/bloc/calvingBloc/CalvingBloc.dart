import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/CalvingRepo.dart';
import 'package:nbis/features/repo/Farmer.dart';

class CalvingBloc extends BaseBloc {
  CalvingRepo calvingRepo;

  StreamController<SCResponse<String>> scSaveCalving =
      StreamController.broadcast();

  StreamController<SCResponse<List<Calving>>> scCalvingList =
      StreamController.broadcast();

  StreamController<SCResponse<List<Calving>>> scUnSyncCalvingList =
  StreamController.broadcast();

  StreamController<SCResponse<List<Calving>>> scSyncCalvingList =
  StreamController.broadcast();

  StreamController<SCResponse<String>> scDeleteCalving =
      StreamController.broadcast();

  StreamController<SCResponse<String>> scUpdateCalving =
      StreamController.broadcast();

  StreamController<SCResponse<List<Calving>>> scSendCalving =
  StreamController.broadcast();


  StreamController<SCResponse<List<Calving>>> scUpdateCalvingId =
  StreamController.broadcast();
//  StreamController<SCResponse<String>> scSyncGetCalvingData =
//  StreamController.broadcast();

  StreamController<SCResponse<List<Calving>>> scSyncGetCalvingData =
  StreamController.broadcast();

  StreamController<SCResponse<List<Calving>>> scCalvingListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<Calving>>> scUnSyncCalvingListByAnimal =
  StreamController.broadcast();

  StreamController<SCResponse<List<Calving>>> scSyncCalvingListByAnimal =
  StreamController.broadcast();

  CalvingBloc(this.calvingRepo);

  Stream<SCResponse<String>> saveCalving(context,Calving calving, Farmer farmer,
      Animal animal, int speciesId, int breedId) {
    this.calvingRepo.saveCalving(context,
        calving, farmer, animal, speciesId, breedId, scSaveCalving.sink);
    return scSaveCalving.stream;
  }

  Stream<SCResponse<List<Calving>>> getCalvingList() {
    this.calvingRepo.getCalvingList(scCalvingList.sink);
    return scCalvingList.stream;
  }

  Stream<SCResponse<List<Calving>>> getUnSyncCalvingList() {
    this.calvingRepo.getUnSyncCalvingList(scUnSyncCalvingList.sink);
    return scUnSyncCalvingList.stream;
  }

  Stream<SCResponse<List<Calving>>> getSyncCalvingList() {
    this.calvingRepo.getSyncCalvingList(scSyncCalvingList.sink);
    return scSyncCalvingList.stream;
  }

  Stream<SCResponse<String>> deleteCalving(context,Calving calving) {
    this.calvingRepo.deleteCalving(context,calving, scDeleteCalving.sink);
    return scDeleteCalving.stream;
  }

  Stream<SCResponse<String>> updateCalving(context,Calving calving,
      Farmer farmer,
      Animal animal,) {
    this
        .calvingRepo
        .updateCalving(context,calving, farmer, animal, scUpdateCalving.sink);
    return scUpdateCalving.stream;
  }

  Stream<SCResponse<List<Calving>>> sendCalvingData(List<Calving> calvingList) {
    this.calvingRepo.sendCalvingData(calvingList, scSendCalving.sink);
    return scSendCalving.stream;
  }

  Stream<SCResponse<List<Calving>>> syncGetCalvingData() {
    this.calvingRepo.syncGetCalvingData(scSyncGetCalvingData.sink);
    return scSyncGetCalvingData.stream;
  }

//  Stream<SCResponse<String>> syncGetCalvingData() {
//    this.calvingRepo.syncGetCalvingData(scSyncGetCalvingData.sink);
//    return scSyncGetCalvingData.stream;
//  }

  Stream<SCResponse<List<Calving>>> getCalvingListByAnimal(Animal animal) {
    this.calvingRepo.getCalvingListByAnimal(animal,scCalvingListByAnimal.sink);
    return scCalvingListByAnimal.stream;
  }

  Stream<SCResponse<List<Calving>>> getUnSyncCalvingListByAnimal(Animal animal) {
    this.calvingRepo.getUnSyncCalvingListByAnimal(animal,scUnSyncCalvingListByAnimal.sink);
    return scUnSyncCalvingListByAnimal.stream;
  }

  Stream<SCResponse<List<Calving>>> getSyncCalvingListByAnimal(Animal animal) {
    this.calvingRepo.getSyncCalvingListByAnimal(animal,scSyncCalvingListByAnimal.sink);
    return scSyncCalvingListByAnimal.stream;
  }


  Stream<SCResponse<List<Calving>>> updateCalvingServerIdAndStatus(List<Calving> calvingList) {
    this.calvingRepo.updateCalvingServerIdAndStatus(calvingList, scUpdateCalvingId.sink);
    return scUpdateCalvingId.stream;
  }


  @override
  void onDispose() {
    scCalvingList.close();
    scSaveCalving.close();
    scUpdateCalving.close();
    scDeleteCalving.close();
    scSendCalving.close();
    scSyncGetCalvingData.close();
    scCalvingListByAnimal.close();
    scUnSyncCalvingList.close();
    scSyncCalvingList.close();
    scUnSyncCalvingListByAnimal.close();
    scSyncCalvingListByAnimal.close();
    scUpdateCalvingId.close();
  }
}
