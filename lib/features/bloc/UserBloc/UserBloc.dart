import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/LoginRequest.dart';
import 'package:nbis/features/repo/UserRepo.dart';
import 'package:nbis/features/sharedPreferences/Session.dart';
import 'package:nbis/features/sharedPreferences/User.dart';

class UserBloc extends BaseBloc {
  UserRepo userRepo;

  StreamController<SCResponse<User>> scLogin = StreamController.broadcast();
  StreamController<SCResponse<String>> scForgot = StreamController.broadcast();

  UserBloc(this.userRepo);

  Stream<SCResponse<User>> sendLoginRequest(LoginRequest loginRequest) {
    this.userRepo.sendLoginRequest(loginRequest, scLogin.sink);
    return scLogin.stream;
  }


  Stream<SCResponse<String>> sendForgotPasswordRequest(String technicianId) {
    this.userRepo.sendForgotPasswordRequest(technicianId, scForgot.sink);
    return scForgot.stream;
  }

  Session getSession() {
    return this.userRepo.getSession();
  }

  @override
  void onDispose() {
    scLogin.close();
    scForgot.close();
  }
}
