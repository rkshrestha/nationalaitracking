
import 'dart:async';

import 'package:nbis/features/bloc/BaseBloc.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/ChangePasswordModel.dart';
import 'package:nbis/features/repo/ChangePasswordRepo.dart';
//import 'package:nbis/features/sharedPreferences/User.dart';

class ChangePasswordBloc extends BaseBloc{

  ChangePasswordRepo changePasswordRepo;

  StreamController<SCResponse<String>> scChange = StreamController.broadcast();

  ChangePasswordBloc(this.changePasswordRepo);


  Stream<SCResponse<String>> sendChangePasswordRequest(ChangePasswordModel changeRequest) {
    this.changePasswordRepo.sendChangePasswordRequest(changeRequest, scChange.sink);
    return scChange.stream;
  }

  @override
  void onDispose() {
    scChange.close();
  }
}