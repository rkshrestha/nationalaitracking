import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/home/ExitRecordPage.dart';
import 'package:nbis/features/home/ExitRecordPageList.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class ExitRecordPageListLogic {
  ExitRecordPageListState state;
  ExitRecordBloc exitRecordBloc;

  List<ExitRecord> exitRecordList = [];
  List<ExitRecord> exitUnSyncRecordList = [];
  List<ExitRecord> exitSyncRecordList = [];
  static ExitRecord exitRecord = ExitRecord();

  ExitRecordPageListLogic(this.state, this.exitRecordBloc);

  void onPostInit(Animal animal) {
    state.setState(() {
      if (animal != null) {
        Logger.v("display id" + animal.aId.toString());
//        getUnSyncExitListByAnimal(animal);
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//          getSyncExitListByAnimal(animal);
        getExitListByAnimal(animal);
//        });
      } else {
        getExitRecordList();
//        getUnSyncExitRecordList();
//        new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//          getSyncExitRecordList();
//        });
      }
    });
//
  }

  getExitListByAnimal(animal) {
    this.exitRecordBloc.getExitRecordListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.exitRecordList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //sync single id
  getUnSyncExitListByAnimal(animal) {
    this.exitRecordBloc.getUnSyncExitRecordListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.exitUnSyncRecordList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncExitListByAnimal(animal) {
    this.exitRecordBloc.getSyncExitRecordListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.exitSyncRecordList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getExitRecordList() {
    this.exitRecordBloc.getExitRecordList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("exit  list is " + it.data.toString());
        this.state.setState(() {
          this.exitRecordList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncExitRecordList() {
    this.exitRecordBloc.getUnSyncExitRecordList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("exit  list is " + it.data.toString());
        this.state.setState(() {
          this.exitUnSyncRecordList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncExitRecordList() {
    this.exitRecordBloc.getSyncExitRecordList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("exit  list is " + it.data.toString());
        this.state.setState(() {
          this.exitSyncRecordList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  onEditExitRecord(ExitRecord exitRecord, int position) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return ExitRecordPage(exitRecord: exitRecord, isEdit: true);
    }));
    state.setState(() {
      state.refreshExitRecordList();
    });
  }

  onAddExitRecord(Animal animal) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return ExitRecordPage(
        animal: animal,
      );
    }));

    state.setState(() {
      state.refreshExitRecordList();
    });
  }

  void confirmationDelete(ExitRecord exitRecords, Animal animal, int position) {
    DialogUtils.showAlertDialog(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "Are you sure,you want to delete?", (it) {
//        AppTranslations.of(state.context).text("delete_warning"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.pop(this.state.context, true);
            if (animal != null) {
              getExitListByAnimal(animal);
            } else {
              getExitRecordList();
            }

            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Logger.v('position exit' + position.toString());

            this
                .exitRecordBloc
                .deleteExitRecord(this.state.context, exitRecords)
                .listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {
//                state.setState(() {
//                  exitRecord.syncToServer == true
//                      ? this.exitSyncRecordList.removeAt(position)
//                      : this.exitUnSyncRecordList.removeAt(position);
//                });
//
//                state.setState(() {
//                  this.exitRecordList.removeAt(position);
//                });

                Navigator.pop(this.state.context, true);
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(state.context).pop();
                });
              }
            });

            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }
}
