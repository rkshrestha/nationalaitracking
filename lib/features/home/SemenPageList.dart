import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/semenBloc/SemenBloc.dart';
import 'package:nbis/features/home/SemenPageListLogic.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/repo/SemenRepo.dart';
//import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class SemenPageList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SemenPageListState();
  }
}

class SemenPageListState extends State<SemenPageList> {
  SemenPageListLogic logic;
  SemenBloc semenBloc;
  Semen semen;
  int count = 0;

  void showToast(String message) {
    ToastUtils.show(message);
  }

  var refreshSemenListKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    semenBloc = SemenBloc(SemenRepo(context));
    logic = SemenPageListLogic(this, semenBloc);

//    this.logic.onPostInit();
    refreshSemenList();
  }

  Future<Null> refreshSemenList() async {
    refreshSemenListKey.currentState?.show(atTop: false);
//    await Future.delayed(Duration(seconds: 1));

    setState(() {
      this.logic.onPostInit();
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    this
        .logic
        .semenList
        .sort((a, b) => b.receivedDate.compareTo(a.receivedDate));
    //tab

    final tabSync = <Tab>[
      Tab(text: "Not Sync", icon: new Icon(Icons.sync)),
      Tab(text: " Sync", icon: new Icon(Icons.sync))
    ];

    return DefaultTabController(
      length: tabSync.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Semen",
//            AppTranslations.of(context).text("semen_registered_title"),
          ),
//          bottom: TabBar(tabs: tabSync),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 45.0,
            color: Theme.of(context).primaryColor,
            child: GestureDetector(
              onTap: () {
                logic.onAddSemen();
              },

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  Divider(
                    indent: 12.0,
                  ),
                  Text(
//                          AppTranslations.of(context)
//                              .text("semen_register_button"),
                    "Add Semen",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),

                ]),

//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 4,
//                    child: Text(''),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: Row(
//                      children: <Widget>[
//                        Icon(
//                          Icons.add,
//                          color: Colors.white,
//                        ),
//                        Divider(
//                          indent: 12.0,
//                        ),
//                        Text(
////                          AppTranslations.of(context)
////                              .text("semen_register_button"),
//                        "Add Semen",
//                          style: TextStyle(
//                              color: Colors.white, fontWeight: FontWeight.bold),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Text(''),
//                  ),
//                ],
//              ),
            ),
          ),
        ),
        body: BlocProvider(
          child: SafeArea(
            child: Column(
              children: <Widget>[
//                Expanded(
//                  child: TabBarView(
//                    children: <Widget>[
//                      logic.semenUnSyncList.isEmpty
//                          ? noData()
//                          : getSemenUnSyncList(),
//                      logic.semenSyncList.isEmpty
//                          ? noData()
//                          : getSemenSyncList(),
//                    ],
//                  ),
//                ),

                Expanded(
                  child: RefreshIndicator(
                    key: refreshSemenListKey,
                    child: logic.semenList.isEmpty ? noData() : getSemenList(),
                    onRefresh: refreshSemenList,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

//    return Scaffold(
//
//      appBar: AppBar(
//        title: Text(   AppTranslations.of(context).text("semen_registered_title"),),
//      ),
//      bottomNavigationBar: BottomAppBar(
//        child: Container(
//          height: 45.0,
//          color: Theme.of(context).primaryColor,
//          child: GestureDetector(
//            onTap: () {
//              logic.onAddSemen();
//            },
//            child: Row(
//              children: <Widget>[
//                Expanded(
//                  flex: 4,
//                  child: Text(''),
//                ),
//                Expanded(
//                  flex: 6,
//                  child: Row(
//                    children: <Widget>[
//                      Icon(
//                        Icons.add,
//                        color: Colors.white,
//                      ),
//                      Divider(
//                        indent: 12.0,
//                      ),
//                      Text(
//                        AppTranslations.of(context).text("semen_register_button"),
////                        "Add Semen",
//                        style: TextStyle(
//                            color: Colors.white, fontWeight: FontWeight.bold),
//                      ),
//                    ],
//                  ),
//                ),
//                Expanded(
//                  flex: 2,
//                  child: Text(''),
//                ),
//              ],
//            ),
//          ),
//        ),
//      ),
//      body: BlocProvider(
//        child: SafeArea(
//          child: Column(
//            children: <Widget>[
////              Padding(
////                padding: EdgeInsets.only(top: 6.0),
////                child: Text(
////                  "Semen List",
////                  style: TextStyle(
////                    fontWeight: FontWeight.bold,
////                    fontSize: 15.0,
////                  ),
////                  textAlign: TextAlign.center,
////                ),
////              ),
//              Expanded(
//                child: logic.semenList.isEmpty? noData():getSemenList(),
//              ),
//            ],
//          ),
//        ),
//      ),
////      floatingActionButton: Padding(
////        padding: EdgeInsets.only(bottom: 1.0),
////        child: FloatingActionButton(
////            child: Icon(Icons.add),
////            tooltip: 'Add Semen',
////            backgroundColor: Theme.of(context).primaryColor,
////            onPressed: () {
////              this.logic.onAddSemen();
////            }),
////      ),
//
////      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
////      floatingActionButton: Column(
////        crossAxisAlignment: CrossAxisAlignment.stretch,
////        mainAxisSize: MainAxisSize.min,
////        children: <Widget>[
////          FloatingActionButton.extended(
////            isExtended: true,
////            label: Text("Add Semen"),
////            icon: Image.asset("images/hdpi/add.png",color: Colors.white,),
////            tooltip: 'Add semen',
////            backgroundColor: Theme.of(context).primaryColor,
////            onPressed: () {
////              this.logic.onAddSemen();
////            },
////          ),
////        ],
////      ),
//    );
  }

  Widget noData() {
    return Container(
      child: Center(
//        child: Text(
//          AppTranslations.of(context).text("semen_noData"),
//        ),
        child: Text("No Semen Data has been found !!"),
      ),
    );
  }

  ListView getSemenList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.semenList.length,
        itemBuilder: (BuildContext context, int position) {
          semen = this.logic.semenList[position];

          editButton() {
            DateTime lastestDate = DateTime.now();

            if (semen.syncDate != null) {

              String semenSyncDate = semen.syncDate == null? "": semen.syncDate.split(".").first;
//              Logger.v("testHours:"+semenSyncDate.toString());

              DateTime syncDate = DateTime.tryParse(semenSyncDate);
              DateTime addedDate =syncDate.add(new Duration(hours: 24));

              if (lastestDate.compareTo(addedDate)==1) {
//              if (lastestDate.compareTo(addedDate)==1 && semen.syncToServer == true) {
//              if (addedDate.difference(syncDate).inHours >= 24 && semen.syncToServer == true) {

//                if (syncDate.add(new Duration(hours: 24)) == lastestDate && semen.syncToServer == true) {
//              if (lastestDate.difference(syncDate).inHours >= 24 && semen.syncToServer == true) {
                return Container(
                  child: Text(""),
                );
              } else {
                return GestureDetector(
                  child: Container(
                    width: 40.0,
                    height: 40.0,
                    child: Card(
                      color: Colors.white70,
                      elevation: 3.0,
                      child: Image.asset(
                        "images/edits.png",
                      ),

                    ),

                  ),
                  onTap: () {
                    this.logic.onEditSemen(this.logic.semenList[position]);
                  },
                );

//                return Container(
//                 child: GestureDetector(
//                    child: Icon(
//                      Icons.edit,
//                      size: 30.0,
//                      color: Colors.black,
//                    ),
//                    onTap: () {
//                      this.logic.onEditSemen(this.logic.semenList[position]);
//                    },
//                  ),
//                );
              }
            } else {
              return GestureDetector(
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: Card(
                    color: Colors.white70,
                    elevation: 3.0,
                    child: Image.asset(
                      "images/edits.png",
                    ),

                  ),

                ),
                onTap: () {
                  this.logic.onEditSemen(this.logic.semenList[position]);
                },
              );



//              return Container(
//             child:   GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditSemen(this.logic.semenList[position]);
//                  },
//                ),
//              );
            }
          }

          if(semen.syncDate != null){
//          if(semen.syncToServer == true){
            return Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 30.0,
                    height: 30.0,
                    child: semen.syncToServer == true
                        ? Image.asset(
                      "images/xxhdpi/semen.png",
                      color: Colors.green,
                    )
                        : Image.asset(
                      "images/xxhdpi/semen.png",
                      color: Colors.red,
                    ),
                  ),

                  title: Text("Received Date: "+semen.receivedDate),
                  subtitle: Text("Jersey Semen Dose: " +
                      semen.jersey.toString().split(".").first +
                      "\n" +
                      "Murrah Semen Dose: " +
                      semen.murrah.toString().split(".").first +
                      "\n" +
                      "Holstein Friesian Semen Dose: " +
                      semen.holsteinFriesian.toString().split(".").first +
                      "\n" +
                      "Boar Semen Dose: " +
                      semen.boar.toString().split(".").first +
                      "\n" +
                      "Saanen Semen Dose: " +
                      semen.saanen.toString().split(".").first +
                      "\n" +
                      "Yorkshire Semen Dose: " +
                      semen.yorkshire.toString().split(".").first +
                      "\n" +
                      "Landrace Semen Dose: " +
                      semen.landrace.toString().split(".").first +
                      "\n" +
                      "Duroc Semen Dose: " +
                      semen.duroc.toString().split(".").first +
                      "\n" +
                      "Hampshire Semen Dose: " +
                      semen.hampshire.toString().split(".").first),
                  trailing: editButton(),
//                onTap: () {
//                  this.logic.onEditSemen(this.logic.semenList[position]);
//                },
                ),
              );

          }else{
            return Dismissible(
              key: Key(this.logic.semenList[position].msId.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (DismissDirection direction) {
                Semen semens = this.logic.semenList[position];
                setState((){
                  this.logic.semenList.removeAt(position);
                  this.logic.confirmationDelete(semens, position);

                });
              },
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 30.0,
                    height: 30.0,
                    child: semen.syncToServer == true
                        ? Image.asset(
                      "images/xxhdpi/semen.png",
                      color: Colors.green,
                    )
                        : Image.asset(
                      "images/xxhdpi/semen.png",
                      color: Colors.red,
                    ),
                  ),
//                leading: Image.asset(
//                  "images/xxhdpi/semen.png",
//                  width: 30.0,
//                  height: 30.0,
//                ),
                  title: Text("Received Date: "+ semen.receivedDate),
                  subtitle: Text("Jersey Semen Dose: " +
                      semen.jersey.toString().split(".").first +
                      "\n" +
                      "Murrah Semen Dose: " +
                      semen.murrah.toString().split(".").first +
                      "\n" +
                      "Holstein Friesian Semen Dose: " +
                      semen.holsteinFriesian.toString().split(".").first +
                      "\n" +
                      "Boar Semen Dose: " +
                      semen.boar.toString().split(".").first +
                      "\n" +
                      "Saanen Semen Dose: " +
                      semen.saanen.toString().split(".").first +
                      "\n" +
                      "Yorkshire Semen Dose: " +
                      semen.yorkshire.toString().split(".").first +
                      "\n" +
                      "Landrace Semen Dose: " +
                      semen.landrace.toString().split(".").first +
                      "\n" +
                      "Duroc Semen Dose: " +
                      semen.duroc.toString().split(".").first +
                      "\n" +
                      "Hampshire Semen Dose: " +
                      semen.hampshire.toString().split(".").first),
                  trailing: editButton(),
//                onTap: () {
//                  this.logic.onEditSemen(this.logic.semenList[position]);
//                },
                ),
              ),
            );
          }


        });
  }

  ListView getSemenUnSyncList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.semenUnSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          semen = this.logic.semenUnSyncList[position];
          return Dismissible(
            key: Key(this.logic.semenUnSyncList[position].msId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              Semen semens = this.logic.semenUnSyncList[position];
              print('Semen Position: ' + semens.msId.toString());
              this.logic.confirmationDelete(semens, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Image.asset(
                  "images/xxhdpi/semen.png",
                  width: 30.0,
                  height: 30.0,
                ),
                title: Text(semen.receivedDate),
                subtitle: Text("Jersey Semen Dose: " +
                    semen.jersey.toString() +
                    "\n" +
                    "Murrah Semen Dose: " +
                    semen.murrah.toString() +
                    "\n" +
                    "Holstein Friesian Semen Dose: " +
//                    semen.syncToServer.toString(),
                    semen.holsteinFriesian.toString()),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.edit,
                    size: 30.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    this
                        .logic
                        .onEditSemen(this.logic.semenUnSyncList[position]);
                  },
                ),
//                onTap: () {
//                  this.logic.onEditSemen(this.logic.semenList[position]);
//                },
              ),
            ),
          );
        });
  }

  ListView getSemenSyncList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.semenSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          semen = this.logic.semenSyncList[position];
          return Dismissible(
            key: Key(this.logic.semenSyncList[position].msId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              Semen semens = this.logic.semenSyncList[position];
              print('Semen Position: ' + semens.msId.toString());
              this.logic.confirmationDelete(semens, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Image.asset(
                  "images/xxhdpi/semen.png",
                  width: 30.0,
                  height: 30.0,
                ),
                title: Text(semen.receivedDate),
                subtitle: Text("Jersey Semen Dose: " +
                    semen.jersey.toString() +
                    "\n" +
                    "Murrah Semen Dose: " +
                    semen.murrah.toString() +
                    "\n" +
                    "Holstein Friesian Semen Dose: " +
//                    semen.syncToServer.toString(),
                    semen.holsteinFriesian.toString()),
//                trailing: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditSemen(this.logic.semenSyncList[position]);
//                  },
//                ),
//                onTap: () {
//                  this.logic.onEditSemen(this.logic.semenList[position]);
//                },
              ),
            ),
          );
        });
  }
}
