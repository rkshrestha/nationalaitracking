import 'package:flutter/material.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nbis/s/localization/Application.dart';

class SettingsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SettingsStatePage();
  }
}

class SettingsStatePage extends State<SettingsPage> {


//   final List<String> languagesList = [
//    AppTranslations.of(context).text("english"),
//    AppTranslations.of(context).text("nepali"),
//  ];

  static final List<String> languagesList = application.supportedLanguages;
  static final List<String> languageCodesList =
      application.supportedLanguagesCodes;

  static final Map<dynamic, dynamic> languagesMap = {
    languagesList[0]: languageCodesList[0],
    languagesList[1]: languageCodesList[1],
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
//          AppTranslations.of(context).text("settings"),
        "Settings,"
        ),
      ),
//      body: buildLanguagesList(),
      body: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 4,
                  child: Text(
                    "Choose Language: ",
//                    AppTranslations.of(context).text("language"),
                    textAlign: TextAlign.center,
                  ),
                ),
                Expanded(flex: 6, child: buildLanguagesList()),
              ],
            ),
          ],
        ),
      ),
    );
  }

  buildLanguagesList() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: languagesList.length,
      itemBuilder: (context, index) {
        return buildLanguageItem(languagesList[index]);
      },
    );
  }

  buildLanguageItem(String language) {
    return InkWell(
      onTap: () {
        print(language);
        application.onLocaleChanged(Locale(languagesMap[language]));
      },
      child: Container(
        child: Card(
          color: Colors.white70,
          elevation: 2.0,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Text(
              language,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18.0,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
