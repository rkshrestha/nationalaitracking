import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/AIPageListLogic.dart';
import 'package:nbis/features/repo/AIRepo.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';

class AIPageList extends StatefulWidget {
  Animal animal;
  Farmer farmer;

  AIPageList({this.farmer, this.animal});

  @override
  State<StatefulWidget> createState() {
    return AIPageListState(farmer: farmer, animal: animal);
  }
}

class AIPageListState extends State<AIPageList> {
  AIPageListLogic logic;
  ArtificialInsemination ai;
  AIBloc aiBloc;
  Animal animal;
  Farmer farmer;
  String animalCode;
  var refreshAIListKey = GlobalKey<RefreshIndicatorState>();

  AIPageListState({this.farmer, this.animal});

//  void onNumberChanged() {
//    String mobileText = ai.mobileNumber;
////    bool hasFocus = textFocus.hasFocus;
////    Logger.v(mobileText);
////    setState(() {
////      ai.mobileNumber.selection = new TextSelection(
////          baseOffset: mobileText.length, extentOffset: mobileText.length);
////    });
//
//    if (mobileText.length == 10) {
//      this.logic.getFarmerByMobileNumber(mobileText);
////      this.logic.getFarmerByMobileNumber(int.tryParse(mobileText));
//    }
//  }

  @override
  void initState() {
    super.initState();
    this.aiBloc = AIBloc(AIRepo(context));
    AnimalBloc animalBloc = AnimalBloc(AnimalRepo(context));
    FarmerBloc farmerBloc = FarmerBloc(FarmerRepo(context));
    this.logic = AIPageListLogic(this, aiBloc, animalBloc, farmerBloc);
    refreshAIList();
//    logic.onPostInit(animal);


  }

  Future<Null> refreshAIList() async {
    refreshAIListKey.currentState?.show(atTop: false);
//    await Future.delayed(Duration(seconds: 1));

    setState(() {
      logic.onPostInit(animal);
    });

    return null;
  }

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  Widget build(BuildContext context) {
    this.logic.aiList.sort((a, b) => b.aiDate.compareTo(a.aiDate));
    final tabSync = <Tab>[
      Tab(text: "Not Sync", icon: new Icon(Icons.sync)),
      Tab(text: " Sync", icon: new Icon(Icons.sync))
    ];

    return DefaultTabController(
      length: tabSync.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Artificial Insemination",
//            AppTranslations.of(context).text("ai_registered_title"),
          ),
//  bottom: PreferredSize(
//            child: Text(this.logic.farmerName==null?"":logic.farmerName,style: TextStyle(color: Colors.white),),
//          preferredSize: null),
//          bottom: TabBar(tabs: tabSync),
//        title: Text("Artificial Insemination"),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 45.0,
            color: Theme.of(context).primaryColor,
            child: GestureDetector(
              onTap: () {
                logic.onAddAI(farmer, animal);
              },

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  Divider(
                    indent: 2.0,
                  ),
                  Text(
//                          AppTranslations.of(context).text("ai_register_button"),
                    "Add Artificial Insemination",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ],)
//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 3,
//                    child: Text(''),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: Row(
//                      children: <Widget>[
//                        Icon(
//                          Icons.add,
//                          color: Colors.white,
//                        ),
//                        Divider(
//                          indent: 2.0,
//                        ),
//                        Text(
////                          AppTranslations.of(context).text("ai_register_button"),
//                          "Add Artificial Insemination",
//                          style: TextStyle(
//                              color: Colors.white, fontWeight: FontWeight.bold),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Text(''),
//                  ),
//                ],
//              ),
            ),
          ),
        ),

        body: BlocProvider(
          child: SafeArea(
            child: Column(
              children: <Widget>[
//                Expanded(
//                  child: TabBarView(
//                    children: <Widget>[
//                      logic.aiUnSyncList.isEmpty ? noData() : getUnSyncAIList(),
//                      logic.aiSyncList.isEmpty ? noData() : getSyncAIList(),
//                    ],
//                  ),
//                ),
                Expanded(
                  child: RefreshIndicator(
                    key: refreshAIListKey,
                    child: this.logic.aiList.isEmpty ? noData() : getAIList(),
                    onRefresh: refreshAIList,
                  ),
                ),
              ],
            ),
          ),
        ),
//      floatingActionButton: Padding(
//        padding: EdgeInsets.only(bottom: 1.0),
//        child: FloatingActionButton(
//            child: Icon(Icons.add),
//            tooltip: 'Add AI',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              this.logic.onAddAI();
//            }),
//      ),

//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//      floatingActionButton: Column(
//        crossAxisAlignment: CrossAxisAlignment.stretch,
//        mainAxisSize: MainAxisSize.min,
//        children: <Widget>[
//          FloatingActionButton.extended(
//            isExtended: true,
//            label: Text("Add Artificial Insemination"),
//            icon: Image.asset(
//              "images/hdpi/add.png",
//              color: Colors.white,
//            ),
//            tooltip: 'Add AI',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              this.logic.onAddAI();
//            },
//          ),
//        ],
//      ),
      ),
    );
  }

  Widget noData() {
    return Container(
      child: Center(
        child: Text(
//          AppTranslations.of(context).text("ai_noData"),
            "No artificial insemination data has been found !!"),
      ),
    );
  }

  ListView getAIList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.aiList.length,
//        itemCount: logic.aiList.length,
//        itemCount: logic.animalList.length ,
        itemBuilder: (BuildContext context, int position) {
          ai = this.logic.aiList[position];
          editButton() {
            DateTime lastestDate = DateTime.now();

            if (ai.syncDate != null) {
//              Logger.v("AIDATE"+ai.syncDate);
//              Logger.v("AIDATE"+SplitDate.splitDateString(ai.syncDate));
              String aiSyncDate =
                  ai.syncDate == null ? "" :
                  ai.syncDate.split(".").first;
//                  SplitDate.splitDateString(ai.syncDate);
              DateTime syncDate = DateTime.tryParse(aiSyncDate);

              DateTime addedDate =syncDate.add(new Duration(hours: 24));
              Logger.v("syncDate:"+syncDate.toString());
              Logger.v("addedDate:"+addedDate.toString());
              Logger.v("lastestDate:"+lastestDate.toString());
              Logger.v("bool"+lastestDate.compareTo(addedDate).toString());

//              if (syncDate.add(new Duration(hours: 24)) == lastestDate &&
              if (lastestDate.compareTo(addedDate)==1) {
//                  &&   ai.syncToServer == true) {
                return Container(
                  child: Text(""),
                );
              } else {

                return GestureDetector(
                  child: Container(
                    width: 40.0,
                    height: 40.0,
                    child: Card(
                      color: Colors.white70,
                      elevation: 3.0,
                      child: Image.asset(
                        "images/edits.png",
                      ),

                    ),

                  ),
                  onTap: () {
                    this
                        .logic
                        .onEditAI(this.logic.aiList[position], position);
                  },
                );


//                return Container(
//                  child: GestureDetector(
//                    child: Icon(
//                      Icons.edit,
//                      size: 30.0,
//                      color: Colors.black,
//                    ),
//                    onTap: () {
//                      this
//                          .logic
//                          .onEditAI(this.logic.aiList[position], position);
//                    },
//                  ),
//                );
              }
            } else {

              return GestureDetector(
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: Card(
                    color: Colors.white70,
                    elevation: 3.0,
                    child: Image.asset(
                      "images/edits.png",
                    ),

                  ),

                ),
                onTap: () {
                  this
                      .logic
                      .onEditAI(this.logic.aiList[position], position);
                },
              );

//              return Container(
//                child: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditAI(this.logic.aiList[position], position);
//                  },
//                ),
//              );
            }
          }

          if (ai.syncDate != null) {
//          if (ai.syncToServer == true) {
            return Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                  width: 30.0,
                  height: 30.0,
                  child: ai.syncToServer == true
                      ? Image.asset(
                          "images/xxhdpi/pregnancydiagnosis.png",
                          color: Colors.green,
                        )
                      : Image.asset(
                          "images/xxhdpi/pregnancydiagnosis.png",
                          color: Colors.red,
                        ),
                ),
//

//                  title: Text(ai.aiDate),
//                  subtitle: Text(ai.farmerName==null?"":"Farmer Name: " + ai.farmerName +
//                      "\n" +
//                      "Tag: " +
//                      ai.mobileNumber +
//                      "-" +
//                      ai.speciesCode +
//                      "\n" +
//                      "Current Parity: " +
//                      ai.currentParity.toString() +
//                      "\n" +
//                      "AI Serial: " +
//                      ai.aiSerial.toString() +
//                      "\n" +
//                      "Sire Id: " +
//                      ai.shireId
//                      ),

                title: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("AI Date: " +ai.aiDate),
                      Text(ai.farmerName == null
                          ? ""
                          : "Farmer Name: " + ai.farmerName, style: TextStyle(fontSize: 14.0),),
                      Text("Tag: " + ai.mobileNumber + "-" + ai.speciesCode , style: TextStyle(fontSize: 14.0),),
                      Text("Current Parity: " + ai.currentParity.toString() , style: TextStyle(fontSize: 14.0),),
                      Text("AI Serial: " + ai.aiSerial.toString() , style: TextStyle(fontSize: 14.0),
//
                          ),
                      Text("Sire Id: " + ai.shireId , style: TextStyle(fontSize: 14.0),),
                    ],
                  ),
                ),
                trailing: editButton(),
              ),
            );
          } else {
            return Dismissible(
              key: Key(this.logic.aiList[position].aiId.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (DismissDirection direction) {
                ArtificialInsemination aiDelete = this.logic.aiList[position];

                setState((){
                  this.logic.aiList.removeAt(position);
                  this.logic.confirmationDelete(aiDelete,animal, position);

                });
              },
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 30.0,
                    height: 30.0,
                    child: ai.syncToServer == true
                        ? Image.asset(
                            "images/xxhdpi/pregnancydiagnosis.png",
                            color: Colors.green,
                          )
                        : Image.asset(
                            "images/xxhdpi/pregnancydiagnosis.png",
                            color: Colors.red,
                          ),
                  ),
//                leading: Container(
//                    width: 40.0,
//                    height: 40.0,
//                    decoration: new BoxDecoration(
//                        color: Colors.white,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.fill,
//                            image: new AssetImage(
//                                "images/xxhdpi/pregnancydiagnosis.png")))),

//                  title: Text(ai.aiDate),
//                  subtitle: Text(ai.farmerName == null
//                      ? ""
//                      : "Farmer Name: " +
//                          ai.farmerName +
//                          "\n" +
//                          "Tag: " +
//                          ai.mobileNumber +
//                          "-" +
//                          ai.speciesCode +
//                          "\n" +
//                          "Current Parity: " +
//                          ai.currentParity.toString() +
//                          "\n" +
//                          "AI Serial: " +
//                          ai.aiSerial.toString() +
//                          "\n" +
//                          "Sire Id: " +
//                          ai.shireId),
                title:Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("AI Date: " +ai.aiDate),
                      Text(ai.farmerName == null
                          ? ""
                          : "Farmer Name: " + ai.farmerName, style: TextStyle(fontSize: 14.0),),
                      Text("Tag: " + ai.mobileNumber + "-" + ai.speciesCode , style: TextStyle(fontSize: 14.0),),
                      Text("Current Parity: " + ai.currentParity.toString() , style: TextStyle(fontSize: 14.0),),
                      Text("AI Serial: " + ai.aiSerial.toString() , style: TextStyle(fontSize: 14.0),
//
                      ),
                      Text("Sire Id: " + ai.shireId , style: TextStyle(fontSize: 14.0),),
                    ],
                  ),
                ),
                  trailing: editButton(),
//                onTap: () {
//                  this.logic.onEditAI(this.logic.aiList[position], position);
//                },
                ),
              ),
            );
          }
        });
  }

  ListView getUnSyncAIList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.aiUnSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          ai = this.logic.aiUnSyncList[position];
          return Dismissible(
            key: Key(this.logic.aiUnSyncList[position].aiId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              ArtificialInsemination aiDelete =
                  this.logic.aiUnSyncList[position];
//              this.logic.confirmationDelete(aiDelete, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                    width: 40.0,
                    height: 40.0,
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: new AssetImage(
                                "images/xxhdpi/pregnancydiagnosis.png")))),

//                this.logic.aiList[position].speciesId == 1
//                    ? Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                            //   color: Colors.blueAccent,
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                              fit: BoxFit.contain,
//                              image: AssetImage("images/xxhdpi/animals.png"),
//                            )))
////                    ? Icon(
////                  Icons.android,
////                  size: 35.0,
////                )
//                    : Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                            //    color: Colors.blueAccent,
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                              fit: BoxFit.contain,
//                              image:
//                                  new AssetImage("images/xxhdpi/animals.png"),
//                            ))),
//                Icon(
//                        Icons.map,
//                        size: 35.0,
//                      ),
                title: Text(ai.aiDate),
                subtitle: Text(
//                    "Species Code:" +ai.aId.toString() +
//                        "\n" +
                    "Current Parity: " +
                        ai.currentParity.toString() +
                        "\n" +
                        "AI Serial: " +
                        ai.aiSerial.toString()),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.edit,
                    size: 30.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    this
                        .logic
                        .onEditAI(this.logic.aiUnSyncList[position], position);
                  },
                ),
//                onTap: () {
//                  this.logic.onEditAI(this.logic.aiList[position], position);
//                },
              ),
            ),
          );
        });
  }

  ListView getSyncAIList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.aiSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          ai = this.logic.aiSyncList[position];
          return Dismissible(
            key: Key(this.logic.aiSyncList[position].aiId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              ArtificialInsemination aiDelete = this.logic.aiSyncList[position];
//              this.logic.confirmationDelete(aiDelete, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                    width: 40.0,
                    height: 40.0,
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: new AssetImage(
                                "images/xxhdpi/pregnancydiagnosis.png")))),

                title: Text(ai.aiDate),
                subtitle: Text(
//                    "Species Code:" +ai.aId.toString() +
//                        "\n" +
                    "Current Parity: " +
                        ai.currentParity.toString() +
                        "\n" +
                        "AI Serial: " +
                        ai.aiSerial.toString()),
//                trailing: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditAI(this.logic.aiSyncList[position], position);
//                  },
//                ),
//                onTap: () {
//                  this.logic.onEditAI(this.logic.aiList[position], position);
//                },
              ),
            ),
          );
        });
  }
}
