import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/nbisBloc/NbisBloc.dart';
import 'package:nbis/features/bloc/notificationBloc/NotificationBloc.dart';
import 'package:nbis/features/home/AIPageList.dart';
import 'package:nbis/features/home/AboutPage.dart';
import 'package:nbis/features/home/AnimalPageList.dart';
import 'package:nbis/features/home/CalvingPageList.dart';
import 'package:nbis/features/home/ExitRecordPageList.dart';
import 'package:nbis/features/home/FarmerPageList.dart';
import 'package:nbis/features/home/HomePage.dart';
import 'package:nbis/features/home/NitrogenPageList.dart';
import 'package:nbis/features/home/PDPageList.dart';
import 'package:nbis/features/home/ProfilePage.dart';
import 'package:nbis/features/home/SemenPageList.dart';
import 'package:nbis/features/home/SettingsPage.dart';
import 'package:nbis/features/home/SyncServerPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';
import 'package:nbis/s/DsonUtils.dart';
//import 'package:nbis/s/FileUtils.dart';
import 'package:nbis/s/LogOutUtils.dart';
import 'package:nbis/s/Logger.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class HomePageLogic {
  HomePageState state;
  AnimalBloc animalBloc;
  NbisBloc nbisBloc;
  ExitRecordBloc exitRecordBloc;
  List<NotificationRequest> notification = [];
  NotificationBloc notificationBloc;


  HomePageLogic(this.state, this.animalBloc, this.nbisBloc,
      this.exitRecordBloc, this.notificationBloc);

//  void onPostCreate() {
//    //sync exit reason
//    syncExitReasonList();
//    //sync species
//    syncSpeciesList();
//    //sync breed
//    syncBreedList();
//    //sync Province
//    syncProvinceList();
//    //sync District
//    syncDistrictList();
//    //sync municipality
//    syncMunicipalityList();
//  }

  onFarmerClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      Logger.v("Test Farmer");
      return FarmerPageList();
    }));
  }

  onAnimalClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return AnimalPageList();
    }));
  }

  onAIClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return AIPageList();
    }));
  }

  onPDClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return PDPageList();
    }));
  }

  onCalvingClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return CalvingPageList();
    }));
  }

  onExitCattleClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return ExitRecordPageList();
    }));
  }

  onNitrogenClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return NitrogenPageList();
    }));
  }

  onSemenClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return SemenPageList();
    }));
  }

  onSyncClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return SyncServerPage();
    }));
  }

  onAboutClick() {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return AboutPage();
    }));
  }


//  choiceOption(String choice) {
//    if (choice == Constants.Profile) {
////      Navigator.push(state.context, MaterialPageRoute(builder: (context) => ProfilePage()));
//      FileUtils.backupDB();
//    } else if (choice == Constants.Settings) {
//      Navigator.push(state.context, MaterialPageRoute(builder: (context) => SettingsPage()));
//
//    } else if (choice == Constants.LogOut) {
//      state.setState((){ LogOutUtils.logOut(this.state.context);});
//
//
//    }
//  }



  choiceOption(String choice) {
    if (choice ==   "Profile") {
//    if (choice ==   AppTranslations.of(this.state.context).text("profile")) {
      Navigator.push(state.context, MaterialPageRoute(builder: (context) => ProfilePage()));
//      FileUtils.backupDB();
    } else if (choice ==  "Settings") {
//    } else if (choice ==   AppTranslations.of(this.state.context).text("settings")) {
      Navigator.push(state.context, MaterialPageRoute(builder: (context) => SettingsPage()));

    } else if (choice ==   "Logout") {
//    } else if (choice ==   AppTranslations.of(this.state.context).text("logout")) {
      state.setState((){ LogOutUtils.logOut(this.state.context);});


    }
  }

  //
//  syncSpeciesList() async {
////    ProgressBar progressBar = ProgressBar();
////    progressBar.show(this.state.context);
//
//    this.animalBloc.syncSpeciesList().listen((it) {
//      if (it == null) return;
//
//      Logger.v("inside listenere");
//      if (it.status == Status.SUCCESS) {
//        //   progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
//      } else {
//        Logger.v("sync species Logic print " + it.errorMessage);
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          //   progressBar.dismiss(this.state.context);
//          this.state.showToast(it.errorMessage);
//        });
//      }
//    });
//  }

  //sync breed
//  syncBreedList() async {
////    ProgressBar progressBar = ProgressBar();
////    progressBar.show(this.state.context);
//
//    this.animalBloc.syncBreedList().listen((it) {
//      if (it == null) return;
//      Logger.v("inside listenere");
//      if (it.status == Status.SUCCESS) {
//        // progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
//      } else {
//        Logger.v("sync breed Logic print " + it.errorMessage);
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
////          progressBar.dismiss(this.state.context);
//          this.state.showToast(it.errorMessage);
//
//        });
//      }
//    });
//  }

  //
//  syncProvinceList() async {
////    ProgressBar progressBar = ProgressBar();
////    progressBar.show(this.state.context);
//
//    this.nbisBloc.syncProvinceList().listen((it) {
//      if (it == null) return;
//
//      Logger.v("inside listenere");
//      if (it.status == Status.SUCCESS) {
//        //   progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
//      } else {
//        Logger.v("province Logic print " + it.errorMessage);
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          //   progressBar.dismiss(this.state.context);
//          this.state.showToast(it.errorMessage);
//        });
//      }
//    });
//  }

  //district

//  syncDistrictList() async {
////    ProgressBar progressBar = ProgressBar();
////    progressBar.show(this.state.context);
//
//    this.nbisBloc.syncDistrictList().listen((it) {
//      if (it == null) return;
//
//      Logger.v("inside listenere");
//      if (it.status == Status.SUCCESS) {
////        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
//      } else {
//        Logger.v("district Logic print " + it.errorMessage);
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          //        progressBar.dismiss(this.state.context);
//          this.state.showToast(it.errorMessage);
//        });
//      }
//    });
//  }

  //Municipality

//  syncMunicipalityList() async {
////    ProgressBar progressBar = ProgressBar();
////    progressBar.show(this.state.context);
//
//    this.nbisBloc.syncMunicipalityList().listen((it) {
//      if (it == null) return;
//
//      Logger.v("inside listenere");
//      if (it.status == Status.SUCCESS) {
////        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
//      } else {
//        Logger.v("municipality Logic print " + it.errorMessage);
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          //        progressBar.dismiss(this.state.context);
//          this.state.showToast(it.errorMessage);
//        });
//      }
//    });
//  }

  //exit reason

//  syncExitReasonList() async {
////    ProgressBar progressBar = ProgressBar();
////    progressBar.show(this.state.context);
//
//    this.exitRecordBloc.syncExitReasonList().listen((it) {
//      if (it == null) return;
//
//      Logger.v("inside listenere");
//      if (it.status == Status.SUCCESS) {
////        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
//      } else {
//        Logger.v("exit reason Logic print " + it.errorMessage);
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
////          progressBar.dismiss(this.state.context);
//          this.state.showToast(it.errorMessage);
//        });
//      }
//    });
//  }
//


//test for notification list count//
  getNotificationList() {
    this.notificationBloc.getNotificationList().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.notification = it.data;
          Logger.v("getNotification"+DsonUtils.toJsonString(notification));

        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }


}
