import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/calvingBloc/CalvingBloc.dart';
import 'package:nbis/features/home/CalvingPageListLogic.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/CalvingRepo.dart';
//import 'package:nbis/s/Logger.dart';
//import 'package:nbis/s/SplitDate.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class CalvingPageList extends StatefulWidget {
  Animal animal;

  CalvingPageList({this.animal});

  @override
  State<StatefulWidget> createState() {
    return CalvingPageListState(animal: animal);
  }
}

class CalvingPageListState extends State<CalvingPageList> {
  CalvingPageListLogic logic;
  CalvingBloc calvingBloc;
  Calving calving;
  Animal animal;
  var refreshCalvingListKey = GlobalKey<RefreshIndicatorState>();

  CalvingPageListState({this.animal});

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  void initState() {
    super.initState();
    this.calvingBloc = CalvingBloc(CalvingRepo(context));
    this.logic = CalvingPageListLogic(this, calvingBloc);
//    logic.onPostInit(animal);
    refreshCalvingList();
  }

  Future<Null> refreshCalvingList() async {
    refreshCalvingListKey.currentState?.show(atTop: false);
//    await Future.delayed(Duration(seconds: 1));

    setState(() {
      this.logic.onPostInit(animal);
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    this
        .logic
        .calvingList
        .sort((a, b) => b.calvingDate.compareTo(a.calvingDate));
    final tabSync = <Tab>[
      Tab(text: "Not Sync", icon: new Icon(Icons.sync)),
      Tab(text: " Sync", icon: new Icon(Icons.sync))
    ];
    return DefaultTabController(
      length: tabSync.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
           "Calving",
//            AppTranslations.of(context).text("calving_registered_title"),
          ),
//          bottom: TabBar(tabs: tabSync),
//        title: Text("Calving"),
        ),

        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 45.0,
            color: Theme.of(context).primaryColor,
            child: GestureDetector(
              onTap: () {
                logic.onAddCalving(animal);
              },

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  Divider(
                    indent: 12.0,
                  ),
                  Text(
//                          AppTranslations.of(context).text("calving_register_button"),
                    "Add Calving",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ]),
//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 4,
//                    child: Text(''),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: Row(
//                      children: <Widget>[
//                        Icon(
//                          Icons.add,
//                          color: Colors.white,
//                        ),
//                        Divider(
//                          indent: 12.0,
//                        ),
//                        Text(
////                          AppTranslations.of(context).text("calving_register_button"),
//                        "Add Calving",
//                          style: TextStyle(
//                              color: Colors.white, fontWeight: FontWeight.bold),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Text(''),
//                  ),
//                ],
//              ),
            ),
          ),
        ),
        body: BlocProvider(
          child: SafeArea(
            child: Column(
              children: <Widget>[
//                Expanded(
//                  child: TabBarView(
//                    children: <Widget>[
//                      logic.calvingUnSyncList.isEmpty
//                          ? noData()
//                          : getUnSyncCalvingList(),
//                      logic.calvingSyncList.isEmpty
//                          ? noData()
//                          : getSyncCalvingList(),
//                    ],
//                  ),
//                ),
                Expanded(
                  child: RefreshIndicator(
                    key: refreshCalvingListKey,
                    child: this.logic.calvingList.isEmpty
                        ? noData()
                        : getCalvingList(),
                    onRefresh: refreshCalvingList,
                  ),
                ),
              ],
            ),
          ),
        ),
//      floatingActionButton: Padding(
//        padding: EdgeInsets.only(bottom: 1.0),
//        child: FloatingActionButton(
//            child: Icon(Icons.add),
//            tooltip: 'Add Calving',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//         this.logic.onAddCalving();
//            }),
//      ),

//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//      floatingActionButton: Column(
//        crossAxisAlignment: CrossAxisAlignment.stretch,
//        mainAxisSize: MainAxisSize.min,
//        children: <Widget>[
//          FloatingActionButton.extended(
//            isExtended: true,
//            label: Text("Add Calving"),
//            icon: Image.asset("images/hdpi/add.png",color: Colors.white,),
//            tooltip: 'Add calving',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              this.logic.onAddCalving();
//            },
//          ),
//        ],
//      ),
      ),
    );
  }

  Widget noData() {
    return Container(
      child: Center(
//        child: Text(
//          AppTranslations.of(context).text("calving_noData"),
//        ),
        child: Text("No Calving Data has been found. !!"),
      ),
    );
  }

  ListView getCalvingList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.calvingList.length,
        itemBuilder: (BuildContext context, int position) {
          calving = this.logic.calvingList[position];

          editButton() {
            DateTime lastestDate = DateTime.now();

            if (calving.syncDate != null) {
//              String calvingSyncDate = calving.syncDate==null?"":SplitDate.splitDateString(calving.syncDate);
              String calvingSyncDate = calving.syncDate==null?"":calving.syncDate.split(".").first;
//              String calvingSyncDate = calving.syncDate==null?"":calving.syncDate;
              DateTime syncDate = DateTime.tryParse(calvingSyncDate);
              DateTime addedDate =syncDate.add(new Duration(hours: 24));

//              syncDate.add(new Duration(hours: 24));
//              if (syncDate.add(new Duration(hours: 24)) == lastestDate && calving.syncToServer == true) {
//              if (lastestDate.difference(syncDate).inHours >= 24 && calving.syncToServer == true) {
              if (lastestDate.compareTo(addedDate)==1) {
//              if (lastestDate.compareTo(addedDate)==1 && calving.syncToServer == true) {
//              if (addedDate.difference(syncDate).inHours >= 24 && calving.syncToServer == true) {
                return Container(
                  child: Text(""),
                );
              } else {
                return GestureDetector(
                  child: Container(
                    width: 40.0,
                    height: 40.0,
                    child: Card(
                      color: Colors.white70,
                      elevation: 3.0,
                      child: Image.asset(
                        "images/edits.png",
                      ),

                    ),

                  ),
                  onTap: () {
                    logic.onEditCalving(
                        this.logic.calvingList[position], position);
                  },
                );

//                return Container(
//                  child: GestureDetector(
//                    child: Icon(
//                      Icons.edit,
//                      size: 30.0,
//                      color: Colors.black,
//                    ),
//                    onTap: () {
//                      logic.onEditCalving(
//                          this.logic.calvingList[position], position);
//                    },
//                  ),
//                );
              }
            } else {
              return GestureDetector(
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: Card(
                    color: Colors.white70,
                    elevation: 3.0,
                    child: Image.asset(
                      "images/edits.png",
                    ),

                  ),

                ),
                onTap: () {
                  logic.onEditCalving(
                      this.logic.calvingList[position], position);
                },
              );

//
//              return Container(
//                child: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    logic.onEditCalving(
//                        this.logic.calvingList[position], position);
//                  },
//                ),
//              );
            }
          }


          if(calving.syncDate !=null){
//          if(calving.syncToServer ==true){
            return  Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 40.0,
                    height: 40.0,
                    child: calving.syncToServer == true
                        ? Image.asset(
                      "images/calving_animal.png",
//                      "images/xxhdpi/calving.png",
                      color: Colors.green,
                    )
                        : Image.asset(
                      "images/calving_animal.png",
//                      "images/xxhdpi/calving.png",
                      color: Colors.red,
                    ),
                  ),

                  title:Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Calving date: " + calving.calvingDate),
                        Text(calving.farmerName == null
                            ? ""
                            : "Farmer Name: " + calving.farmerName, style: TextStyle(fontSize: 14.0),),
                        Text("Tag: " + calving.mobileNumber + "-" + calving.speciesCode , style: TextStyle(fontSize: 14.0),),
                        Text("Calf Tag: " + calving.mobileNumber + "-" + calving.calfCode , style: TextStyle(fontSize: 14.0),),
                        Text("Calf Gender: "+calving.calfSex , style: TextStyle(fontSize: 14.0),),
                      ],
                    ),
                  ),


//                  title: Text("Calving date: " + calving.calvingDate),
//                  subtitle: Text( calving.farmerName==null?"":"Farmer Name: " +calving.farmerName +
//                      "\n" +"Tag: " +
//                      calving.mobileNumber +
//                      "-" +
//                      calving.speciesCode +
//                      "\n" +
//                      "Calf Tag: " +
//                      calving.mobileNumber +
//                      "-" +
//                      calving.calfCode +
//                      "\n" +
//                      "Calf Gender: " +
//                      calving.calfSex),
                  trailing: editButton(),

                ),


            );
          }else{
            return Dismissible(
              key: Key(this.logic.calvingList[position].cId.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (DismissDirection direction) {
                Calving calvingDelete = this.logic.calvingList[position];
                setState((){
                  this.logic.calvingList.removeAt(position);
                  this.logic.confirmationDelete(calvingDelete, animal,position);

                });
              },
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 40.0,
                    height: 40.0,
                    child: calving.syncToServer == true
                        ? Image.asset(
                      "images/calving_animal.png",
//                      "images/xxhdpi/calving.png",
                      color: Colors.green,
                    )
                        : Image.asset(
                      "images/calving_animal.png",
//                      "images/xxhdpi/calving.png",
                      color: Colors.red,
                    ),
                  ),
//                leading: Image.asset(
//                  "images/xxhdpi/calving.png",
//                  width: 40.0,
//                  height: 40.0,
//                ),

//                  title: Text("Calving date: " + calving.calvingDate),
////                  subtitle: Text( calving.farmerName==null?"":"Farmer Name: " +calving.farmerName +
////                      "\n" +"Tag: " +
////                      calving.mobileNumber +
////                      "-" +
////                      calving.speciesCode +
////                      "\n" +
////                      "Calf Tag: " +
////                      calving.mobileNumber +
////                      "-" +
////                      calving.calfCode +
////                      "\n" +
////                      "Calf Gender: " +
////                      calving.calfSex),

                  title:Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Calving date: " + calving.calvingDate),
                        Text(calving.farmerName == null
                            ? ""
                            : "Farmer Name: " + calving.farmerName, style: TextStyle(fontSize: 14.0),),
                        Text("Tag: " + calving.mobileNumber + "-" + calving.speciesCode , style: TextStyle(fontSize: 14.0),),
                        Text("Calf Tag: " + calving.mobileNumber + "-" + calving.calfCode , style: TextStyle(fontSize: 14.0),),
                        Text("Calf Gender: "+calving.calfSex , style: TextStyle(fontSize: 14.0),),
                      ],
                    ),
                  ),

                  trailing: editButton(),
//                onTap: () {
//                  logic.onEditCalving(
//                      this.logic.calvingList[position], position);
//                },
                ),
              ),
            );
          }


        });
  }

  ListView getUnSyncCalvingList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.calvingUnSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          calving = this.logic.calvingUnSyncList[position];
          return Dismissible(
            key: Key(this.logic.calvingUnSyncList[position].cId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              Calving calvingDelete = this.logic.calvingUnSyncList[position];
//              this.logic.confirmationDelete(calvingDelete, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Image.asset(
                  "images/xxhdpi/calving.png",
                  width: 40.0,
                  height: 40.0,
                ),
//                Container(
//                    width: 50.0,
//                    height: 50.0,
//                    decoration: new BoxDecoration(
//                        color: Colors.white,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.fill,
//                            image: new AssetImage(
//                                "images/xxhdpi/calving.png")))),

//                this.logic.calvingList[position].speciesId == 1
//                    ? Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                            color: Colors.blueAccent,
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                                fit: BoxFit.cover,
//                                image: new AssetImage("images/cattle.png"))))
////                    ? Icon(
////                  Icons.android,
////                  size: 35.0,
////                )
//                    : Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                            color: Colors.blueAccent,
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                                fit: BoxFit.contain,
//                                image: new AssetImage("images/buffalo.png")))),
//                Icon(
//                        Icons.map,
//                        size: 35.0,
//                      ),
                title: Text("Calving date: " + calving.calvingDate),
                subtitle: Text("Calf Code: " + calving.calfCode),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.edit,
                    size: 30.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    logic.onEditCalving(
                        this.logic.calvingUnSyncList[position], position);
                  },
                ),
//                onTap: () {
//                  logic.onEditCalving(
//                      this.logic.calvingList[position], position);
//                },
              ),
            ),
          );
        });
  }

  ListView getSyncCalvingList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.calvingSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          calving = this.logic.calvingSyncList[position];
          return Dismissible(
            key: Key(this.logic.calvingSyncList[position].cId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              Calving calvingDelete = this.logic.calvingSyncList[position];
//              this.logic.confirmationDelete(calvingDelete, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Image.asset(
                  "images/xxhdpi/calving.png",
                  width: 40.0,
                  height: 40.0,
                ),
//
                title: Text("Calving date: " + calving.calvingDate),
                subtitle: Text("Calf Code: " + calving.calfCode),
//                trailing: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    logic.onEditCalving(
//                        this.logic.calvingSyncList[position], position);
//                  },
//                ),
//                onTap: () {
//                  logic.onEditCalving(
//                      this.logic.calvingList[position], position);
//                },
              ),
            ),
          );
        });
  }
}
