import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/nitrogenBloc/NitrogenBloc.dart';
import 'package:nbis/features/home/NitrogenPageLogic.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/LiquidNitrogenRepo.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_utils/nepali_utils.dart';

class NitrogenPage extends StatefulWidget {
  bool isEdit;
  LiquidNitrogen nitrogen;

  NitrogenPage({this.nitrogen, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return NitrogenPageState(nitrogen: this.nitrogen, isEdit: this.isEdit);
  }
}

class NitrogenPageState extends State<NitrogenPage> {
  bool isEdit;
  LiquidNitrogen nitrogen;
  NitrogenBloc nitrogenBloc;
  NitrogenPageLogic logic;
  AppBar appBar;
  TextFormField tfDateReceived;
  TextFormField tfQuantity;
  NepaliDateTime selectedDateTime;
  Language language = Language.ENGLISH;
  TextEditingController dateController = TextEditingController(text: " ");

  GlobalKey<FormState> nitrogenFormKey = GlobalKey<FormState>();

  NitrogenPageState({this.nitrogen, this.isEdit = false});

  @override
  void initState() {
    super.initState();

    nitrogenBloc = NitrogenBloc(LiquidNitrogenRepo(context));
    this.logic = NitrogenPageLogic(this, nitrogenBloc);
    logic.onPostState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  Widget getNitrogenForm() {
    LiquidNitrogen nitrogen = this.logic.nitrogen;

    tfDateReceived = TextFormField(
        controller: dateController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          //   icon: Icon(Icons.date_range),
          labelText:   "Date of Nitrogen Received",
//          labelText:   AppTranslations.of(context).text("nitrogen_form_date"),
//          hintText: "Select date",
        ),
        onSaved: (String date) {
          nitrogen.receivedDate = date;
        },
        validator: (date) {
          if (date.isEmpty) {
//            return AppTranslations.of(context).text("nitrogenDate_validate");
            return "Date of Received can't be empty !!";
          }
        });

     tfQuantity = TextFormField(
       autofocus: true,
        maxLength: 3,
        keyboardType: TextInputType.number,
        initialValue:
           this.nitrogen == null ? "" : this.nitrogen.quantity.toString().split(".")[0],
        decoration: InputDecoration(
          //  icon: Icon(Icons.panorama_vertical),
          labelText:  "Quantity (In Litre)",
//          labelText:    AppTranslations.of(context).text("nitrogen_form_quantity"),
        ),
        onSaved: (quantity) {
          nitrogen.quantity = double.tryParse(quantity);
        },
        validator: (quantity) {
          if (quantity.isEmpty) {
//            return AppTranslations.of(context).text("quantity_validate");
            return "Quantity can't be empty !!";
          }
        });

//Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text(
       "Save",
//        AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (nitrogenFormKey.currentState.validate()) {
          nitrogenFormKey.currentState.save();

          this.logic.saveNitrogen();

        }
      },
    );

    //Raised Update Button
    RaisedButton rbUpdate = RaisedButton(
      child: Text(
      "Update",
//        AppTranslations.of(context).text("form_update"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (nitrogenFormKey.currentState.validate()) {
          nitrogenFormKey.currentState.save();
          setState((){
            this.logic.updateNitrogen();
          });
        }
      },
    );

    //astrick
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: nitrogenFormKey,
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),

    child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfQuantity,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: InkWell(
                      onTap: () {
                        this.logic.showDatePickers();
                      },
                      child: IgnorePointer(
                        child: tfDateReceived,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: Visibility(
                      visible: !this.isEdit,
                      child: rbSave,
                      replacement: rbUpdate,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(''),
                  ),
                ],
              ),
            ),
          ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: this.appBar,
      body: SafeArea(
        child: BlocProvider(
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      //   Log.v("scrolled");
                    },
                    child: SingleChildScrollView(
                      child: getNitrogenForm(),
                    ),
                  ),

                  //
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }
}
