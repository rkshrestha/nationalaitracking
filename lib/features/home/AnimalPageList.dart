import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/calvingBloc/CalvingBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/bloc/pdBloc/PDBloc.dart';
import 'package:nbis/features/home/AnimalPageListLogic.dart';
import 'package:nbis/features/repo/AIRepo.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/CalvingRepo.dart';
import 'package:nbis/features/repo/ExitRecordRepo.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/features/repo/PDRepo.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';

class AnimalPageList extends StatefulWidget {
  Farmer farmer;

  AnimalPageList({this.farmer});

  @override
  State<StatefulWidget> createState() {
    return AnimalPageListState(farmer: farmer);
  }
}

class AnimalPageListState extends State<AnimalPageList> {
  AnimalPageListLogic logic;
  FarmerBloc farmerBloc;
  AnimalBloc animalBloc;
  AIBloc aiBloc;
  PDBloc pdBloc;
  CalvingBloc calvingBloc;
  ExitRecordBloc exitRecordBloc;

  Animal animal;
  String speciesName;
  Farmer farmer;

  AnimalPageListState({this.farmer});

  var refreshAnimalListKey = GlobalKey<RefreshIndicatorState>();

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  void initState() {
    super.initState();
    farmerBloc = FarmerBloc(FarmerRepo(context));
    animalBloc = AnimalBloc(AnimalRepo(context));
    aiBloc = AIBloc(AIRepo(context));
    pdBloc = PDBloc(PDRepo(context));
    calvingBloc = CalvingBloc(CalvingRepo(context));
    exitRecordBloc = ExitRecordBloc(ExitRecordRepo(context));
    logic = AnimalPageListLogic(
        this, animalBloc, aiBloc, pdBloc, calvingBloc, exitRecordBloc, farmerBloc);
//    logic.onPostInit(farmer);
    refreshAnimalList();
  }

  Future<Null> refreshAnimalList() async {
    refreshAnimalListKey.currentState?.show(atTop: false);
    setState(() {
      logic.onPostInit(farmer);
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    this
        .logic
        .animalList
        .sort((a, b) => b.createdDate.compareTo(a.createdDate));

    final tabSync = <Tab>[
      Tab(text: "Not Sync", icon: new Icon(Icons.sync)),
      Tab(text: " Sync", icon: new Icon(Icons.sync))
    ];

    return DefaultTabController(
      length: tabSync.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Registered Animals",
//            AppTranslations.of(context).text("animal_registered_title"),
          ),
//          bottom: TabBar(tabs: tabSync),
//        title: Text('Registered Animals'),
//        title: Text('Registered Cattle/Buffalo'),
        ),

        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 45.0,
            color: Theme.of(context).primaryColor,
            child: GestureDetector(
              onTap: () {
                logic.onAddAnimal(farmer);
                Logger.v("farmerDatas: " + farmer.toString());
              },

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  Divider(
                    indent: 6.0,
                  ),
                  Text(
//                          AppTranslations.of(context).text("animal_register_button"),
                    "Register Animals",
//                        "Register Cattle/Buffalo",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ]),
//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 4,
//                    child: Text(''),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: Row(
//                      children: <Widget>[
//                        Icon(
//                          Icons.add,
//                          color: Colors.white,
//                        ),
//                        Divider(
//                          indent: 6.0,
//                        ),
//                        Text(
////                          AppTranslations.of(context).text("animal_register_button"),
//                          "Register Animals",
////                        "Register Cattle/Buffalo",
//                          style: TextStyle(
//                              color: Colors.white, fontWeight: FontWeight.bold),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Text(''),
//                  ),
//                ],
//              ),
            ),
          ),
        ),
        body: BlocProvider(
          child: SafeArea(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: RefreshIndicator(
                    key: refreshAnimalListKey,
                    child: this.logic.animalList.isEmpty
                        ? noData()
                        : getAnimalList(),
                    onRefresh: refreshAnimalList,
                  ),
                ),
//                Expanded(
//                  child: TabBarView(
//                    children: <Widget>[
//                      logic.animalUnSyncList.isEmpty
//                          ? noData()
//                          : getUnSyncAnimalList(),
//                      logic.animalSyncList.isEmpty
//                          ? noData()
//                          : getSyncAnimalList(),
//                    ],
//                  ),
//                ),
              ],
            ),
          ),
        ),
//      floatingActionButton: Padding(
////        padding: EdgeInsets.only(bottom: 1.0),
////        child: FloatingActionButton(
////            child: Icon(Icons.add),
////            tooltip: 'Add Animal',
////            backgroundColor: Theme.of(context).primaryColor,
////            onPressed: () {
////         this.logic.onAddAnimal();
////            }),
////      ),

//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//      floatingActionButton: Column(
//        crossAxisAlignment: CrossAxisAlignment.stretch,
//        mainAxisSize: MainAxisSize.min,
//        children: <Widget>[
//          FloatingActionButton.extended(
//            isExtended: true,
//            label: Text("Register Animal"),
//            icon: Image.asset("images/hdpi/add.png",color: Colors.white,),
//            tooltip: 'Add Farmer',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              this.logic.onAddAnimal();
//            },
//          ),
//        ],
//      ),
      ),
    );
  }

//  GridView getAnimalList() {
//    return GridView.builder(
//      shrinkWrap: true,
//      itemCount: logic.animalList.length,
//      gridDelegate:
//          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
//      itemBuilder: (BuildContext context, int position) {
//        speciesName = this.logic.animalList[position].speciesName;
//        return new Card(
//          color: Colors.blueGrey,
//          child: new GridTile(
//            footer: new Text(this.logic.animalList[position].speciesCode),
//            child: this.logic.animalList[position].speciesName == "Cattle"
//                ? Icon(
//                    Icons.android,
//                    size: 35.0,
//                  )
//                : Icon(
//                    Icons.map,
//                    size: 35.0,
//                  ),
//            //  child: new Text(this.logic.animalList[position].speciesName),
//          ),
//        );
//      },
//    );
//  }

  Widget noData() {
    return Container(
      child: Center(
        child: Text("No animal data has been found !!"
//          AppTranslations.of(context).text("animal_noData"),
            ),
//        child: Text("No Cattle/Buffalo Data has been found."),
      ),
    );
  }

  ListView getAnimalList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.animalList.length,
        itemBuilder: (BuildContext context, int position) {
          animal = this.logic.animalList[position];
          pdButton() {
            if (animal.speciesName == "Goat") {
              return Container(width: 33.0, height: 30.0, child: Text(""));
            } else if (animal.speciesName == "Pig") {
              return Container(width: 33.0, height: 30.0, child: Text(""));
            } else {
              return Card(
                elevation: 1,
                color: Colors.white30,
                child: Container(
                  width: 33.0,
                  height: 33.0,
                  child: IconButton(
                      tooltip: "Pregnancy Diagnosis",
                      icon: Image.asset(
                        "images/pregnancy.png",
//                        "images/xxhdpi/pd.png",
                        color: Colors.black,
                      ),
                      onPressed: () {
                        logic.onMoveToPD(this.logic.animalList[position]);
                      }),
                ),
              );
            }
          }

          editButton() {
            DateTime lastestDate = DateTime.now();

            if (animal.syncDate != null) {
//              String animalSyncDate = animal.syncDate == null
//                  ? "":SplitDate.splitDateString(animal.syncDate);

              String animalSyncDate = animal.syncDate == null
                  ? "": animal.syncDate.split(".").first;
              DateTime syncDate = DateTime.tryParse(animalSyncDate);

              DateTime addedDate =syncDate.add(new Duration(hours: 24));

              if (lastestDate.compareTo(addedDate)==1) {
//              if (lastestDate.compareTo(addedDate)==1 && animal.syncToServer == true) {
//              if (addedDate.difference(syncDate).inHours >= 24 && animal.syncToServer == true) {

//              if (syncDate.add(new Duration(hours: 24)) == lastestDate && animal.syncToServer == true) {
//              if (lastestDate.difference(syncDate).inHours >= 24 && animal.syncToServer == true) {
                return Container(
                  child: Text(""),
                );
              } else {
                return GestureDetector(
                  child: Container(
                    width: 40.0,
                    height: 40.0,
                    child: Card(
                      color: Colors.white70,
                      elevation: 3.0,
                      child: Image.asset(
                        "images/edits.png",
                      ),

                    ),

                  ),
                  onTap: () {
                    this.logic.onEditAnimal(this.logic.animalList[position]);
                  },
                );

//                return Container(
//                  child: GestureDetector(
//                    child: Icon(
//                      Icons.edit,
//                      size: 30.0,
//                      color: Colors.black,
//                    ),
//                    onTap: () {
//                      this.logic.onEditAnimal(this.logic.animalList[position]);
//                    },
//                  ),
//                );
              }
            } else {

              return GestureDetector(
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: Card(
                    color: Colors.white70,
                    elevation: 3.0,
                    child: Image.asset(
                      "images/edits.png",
                    ),

                  ),

                ),
                onTap: () {
                  this.logic.onEditAnimal(this.logic.animalList[position]);
                },
              );
//              return Container(
//                child: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditAnimal(this.logic.animalList[position]);
//                  },
//                ),
//              );
            }
          }

          if (animal.syncDate != null) {
//          if (animal.syncToServer == true) {
            return Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  child: animal.syncToServer == true
                      ? Image.asset(
                    "images/cow.png",
//                          "images/xxhdpi/animals.png",
                          color: Colors.green,
                        )
                      : Image.asset(
                    "images/cow.png",
//                          "images/xxhdpi/animals.png",
                          color: Colors.red,
                        ),
                ),

                title: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
//                        "Tag: " + animal.speciesCode,
                        "Tag: " +
                            animal.mobileNumber +
                            "-" +
                            animal.speciesCode,
//                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        animal.farmerName == null
                            ? ""
                            : "Farmer Name: " +animal.farmerName,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        animal.speciesName + "/" + animal.breedName.toString(),
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//mainAxisSize: MainAxisSize.min,
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 33.0,
                              height: 33.0,
                              child: IconButton(
                                  icon: Image.asset(
                                    "images/xxhdpi/pregnancydiagnosis.png",
                                    color: Colors.black,
                                  ),
                                  tooltip: "Artificial Insemination",
                                  onPressed: () {
                                    logic.onMoveToAI(farmer,
                                        this.logic.animalList[position]);
                                  }),
                            ),
                          ),
                          pdButton(),
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 33.0,
                              height: 33.0,
                              child: IconButton(
                                  tooltip: "Calving",
                                  icon: Image.asset(
                                    "images/calving_animal.png",
//                                    "images/xxhdpi/calving.png",
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    logic.onMoveToCalving(
                                        this.logic.animalList[position]);
                                  }),
                            ),
                          ),
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 33.0,
                              height: 33.0,
                              child: IconButton(
                                  tooltip: "Exit",
                                  icon: Image.asset(
                                    "images/xxhdpi/exit.png",
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    logic.onMoveToExit(
                                        this.logic.animalList[position]);
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                trailing: editButton(),
//                onTap: () {
//                  this.logic.onEditAnimal(this.logic.animalList[position]);
//                },
              ),
            );
          } else {
            return Dismissible(
              key: Key(this.logic.animalList[position].aId.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (DismissDirection direction) {
                Animal animalDelete = this.logic.animalList[position];
//              this.logic.confirmationDelete(animalDelete, position);
                setState(() {
                  this.logic.animalList.removeAt(position);
                  this.logic.confirmationForDeletion(animalDelete,farmer, position);
                });
              },
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 40.0,
                    height: 40.0,
                    child: animal.syncToServer == true
                        ? Image.asset(
                      "images/cow.png",
//                            "images/xxhdpi/animals.png",
                            color: Colors.green,
                          )
                        : Image.asset(
                      "images/cow.png",
//                            "images/xxhdpi/animals.png",
                            color: Colors.red,
                          ),
                  ),
//                Container(
//                    width: 40.0,
//                    height: 40.0,
//                    decoration: new BoxDecoration(
////                        color: Colors.blueGrey,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.fill,
//                            image:
//                            new AssetImage("images/xxhdpi/animals.png")))),

                  title: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
//                        "Tag: " + animal.speciesCode,
                          "Tag: " +
                              animal.mobileNumber +
                              "-" +
                              animal.speciesCode,
//                          style: TextStyle(fontSize: 14.0),
                        ),
                        Text(
                        animal.farmerName == null
                              ? ""
                              :    "Farmer Name: " +animal.farmerName,
                          style: TextStyle(fontSize: 14.0),
                        ),
                        Text(
                          animal.speciesName +
                              "/" +
                              animal.breedName.toString(),
                          style: TextStyle(fontSize: 14.0),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Card(
                              elevation: 1,
                              color: Colors.white30,
                              child: Container(
                                width: 33.0,
                                height: 33.0,
                                child: IconButton(
                                    icon: Image.asset(
                                      "images/xxhdpi/pregnancydiagnosis.png",
                                      color: Colors.black,
                                    ),
                                    tooltip: "Artificial Insemination",
                                    onPressed: () {
                                      logic.onMoveToAI(farmer,
                                          this.logic.animalList[position]);
                                    }),
                              ),
                            ),
//pd

                            pdButton(),

//

                            //
//                          this.logic.animalList[position].speciesName ==
//                                      "Goat" &&
//                                  this.logic.animalList[position].speciesName ==
//                                      "Pig"
//                              ? Card(
//                                  elevation: 1,
//                                  color: Colors.white,
//                                  child: Container(
//                                      width: 35.0,
//                                      height: 30.0,
//                                      child: Text("")),
//                                )
//                              : Card(
//                                  elevation: 1,
//                                  color: Colors.white30,
//                                  child: Container(
//                                    width: 35.0,
//                                    height: 30.0,
//                                    child: IconButton(
//                                        tooltip: "Pregnancy Diagnosis",
//                                        icon: Image.asset(
//                                          "images/xxhdpi/pd.png",
//                                          color: Colors.black,
//                                        ),
//                                        onPressed: () {
//                                          logic.onMoveToPD(
//                                              this.logic.animalList[position]);
//                                        }),
//                                  ),
//                                ),
                            Card(
                              elevation: 1,
                              color: Colors.white30,
                              child: Container(
                                width: 33.0,
                                height: 33.0,
                                child: IconButton(
                                    tooltip: "Calving",
                                    icon: Image.asset(
                                      "images/calving_animal.png",
//                                      "images/xxhdpi/calving.png",
                                      color: Colors.black,
                                    ),
                                    onPressed: () {
                                      logic.onMoveToCalving(
                                          this.logic.animalList[position]);
                                    }),
                              ),
                            ),
                            Card(
                              elevation: 1,
                              color: Colors.white30,
                              child: Container(
                                width: 33.0,
                                height: 33.0,
                                child: IconButton(
                                    tooltip: "Exit",
                                    icon: Image.asset(
                                      "images/xxhdpi/exit.png",
                                      color: Colors.black,
                                    ),
                                    onPressed: () {
                                      logic.onMoveToExit(
                                          this.logic.animalList[position]);
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  trailing: editButton(),
//                onTap: () {
//                  this.logic.onEditAnimal(this.logic.animalList[position]);
//                },
                ),
              ),
            );
          }
        });
  }

  ListView getUnSyncAnimalList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.animalUnSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          animal = this.logic.animalUnSyncList[position];
          return Dismissible(
            key: Key(this.logic.animalUnSyncList[position].aId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              Animal animalDelete = this.logic.animalUnSyncList[position];
//              this.logic.confirmationDelete(animalDelete, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                    width: 40.0,
                    height: 40.0,
                    decoration: new BoxDecoration(
//                        color: Colors.blueGrey,
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image:
                                new AssetImage("images/xxhdpi/animals.png")))),

//                this.logic.animalList[position].speciesId == 1
//                    ? Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                            color: Colors.blueAccent,
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                                fit: BoxFit.cover,
//                                image: new AssetImage("images/cattle.png"))))
//
////                    ? Icon(
////                  Icons.android,
////                  size: 35.0,
////                )
//                    : Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                            color: Colors.blueAccent,
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                                fit: BoxFit.contain,
//                                image: new AssetImage("images/buffalo.png")))),

                title: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Species Code:" + animal.speciesCode,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        "Species: " + animal.speciesName,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        "Breed : " + animal.breedName,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 35.0,
                              height: 30.0,
                              child: IconButton(
                                  icon: Image.asset(
                                    "images/xxhdpi/pregnancydiagnosis.png",
                                    color: Colors.black,
                                  ),
                                  tooltip: "Artificial Insemination",
                                  onPressed: () {
                                    logic.onMoveToAI(farmer,
                                        this.logic.animalUnSyncList[position]);
                                  }),
                            ),
                          ),
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 35.0,
                              height: 30.0,
                              child: IconButton(
                                  tooltip: "Pregnancy Diagnosis",
                                  icon: Image.asset(
                                    "images/xxhdpi/pd.png",
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    logic.onMoveToPD(
                                        this.logic.animalUnSyncList[position]);
                                  }),
                            ),
                          ),
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 35.0,
                              height: 30.0,
                              child: IconButton(
                                  tooltip: "Calving",
                                  icon: Image.asset(
                                    "images/xxhdpi/calving.png",
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    logic.onMoveToCalving(
                                        this.logic.animalUnSyncList[position]);
                                  }),
                            ),
                          ),
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 35.0,
                              height: 30.0,
                              child: IconButton(
                                  tooltip: "Exit",
                                  icon: Image.asset(
                                    "images/xxhdpi/exit.png",
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    logic.onMoveToExit(
                                        this.logic.animalUnSyncList[position]);
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.edit,
                    size: 30.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    this
                        .logic
                        .onEditAnimal(this.logic.animalUnSyncList[position]);
                  },
                ),
//                onTap: () {
//                  this.logic.onEditAnimal(this.logic.animalList[position]);
//                },
              ),
            ),
          );
        });
  }

  ListView getSyncAnimalList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.animalSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          animal = this.logic.animalSyncList[position];
          return Dismissible(
            key: Key(this.logic.animalSyncList[position].aId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              Animal animalDelete = this.logic.animalSyncList[position];
//              this.logic.confirmationDelete(animalDelete, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                    width: 40.0,
                    height: 40.0,
                    decoration: new BoxDecoration(
//                        color: Colors.blueGrey,
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image:
                                new AssetImage("images/xxhdpi/animals.png")))),

//                this.logic.animalList[position].speciesId == 1
//                    ? Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                            color: Colors.blueAccent,
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                                fit: BoxFit.cover,
//                                image: new AssetImage("images/cattle.png"))))
//
////                    ? Icon(
////                  Icons.android,
////                  size: 35.0,
////                )
//                    : Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                            color: Colors.blueAccent,
//                            shape: BoxShape.circle,
//                            image: new DecorationImage(
//                                fit: BoxFit.contain,
//                                image: new AssetImage("images/buffalo.png")))),

                title: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Species Code:" + animal.speciesCode,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        "Species: " + animal.speciesName,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        "Breed : " + animal.breedName,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 35.0,
                              height: 30.0,
                              child: IconButton(
                                  icon: Image.asset(
                                    "images/xxhdpi/pregnancydiagnosis.png",
                                    color: Colors.black,
                                  ),
                                  tooltip: "Artificial Insemination",
                                  onPressed: () {
                                    logic.onMoveToAI(farmer,
                                        this.logic.animalSyncList[position]);
                                  }),
                            ),
                          ),
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 35.0,
                              height: 30.0,
                              child: IconButton(
                                  tooltip: "Pregnancy Diagnosis",
                                  icon: Image.asset(
                                    "images/xxhdpi/pd.png",
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    logic.onMoveToPD(
                                        this.logic.animalSyncList[position]);
                                  }),
                            ),
                          ),
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 35.0,
                              height: 30.0,
                              child: IconButton(
                                  tooltip: "Calving",
                                  icon: Image.asset(
                                    "images/xxhdpi/calving.png",
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    logic.onMoveToCalving(
                                        this.logic.animalSyncList[position]);
                                  }),
                            ),
                          ),
                          Card(
                            elevation: 1,
                            color: Colors.white30,
                            child: Container(
                              width: 35.0,
                              height: 30.0,
                              child: IconButton(
                                  tooltip: "Exit",
                                  icon: Image.asset(
                                    "images/xxhdpi/exit.png",
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    logic.onMoveToExit(
                                        this.logic.animalSyncList[position]);
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
//                trailing: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditAnimal(this.logic.animalSyncList[position]);
//                  },
//                ),
//                onTap: () {
//                  this.logic.onEditAnimal(this.logic.animalList[position]);
//                },
              ),
            ),
          );
        });
  }
}
