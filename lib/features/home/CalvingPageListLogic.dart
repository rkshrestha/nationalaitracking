import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/calvingBloc/CalvingBloc.dart';
import 'package:nbis/features/home/CalvingPage.dart';
import 'package:nbis/features/home/CalvingPageList.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class CalvingPageListLogic {
  CalvingPageListState state;
  CalvingBloc calvingBloc;

  List<Calving> calvingList = [];
  List<Calving> calvingUnSyncList = [];
  List<Calving> calvingSyncList = [];
  static Calving calving = Calving();

  CalvingPageListLogic(this.state, this.calvingBloc);

  void onPostInit(Animal animal) {
    if (animal != null) {
//      getUnSyncCalvingListByAnimal(animal);
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//        getSyncCalvingListByAnimal(animal);
//      });
      getCalvingListByAnimal(animal);
    } else {
      getCalvingList();
//      getUnSyncCalvingList();
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//        getSyncCalvingList();
//      });
    }
  }

  getCalvingListByAnimal(animal) {
    this.calvingBloc.getCalvingListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.calvingList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncCalvingListByAnimal(animal) {
    this.calvingBloc.getUnSyncCalvingListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.calvingUnSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncCalvingListByAnimal(animal) {
    this.calvingBloc.getSyncCalvingListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.calvingSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getCalvingList() {
    this.calvingBloc.getCalvingList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("calving  list is " + it.data.toString());
        this.state.setState(() {
          this.calvingList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncCalvingList() {
    this.calvingBloc.getUnSyncCalvingList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("calving  list is " + it.data.toString());
        this.state.setState(() {
          this.calvingUnSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncCalvingList() {
    this.calvingBloc.getSyncCalvingList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("calving  list is " + it.data.toString());
        this.state.setState(() {
          this.calvingSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //delete

  void confirmationDelete(Calving calving, Animal animal, int position) {
    DialogUtils.showAlertDialog(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "Are you sure,you want to delete?", (it) {
//        AppTranslations.of(state.context).text("delete_warning"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.pop(this.state.context, true);

            if (animal != null) {

              getCalvingListByAnimal(animal);
            } else {
              getCalvingList();

            }
            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Logger.v('position calving' + position.toString());

            this
                .calvingBloc
                .deleteCalving(this.state.context, calving)
                .listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {
//                state.setState(() {
//                  calving.syncToServer == true
//                      ? this.calvingSyncList.removeAt(position)
//                      : this.calvingUnSyncList.removeAt(position);
//                });
//                state.setState(() {
//                  this.calvingList.removeAt(position);
//                });

                Navigator.pop(this.state.context, true);
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(state.context).pop();
                });
              }
            });

            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }

  onEditCalving(Calving calving, int position)async {
   await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return CalvingPage(calving: calving, isEdit: true);
    }));
   state.setState(() {
     state.refreshCalvingList();
   });
  }

  onAddCalving(Animal animal)async {
   await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return CalvingPage(
        animal: animal,
      );
    }));
   state.setState(() {
     state.refreshCalvingList();
   });
  }
}
