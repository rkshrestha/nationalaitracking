import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/calvingBloc/CalvingBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/CalvingPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/SplitDate.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:nepali_utils/nepali_utils.dart';

class CalvingPageLogic {
  CalvingPageState state;
  CalvingBloc calvingBloc;
  Animal animal = Animal();
  Farmer farmer = Farmer();
  AIBloc aiBloc;
  AnimalBloc animalBloc;
  FarmerBloc farmerBloc;
  int speciesId;
  int breedId;

  Calving calving = Calving();

  CalvingPageLogic(
      this.state, this.calvingBloc, this.farmerBloc, this.animalBloc,this.aiBloc);

  void onPostState() {
    if (this.state.isEdit) {
      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//              AppTranslations.of(state.context).text("calving_form_edit"),
//            );
        this.state.setAppBar('Edit Calving');
      });
      this.calving = state.calving;

      //display vet assist in update
      if (calving.vetAssist) {
        state.selectedAssist = 0;
      } else {
        state.selectedAssist = 1;
      }

//display calf gender in update
      if (calving.calfSex == "Male") {
        state.selectedGender = 0;
      } else {
        state.selectedGender = 1;
      }
      //
      //
      //get farmer update
      this.farmerBloc.getFarmerByFId(this.calving.mFarmerId).listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          farmer = it.data;
          state.tfMobileNumber.controller.text = it.data.mobileNumber;

          //get speces code update
          this.animalBloc.getAnimalByaId(this.calving.mAnimalId).listen((it) {
            if (it == null) return;
            if (it.status == Status.SUCCESS) {
              animal = it.data;
              state.tfSpeciesCode.controller.text = it.data.speciesCode;
//                  it.data.speciesCode.split("-")[1];
            } else if (it.status == Status.ERROR) {
              ToastUtils.show(it.errorMessage);
            } else {
              new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                Navigator.of(this.state.context).pop();
              });
            }
          });
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
            Navigator.of(this.state.context).pop();
          });
        }
      });

      state.dateController.text = this.calving.calvingDate;
    } else {
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//              AppTranslations.of(state.context)
//                  .text("calving_form_add_calving"),
//            );
      this.state.setAppBar('Add Calving');
//      });
//      DateTime today = new DateTime.now();
//      DateTime todayDate =
//          new DateTime(today.year + 56, today.month + 8, today.day + 17);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
//      state.dateController.text = todayNepaliDate.toString();

      NepaliDateTime currentTime = NepaliDateTime.now();
      String todayDateNepali =  SplitDate.splitNepali(currentTime);
//      String todayDateNepali =  currentTime.toIso8601String().split("T").first;
      state.dateController.text = todayDateNepali;

      Logger.v('inside ' + this.state.isEdit.toString());
    }
  }

  saveCalving() async {
    speciesId = animal.speciesId;
    breedId = animal.breedId;
    if (state.selectedGender < 0) {
      this.state.showToast("Please select Gender");
    } else if (state.selectedAssist < 0) {
      this.state.showToast("Please select Vet assist");
    } else {
      this
          .calvingBloc
          .saveCalving(this.state.context, this.calving, this.farmer,
              this.animal, speciesId, breedId)
          .listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          ToastUtils.show(it.data);
          state.setState(() {
//            Navigator.pop(this.state.context);
          });
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(this.state.context).pop();
//        });
        }
      });
    }
  }

  updateCalving() async {
    this
        .calvingBloc
        .updateCalving(
            this.state.context, this.calving, this.farmer, this.animal)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        Navigator.pop(this.state.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  selectedCalfGender(int value) {
    state.setState(() {
      state.selectedGender = value;
    });
  }

  selectedVetAssist(int value) {
    state.setState(() {
      state.selectedAssist = value;
    });
  }

  //date picker

  String formattedDate;

//  Future<Null> showDatePickers() async {
//    final DateTime picked = await showDatePicker(
//        context: state.context,
//        initialDate: DateTime.now(),
//        firstDate: DateTime(2010),
//        lastDate: DateTime(2100));
//
//    if (picked != null) {
//      state.setState(() {
//        DateTime dates = picked;
//        formattedDate = DateFormat('yyyy-MM-dd').format(picked);
//        state.setState(() {
//          state.tfDateCalving.controller.text = formattedDate.toString();
//        });
//      });
//    }
//  }

  showDatePickers()async {

    state.selectedDateTime = await showNepaliDatePicker(
      context: state.context,
      initialDate: NepaliDateTime.now(),
      firstDate: NepaliDateTime(2050),
      lastDate: NepaliDateTime(2090),
      language: state.language,
    );
    state.setState(() {
      if (state.selectedDateTime != null){
        state.dateController.text =SplitDate.splitNepali(state.selectedDateTime);
//        state.dateController.text =state.selectedDateTime.toIso8601String().split("T").first;

      }else{
        state.dateController.text ="";

      }
    });
//    NepaliDatePicker.showPicker(
//        context: state.context,
//        startYear: 2052,
//        endYear: 2100,
//        color: Colors.blue,
//        barrierDismissible: false,
//        onPicked: (DateTime picked) {
//          if (picked != null) {
//            state.setState(() {
//              DateTime dates = picked;
//              state.setState(() {
//                state.tfDateCalving.controller.text =
//                    picked.toIso8601String().split("T").first;
//              });
//            });
//          }
//        });
  }

  getFarmerByMobileNumber(String mblNumber) async {
    this
        .farmerBloc
        .getFarmerByMobileNumber(this.state.context, mblNumber)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        farmer = it.data;

        // ToastUtils.show(it.data.toString());
        state.tfFarmer.controller.text = it.data.farmerName.toString();
        state.tfAddress.controller.text = it.data.municipality.toString();
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfFarmer.controller.text = "";
        state.tfAddress.controller.text = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }

  //get animal

  getAnimalBySpeciesCode(String speciesCode) async {
    this
        .animalBloc
        .getAnimalBySpeciesCode(this.state.context, speciesCode)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        animal = it.data;
        // ToastUtils.show(it.data.toString());
        state.tfSpecies.controller.text = it.data.speciesName.toString();
        state.tfBreed.controller.text = it.data.breedName.toString();
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfSpecies.controller.text = "";
        state.tfBreed.controller.text = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }
}
