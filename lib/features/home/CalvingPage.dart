import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/calvingBloc/CalvingBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/CalvingPageLogic.dart';
import 'package:nbis/features/repo/AIRepo.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/CalvingRepo.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_utils/nepali_utils.dart';

class CalvingPage extends StatefulWidget {
  bool isEdit;
  Animal animal;
  Calving calving;

  CalvingPage({this.calving, this.animal, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return CalvingPageState(
        calving: this.calving, animal: this.animal, isEdit: this.isEdit);
  }
}

class CalvingPageState extends State<CalvingPage> {
  bool isEdit;
  Calving calving;
  CalvingBloc calvingBloc;
  CalvingPageLogic logic;
  TextFormField tfDateCalving;
  AnimalBloc animalBloc;
  FarmerBloc farmerBloc;
  AIBloc aiBloc;
  AppBar appBar;
  Animal animal;
  NepaliDateTime selectedDateTime;
  Language language = Language.ENGLISH;

  TextFormField tfMobileNumber;
  TextFormField tfFarmer;
  TextFormField tfAddress;
  TextFormField tfSpeciesCode;
  TextFormField tfSpecies;
  TextFormField tfBreed;
  TextEditingController dateController = TextEditingController(text: " ");
  TextEditingController mblController = TextEditingController();
  TextEditingController farmerController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController aCodeController = TextEditingController();
  TextEditingController speciesController = TextEditingController();
  TextEditingController breedController = TextEditingController();
  FocusNode textFocus = new FocusNode();

  CalvingPageState({this.calving, this.animal, this.isEdit = false});

  GlobalKey<FormState> calvingFormKey = GlobalKey<FormState>();
  static var farmer = ['Select Farmer', 'Abd', 'cde'];
  int selectedGender = -1;
  int selectedAssist = -1;


  void showToast(String message) {
    ToastUtils.show(message);
  }
  //get number and animal

  void onNumberChanged() {
    String mobileText = mblController.text;
//    bool hasFocus = textFocus.hasFocus;
    Logger.v(mobileText);
    setState(() {
      mblController.selection = new TextSelection(
          baseOffset: mobileText.length, extentOffset: mobileText.length);
    });

    if (mobileText.length == 10) {
//      this.logic.getFarmerByMobileNumber(int.tryParse(mobileText));
      this.logic.getFarmerByMobileNumber(mobileText);
    }
  }

  void onAnimalChanged() {
    String animalText = aCodeController.text;
    Logger.v('getCOde:' + aCodeController.text);
//    bool hasFocus = textFocus.hasFocus;
    Logger.v(animalText);

    setState(() {
      aCodeController.selection = new TextSelection(
          baseOffset: animalText.length, extentOffset: animalText.length);
    });

    this.logic.getAnimalBySpeciesCode(animalText);
//    this.logic.getAnimalBySpeciesCode(mblController.text + "-" + animalText);
  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  @override
  initState() {
    super.initState();
    this.calvingBloc = CalvingBloc(CalvingRepo(context));
    this.animalBloc = AnimalBloc(AnimalRepo(context));
    this.farmerBloc = FarmerBloc(FarmerRepo(context));
    this.aiBloc = AIBloc(AIRepo(context));
    this.logic = CalvingPageLogic(this, calvingBloc, farmerBloc, animalBloc,aiBloc);
    mblController.text = animal == null ? "" : animal.mobileNumber;
//    mblController.text = animal == null ? "" : animal.speciesCode.split("-")[0];
    aCodeController.text =
//        animal == null ? "" : animal.speciesCode.split("-")[1];
        animal == null ? "" : animal.speciesCode;
    logic.onPostState();
    initUIController();

    if(animal != null){
      Logger.v("animal"+animal.speciesCode);
      Logger.v("farmer number: "+animal.mobileNumber);

      if (animal.mobileNumber.length == 10) {
        Logger.v("farmer number inside: "+animal.mobileNumber);
        this.logic.getFarmerByMobileNumber(animal.mobileNumber);
      }
//      setState((){
//
//      });
      this.logic.getAnimalBySpeciesCode(animal.speciesCode);
    }


  }

  void initUIController() {
    //   aCodeController = TextEditingController(text: "");
    mblController.addListener(onNumberChanged);
    aCodeController.addListener(onAnimalChanged);
  }

  @override
  void dispose() {
    mblController.removeListener(onNumberChanged);
    aCodeController.removeListener(onAnimalChanged);
    mblController.dispose();
    textFocus.dispose();
    super.dispose();
  }

  Widget getCalvingForm() {
    Calving calving = this.logic.calving;

//    DropdownButton dbFarmer = DropdownButton(
//        items: farmer.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        value: 'Select Farmer',
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

    //animal code

    tfSpeciesCode = TextFormField(
        keyboardType: TextInputType.number,
        controller: aCodeController,
//        autofocus: true,
        decoration: InputDecoration(
          //icon: Icon(Icons.confirmation_number),
          labelText: "Animal Code",
//          labelText: AppTranslations.of(context).text("calving_form_code"),
          prefixText: mblController.text + "-",
          //    hintText: "XXXX-XXXXX-XXXXXX",
        ),
        onSaved: (String animalCode) {
//          String animalsCode = mblController.text+"-"+animalCode;
          calving.speciesCode = animalCode;
        },
        validator: (animalCode) {
          if (animalCode.isEmpty) {
//            return AppTranslations.of(context).text("code_validate");
            return " Animal Code can't be empty";
          }
        });

    //calf code
    TextFormField tfCalfId = TextFormField(
        keyboardType: TextInputType.number,
//        initialValue: this.calving == null ? "" : this.calving.calfCode.split("-")[1],
        inputFormatters: [
          new BlacklistingTextInputFormatter(new RegExp('[\\.|\\,|\\-|\\*|\\+|\\#]')),
        ],
        initialValue: this.calving == null ? "" : this.calving.calfCode,
        decoration: InputDecoration(
          //icon: Icon(Icons.confirmation_number),
          labelText: "Calf Id",
//          labelText: AppTranslations.of(context).text("calving_form_calf"),
          prefixText: mblController.text + "-",
        ),
        onSaved: (String calf) {
//          String calfCode = mblController.text + "-" + calf;
          calving.calfCode = calf;
        },
        validator: (calf) {
          if (calf.isEmpty) {
//            return AppTranslations.of(context).text("calf_validate");
            return " Calf Id can't be empty";
          }
        });

    //date calving

    tfDateCalving = TextFormField(
        keyboardType: TextInputType.number,
        controller: dateController,
        decoration: InputDecoration(
          labelText: "Calving Date",
//          labelText: AppTranslations.of(context).text("calving_form_date"),
//          hintText: "Select date",
          //  icon: Icon(Icons.date_range),
        ),
        onSaved: (String calvingDate) {
          calving.calvingDate = calvingDate;
        },
        validator: (calvingDate) {
          if (calvingDate.isEmpty) {
//            return AppTranslations.of(context).text("calvingDate_validate");
            return "Calving Date can't be empty";
          }
        });

    //sex
    radioCalfGender() {
      List<Widget> radioGenderList = new List<Widget>();

      radioGenderList.add(new Row(
        children: <Widget>[
          Row(
            children: <Widget>[
              Radio(
                  value: 0,
                  groupValue: selectedGender,
                  onChanged: (int value) {
                    logic.selectedCalfGender(value);
                    calving.calfSex = "Male";
                  }),
              Text("Male"),
//              Text(AppTranslations.of(context).text("calving_form_male")),
            ],
          ),
          Row(
            children: <Widget>[
              Radio(
                  value: 1,
                  groupValue: selectedGender,
                  onChanged: (int value) {
                    logic.selectedCalfGender(value);
                    calving.calfSex = "Female";
                  }),
              Text(
              "Female",
//                AppTranslations.of(context).text("calving_form_female"),
              ),
            ],
          ),
          Text(
            '*',
            style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
            ),
          ),
        ],
      ));
      return radioGenderList;
    }

//
//    // male radio button
//    Radio maleRadio = Radio(
//      value: 0,
//      groupValue: radioValue,
//      //onChanged: ,
//    );
//    //female radio button
//    Radio femaleRadio = Radio(
//      value: 1,
//      groupValue: radioValue,
//      //onChanged: ,
//    );

//assist
    // yes radio button
    radioVetAssist() {
      List<Widget> radioVetAssistList = new List<Widget>();

      radioVetAssistList.add(new Row(
        children: <Widget>[
          Row(
            children: <Widget>[
              Radio(
                  value: 0,
                  groupValue: selectedAssist,
                  onChanged: (int value) {
                    logic.selectedVetAssist(value);
                    calving.vetAssist = true;
                  }),
              Text('Yes'),
            ],
          ),
          Row(
            children: <Widget>[
              Radio(
                  value: 1,
                  groupValue: selectedAssist,
                  onChanged: (int value) {
                    logic.selectedVetAssist(value);
                    calving.vetAssist = false;
                  }),
              Text('No'),
            ],
          ),
          Text(
            '*',
            style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
            ),
          ),
        ],
      ));
      return radioVetAssistList;
    }

//    Radio yesRadio = Radio(
//      value: 0,
//      groupValue: radioValue,
//      //onChanged: ,
//    );
//    //no radio button
//    Radio noRadio = Radio(
//      value: 1,
//      groupValue: radioValue,
//      //onChanged: ,
//    );

    //remarks
    TextFormField tfRemarks = TextFormField(
      keyboardType: TextInputType.multiline,
      maxLines: null,
      initialValue: this.calving == null ? "" : this.calving.remarks,
      decoration: InputDecoration(
        //  icon: Icon(Icons.ac_unit),
        labelText: "Remarks",
//        labelText: AppTranslations.of(context).text("calving_form_remarks"),
      ),
      onSaved: (String remarks) {
        calving.remarks = remarks;
      },
//        validator: (remarks) {
//          if (remarks.isEmpty) {
//            return " Remarks can't be empty";
//          }
//        }
    );

//Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text(
      "Save",
//        AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (calvingFormKey.currentState.validate()) {
          calvingFormKey.currentState.save();
          this.logic.saveCalving();
        }
      },
    );

    //Raised Update Button
    RaisedButton rbUpdate = RaisedButton(
      child: Text(
       "Update",
//        AppTranslations.of(context).text("form_update"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (calvingFormKey.currentState.validate()) {
          calvingFormKey.currentState.save();
          this.logic.updateCalving();
        }
      },
    );

    //mobile number tf
    tfMobileNumber = TextFormField(
        keyboardType: TextInputType.phone,
        controller: mblController,
//        autofocus: true,
        maxLength: 10,
        decoration: InputDecoration(
          labelText:
             "Farmer's Mobile Number",
//              AppTranslations.of(context).text("calving_form_mobile_number"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String mbl) {
          calving.mobileNumber = mbl;
        },
        validator: (mblNumber) {
          if (mblNumber.isEmpty) {
//            return AppTranslations.of(context).text("mobile_number_validate");
            return "Mobile Number can't be empty !!";
          } else if (mblNumber.length != 10) {
//            return AppTranslations.of(context)
//                .text("mobile_number_ten_digit_validate");
            return "Please enter ten digits number !!";
          }
        });

    //farmer name tf
    tfFarmer = TextFormField(
        enabled: false,
        controller: farmerController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText:
              "Farmer's Name",
//              AppTranslations.of(context).text("calving_form_farmer_name"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String farmer) {
          calving.farmerName = farmer;
        },
        validator: (farmer) {
          if (farmer.isEmpty) {
            return "Farmer Name can't be empty";
          }
        });

    //address tf
    tfAddress = TextFormField(
        enabled: false,
        keyboardType: TextInputType.number,
        controller: addressController,
        decoration: InputDecoration(
          labelText: "Address",
//          labelText: AppTranslations.of(context).text("calving_form_address"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String address) {},
        validator: (address) {
          if (address.isEmpty) {
            return "Address can't be empty";
          }
        });

    //species tf
    tfSpecies = TextFormField(
        enabled: false,
        controller: speciesController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText:"Animal",
//          labelText: AppTranslations.of(context).text("calving_form_species"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String species) {
          calving.speciesName =species;

        },
        validator: (species) {
          if (species.isEmpty) {
            return "Species can't be empty";
          }
        });

    //breed
    tfBreed = TextFormField(
        enabled: false,
        controller: breedController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Breed",
//          labelText: AppTranslations.of(context).text("calving_form_breed"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String breed) {},
        validator: (breed) {
          if (breed.isEmpty) {
            return "Breed can't be empty";
          }
        });

    //astrick
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: calvingFormKey,
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
//          Padding(
//            padding: EdgeInsets.only(top: 6.0),
//            child: ListTile(
//              title: Row(
//                children: <Widget>[
//                  Icon(Icons.select_all),
//                  Padding(
//                    padding: EdgeInsets.only(left: 12.0),
//                  ),
//                  Expanded(
//                    flex: 7,
//                    child: dbFarmer,
//                  ),
//                  Expanded(
//                    flex: 1,
//                    child: astrickText,
//                  ),
//                ],
//              ),
//            ),
//          ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfMobileNumber,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfFarmer,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfAddress,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfSpeciesCode,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfSpecies,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfBreed,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: InkWell(
                      onTap: () {
                        this.logic.showDatePickers();
                      },
                      child: IgnorePointer(
                        child: tfDateCalving,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfCalfId,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(left: 14.0, top: 8.0),
              child: Text(
//                AppTranslations.of(context).text("calving_form_gender"),
                'Calf Gender: ',
                //  textAlign: TextAlign.left,
              ),
            ),
            ListTile(
              title: Row(
                children: radioCalfGender(),
//              children: <Widget>[
                //  Icon(Icons.repeat_one),
//                Expanded(
//                  child: Row(
//                    children: <Widget>[
//                      maleRadio,
//                      Text('Male'),
//                    ],
//                  ),
//                ),
//                Expanded(
//                  child: Row(
//                    children: radioCalfGender(),
//                    children: <Widget>[
//                      femaleRadio,
//                      Text('Female'),
//                    ],
//                  ),
//                ),
//                Expanded(
//                  child: astrickText,
//                ),
//              ],
              ),
            ),

//assist
            Padding(
              padding: EdgeInsets.only(left: 14.0, top: 8.0),
              child: Text(
//                AppTranslations.of(context).text("calving_form_vet"),
                'Vet Assist: ',
                //  textAlign: TextAlign.left,
              ),
            ),
            ListTile(
              title: Row(
                children: radioVetAssist(),
//              children: <Widget>[
//                //  Icon(Icons.repeat_one),
//                Expanded(
//                  child: Row(
//                    children: <Widget>[
//                      yesRadio,
//                      Text('Yes'),
//                    ],
//                  ),
//                ),
//                Expanded(
//                  child: Row(
//                    children: <Widget>[
//                      noRadio,
//                      Text('No'),
//                    ],
//                  ),
//                ),
//                Expanded(
//                  child: astrickText,
//                ),
//              ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfRemarks,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: Visibility(
                      visible: !this.isEdit,
                      child: rbSave,
                      replacement: rbUpdate,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(''),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: this.appBar,
      body: SafeArea(
        child: BlocProvider(
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      //   Log.v("scrolled");
                    },
                    child: SingleChildScrollView(
                      child: getCalvingForm(),
                    ),
                  ),

                  //
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }
}
