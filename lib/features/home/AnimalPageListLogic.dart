import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/calvingBloc/CalvingBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/bloc/pdBloc/PDBloc.dart';
import 'package:nbis/features/home/AIPageList.dart';
import 'package:nbis/features/home/AnimalPage.dart';
import 'package:nbis/features/home/AnimalPageList.dart';
import 'package:nbis/features/home/CalvingPageList.dart';
import 'package:nbis/features/home/ExitRecordPageList.dart';
import 'package:nbis/features/home/PDPageList.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';

class AnimalPageListLogic {
  AnimalPageListState state;
  AnimalBloc animalBloc;

  FarmerBloc farmerBloc;
  AIBloc aiBloc;
  PDBloc pdBloc;
  CalvingBloc calvingBloc;
  ExitRecordBloc exitRecordBloc;

  List<Animal> animalList = [];
  List<Animal> farmerListCheck = [];
  List<Animal> animalUnSyncList = [];
  List<Animal> animalSyncList = [];
  static Animal animal = Animal();
  var aiCount;
  var pdCount;
  var calvingCount;
  var exitRecordCount;
  List<ArtificialInsemination> aiListCheck = [];
  List<PregnancyDiagnosis> pdListCheck = [];
  List<Calving> calvingListCheck = [];
  List<ExitRecord> exitRecordListCheck = [];

  AnimalPageListLogic(this.state, this.animalBloc, this.aiBloc, this.pdBloc,
      this.calvingBloc, this.exitRecordBloc, this.farmerBloc);

  void onPostInit(Farmer farmer) {
    if (farmer != null) {
      getAnimalListByFarmer(farmer);
      Logger.v("Specific data:" + farmer.fId.toString());
//      getUnSyncAnimalListByFarmer(farmer);
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//        getSyncAnimalListByFarmer(farmer);
//      });
    } else {
      getAnimalList();

//      getUnSyncAnimalList();
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//        getSyncAnimalList();
//      });
    }


    getAIList();
    getPDList();
    getCalvingList();
    getExitRecordList();
  }

  getAnimalListByFarmer(farmer) {
    this.animalBloc.getAnimalListByFarmer(farmer).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        Logger.v("Animal list is " + it.data.toString());
        this.state.setState(() {
          this.animalList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncAnimalListByFarmer(farmer) {
    this.animalBloc.getUnSyncAnimalListByFarmer(farmer).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        Logger.v("Animal list is " + it.data.toString());
        this.state.setState(() {
          this.animalUnSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncAnimalListByFarmer(farmer) {
    this.animalBloc.getSyncAnimalListByFarmer(farmer).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        Logger.v("Animal list is " + it.data.toString());
        this.state.setState(() {
          this.animalSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getAnimalList() {
    this.animalBloc.getAnimalList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("Animal list is " + it.data.toString());
        this.state.setState(() {
          this.animalList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncAnimalList() {
    this.animalBloc.getUnSyncAnimalList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("Animal list is " + it.data.toString());
        this.state.setState(() {
          this.animalUnSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncAnimalList() {
    this.animalBloc.getSyncAnimalList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("Animal list is " + it.data.toString());
        this.state.setState(() {
          this.animalSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //get List of ai

  getAIList() {
    this.aiBloc.getAIList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("ai list is " + it.data.toString());

        this.state.setState(() {
          this.aiListCheck = it.data;

          Logger.v("getIds:");
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }


  //get List of pd

  getPDList() {
    this.pdBloc.getPdList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("pd list is " + it.data.toString());
        this.state.setState(() {
          this.pdListCheck = it.data;

        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //get List of calving
  getCalvingList() {
    this.calvingBloc.getCalvingList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("calving  list is " + it.data.toString());
        this.state.setState(() {
          this.calvingListCheck = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }


  //get List of exit
  getExitRecordList() {
    this.exitRecordBloc.getExitRecordList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("exit  list is " + it.data.toString());
        this.state.setState(() {
          this.exitRecordListCheck = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }




  confirmationForDeletion(Animal animal, Farmer farmer, int position) {
    aiCount = aiListCheck.where((c) => c.aId == animal.aId).toList().length;
    Logger.v("aiCount:" + aiCount.toString());
    pdCount =
        pdListCheck.where((c) => c.mAnimalId == animal.aId).toList().length;

    calvingCount = calvingListCheck
        .where((c) => c.mAnimalId == animal.aId)
        .toList()
        .length;

    exitRecordCount = exitRecordListCheck
        .where((c) => c.mAnimalId == animal.aId)
        .toList()
        .length;

                    Logger.v("aiCount:" + aiCount.toString());
                Logger.v("pdCount:" + pdCount.toString());
                Logger.v("calvingCount:" + calvingCount.toString());
                Logger.v("exitRecordCount:" + exitRecordCount.toString());

    if (aiCount == 0 &&
        pdCount == 0 &&
        calvingCount == 0 &&
        exitRecordCount == 0) {
      confirmationDelete(animal, farmer, position);
    } else {
      confirmationDeleteWithData(farmer);
    }
  }

//  confirmationForDeletion(Animal animal, Farmer farmer, int position) {
//    this.aiBloc.getAIList().listen((it) {
////      if (it == null) return;
//      if (it.status == Status.SUCCESS) {
////        this.state.setState(() {
//        this.aiListCheck = it.data;
//
//        aiCount = aiListCheck.where((c) => c.aId == animal.aId).toList().length;
////        });
//
//        //pd check
//        this.pdBloc.getPdList().listen((it) {
////        if (it == null) return;
//          if (it.status == Status.SUCCESS) {
////            this.state.setState(() {
//            this.pdListCheck = it.data;
//            pdCount = pdListCheck
//                .where((c) => c.mAnimalId == animal.aId)
//                .toList()
//                .length;
////            });
//          }
////        else {
////          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
////            Navigator.of(state.context).pop();
////          });
////        }
//
//          this.calvingBloc.getCalvingList().listen((it) {
////          if (it == null) return;
//            Logger.v("inside listener it");
//            if (it.status == Status.SUCCESS) {
//              Logger.v("calving  list is " + it.data.toString());
////              this.state.setState(() {
//              this.calvingListCheck = it.data;
//
//              calvingCount = calvingListCheck
//                  .where((c) => c.mAnimalId == animal.aId)
//                  .toList()
//                  .length;
////              });
//            }
////      else {
////        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
////          Navigator.of(state.context).pop();
////        });
////      }
//
//            //exit
//            this.exitRecordBloc.getExitRecordList().listen((it) {
////            if (it == null) return;
//              Logger.v("inside listener it");
//              if (it.status == Status.SUCCESS) {
//                Logger.v("exit  list is " + it.data.toString());
////                this.state.setState(() {
//                this.exitRecordListCheck = it.data;
//
//                exitRecordCount = exitRecordListCheck
//                    .where((c) => c.mAnimalId == animal.aId)
//                    .toList()
//                    .length;
////                });
//
//                Logger.v("aiCount:" + aiCount.toString());
//                Logger.v("pdCount:" + pdCount.toString());
//                Logger.v("calvingCount:" + calvingCount.toString());
//                Logger.v("exitRecordCount:" + exitRecordCount.toString());
//                if (aiCount == 0 &&
//                    pdCount == 0 &&
//                    calvingCount == 0 &&
//                    exitRecordCount == 0) {
//                  confirmationDelete(animal, farmer, position);
//                } else {
//                  confirmationDeleteWithData(farmer);
//                }
//              }
//            });
//          });
//
//          //
//        });
//      }
////      else {
////        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
////          Navigator.of(state.context).pop();
////        });
////      }
//    });
//  }

  void confirmationDeleteWithData(Farmer farmer) {
    DialogUtils.showAlertDialogSingleButton(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "You can't delete this data because it is associated with other data!!!",
        (it) {
//        AppTranslations.of(state.context).text("associated_data_delete"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.of(this.state.context).pop();
            if (farmer != null) {
              getAnimalListByFarmer(farmer);
            } else {
              getAnimalList();
            }

            break;
          }
      }
    }, "OK");
  }

  void confirmationDelete(Animal animal, Farmer farmer, int position) {
    DialogUtils.showAlertDialog(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "Are you sure,you want to delete?", (it) {
//        AppTranslations.of(state.context).text("delete_warning"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
//            Navigator.pop(this.state.context, true);

            Navigator.of(this.state.context).pop();
            if (farmer != null) {
              getAnimalListByFarmer(farmer);
            } else {
              getAnimalList();
            }

            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Logger.v('position ' + position.toString());

            this
                .animalBloc
                .deleteAnimal(this.state.context, animal)
                .listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {
//                Navigator.pop(this.state.context, true);
              state.setState((){
                this.animalList.removeAt(position);
              });
//                this.animalList.removeAt(position);
                Navigator.of(this.state.context).pop();
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(state.context).pop();
                });
              }
            });

            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }

  onEditAnimal(Animal animal) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return AnimalPage(animal: animal, isEdit: true);
    }));
    state.setState(() {
      state.refreshAnimalList();
    });
  }

  onAddAnimal(Farmer farmer) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return AnimalPage(farmer: farmer);
    }));
    state.setState(() {
      state.refreshAnimalList();
    });
  }

  onMoveToAI(Farmer farmer, Animal animal) {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      Logger.v("animalsIds:" + DsonUtils.toJsonString(animal));
//      Logger.v("farmerDetails:"+ DsonUtils.toJsonString(farmer));
      return AIPageList(
        farmer: farmer,
        animal: animal,
      );
    }));
  }

  onMoveToPD(Animal animal) {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return PDPageList(
        animal: animal,
      );
    }));
  }

  onMoveToCalving(Animal animal) {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return CalvingPageList(
        animal: animal,
      );
    }));
  }

  onMoveToExit(Animal animal) {
    Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return ExitRecordPageList(
        animal: animal,
      );
    }));
  }
}
