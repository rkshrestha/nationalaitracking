import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/FarmerPage.dart';
import 'package:nbis/features/home/FarmerPageList.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';

class FarmerPageListLogic {
  FarmerPageListState state;
  FarmerBloc farmerBloc;
  AnimalBloc animalBloc;
  List<Farmer> farmerList = [];
  List<Animal> animalList = [];
  List<Farmer> farmerUnSyncList = [];
  List<Farmer> farmerSyncList = [];
  static Farmer farmer = Farmer();
  static Animal animal = Animal();
  var animalCount;
  String animalBuffaloCount;
  String animalCattleCount;
  String animalGoatCount;
  String animalPigCount;

  FarmerPageListLogic(this.state, this.farmerBloc, this.animalBloc);

  void onPostInit() {
    this.state.setState(() {
      getFarmerList();

      getAnimalList();
    });
//    getUnSyncFarmerList();
//    new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//      getSyncFarmerList();
//    });
  }

  getFarmerList() {
    this.farmerBloc.getFarmerList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("farmer list is " + it.data.toString());
        this.state.setState(() {
          this.farmerList = it.data;
//          getAnimalList(farmer);
//          for (Farmer farmer in farmerList) {
//            getAnimalList(farmer);
//          }
        });

        farmerList.sort((a, b) {
          return a.farmerName
              .toLowerCase()
              .compareTo(b.farmerName.toLowerCase());
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getAnimalList() {
    this.animalBloc.getAnimalList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.animalList = it.data;
          for (Animal animal in animalList) {

          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncFarmerList() {
    this.farmerBloc.getUnSyncFarmerList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("farmer list is " + it.data.toString());
        this.state.setState(() {
          this.farmerUnSyncList = it.data;
        });

        farmerList.sort((a, b) {
          return a.farmerName
              .toLowerCase()
              .compareTo(b.farmerName.toLowerCase());
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

//  getSyncFarmerList() {
//    this.farmerBloc.getSyncFarmerList().listen((it) {
//      if (it == null) return;
//      Logger.v("inside listener it");
//      if (it.status == Status.SUCCESS) {
//        Logger.v("farmer list is " + it.data.toString());
//        this.state.setState(() {
//          this.farmerSyncList = it.data;
//        });
//
//        farmerList.sort((a, b) {
//          return a.farmerName
//              .toLowerCase()
//              .compareTo(b.farmerName.toLowerCase());
//        });
//      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(state.context).pop();
//        });
//      }
//    });
//  }

  onEditFarmer(Farmer farmer, int position) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return FarmerPage(farmer: farmer, isEdit: true);
    }));
    state.setState(() {
      state.refreshFarmerList();
    });
  }

  onAddFarmer() async {
    final result = await Navigator.push(state.context,
        MaterialPageRoute(builder: (context) {
      return FarmerPage();
    }));

    state.setState(() {
      state.refreshFarmerList();
    });
  }

//
  confirmationForDeletion(Farmer farmer, int position) {
//    this.animalBloc.getAnimalList().listen((it) {
//      if (it == null) return;
//      Logger.v("inside  it");
//      if (it.status == Status.SUCCESS) {
//        Logger.v("Animal list is " + it.data.toString());

//
//        this.animalList = it.data;
        var count =
            animalList.where((c) => c.mfarmerId == farmer.fId).toList().length;

        Logger.v("Count  " + count.toString());

        if (count == 0) {
          confirmationDelete(farmer, position);
        } else {
          confirmationDeleteWithData();
        }
//      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(state.context).pop();
//        });
//      }
//    });
  }

  void confirmationDelete(Farmer farmers, int position) {
    DialogUtils.showAlertDialog(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "Are you sure,you want to delete?", (it) {
//        AppTranslations.of(state.context).text("delete_warning"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.pop(this.state.context, true);

              getFarmerList();

//
//            state.setState(() {
////              state.refreshFarmerList();
////              getFarmerList();
//            });
            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Logger.v('position farmer' + position.toString());
            this.farmerBloc.deleteFarmer(state.context, farmers).listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {
//                if (farmerList.contains(farmers)) {
//                  state.setState(() {
//                    this.farmerList.removeAt(farmers.fId);
//                  });
//                }

                Navigator.pop(this.state.context, true);
                state.setState(() {});
//              state.refreshFarmerList();
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(state.context).pop();
                });
              }
            });

            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
//    .then((_)=>state.setState((){
//      state.refreshFarmerList();
//    }));
  }

  void confirmationDeleteWithData() {
    DialogUtils.showAlertDialogSingleButton(
            this.state.context,
            "Alert",
//        AppTranslations.of(state.context).text("alert"),
            "You can't delete this data because it is associated with other data!!!",
            (it) {
//        AppTranslations.of(state.context).text("associated_data_delete"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.pop(this.state.context, true);
            getFarmerList();
            break;
          }
      }
    }, "OK");
  }
}
