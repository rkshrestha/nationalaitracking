//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/UserBloc/UserBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/nbisBloc/NbisBloc.dart';
import 'package:nbis/features/home/LoginPageLogic.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/ExitRecordRepo.dart';
import 'package:nbis/features/repo/LoginRequest.dart';
import 'package:nbis/features/repo/NbisRepo.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';
import 'package:nbis/features/repo/UserRepo.dart';

//import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  UserBloc loginRequestBloc;
  LoginPageLogic logic;
  AnimalBloc animalBloc;
  NbisBloc nbisBloc;
  ExitRecordBloc exitRecordBloc;
  LoginRequest loginRequest;
  GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();
  bool obscureText = true;
  String fcmToken = "";
  bool rememberMe = false;
  var ttechId;
  TextEditingController techController = TextEditingController();

  //fcm token
//  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
  final List<NotificationRequest> messages = [];

  //

  String appName;
  String packageName;
  String version;
  String buildNumber;

  package() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    appName = packageInfo.appName;
    packageName = packageInfo.packageName;
    version = packageInfo.version;
    buildNumber = packageInfo.buildNumber;
  }

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  void initState() {
    super.initState();
    loginRequestBloc = UserBloc(UserRepo(this.context));
    animalBloc = AnimalBloc(AnimalRepo(this.context));
    nbisBloc = NbisBloc(NbisRepo(this.context));
    exitRecordBloc = ExitRecordBloc(ExitRecordRepo(this.context));
    logic = LoginPageLogic(
        this, loginRequestBloc, animalBloc, nbisBloc, exitRecordBloc);
//    logic.onPostInit();
    //fcm token

    technician();
    package();
//    firebaseMessaging.configure(
//      onMessage: (Map<String, dynamic> message) {
//        print('on message $message');
//        final notification = message['notification'];
//        setState(() {
//          messages.add(NotificationRequest.make(
//              title: notification['title'], body: notification['body']));
//        });
//      },
//      onResume: (Map<String, dynamic> message) {
//        print('on resume $message');
//        final notification = message['data'];
//        setState(() {
//          messages.add(NotificationRequest.make(
//              title: notification['title'], body: notification['body']));
//        });
//      },
//      onLaunch: (Map<String, dynamic> message) {
//        print('on launch $message');
//
//        final notification = message['data'];
//        setState(() {
//          messages.add(NotificationRequest.make(
//              title: notification['title'], body: notification['body']));
//        });
//      },
//    );
//    firebaseMessaging.requestNotificationPermissions(
//        const IosNotificationSettings(sound: true, badge: true, alert: true));
//    firebaseMessaging.getToken().then((token) {
//      print(token);
//      fcmToken = token;
//    });
  }

  technician() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      ttechId = prefs.getString('technicianId');
      techController.text = ttechId == null ? "" : ttechId;
    });
//    Logger.v("TechId:" + ttechId);
  }

  @override
  void dispose() {
    super.dispose();
//    logic.timers.cancel();
  }

  Widget loginForm() {
    loginRequest = LoginRequest();
//    loginRequest.fcmToken = fcmToken;

//    TextFormField tfUserName = TextFormField(
//        keyboardType: TextInputType.number,
//        maxLength: 10,
////        initialValue: "8888888888",
//        decoration: InputDecoration(
//            icon: Icon(Icons.person_pin),
//            labelText: "Username",
//            hintText: "Mobile Number"),
//        onSaved: (String mblNumber) {
//          loginRequest.mobileno = mblNumber;
//        },
//        validator: (mblNumber) {
//          if (mblNumber.isEmpty) {
//            return "UserName can't be empty";
//          } else if (mblNumber.length != 10) {
//            return "Username must be ten digits";
//          }
//        });

//    print("getTech:"+ttechId);
    TextFormField tfTechnician = TextFormField(
        controller: techController,
//        initialValue: ttechId == null ? "" : ttechId,
        keyboardType: TextInputType.text,
        maxLength: 5,
        decoration: InputDecoration(
          icon: Icon(Icons.person_pin),
          labelText: "Technician Id/ Username",
//            hintText: "Technician Id"
        ),
        onSaved: (String techId) {
          loginRequest.technicianId = techId;
        },
        validator: (mblNumber) {
          if (mblNumber.isEmpty) {
            return "UserName can't be empty";
          } else if (mblNumber.length != 5) {
            return "Username must be five digits";
          }
        });

    TextFormField tfPassword = TextFormField(
        keyboardType: TextInputType.text,
//        initialValue: "admin",
        decoration: InputDecoration(
            icon: Icon(Icons.lock),
            labelText: "Password",
            suffixIcon: new GestureDetector(
              onTap: () {
                setState(() {
                  obscureText = !obscureText;
                });
              },
              child:
                  Icon(obscureText ? Icons.visibility_off : Icons.visibility),
            )),
        obscureText: obscureText,
        onSaved: (String password) {
          loginRequest.password = password;
        },
        validator: (password) {
          if (password.isEmpty) {
            return "Password can't be empty";
          }
        });

    //Raised login Button
    RaisedButton rbLogin = RaisedButton(
      child: Text(
        'Log In',
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (loginFormKey.currentState.validate()) {
          loginFormKey.currentState.save();
//          this.logic.onPostCreate();
          this.logic.sendLoginRequest(loginRequest);
//               Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>HomePage()));
        }
      },
    );

//    var rememberMeLogin =
//        Checkbox(value: rememberMe, onChanged: logic.onRememberMeChanged);

    //astrick
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: loginFormKey,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: tfTechnician,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
//            ListTile(
//              title: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 7,
//                    child: tfUserName,
//                  ),
//                  Expanded(
//                    flex: 1,
//                    child: Text(""),
////                    child: astrickText,
//                  ),
//                ],
//              ),
//            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: tfPassword,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),

//                    child: astrickText,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 5.0, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(""),
//                      rememberMeLogin,
//                      Text(
//                        "Remember Me",
//                        style: TextStyle(
//                          fontSize: 14.0,
//                        ),
//                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 40.0, 0),
                    child: GestureDetector(
                      child: Text(
                        "Forgot Password?",
                        textAlign: TextAlign.end,
                        style: TextStyle(color: Colors.blue, fontSize: 14.0),
                      ),
                      onTap: () {
                        forgotPasswordDialog();
                      },
                    ),
                  ),
                ],
              ),
            ),
//            ListTile(
//              title: Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: <Widget>[
//                  Row(
//                    children: <Widget>[
//                      Text(""),
////                      rememberMeLogin,
////                      Text(
////                        "Remember Me",
////                        style: TextStyle(
////                          fontSize: 14.0,
////                        ),
////                      ),
//                    ],
//                  ),
//                  Container(
//                    margin: EdgeInsets.fromLTRB(0, 0, 40.0, 0),
//                    child: GestureDetector(
//                      child: Text(
//                        "Forgot Password?",
//                        textAlign: TextAlign.end,
//                        style: TextStyle(color: Colors.blue, fontSize: 14.0),
//                      ),
//                      onTap: () {
//                        forgotPasswordDialog();
//                      },
//                    ),
//                  ),
//                ],
//              ),
//            ),

            ListTile(
              title: Row(
//                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Text(''),
                  ),
                  Expanded(
                    flex: 6,
                    child: rbLogin,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(''),
                  ),
                ],
              ),
            ),
            Container(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("App Version: " + version.toString()),
                  ]),
            ),
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        child: SafeArea(
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        height: 119.0,
//                        margin: EdgeInsets.fromLTRB(0, 15.0, 0, 0),
                        child: Image.asset('images/mdpi/govLogo_big.png'),
                      ),
//                      Container(
//
//                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 11.0, 0, 0),
                        child: Text(
                          "National Livestock Breeding Information System",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
//                      Container(
//                        child: Text(
//                          "App Version: " + version.toString(),
////                              + buildNumber.toString(),
////                          style: TextStyle(fontWeight: FontWeight.bold),
//                        ),
//                      ),
                    ],
                  ),
                ),

//                Expanded(
//                  flex: 5,
//                  child: Stack(
//                    alignment: Alignment.topCenter,
//                    children: <Widget>[
//                      Card(
//                        elevation: 4.0,
////                                 color: Color.fromARGB(0, 0, 0, 0),
//                        shape: RoundedRectangleBorder(
//                            borderRadius: BorderRadius.circular(10)),
//
//                        child: NotificationListener<ScrollNotification>(
//                          onNotification: (scrollNotification) {
//                            //   Log.v("scrolled");
//                          },
//                          child: SingleChildScrollView(
//                            //     child: Card(
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.center,
//                              children: <Widget>[
//                                loginForm(),
////                                Text(""),
////                                GestureDetector(
////                                  child: Text(
////                                    "Forgot Password?",
////                                    textAlign: TextAlign.end,
////                                    style: TextStyle(color: Colors.blue),
////                                  ),
////                                  onTap: () {
////                                    forgotPasswordDialog();
////                                  },
////                                ),
//                                Text(""),
//                              ],
//                            ),
//                            //  ),
//
//                            //
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),

                Expanded(
                  flex: 5,
                  child: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      //   Log.v("scrolled");
                    },
                    child: SingleChildScrollView(
                      //     child: Card(
                      child: loginForm(),
                      //  ),

                      //
                    ),
                  ),

                  //
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  forgotPasswordDialog() {
    TextEditingController technicianController = TextEditingController();

    GlobalKey<FormState> resetFormKey = GlobalKey<FormState>();
    TextFormField tfOldPassword = TextFormField(
        maxLength: 5,
        controller: technicianController,
        decoration: InputDecoration(
          labelText: "TechnicianId/ Username",
//          labelText:  AppTranslations.of(context).text("old_password"),

          //  icon: Icon(Icons.confirmation_number),
        ),
        onSaved: (String technicianId) {
//          change.password = old;
        },
        validator: (technicianId) {
          if (technicianId.isEmpty) {
            return "Technician Id/Username is required!!";
          } else if (technicianId.length != 5) {
            return "Technician Id must be five digits!!";
          }
        });

    MaterialButton rbSave = MaterialButton(
      child: Text(
        "Send",
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (resetFormKey.currentState.validate()) {
          resetFormKey.currentState.save();
          this.logic.sendForgotPasswordRequest(technicianController.text);
        }
      },
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Forgot Password",
//              AppTranslations.of(context).text("change_password"),
              textAlign: TextAlign.center,
            ),
            content: SafeArea(
              child: BlocProvider(
                child: Container(
                  height: 130.0,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: NotificationListener<ScrollNotification>(
                          onNotification: (scrollNotification) {
                            //   Log.v("scrolled");
                          },
                          child: SingleChildScrollView(
                            child: Form(
                              key: resetFormKey,
                              child: Container(
                                child: Column(
                                  children: <Widget>[
                                    tfOldPassword,
                                    Container(
                                      height: 1.0,
                                    ),
                                    rbSave,
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),

                        //
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
