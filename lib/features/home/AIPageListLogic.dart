import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/AIPage.dart';
import 'package:nbis/features/home/AIPageList.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class AIPageListLogic {
  AIPageListState state;
  AIBloc aiBloc;
  FarmerBloc farmerBloc;
  AnimalBloc animalBloc;
  int aiIds;
  String farmerName;
  List<ArtificialInsemination> aiList = [];
  List<ArtificialInsemination> aiUnSyncList = [];
  List<ArtificialInsemination> aiSyncList = [];
  List<Animal> animalList = [];
  Animal animals = Animal();
  String speciesCode;

  Farmer farmer = Farmer();
  static ArtificialInsemination ai = ArtificialInsemination();

  AIPageListLogic(this.state, this.aiBloc, this.animalBloc, this.farmerBloc);

  void onPostInit(Animal animal) {
    if (animal != null) {
//      getUnSyncAIListByAnimal(animal);
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//        getSyncAIListByAnimal(animal);
      Logger.v("test:" + animal.aId.toString());
      getAIListByAnimal(animal);

//      });

    } else {
//      getUnSyncAIList();
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//        getSyncAIList();
      getAIList();

//      });
    }
  }

  getAIListByAnimal(animal) {
    Logger.v("animals" + animal.aId.toString());
    this.aiBloc.getAIListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        if (state.mounted) {
          this.state.setState(() {
            this.aiList = it.data;
          });
        }
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncAIListByAnimal(animal) {
    this.aiBloc.getUnSyncAIListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.aiUnSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncAIListByAnimal(animal) {
    this.aiBloc.getSyncAIListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.aiSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  String getSpeciesCode(String aiIds) {
    Logger.v("aID getSpeciesCode" + aiIds.toString());

    this.animalBloc.getAnimalByaId(aiIds).listen((it) async {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        animals = it.data;
//          speciesCode = animals.speciesCode;
        Logger.v("SpeciesCOde:" + animals.speciesCode);
        speciesCode = animals.speciesCode;
        Logger.v("SpeciesCOde:" + speciesCode);

//            return  speciesCode;
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
    return "$speciesCode";
  }

  getAIList() {
    this.aiBloc.getAIList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("ai list is " + it.data.toString());

        this.state.setState(() {
          this.aiList = it.data;

          Logger.v("getIds:");
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncAIList() {
    this.aiBloc.getUnSyncAIList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("ai list is " + it.data.toString());

        this.state.setState(() {
          this.aiUnSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncAIList() {
    this.aiBloc.getSyncAIList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("ai list is " + it.data.toString());

        this.state.setState(() {
          this.aiSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  onEditAI(ArtificialInsemination ai, int position) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return AIPage(ai: ai, isEdit: true);
    }));
    state.setState(() {
      state.refreshAIList();
    });
  }

  onAddAI(Farmer farmer, Animal animal) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      Logger.v("animalsdetails:" + DsonUtils.toJsonString(animal));
      return AIPage(farmer: farmer, animal: animal);
    }));
    state.setState(() {
      state.refreshAIList();
    });
  }

  void confirmationDelete(
      ArtificialInsemination ai, Animal animal, int position) {
    DialogUtils.showAlertDialog(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "Are you sure,you want to delete?", (it) {
//        AppTranslations.of(state.context).text("delete_warning"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.pop(this.state.context, true);

            if (animal != null) {
              getAIListByAnimal(animal);
            } else {
              getAIList();
            }

            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Logger.v('position ' + position.toString());

            this.aiBloc.deleteAI(this.state.context, ai).listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {
//                state.setState(() {
//                  ai.syncToServer == true
//                      ? this.aiSyncList.removeAt(position)
//                      : this.aiUnSyncList.removeAt(position);
//                });

//                state.setState(() {
//                  this.aiList.removeAt(position);
//                });

                Navigator.pop(this.state.context, true);
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(state.context).pop();
                });
              }
            });

            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }

  getFarmerByMobileNumber(String mblNumber) async {
    this
        .farmerBloc
        .getFarmerByMobileNumber(this.state.context, mblNumber)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        farmer = it.data;

        farmerName = it.data.farmerName;
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        farmerName = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }
}
