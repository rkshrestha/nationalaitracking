import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/semenBloc/SemenBloc.dart';
import 'package:nbis/features/home/SemenPageLogic.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/repo/SemenRepo.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_utils/nepali_utils.dart';

class SemenPage extends StatefulWidget {
  bool isEdit;
  Semen semen;

  SemenPage({this.semen, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return SemenPageState(semen: this.semen, isEdit: this.isEdit);
  }
}

class SemenPageState extends State<SemenPage> {
  bool isEdit;
  Semen semen;
  SemenBloc semenBloc;
  SemenPageLogic logic;

  AppBar appBar;
  TextFormField tfSemenDate;
  NepaliDateTime selectedDateTime;
  Language language = Language.ENGLISH;
  TextEditingController dateSemenController = TextEditingController(text: " ");
  GlobalKey<FormState> semenFormKey = GlobalKey<FormState>();

  SemenPageState({this.semen, this.isEdit = false});

  @override
  void initState() {
    super.initState();
    this.semenBloc = SemenBloc(SemenRepo(context));
    this.logic = SemenPageLogic(this, semenBloc);

    logic.onPostState();
  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  Widget semenForm() {
    Semen semen = this.logic.semen;

    tfSemenDate = TextFormField(
        controller: dateSemenController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          //  icon: Icon(Icons.date_range),
          labelText: "Date of Semen Received",
//          labelText: AppTranslations.of(context).text("semen_form_date"),
//          hintText: "Select date",
        ),
        onSaved: (date) {
          semen.receivedDate = date;
        },
        validator: (date) {
          if (date.isEmpty) {
//            return AppTranslations.of(context).text("semenDate_validate");

            return "Semen received date can't be empty";
          }
        });

//    TextFormField tfBreedJ = TextFormField(
//        keyboardType: TextInputType.text,
//        initialValue: "J",
//        decoration: InputDecoration(
//          //   icon: Icon(Icons.date_range),
//          labelText: "J Breed",
//        ),
//        onSaved: (String date) {},
//        validator: (date) {
//          if (date.isEmpty) {
//            return "J Breed can't be empty";
//          }
//        });
//
//    TextFormField tfBreedHF = TextFormField(
//        initialValue: "HF",
//        keyboardType: TextInputType.text,
//        decoration: InputDecoration(
//          //    icon: Icon(Icons.date_range),
//          labelText: "HF Breed",
//        ),
//        onSaved: (String date) {},
//        validator: (date) {
//          if (date.isEmpty) {
//            return "HF Breed can't be empty";
//          }
//        });
//
//    TextFormField tfBreedM = TextFormField(
//        initialValue: "M",
//        keyboardType: TextInputType.text,
//        decoration: InputDecoration(
//          //   icon: Icon(Icons.date_range),
//          labelText: "M Breed",
//        ),
//        onSaved: (String date) {},
//        validator: (date) {
//          if (date.isEmpty) {
//            return "M Breed can't be empty";
//          }
//        });

    TextFormField tfBreedJDose = TextFormField(
        autofocus: true,
        maxLength: 3,
        keyboardType: TextInputType.number,
        initialValue:
            semen.jersey == null ? "" : semen.jersey.toString().split(".")[0],
        decoration: InputDecoration(
          //    icon: Icon(Icons.date_range),
          labelText: "Jersey Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_jersey"),
//            hintText: "Enter 0 if Breed Jersey is not taken"
        ),
        onSaved: (jersey) {
          semen.jersey = double.tryParse(jersey);
        },
        validator: (jersey) {
          if (jersey.isEmpty) {
//            return AppTranslations.of(context).text("jersey_validate");
            return "Jersey Breed Semen Dose can't be empty";
          }
        });

    TextFormField tfBreedHFDose = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 3,
        initialValue: semen.holsteinFriesian == null
            ? ""
            : semen.holsteinFriesian.toString().split(".")[0],
        decoration: InputDecoration(
          //     icon: Icon(Icons.date_range),
          labelText: "Holstein Friesian Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_hf"),
//            hintText: "Enter 0 if Breed Holstein Friesian Dose is not taken"
        ),
        onSaved: (hf) {
          semen.holsteinFriesian = double.tryParse(hf);
        },
        validator: (hf) {
          if (hf.isEmpty) {
//            return AppTranslations.of(context).text("hf_validate");

            return "Holstein Friesian Breed Dose can't be empty";
          }
        });

    TextFormField tfBreedMDose = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 3,
        initialValue:
            semen.murrah == null ? "" : semen.murrah.toString().split(".")[0],
        decoration: InputDecoration(
          //    icon: Icon(Icons.date_range),
          labelText: "Murrah Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_murrah"),
//            hintText: "Enter 0 if Breed Murrah Dose is not taken"
        ),
        onSaved: (murrah) {
          semen.murrah = double.tryParse(murrah);
        },
        validator: (murrah) {
          if (murrah.isEmpty) {
//            return AppTranslations.of(context).text("murrah_validate");
            return "Murrah Breed Dose can't be empty";
          }
        });

    //tfboar
    TextFormField tfBreedBoarDose = TextFormField(
        maxLength: 3,
        keyboardType: TextInputType.number,
        initialValue:
            semen.boar == null ? "" : semen.boar.toString().split(".")[0],
        decoration: InputDecoration(
          //     icon: Icon(Icons.date_range),
          labelText: "Boar Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_boar"),
//            hintText: "Enter 0 if Breed Holstein Friesian Dose is not taken"
        ),
        onSaved: (boar) {
          semen.boar = double.tryParse(boar);
        },
        validator: (boar) {
          if (boar.isEmpty) {
//            return AppTranslations.of(context).text("boar_validate");

            return "Boar Breed Dose can't be empty";
          }
        });

    TextFormField tfBreedSaanenDose = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 3,
        initialValue:
            semen.saanen == null ? "" : semen.saanen.toString().split(".")[0],
        decoration: InputDecoration(
          //     icon: Icon(Icons.date_range),
          labelText: "Saanen Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_saanen"),
//            hintText: "Enter 0 if Breed Holstein Friesian Dose is not taken"
        ),
        onSaved: (saanen) {
          semen.saanen = double.tryParse(saanen);
        },
        validator: (saanen) {
          if (saanen.isEmpty) {
//            return AppTranslations.of(context).text("saanen_validate");

            return "Saanen Breed Dose can't be empty";
          }
        });

    TextFormField tfBreedYorkshireDose = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 3,
        initialValue: semen.yorkshire == null
            ? ""
            : semen.yorkshire.toString().split(".")[0],
        decoration: InputDecoration(
          //     icon: Icon(Icons.date_range),
          labelText: "Yorkshire Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_yorkshire"),
//            hintText: "Enter 0 if Breed Holstein Friesian Dose is not taken"
        ),
        onSaved: (yorkshire) {
          semen.yorkshire = double.tryParse(yorkshire);
        },
        validator: (yorkshire) {
          if (yorkshire.isEmpty) {
//            return AppTranslations.of(context).text("yorkshire_validate");

            return "Yorkshire Breed Dose can't be empty";
          }
        });

    TextFormField tfBreedlandraceDose = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 3,
        initialValue: semen.landrace == null
            ? ""
            : semen.landrace.toString().split(".")[0],
        decoration: InputDecoration(
          //     icon: Icon(Icons.date_range),
          labelText: "Landrace Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_landrace"),
//            hintText: "Enter 0 if Breed Holstein Friesian Dose is not taken"
        ),
        onSaved: (landrace) {
          semen.landrace = double.tryParse(landrace);
        },
        validator: (landrace) {
          if (landrace.isEmpty) {
//            return AppTranslations.of(context).text("landrace_validate");

            return "Landrace Breed Dose can't be empty";
          }
        });

    TextFormField tfBreedDurocDose = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 3,
        initialValue:
            semen.duroc == null ? "" : semen.duroc.toString().split(".")[0],
        decoration: InputDecoration(
          //     icon: Icon(Icons.date_range),
          labelText: "Duroc Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_duroc"),
//            hintText: "Enter 0 if Breed Holstein Friesian Dose is not taken"
        ),
        onSaved: (duroc) {
          semen.duroc = double.tryParse(duroc);
        },
        validator: (duroc) {
          if (duroc.isEmpty) {
//            return AppTranslations.of(context).text("duroc_validate");

            return "Duroc Breed Dose can't be empty";
          }
        });

    TextFormField tfBreedHampshireDose = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 3,
        initialValue: semen.hampshire == null
            ? ""
            : semen.hampshire.toString().split(".")[0],
        decoration: InputDecoration(
          //     icon: Icon(Icons.date_range),
          labelText: "Hampshire Breed Dose",
//          labelText: AppTranslations.of(context).text("semen_form_hampshire"),
//            hintText: "Enter 0 if Breed Holstein Friesian Dose is not taken"
        ),
        onSaved: (hampshire) {
          semen.hampshire = double.tryParse(hampshire);
        },
        validator: (hampshire) {
          if (hampshire.isEmpty) {
//            return AppTranslations.of(context).text("hampshire_validate");

            return "Hampshire Breed Dose can't be empty";
          }
        });

    //Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text(
        "Save",
//        AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (semenFormKey.currentState.validate()) {
          semenFormKey.currentState.save();
          setState(() {
            this.logic.saveSemen();
          });
        }
      },
    );

    //Raised Update Button
    RaisedButton rbUpdate = RaisedButton(
      child: Text(
       "Update",
//        AppTranslations.of(context).text("form_update"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (semenFormKey.currentState.validate()) {
          semenFormKey.currentState.save();
          setState(() {
            this.logic.updateSemen();
          });
        }
      },
    );

    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: semenFormKey,
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(15.0, 8.0, 0, 0),
                child: Text(
                  "Cattle Semen: ",
//                  AppTranslations.of(context).text("semen_form_cattle_semen"),
                  style: TextStyle(fontSize: 16.0),
                ),
              ),

              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedJDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedHFDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

              Padding(
                padding: EdgeInsets.fromLTRB(15.0, 8.0, 0, 0),
                child: Text(
                  "Buffalo Semen: ",
//                  AppTranslations.of(context).text("semen_form_buffalo_semen"),
                  style: TextStyle(fontSize: 16.0),
                ),
              ),
              //buffalo
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedMDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

              Padding(
                padding: EdgeInsets.fromLTRB(15.0, 8.0, 0, 0),
                child: Text(
                  "Goat Semen: ",
//                  AppTranslations.of(context).text("semen_form_goat_semen"),
                  style: TextStyle(fontSize: 16.0),
                ),
              ),
              //goat
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedBoarDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

              //goat
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedSaanenDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

              Padding(
                padding: EdgeInsets.fromLTRB(15.0, 8.0, 0, 0),
                child: Text(
                  "Pig Semen: ",
//                  AppTranslations.of(context).text("semen_form_pig_semen"),
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
              ),
              //pig
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedYorkshireDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

//pig
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedlandraceDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

              //pig
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedDurocDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),
//pig
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreedHampshireDose,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: InkWell(
                        onTap: () {
                          this.logic.showDatePickers();
                        },
                        child: IgnorePointer(
                          child: tfSemenDate,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

//        Row(
//          children: <Widget>[
//            Expanded(
//              flex: 5,
//              child: ListTile(
//                title: Row(
//                  children: <Widget>[
//                    Expanded(
//                      flex: 4,
//                      child: tfBreedJ,
//                    ),
//                    Expanded(
//                      flex: 1,
//                      child: astrickText,
//                    ),
//                  ],
//                ),
//              ),
//            ),
//            Expanded(
//              flex: 5,
//              child: ListTile(
//                title: Row(
//                  children: <Widget>[
//                    Expanded(
//                      flex: 4,
//                      child: tfBreedJDose,
//                    ),
//                    Expanded(
//                      flex: 1,
//                      child: astrickText,
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ],
//        ),
//        Row(
//          children: <Widget>[
//            Expanded(
//              flex: 5,
//              child: ListTile(
//                title: Row(
//                  children: <Widget>[
//                    Expanded(
//                      flex: 4,
//                      child: tfBreedHF,
//                    ),
//                    Expanded(
//                      flex: 1,
//                      child: astrickText,
//                    ),
//                  ],
//                ),
//              ),
//            ),
//            Expanded(
//              flex: 5,
//              child: ListTile(
//                title: Row(
//                  children: <Widget>[
//                    Expanded(
//                      flex: 4,
//                      child: tfBreedHFDose,
//                    ),
//                    Expanded(
//                      flex: 1,
//                      child: astrickText,
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ],
//        ),
//        Row(
//          children: <Widget>[
//            Expanded(
//              flex: 5,
//              child: ListTile(
//                title: Row(
//                  children: <Widget>[
//                    Expanded(
//                      flex: 4,
//                      child: tfBreedM,
//                    ),
//                    Expanded(
//                      flex: 1,
//                      child: astrickText,
//                    ),
//                  ],
//                ),
//              ),
//            ),
//            Expanded(
//              flex: 5,
//              child: ListTile(
//                title: Row(
//                  children: <Widget>[
//                    Expanded(
//                      flex: 4,
//                      child: tfBreedMDose,
//                    ),
//                    Expanded(
//                      flex: 1,
//                      child: astrickText,
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ],
//        ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: Visibility(
                        visible: !this.isEdit,
                        child: rbSave,
                        replacement: rbUpdate,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(''),
                    ),
                  ],
                ),
              ),
            ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: this.appBar,
      body: SafeArea(
        child: BlocProvider(
          //    child: Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: NotificationListener<ScrollNotification>(
                  onNotification: (scrollNotification) {
                    //   Log.v("scrolled");
                  },
                  child: SingleChildScrollView(
                    child: semenForm(),
                  ),
                ),

                //
              ),
            ],
          ),
          //  ),
        ),
      ),
    );
  }
}
