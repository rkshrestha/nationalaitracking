import 'package:nbis/features/bloc/UserBloc/ChangePasswordBloc.dart';
import 'package:nbis/features/home/ProfilePage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/ChangePasswordModel.dart';
//import 'package:nbis/features/sharedPreferences/Session.dart';
//import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ProgressBar.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class ProfilePageLogic {
  ProfilePageState state;
  ChangePasswordBloc changePasswordBloc;

  ProfilePageLogic(this.state, this.changePasswordBloc);

  ProgressBar progressBar = ProgressBar();

  sendChangePasswordRequest(ChangePasswordModel changeRequest) async {
    progressBar.show(this.state.context);
    this
        .changePasswordBloc
        .sendChangePasswordRequest(changeRequest)
        .listen((it) {
      if (it == null) return;
      Logger.v("inside logic sendChangeRequest");

      if (it.status == Status.SUCCESS) {
        progressBar.dismissToLogin(this.state.context);
        this.state.showToast("Password Changed Successfully.");
//        this.state.showToast( AppTranslations.of(state.context).text("password_changed_success"),);

      } else if (it.status == Status.TOKEN_EXPIRED) {
      }
//      else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast("No Internet Connection");
//      }
      else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          state.setState(() {
            progressBar.dismiss(this.state.context);
            this.state.showToast(it.errorMessage);
          });
        });
      }
    });
  }
}
