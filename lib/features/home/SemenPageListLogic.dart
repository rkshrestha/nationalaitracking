import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/semenBloc/SemenBloc.dart';
import 'package:nbis/features/home/SemenPage.dart';
import 'package:nbis/features/home/SemenPageList.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class SemenPageListLogic {
  SemenPageListState state;
  SemenBloc semenBloc;
  List<Semen> semenList = [];
  List<Semen> semenSyncList = [];
  List<Semen> semenUnSyncList = [];
  static Semen semen = Semen();
  int count = 0;

  SemenPageListLogic(this.state, this.semenBloc);

  void onPostInit() {
//    getSemenUnSyncList();

   getSemenList();
  }

  getSemenList() {
    this.semenBloc.getSemenList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("semen list is " + it.data.toString());
        this.state.setState(() {
          this.semenList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSemenUnSyncList() {
    this.semenBloc.getSemenUnSyncList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("semen list is " + it.data.toString());
        this.state.setState(() {
          this.semenUnSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //synced data
//  getSemenSyncList() {
//    this.semenBloc.getSemenSyncList().listen((it) {
//      if (it == null) return;
//      if (it.status == Status.SUCCESS) {
//        Logger.v("semens list is " + it.data.toString());
//        this.state.setState(() {
//          Logger.v(semen.syncToServer.toString());
//          this.semenSyncList = it.data;
////          }
//        });
//      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(state.context).pop();
//        });
//      }
//    });
//  }

  onEditSemen(Semen semen) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return SemenPage(semen: semen, isEdit: true);
    }));
    state.setState(() {
      state.refreshSemenList();
    });
  }

  onAddSemen()async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return SemenPage();
    }));
    state.setState(() {
      state.refreshSemenList();
    });
  }

  void confirmationDelete(Semen semens, int position) {
    DialogUtils.showAlertDialog(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "Are you sure,you want to delete?", (it) {
//        AppTranslations.of(state.context).text("delete_warning"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');

            Navigator.pop(this.state.context, true);
            getSemenList();
            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Logger.v('position semen' + position.toString());

            this.semenBloc.deleteSemen(this.state.context, semens).listen((it) {
              if (it == null) return;

              if (it.status == Status.SUCCESS) {
//                state.setState(() {
//                  semens.syncToServer == true
//                      ? this.semenSyncList.removeAt(position)
//                      : this.semenUnSyncList.removeAt(position);
//                });

//                state.setState(() {
//                  if(semenList.contains(semens)){
//                    this.semenList.removeAt(position);
//
//                  }
//                });

                Navigator.pop(this.state.context, true);
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(state.context).pop();
                });
              }
            });

            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }
}
