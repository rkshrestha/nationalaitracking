import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/pdBloc/PDBloc.dart';
import 'package:nbis/features/home/PDPageListLogic.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/PDRepo.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class PDPageList extends StatefulWidget {
  Animal animal;

  PDPageList({this.animal});

  @override
  State<StatefulWidget> createState() {
    return PDPageListState(animal: animal);
  }
}

class PDPageListState extends State<PDPageList> {
  PDPageListLogic logic;
  PDBloc pdBloc;
  PregnancyDiagnosis pd;
  Animal animal;
  var refreshPDListKey = GlobalKey<RefreshIndicatorState>();

  PDPageListState({this.animal});

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  void initState() {
    super.initState();
    pdBloc = PDBloc(PDRepo(context));
    logic = PDPageListLogic(this, pdBloc);
//    logic.onPostInit(animal);
    refreshPDList();
  }

  Future<Null> refreshPDList() async {
    refreshPDListKey.currentState?.show(atTop: false);
//    await Future.delayed(Duration(seconds: 1));

    setState(() {
      this.logic.onPostInit(animal);
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    this.logic.pdList.sort((a, b) => b.pdDate.compareTo(a.pdDate));

    final tabSync = <Tab>[
      Tab(text: "Not Sync", icon: new Icon(Icons.sync)),
      Tab(text: " Sync", icon: new Icon(Icons.sync))
    ];

    return DefaultTabController(
      length: tabSync.length,
      child: Scaffold(
        appBar: AppBar(
//          title: Text(
//            AppTranslations.of(context).text("pd_registered_title"),
//          ),
          title: Text("Pregnancy Diagnosis"),
//          bottom: TabBar(tabs: tabSync),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 45.0,
            color: Theme.of(context).primaryColor,
            child: GestureDetector(
              onTap: () {
                logic.onAddPD(animal);
              },

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  Divider(
                    indent: 2.0,
                  ),
                  Text(
//                          AppTranslations.of(context)
//                              .text("pd_register_button"),
                    "Add Pregnancy Diagnosis",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),

                ]),

//
//
//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 3,
//                    child: Text(''),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: Row(
//                      children: <Widget>[
//                        Icon(
//                          Icons.add,
//                          color: Colors.white,
//                        ),
//                        Divider(
//                          indent: 2.0,
//                        ),
//                        Text(
////                          AppTranslations.of(context)
////                              .text("pd_register_button"),
//                          "Add Pregnancy Diagnosis",
//                          style: TextStyle(
//                              color: Colors.white, fontWeight: FontWeight.bold),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Text(''),
//                  ),
//                ],
//              ),
            ),
          ),
        ),

        body: BlocProvider(
          child: SafeArea(
            child: Column(
              children: <Widget>[
//                Expanded(
//                  child: TabBarView(
//                    children: <Widget>[
//                      logic.pdUnSyncList.isEmpty ? noData() : getUnSyncPDList(),
//                      logic.pdSyncList.isEmpty ? noData() : getSyncPDList(),
//                    ],
//                  ),
//                ),
                Expanded(
                  child: RefreshIndicator(
                    key: refreshPDListKey,
                    child: this.logic.pdList.isEmpty ? noData() : getPDList(),
                    onRefresh: refreshPDList,
                  ),
                ),
              ],
            ),
          ),
        ),
//      floatingActionButton: Padding(
//        padding: EdgeInsets.only(bottom: 1.0),
//        child: FloatingActionButton(
//            child: Icon(Icons.add),
//            tooltip: 'Add PD',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              logic.onAddPD();
////              Navigator.push(
////                  context, MaterialPageRoute(builder: (context) => PDPage()));
//            }),
//      ),

//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//      floatingActionButton: Column(
//        crossAxisAlignment: CrossAxisAlignment.stretch,
//        mainAxisSize: MainAxisSize.min,
//        children: <Widget>[
//          FloatingActionButton.extended(
//            isExtended: true,
//            label: Text("Add Pregnancy Diagnosis"),
//            icon: Image.asset("images/hdpi/add.png",color: Colors.white,),
//            tooltip: 'Add PD',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              this.logic.onAddPD();
//            },
//          ),
//        ],
//      ),
      ),
    );
  }

  Widget noData() {
    return Container(
      child: Center(
//        child: Text(
//          AppTranslations.of(context).text("pd_noData"),
//        ),
        child: Text("No Pregnancy Diagnosis Data has been found !!"),
      ),
    );
  }

  ListView getPDList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.pdList.length,
        itemBuilder: (BuildContext context, int position) {
          pd = this.logic.pdList[position];
          String result = pd.pdResult == true ? "Positive" : "Negative";

          editButton() {
            DateTime lastestDate = DateTime.now();

            if (pd.syncDate != null) {
              String pdSyncDate =
                  pd.syncDate == null ? "" : pd.syncDate.split(".").first;
              DateTime syncDate = DateTime.tryParse(pdSyncDate);
              DateTime addedDate = syncDate.add(new Duration(hours: 24));

              if (lastestDate.compareTo(addedDate) == 1) {
//              if (lastestDate.compareTo(addedDate)==1 && pd.syncToServer == true) {
//              if (addedDate.difference(syncDate).inHours >= 24 && pd.syncToServer == true) {

//              if (syncDate.add(new Duration(hours: 24)) == lastestDate && pd.syncToServer == true) {
//              if (lastestDate.difference(syncDate).inHours >= 24 && pd.syncToServer == true) {
                return Container(
                  child: Text(""),
                );
              } else {
                return GestureDetector(
                  child: Container(
                    width: 40.0,
                    height: 40.0,
                    child: Card(
                      color: Colors.white70,
                      elevation: 3.0,
                      child: Image.asset(
                        "images/edits.png",
                      ),
                    ),
                  ),
                  onTap: () {
                    logic.onEditPD(this.logic.pdList[position], position);
                  },
                );

//                return Container(
//                  child: GestureDetector(
//                    child: Icon(
//                      Icons.edit,
//                      size: 30.0,
//                      color: Colors.black,
//                    ),
//                    onTap: () {
//                      logic.onEditPD(this.logic.pdList[position], position);
//                    },
//                  ),
//                );
              }
            } else {
              return GestureDetector(
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: Card(
                    color: Colors.white70,
                    elevation: 3.0,
                    child: Image.asset(
                      "images/edits.png",
                    ),
                  ),
                ),
                onTap: () {
                  logic.onEditPD(this.logic.pdList[position], position);
                },
              );

//              return Container(
//                child:  GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    logic.onEditPD(this.logic.pdList[position], position);
//                  },
//                ),
//              );
            }
          }

          if (pd.syncDate != null) {
//          if(pd.syncToServer ==true){

            return Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                  width: 30.0,
                  height: 30.0,
                  child: pd.syncToServer == true
                      ? Image.asset(
                          "images/pregnancy.png",
//                      "images/xxhdpi/pd.png",
                          color: Colors.green,
                        )
                      : Image.asset(
                          "images/pregnancy.png",
//                      "images/xxhdpi/pd.png",
                          color: Colors.red,
                        ),
                ),
//                  title: Text("PD date: " + pd.pdDate),
//                  subtitle: Text( pd.farmerName ==null?"":"Farmer Name: " +pd.farmerName +
//                      "\n" +"Tag: " +pd.mobileNumber +"-"+
//                      pd.speciesCode +
//                      "\n" +"Report: " +result),

                title: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("PD date:" + pd.pdDate),
                      Text(
                        pd.farmerName == null
                            ? ""
                            : "Farmer Name: " + pd.farmerName,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        "Tag: " + pd.mobileNumber + "-" + pd.speciesCode,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        "Report: " + result,
                        style: TextStyle(fontSize: 14.0),
                      ),
                    ],
                  ),
                ),

                trailing: editButton(),
//                onTap: () {
//                  logic.onEditPD(this.logic.pdList[position], position);
//                },
              ),
            );
          } else {
            return Dismissible(
              key: Key(this.logic.pdList[position].pdId.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (DismissDirection direction) {
                PregnancyDiagnosis pd = this.logic.pdList[position];

                setState(() {
                  this.logic.pdList.removeAt(position);
                  this.logic.confirmationDelete(pd, animal, position);
                });
              },
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
//                leading: Image.asset(
//                  "images/xxhdpi/pd.png",
//                  width: 30.0,
//                  height: 30.0,
//                ),

                  leading: Container(
                    width: 30.0,
                    height: 30.0,
                    child: pd.syncToServer == true
                        ? Image.asset(
                            "images/pregnancy.png",
//                      "images/xxhdpi/pd.png",
                            color: Colors.green,
                          )
                        : Image.asset(
                            "images/pregnancy.png",
//                      "images/xxhdpi/pd.png",
                            color: Colors.red,
                          ),
                  ),
//                  title: Text("PD date:" + pd.pdDate),
//                  subtitle: Text(pd.farmerName ==null?"":"Farmer Name: " +pd.farmerName +
//                      "\n" +"Tag: " +pd.mobileNumber +"-"+
//                      pd.speciesCode +
//                      "\n" +"Report: " +result),
//
                  title: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("PD date:" + pd.pdDate),
                        Text(
                          pd.farmerName == null
                              ? ""
                              : "Farmer Name: " + pd.farmerName,
                          style: TextStyle(fontSize: 14.0),
                        ),
                        Text(
                          "Tag: " + pd.mobileNumber + "-" + pd.speciesCode,
                          style: TextStyle(fontSize: 14.0),
                        ),
                        Text(
                          "Report: " + result,
                          style: TextStyle(fontSize: 14.0),
                        ),
                      ],
                    ),
                  ),

                  trailing: editButton(),
//                onTap: () {
//                  logic.onEditPD(this.logic.pdList[position], position);
//                },
                ),
              ),
            );
          }
        });
  }

  ListView getUnSyncPDList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.pdUnSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          pd = this.logic.pdUnSyncList[position];
          return Dismissible(
            key: Key(this.logic.pdUnSyncList[position].pdId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              PregnancyDiagnosis pd = this.logic.pdUnSyncList[position];
//              this.logic.confirmationDelete(pd, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Image.asset(
                  "images/xxhdpi/pd.png",
                  width: 30.0,
                  height: 30.0,
                ),
//                Container(
//                    width: 40.0,
//                    height: 40.0,
//                    decoration: new BoxDecoration(
//                        color: Colors.white,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.cover,
//                            image: new AssetImage(
//                                "images/xxhdpi/pd.png")))),

                title: Text("PD date:" + pd.pdDate),
//                subtitle: Text(
//                    "Excepted Calving Date: " + pd.expectedDeliveryDate == null
//                        ? ""
//                        : pd.expectedDeliveryDate
//                    +
//                    "\n" +
//                    "AI Result: "
//                    +
//                    logic.getPDData()
//                    ),
//                subtitle: Text(pd.pdResult.toString()),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.edit,
                    size: 30.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    logic.onEditPD(this.logic.pdUnSyncList[position], position);
                  },
                ),
//                onTap: () {
//                  logic.onEditPD(this.logic.pdList[position], position);
//                },
              ),
            ),
          );
        });
  }

  ListView getSyncPDList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.pdSyncList.length,
        itemBuilder: (BuildContext context, int position) {
          pd = this.logic.pdSyncList[position];
          return Dismissible(
            key: Key(this.logic.pdSyncList[position].pdId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              PregnancyDiagnosis pd = this.logic.pdSyncList[position];
//              this.logic.confirmationDelete(pd, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Image.asset(
                  "images/xxhdpi/pd.png",
                  width: 30.0,
                  height: 30.0,
                ),
//                Container(
//                    width: 40.0,
//                    height: 40.0,
//                    decoration: new BoxDecoration(
//                        color: Colors.white,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.cover,
//                            image: new AssetImage(
//                                "images/xxhdpi/pd.png")))),

                title: Text("PD date:" + pd.pdDate),
//                subtitle: Text("Excepted Calving Date: " +
//                    pd.expectedDeliveryDate +
//                    "\n" +
//                    "AI Result: " +
//                    logic.getPDData()),
//                trailing: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    logic.onEditPD(this.logic.pdSyncList[position], position);
//                  },
//                ),
//                onTap: () {
//                  logic.onEditPD(this.logic.pdList[position], position);
//                },
              ),
            ),
          );
        });
  }
}
