import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:nbis/features/bloc/nitrogenBloc/NitrogenBloc.dart';
import 'package:nbis/features/home/NitrogenPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/s/Logger.dart';
//import 'package:nbis/s/SplitDate.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:nepali_utils/nepali_utils.dart';

class NitrogenPageLogic {
  NitrogenPageState state;
  NitrogenBloc nitrogenBloc;
  LiquidNitrogen nitrogen = LiquidNitrogen();

  NitrogenPageLogic(this.state, this.nitrogenBloc);

  void onPostState() {
    if (this.state.isEdit) {
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//              AppTranslations.of(state.context).text("nitrogen_form_edit"),
//            );
//      });
      this.state.setAppBar('Edit Liquid Nitrogen');
      this.nitrogen = state.nitrogen;
      state.dateController.text = this.nitrogen.receivedDate;

      Logger.v('inside ' + this.state.isEdit.toString());
    } else {
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//              AppTranslations.of(state.context)
//                  .text("nitrogen_form_add_nitrogen"),
//            );
//      });
      this.state.setAppBar(
         "Add Liquid Nitrogen"
            );
      DateTime today = new DateTime.now();
//      DateTime todayDate =
//      new DateTime(today.year + 56, today.month + 8, today.day + 17);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
//      state.dateController.text = todayNepaliDate.toString();

      NepaliDateTime currentTime = NepaliDateTime.now();
//      String todayDateNepali =  SplitDate.splitNepali(currentTime);
      String todayDateNepali =  currentTime.toIso8601String().split("T").first;
      Logger.v("dates:"+todayDateNepali);
      state.dateController.text = todayDateNepali;
      Logger.v('inside ' + this.state.isEdit.toString());
    }
  }

  saveNitrogen() async {
    this
        .nitrogenBloc
        .saveNitrogen(this.state.context, this.nitrogen)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        state.setState(() {
//          Navigator.of(this.state.context, rootNavigator: true).pop();
//        });
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(state.context).pop();
//        });
      }
    });
  }

  updateNitrogen() async {
    this
        .nitrogenBloc
        .updateNitrogen(this.state.context, this.nitrogen)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        Navigator.pop(this.state.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  String formattedDate;

//  Future<Null> showDatePickers() async {
//    final DateTime picked = await showDatePicker(
//        context: state.context,
//        initialDate: DateTime.now(),
//        firstDate: DateTime(2010),
//        lastDate: DateTime(2100));
//
//    if (picked != null) {
//      state.setState(() {
//        DateTime dates = picked;
//
//        formattedDate = DateFormat('yyyy-MM-dd').format(picked);
//        state.setState(() {
//          state.tfDateReceived.controller.text = formattedDate.toString();
//        });
//      });
//    }
//  }

  showDatePickers()async {

    state.selectedDateTime = await showNepaliDatePicker(
      context: state.context,
      initialDate: NepaliDateTime.now(),
      firstDate: NepaliDateTime(2050),
      lastDate: NepaliDateTime(2090),
      language: state.language,
    );
    state.setState(() {
      if (state.selectedDateTime != null){
//        state.dateController.text =SplitDate.splitDateString(state.selectedDateTime);
        state.dateController.text =state.selectedDateTime.toIso8601String().split("T").first;

      }else{
        state.dateController.text ="";

      }
    });
//    NepaliDatePicker.showPicker(
//        context: state.context,
//        startYear: 2052,
//        endYear: 2100,
//        color: Colors.blue,
//        barrierDismissible: false,
//        onPicked: (DateTime picked) {
//          if (picked != null) {
//            state.setState(() {
//              DateTime dates = picked;
//              state.setState(() {
//                state.tfDateReceived.controller.text =
//                    picked.toIso8601String().split("T").first;
//              });
//            });
//          }
//        });
  }
}
