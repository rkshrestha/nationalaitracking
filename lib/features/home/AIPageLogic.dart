import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/AIPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/SplitDate.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';



class AIPageLogic {
  AIPageState state;
  AIBloc aiBloc;
  Animal animal = Animal();
  Farmer farmer = Farmer();

  AnimalBloc animalBloc;
  FarmerBloc farmerBloc;
  int speciesId;
  int breedId;
  bool isVisible = true;

  ArtificialInsemination ai = ArtificialInsemination();

  AIPageLogic(this.state, this.aiBloc, this.farmerBloc, this.animalBloc);

  void onPostState() {
    if (this.state.isEdit) {
      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//              AppTranslations.of(state.context).text("ai_form_edit"),
//            );
        this.state.setAppBar('Edit Artificial Insemination');
      });
      this.ai = state.ai;

      //get farmer update
      this.farmerBloc.getFarmerByFId(this.ai.mfarmerId).listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          farmer = it.data;
          state.tfMobileNumber.controller.text = it.data.mobileNumber;

          //get speces code update
          this.animalBloc.getAnimalByaId(this.ai.aId).listen((it) {
            if (it == null) return;
            if (it.status == Status.SUCCESS) {
              animal = it.data;
//              state.tfSpeciesCode.controller.text =
//                  it.data.speciesCode.split("-")[1];

              state.tfSpeciesCode.controller.text = it.data.speciesCode;
            } else if (it.status == Status.ERROR) {
              ToastUtils.show(it.errorMessage);
            } else {
              new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                Navigator.of(this.state.context).pop();
              });
            }
          });
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
            Navigator.of(this.state.context).pop();
          });
        }
      });

      Logger.v('inside ' + this.state.isEdit.toString());
      state.dateAIController.text = this.ai.aiDate;
      state.tentativePdDateController.text = this.ai.tentativePDDate;
//      state.tentativePdDateController.text = this.ai.tentativePDDate.split(" ").first;

    } else {
//      this.state.setAppBar('Add Artificial Insemination');

//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
        this.state.setAppBar(
             "Add Artificial Insemination",
//              AppTranslations.of(state.context).text("ai_form_add_ai"),
            );
//      });
//

      NepaliDateTime currentTime = NepaliDateTime.now();
      String todayDateNepali =  SplitDate.splitNepali(currentTime);
//      String todayDateNepali =  currentTime.toIso8601String().split("T").first;
      state.dateAIController.text = todayDateNepali;

      DateTime today = new DateTime.now();


      NepaliDateTime nepaliConvertedDate = DateConverter.toBS(today);

//      DateTime todayDate =
//          new DateTime(today.year + 56, today.month + 8, today.day + 17);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);

      Logger.v("Date in nepali:" + nepaliConvertedDate.toIso8601String());

//tentative pd with today date
      NepaliDateTime pdTentativeDate = NepaliDateTime.tryParse(state.dateAIController.text);
      Logger.v("nepali dates:"+pdTentativeDate.toString());
      DateTime changeToEnglish = DateConverter.toAD(pdTentativeDate);
      Logger.v("engliah dates:"+changeToEnglish.toString());
      DateTime tPDDate = new DateTime(changeToEnglish.year,
          changeToEnglish.month, changeToEnglish.day + 90);
      Logger.v(" change dates:"+tPDDate.toString());

      NepaliDateTime changeToNepali = DateConverter.toBS(tPDDate);
      Logger.v(" changess dates:"+changeToNepali.toString());
//      String todayNepaliDatePdTentative =
//          DateFormat('yyyy-MM-dd').format(tPDDate);

      state.tentativePdDateController.text = SplitDate.splitNepali(changeToNepali);
//      state.tentativePdDateController.text = changeToNepali.toIso8601String().split("T").first;
//      state.tentativePdDateController.text = todayNepaliDatePdTentative;

      Logger.v('inside ' + this.state.isEdit.toString());
    }
  }


  selectedGoatOrPig() {
    if (state.speciesController.text == "Goat" || state.speciesController.text == "Pig") {

      return isVisible = false;
    } else {
      return isVisible = true;
    }
  }



  saveAI() async {
    speciesId = animal.speciesId;
    breedId = animal.breedId;

    this
        .aiBloc
        .saveAI(this.state.context, this.ai, this.animal, this.farmer,
            speciesId, breedId)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        state.setState(() {
//          if (this.state.context != null) {
//          Navigator.of(this.state.context).pop();

//          }
//        });
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(this.state.context).pop();
//        });
      }
    });
  }

  tentativePdDate(String date) {
//    //tentative pd
//    DateTime pdTentativeDate = DateTime.tryParse(date);
//    DateTime tPDDate = new DateTime(
//        pdTentativeDate.year, pdTentativeDate.month, pdTentativeDate.day + 90);
//    String todayNepaliDatePdTentative =
//        DateFormat('yyyy-MM-dd').format(tPDDate);
//    state.tentativePdDateController.text = todayNepaliDatePdTentative;

    NepaliDateTime pdTentativeDate = NepaliDateTime.tryParse(state.dateAIController.text);
    Logger.v("nepali dates:"+pdTentativeDate.toString());
    DateTime changeToEnglish = DateConverter.toAD(pdTentativeDate);
    Logger.v("engliah dates:"+changeToEnglish.toString());
    DateTime tPDDate = new DateTime(changeToEnglish.year,
        changeToEnglish.month, changeToEnglish.day + 90);
    Logger.v(" change dates:"+tPDDate.toString());

    NepaliDateTime changeToNepali = DateConverter.toBS(tPDDate);
    Logger.v(" changess dates:"+changeToNepali.toString());
//      String todayNepaliDatePdTentative =
//          DateFormat('yyyy-MM-dd').format(tPDDate);
    state.tentativePdDateController.text = SplitDate.splitNepali(changeToNepali);
//    state.tentativePdDateController.text = changeToNepali.toIso8601String().split("T").first;
  }

  getFarmerByMobileNumber(String mblNumber) async {

    this
        .farmerBloc
        .getFarmerByMobileNumber(this.state.context, mblNumber)
        .listen((it) {

      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        farmer = it.data;

        Logger.v('getFarmerId: ' + it.data.fId.toString());
        // ToastUtils.show(it.data.toString());
        state.setState(() {
          state.tfFarmer.controller.text = it.data.farmerName.toString();
          state.tfAddress.controller.text = it.data.municipality.toString();

        });


      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfFarmer.controller.text = "";
        state.tfAddress.controller.text = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }

  //get animal

  getAnimalBySpeciesCode(String speciesCode) async {
    this
        .animalBloc
        .getAnimalBySpeciesCode(this.state.context, speciesCode)
        .listen((it) {
//      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        animal = it.data;
        Logger.v("getAnimalBySpeciesCode "+animal.speciesCode);
        state.setState((){
          state.tfSpecies.controller.text = it.data.speciesName.toString();
          state.tfBreed.controller.text = it.data.breedName.toString();

        });
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfSpecies.controller.text = "";
        state.tfBreed.controller.text = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }

  //date picker

  String formattedDate;

//  Future<Null> showDatePickers() async {
//    final DateTime picked = await showDatePicker(
//        context: state.context,
//        initialDate: DateTime.now(),
//        firstDate: DateTime(2010),
//        lastDate: DateTime(2100));
//
//    if (picked != null) {
//      state.setState(() {
//        DateTime dates = picked;
//        formattedDate = DateFormat('yyyy-MM-dd').format(picked);
//        state.setState(() {
//          state.tfDateAI.controller.text = formattedDate.toString();
//        });
//      });
//    }
//  }

  showDatePickers()async {
    NepaliDateTime date = NepaliDateTime.now();
    var prevMonth = new NepaliDateTime(date.year, date.month - 1, date.day);

    state.selectedDateTime =  await showNepaliDatePicker(
      context: state.context,
      initialDate: NepaliDateTime.now(),
//      firstDate: NepaliDateTime.now().add(new Duration(days: 30)),
      firstDate: prevMonth,
//      firstDate: NepaliDateTime(2050),
      lastDate: NepaliDateTime(2090),
      language: state.language,
    );
    state.setState(() {
      if (state.selectedDateTime != null){
//        if (state.selectedDateTime != null && state.selectedDateTime.isAfter(prevMonth)){
          state.dateAIController.text =SplitDate.splitNepali(state.selectedDateTime);
//        state.dateAIController.text =state.selectedDateTime.toIso8601String().split("T").first;

      }else{
        state.dateAIController.text ="";

      }
    });

//    NepaliDatePicker.showPicker(
//        context: state.context,
//        startYear: 2052,
//        endYear: 2100,
//        color: Colors.blue,
//        barrierDismissible: false,
//        onPicked: (DateTime picked) {
//          if (picked != null) {
//            state.setState(() {
//              DateTime dates = picked;
//              state.setState(() {
//                state.tfDateAI.controller.text =
//                    picked.toIso8601String().split("T").first;
//              });
//            });
//          }
//        });
  }

  updateAI() async {
    speciesId = animal.speciesId;
    breedId = animal.breedId;
    this
        .aiBloc
        .updateAI(this.state.context, this.ai, this.animal, this.farmer,
            speciesId, breedId)
        .listen((it) {
//      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        Navigator.pop(this.state.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }
}
