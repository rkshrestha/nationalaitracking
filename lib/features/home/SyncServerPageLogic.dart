import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/calvingBloc/CalvingBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/bloc/nbisBloc/NbisBloc.dart';
import 'package:nbis/features/bloc/nitrogenBloc/NitrogenBloc.dart';
import 'package:nbis/features/bloc/notificationBloc/NotificationBloc.dart';
import 'package:nbis/features/bloc/pdBloc/PDBloc.dart';
import 'package:nbis/features/bloc/semenBloc/SemenBloc.dart';
import 'package:nbis/features/home/SyncServerPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Calving.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/repo/Species.dart';
import 'package:nbis/features/repo/SyncModel.dart';
import 'package:nbis/s/DsonUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ProgressBar.dart';

class SyncServerPageLogic {
  SyncServerPageState state;

//  static BuildContext context;
  AnimalBloc animalBloc;
  NbisBloc nbisBloc;
  ExitRecordBloc exitRecordBloc;
  FarmerBloc farmerBloc;
  AIBloc aiBloc;
  CalvingBloc calvingBloc;
  NitrogenBloc nitrogenBloc;
  PDBloc pdBloc;
  SemenBloc semenBloc;
  NotificationBloc notificationBloc;

  var firstPressSync = true;

//  List<NotificationRequest> notificationList = [];
  List<Semen> semenList = [];
  List<LiquidNitrogen> liquidNitrogenList = [];
  List<Farmer> farmerList = [];
  List<Animal> animalList = [];
  List<ArtificialInsemination> aiList = [];
  List<PregnancyDiagnosis> pdList = [];
  List<Calving> calvingList = [];
  List<ExitRecord> exitRecordList = [];

  //t
  List<Semen> semenListAll = [];
  List<LiquidNitrogen> liquidNitrogenListAll = [];
  List<Farmer> farmerListAll = [];
  List<Animal> animalListAll = [];
  List<ArtificialInsemination> aiListAll = [];
  List<PregnancyDiagnosis> pdListAll = [];
  List<Calving> calvingListAll = [];
  List<ExitRecord> exitRecordListAll = [];

  Farmer farmer = Farmer();
  Species species = Species();

  String showSemenSend;
  String showNitrogenSend;
  String semenDataSend;
  String nitrogenDataSend;

  var restoreDate = "";
  var syncDate = "";

  SyncServerPageLogic(
      this.state,
      this.animalBloc,
      this.exitRecordBloc,
      this.nbisBloc,
      this.farmerBloc,
      this.aiBloc,
      this.calvingBloc,
      this.nitrogenBloc,
      this.pdBloc,
      this.semenBloc,
      this.notificationBloc);

  SyncServerPageLogic.makes(
      this.aiBloc); //start sending data to server multiple button
//

  //teststart
  void onPostInit() {
    //get all list of data

    getFarmerListUnSync();
    getAnimalListUnSync();
    getAIListUnSync();
    getPDListUnSync();
    getCalvingListUnSync();
    getExitRecordListUnSync();
    getNitrogenListUnSync();
    getSemenListUnSync();
  }

  //get all unsync farmer data
  getFarmerListUnSync() {
    this.farmerBloc.getUnSyncFarmerList().listen((it) {
      if (it.status == Status.SUCCESS) {
        if (it.data != null) {
          this.farmerListAll = it.data;
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//get all unsync animal data

  getAnimalListUnSync() {
    this.animalBloc.getUnSyncAnimalList().listen((it) {
      if (it.status == Status.SUCCESS) {
        if (it.data != null) {
          this.animalListAll = it.data;
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//get all unsync ai data
  getAIListUnSync() {
    this.aiBloc.getUnSyncAIList().listen((it) {
      if (it.status == Status.SUCCESS) {
        if (it.data != null) {
          this.aiListAll = it.data;
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//get all unsync pd data
  getPDListUnSync() {
    this.pdBloc.getUnSyncPdList().listen((it) {
      if (it.status == Status.SUCCESS) {
        if (it.data != null) {
          this.pdListAll = it.data;
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//get all unsync calving data
  getCalvingListUnSync() {
    this.calvingBloc.getUnSyncCalvingList().listen((it) {
      if (it.status == Status.SUCCESS) {
        if (it.data != null) {
          this.calvingListAll = it.data;
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//get all unsync exit record data
  getExitRecordListUnSync() {
    this.exitRecordBloc.getUnSyncExitRecordList().listen((it) {
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        if (it.data != null) {
          this.exitRecordListAll = it.data;
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//get all unsync semen data
  getSemenListUnSync() {
    this.semenBloc.getSemenUnSyncList().listen((it) {
      if (it.status == Status.SUCCESS) {
        Logger.v("semen list is " + it.data.toString());
        if (it.data != null) {
          this.semenListAll = it.data;
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //get all unsync nitrogen data
  getNitrogenListUnSync() {
    this.nitrogenBloc.getUnSyncLiquidNitrogenList().listen((it) {
      if (it.status == Status.SUCCESS) {
        if (it.data != null) {
          this.liquidNitrogenListAll = it.data;
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //testend


//multiple button get all data start
  //semen
  sendSemenList() {
    this.semenBloc.getSemenUnSyncList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("semen list is " + it.data.toString());
        this.state.setState(() {
          this.semenList = it.data;
          //todo call sync to server semenList
          if (semenList.length > 0) {
            sendSemenData(semenList);
          } else {
            this.state.showToast("No semen has been found in local");
          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //nitrogen list
  sendNitrogenList() {
    this.nitrogenBloc.getUnSyncLiquidNitrogenList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.liquidNitrogenList = it.data;
          //todo call sync to server
          if (liquidNitrogenList.length > 0) {
            sendNitrogenData(liquidNitrogenList);
          } else {
            this.state.showToast("No liquid nitrogen has been found in local");
          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //farmer
  sendFarmerList() {
    this.farmerBloc.getUnSyncFarmerList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.farmerList = it.data;
          //todo call sync to server
          if (farmerList.length > 0) {
            sendFarmerData(farmerList);
          } else {
            this.state.showToast("No farmer has been found in local");
          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //animal

  sendAnimalList() {
    this.animalBloc.getUnSyncAnimalList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.animalList = it.data;
          //todo call sync to server

          if (animalList.length > 0) {
            sendAnimalData(animalList);
          } else {
            this.state.showToast("No animal has been found in local");
          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //ai
  sendAIList() {
    this.aiBloc.getUnSyncAIList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.aiList = it.data;
          //todo call sync to server
          if (aiList.length > 0) {
            sendAIData(aiList);
          } else {
            this.state.showToast(
                "No Artificial Insemination has been found in local");
          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //pd
  sendPDList() {
    this.pdBloc.getUnSyncPdList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.pdList = it.data;
          //todo call sync to server
          if (pdList.length > 0) {
            sendPDData(pdList);
          } else {
            this
                .state
                .showToast("No Pregnancy Diagnosis has been found in local");
          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //calving
  sendCalvingList() {
    this.calvingBloc.getUnSyncCalvingList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.calvingList = it.data;
          //todo call sync to server
          if (calvingList.length > 0) {
            sendCalvingData(calvingList);
          } else {
            this.state.showToast("No calving has been found in local");
          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //exit record
  sendExitRecordList() {
    this.exitRecordBloc.getUnSyncExitRecordList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.exitRecordList = it.data;
          //todo call sync to server
          if (exitRecordList.length > 0) {
            sendExitRecordData(exitRecordList);
          } else {
            this.state.showToast("No Exit Record has been found in local");
          }
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //multiple button get all data end


  //multiple button get send data to server start

//send semen

  sendSemenData(List<Semen> sList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);
    this.semenBloc.sendSemenData(sList).listen((it) {
      if (it == null) return;
      Logger.v("inside logic send semen");
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Sync semen data to server successfully");
      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          //  Logger.v("showDatassss: " + it.errorMessage.toString());
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //send nitrogen

  sendNitrogenData(List<LiquidNitrogen> lnList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);
    this.nitrogenBloc.sendNitrogenData(lnList).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Successfully sync nitrogen data to server ");
      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //send farmer
  sendFarmerData(List<Farmer> fList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);
    this.farmerBloc.sendFarmerData(fList).listen((it) {
      if (it == null) return;

      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync farmer data to server.");
      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

//send animal

  sendAnimalData(List<Animal> aList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);
    this.animalBloc.sendAnimalData(aList).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync animal data to server.");
      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

//send ai
  sendAIData(List<ArtificialInsemination> aiDataList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);
    getNotificationPDList();
    getNotificationList();
    this.aiBloc.sendAIData(aiDataList).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync AI data to server.");
//        getNotificationPDList();
//        getNotificationList();

      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //test for notification start

  getNotificationList() async {
    this.notificationBloc.saveNotificationData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        this.state.showToast(it.data);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  getNotificationPDList() async {
    this.notificationBloc.saveNotificationPDData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        this.state.showToast(it.data);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }


  //test for notification end

  //send pd

  sendPDData(List<PregnancyDiagnosis> pdDataList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);
    this.pdBloc.sendPDData(pdDataList).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync PD data to server.");
      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //send calving

  sendCalvingData(List<Calving> cList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);
    this.calvingBloc.sendCalvingData(cList).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync Calving data to server.");
      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //send exit record
  sendExitRecordData(List<ExitRecord> eList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);
    this.exitRecordBloc.sendExitRecordData(eList).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync Exit Record data to server.");
      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //multiple button get send data to server start


  //get data of table from server

//semen data

  syncGetSemenList() async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.semenBloc.syncGetSemenList().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Successfully Sync Semen Data from Server");
//        this.state.showToast(it.data);
      } else {
        Logger.v("sync get semen data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get liquid nitrogen

  syncGetLiquidNitrogenList() async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.nitrogenBloc.syncGetLiquidNitrogenList().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this
            .state
            .showToast("Successfully Sync Liquid Nitrogen Data from Server");
//        this.state.showToast(it.data);
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

//get farmer
  syncGetFarmerData() async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.farmerBloc.syncGetFarmerData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Successfully Sync Farmer Data from Server");
//        this.state.showToast(it.data);
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get animal
  syncGetAnimalData() async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.animalBloc.syncGetAnimalData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Successfully Sync Animal Data from Server");
//        this.state.showToast(it.data);
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get ai from server
  syncGetAIData() async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.aiBloc.syncGetAIData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        this.state.showToast(
            "Successfully Sync Artificial Insemination Data from Server");
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get pd from sever
  syncGetPDData() async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.pdBloc.syncGetPdData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        this.state.showToast(
            "Successfully Sync Pregnancy Diagnosis Data from Server");
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get calving data from server
  syncGetCalvingData() async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.calvingBloc.syncGetCalvingData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        this.state.showToast("Successfully Sync Calving Data from Server");
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get exit record from server
  syncGetExitRecordData() async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.exitRecordBloc.syncGetExitRecordData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        this.state.showToast("Successfully Sync Exit Record Data from Server");
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //extra sync of data from server
//province, municipality, district, species, breed, exit reason

  syncSpeciesList() async {
//    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    this.animalBloc.syncSpeciesList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncBreedList();

      } else {
        Logger.v("sync species Logic print " + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);

        });
      }
    });
  }

  //sync breed
  syncBreedList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.animalBloc.syncBreedList().listen((it) {
      if (it == null) return;
      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncProvinceList();
      } else {
        Logger.v("sync breed Logic print " + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //

  syncProvinceList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.nbisBloc.syncProvinceList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncDistrictList();
      } else {
        Logger.v("province Logic print " + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //district

  syncDistrictList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.nbisBloc.syncDistrictList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncMunicipalityList();
      } else {
        Logger.v("district Logic print " + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //Municipality

  syncMunicipalityList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.nbisBloc.syncMunicipalityList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncExitReasonList();
      } else {
        Logger.v("municipality Logic print " + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //exit reason

  syncExitReasonList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.exitRecordBloc.syncExitReasonList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncGetSemenListFromServer();
      } else {
        Logger.v("exit reason Logic print " + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

//single button get option
  //get data of table from server
//get semenList data
  ProgressBar progressBar = ProgressBar();

  syncGetSemenListFromServer() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.semenBloc.syncGetSemenList().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast("Successfully Sync Semen Data from Server");
//        this.state.showToast(it.data);
        syncGetLiquidNitrogenListFromServer();
      } else {
        Logger.v("sync get semen data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get liquid nitrogen

  syncGetLiquidNitrogenListFromServer() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.nitrogenBloc.syncGetLiquidNitrogenList().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncGetFarmerDataFromServer();
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

//get farmer
  syncGetFarmerDataFromServer() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.farmerBloc.syncGetFarmerData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncGetAnimalDataFromServer();
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get animal
  syncGetAnimalDataFromServer() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.animalBloc.syncGetAnimalData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncGetAIDataFromServer();
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get ai from server
  syncGetAIDataFromServer() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
//  getNotificationPDList();
//  getNotificationList();

    this.aiBloc.syncGetAIData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncGetPDDataFromServer();
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get pd from sever
  syncGetPDDataFromServer() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.pdBloc.syncGetPdData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncGetCalvingDataFromServer();
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get calving data from server
  syncGetCalvingDataFromServer() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.calvingBloc.syncGetCalvingData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast(it.data);
        syncGetExitRecordDataFromServer();
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //get exit record from server
  syncGetExitRecordDataFromServer() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.exitRecordBloc.syncGetExitRecordData().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("Restore completed");
        DateTime todayDate = DateTime.now();
        this.state.setState(() {
          restoreDate = todayDate.toIso8601String().split(".").first;
          Logger.v("today Date:" + restoreDate);
        });

//        this.state.showToast(it.data);
      } else {
        Logger.v("sync get  data" + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      }
    });
  }

  //to server

////start sending data to server single button

  //farmer
  getFarmerListFromDB() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.farmerBloc.getUnSyncFarmerList().listen((it) {
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        if (it.data != null) {
//          this.state.setState(() {
          this.farmerList = it.data;
          Logger.v("farmerList:" + it.data.toString());
          //todo call sync to server
          if (farmerList.length > 0) {
            sendFarmerDataToServer(farmerList);
          } else {
            this.state.showToast("No farmer has been found in local");
            getAnimalListFromDB();
          }
//          });
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //animal

  getAnimalListFromDB() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.animalBloc.getUnSyncAnimalList().listen((it) {
      Logger.v("inside animal listener it");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);

        if (it.data != null) {
//          this.state.setState(() {
          this.animalList = it.data;
          Logger.v("animalList:");

          //todo call sync to server
          if (animalList.length > 0) {
            sendAnimalDataToServer(animalList);
          } else {
            this.state.showToast("No animal has been found in local");
            getAIListFromDB();
          }
//          });
        }
      } else if (it.status == Status.ERROR) {
//        progressBar.dismiss(this.state.context);
        Logger.v("AnimalList error: " + it.errorMessage);
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //ai
  getAIListFromDB() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    getNotificationPDList();
    getNotificationList();

    this.aiBloc.getUnSyncAIList().listen((it) {
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        if (it.data != null) {
//          this.state.setState(() {
          this.aiList = it.data;
          Logger.v("aiList:" + it.data.toString());
          Logger.v("aiList Length:" + aiList.length.toString());
          Logger.v("aiList Length:" + it.status.toString());

          //todo call sync to server
          if (aiList.length > 0) {
            sendAIDataToServer(aiList);
          } else {
            this.state.showToast(
                "No Artificial Insemination has been found in local");
            getPDListFromDB();
          }
//          });
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //pd
  getPDListFromDB() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    Logger.v("get PD list");

    this.pdBloc.getUnSyncPdList().listen((it) {
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        if (it.data != null) {
//          this.state.setState(() {
          this.pdList = it.data;
          Logger.v("pdList:" + it.data.toString());

          //todo call sync to server
          if (pdList.length > 0) {
            sendPDDataToServer(pdList);
          } else {
            this
                .state
                .showToast("No Pregnancy Diagnosis has been found in local");
            getCalvingListFromDB();
          }
//          });
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //calving
  getCalvingListFromDB() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.calvingBloc.getUnSyncCalvingList().listen((it) {
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        if (it.data != null) {
//          this.state.setState(() {
          this.calvingList = it.data;
          //todo call sync to server
          if (calvingList.length > 0) {
            sendCalvingDataToServer(calvingList);
          } else {
            this.state.showToast("No calving has been found in local");
            getExitRecordListFromDB();
          }
//          });
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //exit record
  getExitRecordListFromDB() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.exitRecordBloc.getUnSyncExitRecordList().listen((it) {
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        if (it.data != null) {
//          this.state.setState(() {
          this.exitRecordList = it.data;
          Logger.v("exitList:" + it.data.toString());

          //todo call sync to server
          if (exitRecordList.length > 0) {
            sendExitRecordDataToServer(exitRecordList);
          } else {
//              progressBar.dismiss(this.state.context);
            this.state.showToast("No Exit Record has been found in local");
            getSemenListFromDB();
          }
//          });
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //  //semen
  getSemenListFromDB() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.semenBloc.getSemenUnSyncList().listen((it) {
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        Logger.v("semen list is " + it.data.toString());
        if (it.data != null) {
//          this.state.setState(() {
          this.semenList = it.data;

          //todo call sync to server semenList
          if (semenList.length > 0) {
            sendSemenDataToServer(semenList);
          } else {
            this.state.showToast("No semen has been found in local");
            getNitrogenListFromDB();
          }
//          });
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //nitrogen list
  getNitrogenListFromDB() {
    showNitrogenSend = "Sending Nitrogen data";
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.nitrogenBloc.getUnSyncLiquidNitrogenList().listen((it) {
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        if (it.data != null) {
//          this.state.setState(() {
          this.liquidNitrogenList = it.data;
          Logger.v("liquidNitrogenList:" + it.data.toString());
          //todo call sync to server

          if (liquidNitrogenList.length > 0) {
            sendNitrogenDataToServer(liquidNitrogenList);
            Logger.v("no data less than 0");
          } else {
            Logger.v("no data");
            this.state.showToast("No liquid nitrogen has been found in local");
//              getFarmerListFromDB();
          }
//          });
        }
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//send semen to server
//
  sendSemenDataToServer(List<Semen> sList) async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.semenBloc.sendSemenData(sList).listen((it) {
      Logger.v("inside logic send semen");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("Sync semen data to server successfully");
        Logger.v("Send TO server semen");
        semenDataSend = "Semen data sucessfully sync to server";
        getNitrogenListFromDB();
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //send nitrogen

  sendNitrogenDataToServer(List<LiquidNitrogen> lnList) async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.nitrogenBloc.sendNitrogenData(lnList).listen((it) {
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("Successfully sync nitrogen data to server ");
        Logger.v("Send TO server Nitrogen");
//        getFarmerListFromDB();
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //send farmer
  sendFarmerDataToServer(List<Farmer> fList) async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.farmerBloc.sendFarmerData(fList).listen((it) {
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync farmer data to server.");
        Logger.v("Send TO server");
        getAnimalListFromDB();
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//send animal

  sendAnimalDataToServer(List<Animal> aList) async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.animalBloc.sendAnimalData(aList).listen((it) {
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync animal data to server.");
        Logger.v("Send TO server Animal");
        getAIListFromDB();
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//send ai
  sendAIDataToServer(List<ArtificialInsemination> aiDataList) async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.aiBloc.sendAIData(aiDataList).listen((it) {
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync AI data to server.");
        Logger.v("Send TO server AI");
        getPDListFromDB();
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //send pd

  sendPDDataToServer(List<PregnancyDiagnosis> pdDataList) async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.pdBloc.sendPDData(pdDataList).listen((it) {
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync PD data to server.");

        getCalvingListFromDB();
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //send calving

  sendCalvingDataToServer(List<Calving> cList) async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.calvingBloc.sendCalvingData(cList).listen((it) {
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync Calving data to server.");
        Logger.v("Send TO server Calving");

        getExitRecordListFromDB();
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

  //send exit record
  sendExitRecordDataToServer(List<ExitRecord> eList) async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    this.exitRecordBloc.sendExitRecordData(eList).listen((it) {
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("Succesfully sync Exit Record data to server.");
        Logger.v("Send TO server Exit");
        getSemenListFromDB();
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
      } else if (it.status == Status.ERROR) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
        });
      } else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }

//end sending data to server single button
//

  //send data to server by single button with if only one model start

//  getFarmerListFromDBs() {
////    ProgressBar progressBar = ProgressBar();ssssss
//    progressBar.show(this.state.context);
//    this.farmerBloc.getUnSyncFarmerList().listen((it) {
//      Logger.v("inside listener it");
//      if (it.status == Status.SUCCESS) {
//        if (it.data != null) {
//          this.farmerList = it.data;
//          Logger.v("farmerList:" + it.data.toString());
//          //animal
//          this.animalBloc.getUnSyncAnimalList().listen((it) {
//            Logger.v("inside animal listener it");
//            if (it.status == Status.SUCCESS) {
//              if (it.data != null) {
//                this.animalList = it.data;
//                Logger.v("animalList:");
//                //ai
//                this.aiBloc.getUnSyncAIList().listen((it) {
//                  Logger.v("inside listener it");
//                  if (it.status == Status.SUCCESS) {
//                    if (it.data != null) {
//                      this.aiList = it.data;
//                      Logger.v("aiList:" + it.data.toString());
//                      Logger.v("aiList Length:" + aiList.length.toString());
//                      Logger.v("aiList Length:" + it.status.toString());
//                      //pd
//                      this.pdBloc.getUnSyncPdList().listen((it) {
//                        Logger.v("inside listener it");
//                        if (it.status == Status.SUCCESS) {
//                          if (it.data != null) {
//                            this.pdList = it.data;
//                            Logger.v("pdList:" + it.data.toString());
//                            //calving
//                            this
//                                .calvingBloc
//                                .getUnSyncCalvingList()
//                                .listen((it) {
//                              if (it.status == Status.SUCCESS) {
//                                if (it.data != null) {
//                                  this.calvingList = it.data;
//                                  //exit
//                                  this
//                                      .exitRecordBloc
//                                      .getUnSyncExitRecordList()
//                                      .listen((it) {
//                                    Logger.v("inside listener it");
//                                    if (it.status == Status.SUCCESS) {
//                                      if (it.data != null) {
//                                        this.exitRecordList = it.data;
//                                        Logger.v(
//                                            "exitList:" + it.data.toString());
//                                        //semen
//                                        this
//                                            .semenBloc
//                                            .getSemenUnSyncList()
//                                            .listen((it) {
//                                          Logger.v("inside listener it");
//                                          if (it.status == Status.SUCCESS) {
//                                            Logger.v("semen list is " +
//                                                it.data.toString());
//                                            if (it.data != null) {
//                                              this.semenList = it.data;
//                                              //nitrogen
//
//                                              this
//                                                  .nitrogenBloc
//                                                  .getUnSyncLiquidNitrogenList()
//                                                  .listen((it) {
//                                                Logger.v("inside listener it");
//                                                if (it.status ==
//                                                    Status.SUCCESS) {
//                                                  if (it.data != null) {
//                                                    this.liquidNitrogenList =
//                                                        it.data;
//                                                    Logger.v(
//                                                        "liquidNitrogenList:" +
//                                                            it.data.toString());
//
////get all data in single model
//
//                                                    SyncModel allList =
//                                                        SyncModel.make(
//                                                      farmers: farmerList,
//                                                      animals: animalList,
//                                                      artificialInseminations:
//                                                          aiList,
//                                                      pregnancyDiagnoses:
//                                                          pdList,
//                                                      calvings: calvingList,
//                                                      exitRecords:
//                                                          exitRecordList,
//                                                      liquidNitrogens:
//                                                          liquidNitrogenList,
//                                                      semen: semenList,
//                                                    );
//
//                                                    //send all data to server
//
////                                                    progressBar.dismiss(
////                                                        this.state.context);
//                                                    sendAllDataToServer(
//                                                        allList);
//                                                  } else if (it.status ==
//                                                      Status.ERROR) {
//                                                    new Future.delayed(
//                                                            new Duration(
//                                                                milliseconds:
//                                                                    3000))
//                                                        .then((_) {
//                                                      progressBar.dismiss(
//                                                          this.state.context);
//                                                      this.state.showToast(
//                                                          it.errorMessage);
//                                                    });
//                                                  } else if (it.status ==
//                                                      Status.TOKEN_EXPIRED) {}
//                                                }
//                                              });
//                                            }
//                                          } else if (it.status ==
//                                              Status.ERROR) {
//                                            new Future.delayed(new Duration(
//                                                    milliseconds: 3000))
//                                                .then((_) {
//                                              progressBar
//                                                  .dismiss(this.state.context);
//                                              this
//                                                  .state
//                                                  .showToast(it.errorMessage);
//                                            });
//                                          } else if (it.status ==
//                                              Status.TOKEN_EXPIRED) {}
//                                        });
//                                      }
//                                    } else if (it.status == Status.ERROR) {
//                                      new Future.delayed(
//                                              new Duration(milliseconds: 3000))
//                                          .then((_) {
//                                        progressBar.dismiss(this.state.context);
//                                        this.state.showToast(it.errorMessage);
//                                      });
//                                    } else if (it.status ==
//                                        Status.TOKEN_EXPIRED) {}
//                                  });
//                                }
//                              } else if (it.status == Status.ERROR) {
//                                new Future.delayed(
//                                        new Duration(milliseconds: 3000))
//                                    .then((_) {
//                                  progressBar.dismiss(this.state.context);
//                                  this.state.showToast(it.errorMessage);
//                                });
//                              } else if (it.status == Status.TOKEN_EXPIRED) {}
//                            });
//                          }
//                        } else if (it.status == Status.ERROR) {
//                          new Future.delayed(new Duration(milliseconds: 3000))
//                              .then((_) {
//                            progressBar.dismiss(this.state.context);
//                            this.state.showToast(it.errorMessage);
//                          });
//                        } else if (it.status == Status.TOKEN_EXPIRED) {}
//                      });
//                    }
//                  } else if (it.status == Status.ERROR) {
//                    new Future.delayed(new Duration(milliseconds: 3000))
//                        .then((_) {
//                      progressBar.dismiss(this.state.context);
//                      this.state.showToast(it.errorMessage);
//                    });
//                  } else if (it.status == Status.TOKEN_EXPIRED) {}
//                });
//              }
//            } else if (it.status == Status.ERROR) {
//              progressBar.dismiss(this.state.context);
//              Logger.v("AnimalList error: " + it.errorMessage);
//              new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//                this.state.showToast(it.errorMessage);
//              });
//            } else if (it.status == Status.TOKEN_EXPIRED) {}
//          });
//        }
//      } else if (it.status == Status.ERROR) {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
//          this.state.showToast(it.errorMessage);
//        });
//      } else if (it.status == Status.TOKEN_EXPIRED) {}
//    });
//  }

  //single button final start
  getAllDataFromDataBase() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
    SyncModel allList = SyncModel.make(
      farmers: farmerListAll,
      animals: animalListAll,
      artificialInseminations: aiListAll,
      pregnancyDiagnoses: pdListAll,
      calvings: calvingListAll,
      exitRecords: exitRecordListAll,
      liquidNitrogens: liquidNitrogenListAll,
      semen: semenListAll,
    );
    Logger.v("aiList:"+DsonUtils.toJsonString(aiListAll));
    Logger.v("farmerListAll:"+DsonUtils.toJsonString(farmerListAll));
    Logger.v("animalListAll:"+DsonUtils.toJsonString(animalListAll));
    Logger.v("pdListAll:"+DsonUtils.toJsonString(pdListAll));
    Logger.v("calvingListAll:"+DsonUtils.toJsonString(calvingListAll));
    Logger.v("exitRecordListAll:"+DsonUtils.toJsonString(exitRecordListAll));
    Logger.v("liquidNitrogenListAll:"+DsonUtils.toJsonString(liquidNitrogenListAll));
    Logger.v("semenListAll:"+DsonUtils.toJsonString(semenListAll));
//    progressBar.dismiss(this.state.context);
    sendAllDataToServer(allList);
  }

  sendAllDataToServer(SyncModel allList) async {
    ProgressBar progressBar = ProgressBar();
    progressBar.show(this.state.context);

    getNotificationPDList();
    getNotificationList();

    this.nbisBloc.sendAllData(allList).listen((it) {
      if (it.status == Status.SUCCESS) {
        SyncModel data = it.data;

        List<Farmer> farmerServer = data.farmers;
        List<Animal> animalServer = data.animals;
        List<ArtificialInsemination> aiServer = data.artificialInseminations;
        List<PregnancyDiagnosis> pdServer = data.pregnancyDiagnoses;
        List<Calving> calvingServer = data.calvings;
        List<ExitRecord> exitRecordServer = data.exitRecords;
        List<LiquidNitrogen> liquidNitrogenServer = data.liquidNitrogens;
        List<Semen> semenServer = data.semen;
        String errorMessage = data.errorMsg;

        if (farmerServer.length > 0) {
          this.farmerBloc.updateServerIdAndStatus(farmerServer);
        }

        if (animalServer.length > 0) {
          this.animalBloc.updateAnimalServerIdAndStatus(animalServer);
        }
        if (aiServer.length > 0) {
          this.aiBloc.updateAIServerIdAndStatus(aiServer);
        }
        if (pdServer.length > 0) {
          this.pdBloc.updatePdServerIdAndStatus(pdServer);
        }
        if (calvingServer.length > 0) {
          this.calvingBloc.updateCalvingServerIdAndStatus(calvingServer);
        }
        if (exitRecordServer.length > 0) {
          this
              .exitRecordBloc
              .updateExitRecordServerIdAndStatus(exitRecordServer);
        }
        if (liquidNitrogenServer.length > 0) {
          this
              .nitrogenBloc
              .updateNitrogenServerIdAndStatus(liquidNitrogenServer);
        }
        if (semenServer.length > 0) {
          this.semenBloc.updateSemenServerIdAndStatus(semenServer);
        }

        progressBar.dismiss(this.state.context);

        if (errorMessage != null && errorMessage.isNotEmpty) {
          this.state.showToast(errorMessage);
        } else {

          this.state.showToast("Succesfully sync all data to server.");
        }

        DateTime todayDate = DateTime.now();
        this.state.setState(() {
          syncDate = todayDate.toIso8601String().split(".").first;
        });
      }
      else if (it.status == Status.NO_INTERNET_AVAILABLE) {
      Logger.v("Error no:"+it.errorMessage);
        progressBar.dismiss(this.state.context);
//        progressBar.dismiss(this.state.context);

        this.state.showToast(it.errorMessage);
//        this.state.showToast("No Internet Connection");
      }
      else if (it.status == Status.ERROR) {
        Logger.v("Error"+it.errorMessage);
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
//        });
      }
//      else if (it.status == Status.TOKEN_EXPIRED) {}
    });
  }


//

//  getAllListFromDbToServer() {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);
//    this.farmerBloc.getUnSyncFarmerList().listen((it) {
//      Logger.v("inside listener it");
//      if (it.status == Status.SUCCESS) {
//        if (it.data != null) {
//          this.farmerList = it.data;
//          Logger.v("farmerList:" + it.data.toString());
//          //animal
//          this.animalBloc.getUnSyncAnimalList().listen((it) {
//            Logger.v("inside animal listener it");
//            if (it.status == Status.SUCCESS) {
//              if (it.data != null) {
//                this.animalList = it.data;
//                Logger.v("animalList:");
//                //ai
//                this.aiBloc.getUnSyncAIList().listen((it) {
//                  Logger.v("inside listener it");
//                  if (it.status == Status.SUCCESS) {
//                    if (it.data != null) {
//                      this.aiList = it.data;
//                      Logger.v("aiList:" + it.data.toString());
//                      Logger.v("aiList Length:" + aiList.length.toString());
//                      Logger.v("aiList Length:" + it.status.toString());
//                      //pd
//                      this.pdBloc.getUnSyncPdList().listen((it) {
//                        Logger.v("inside listener it");
//                        if (it.status == Status.SUCCESS) {
//                          if (it.data != null) {
//                            this.pdList = it.data;
//                            Logger.v("pdList:" + it.data.toString());
//                            //calving
//                            this
//                                .calvingBloc
//                                .getUnSyncCalvingList()
//                                .listen((it) {
//                              if (it.status == Status.SUCCESS) {
//                                if (it.data != null) {
//                                  this.calvingList = it.data;
//                                  //exit
//                                  this
//                                      .exitRecordBloc
//                                      .getUnSyncExitRecordList()
//                                      .listen((it) {
//                                    Logger.v("inside listener it");
//                                    if (it.status == Status.SUCCESS) {
//                                      if (it.data != null) {
//                                        this.exitRecordList = it.data;
//                                        Logger.v(
//                                            "exitList:" + it.data.toString());
//                                        //semen
//                                        this
//                                            .semenBloc
//                                            .getSemenUnSyncList()
//                                            .listen((it) {
//                                          Logger.v("inside listener it");
//                                          if (it.status == Status.SUCCESS) {
//                                            Logger.v("semen list is " +
//                                                it.data.toString());
//                                            if (it.data != null) {
//                                              this.semenList = it.data;
//                                              //nitrogen
//
//                                              this
//                                                  .nitrogenBloc
//                                                  .getUnSyncLiquidNitrogenList()
//                                                  .listen((it) {
//                                                Logger.v("inside listener it");
//                                                if (it.status ==
//                                                    Status.SUCCESS) {
//                                                  if (it.data != null) {
//                                                    this.liquidNitrogenList =
//                                                        it.data;
//                                                    Logger.v(
//                                                        "liquidNitrogenList:" +
//                                                            it.data.toString());
//
//                                                    //send to server now
//
//                                                    getNotificationPDList();
//                                                    getNotificationList();
//
//                                                    if (farmerList.length > 0) {
//                                                      //sendfarmer
//                                                      this
//                                                          .farmerBloc
//                                                          .sendFarmerData(
//                                                              farmerList)
//                                                          .listen((it) {
//                                                        if (it.status ==
//                                                            Status.SUCCESS) {
//                                                          this.state.showToast(
//                                                              "Succesfully sync farmer data to server.");
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .NO_INTERNET_AVAILABLE) {
//                                                          progressBar.dismiss(
//                                                              this
//                                                                  .state
//                                                                  .context);
//                                                          this.state.showToast(
//                                                              "No Internet Connection");
//                                                        } else if (it.status ==
//                                                            Status.ERROR) {
//                                                          new Future.delayed(
//                                                                  new Duration(
//                                                                      milliseconds:
//                                                                          3000))
//                                                              .then((_) {
//                                                            progressBar.dismiss(
//                                                                this
//                                                                    .state
//                                                                    .context);
//                                                            this.state.showToast(
//                                                                it.errorMessage);
//                                                          });
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .TOKEN_EXPIRED) {}
//                                                      });
////                                                      end farmer
//
//                                                    }
//                                                    if (animalList.length > 0) {
//                                                      //send animal
//                                                      this
//                                                          .animalBloc
//                                                          .sendAnimalData(
//                                                              animalList)
//                                                          .listen((it) {
//                                                        if (it.status ==
//                                                            Status.SUCCESS) {
//                                                          this.state.showToast(
//                                                              "Succesfully sync animal data to server.");
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .NO_INTERNET_AVAILABLE) {
//                                                          progressBar.dismiss(
//                                                              this
//                                                                  .state
//                                                                  .context);
//                                                          this.state.showToast(
//                                                              "No Internet Connection");
//                                                        } else if (it.status ==
//                                                            Status.ERROR) {
//                                                          new Future.delayed(
//                                                                  new Duration(
//                                                                      milliseconds:
//                                                                          3000))
//                                                              .then((_) {
//                                                            progressBar.dismiss(
//                                                                this
//                                                                    .state
//                                                                    .context);
//                                                            this.state.showToast(
//                                                                it.errorMessage);
//                                                          });
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .TOKEN_EXPIRED) {}
//                                                      });
//
//                                                      //end animal
//
//                                                    }
//                                                    if (aiList.length > 0) {
//                                                      //send ai
//                                                      this
//                                                          .aiBloc
//                                                          .sendAIData(aiList)
//                                                          .listen((it) {
//                                                        if (it.status ==
//                                                            Status.SUCCESS) {
//                                                          this.state.showToast(
//                                                              "Succesfully sync AI data to server.");
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .NO_INTERNET_AVAILABLE) {
//                                                          progressBar.dismiss(
//                                                              this
//                                                                  .state
//                                                                  .context);
//                                                          this.state.showToast(
//                                                              "No Internet Connection");
//                                                        } else if (it.status ==
//                                                            Status.ERROR) {
//                                                          new Future.delayed(
//                                                                  new Duration(
//                                                                      milliseconds:
//                                                                          3000))
//                                                              .then((_) {
//                                                            progressBar.dismiss(
//                                                                this
//                                                                    .state
//                                                                    .context);
//                                                            this.state.showToast(
//                                                                it.errorMessage);
//                                                          });
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .TOKEN_EXPIRED) {}
//                                                      });
//                                                    }
//                                                    if (pdList.length > 0) {
//                                                      //send pd
//                                                      this
//                                                          .pdBloc
//                                                          .sendPDData(pdList)
//                                                          .listen((it) {
//                                                        if (it.status ==
//                                                            Status.SUCCESS) {
//                                                          this.state.showToast(
//                                                              "Succesfully sync PD data to server.");
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .NO_INTERNET_AVAILABLE) {
//                                                          progressBar.dismiss(
//                                                              this
//                                                                  .state
//                                                                  .context);
//                                                          this.state.showToast(
//                                                              "No Internet Connection");
//                                                        } else if (it.status ==
//                                                            Status.ERROR) {
//                                                          new Future.delayed(
//                                                                  new Duration(
//                                                                      milliseconds:
//                                                                          3000))
//                                                              .then((_) {
//                                                            progressBar.dismiss(
//                                                                this
//                                                                    .state
//                                                                    .context);
//                                                            this.state.showToast(
//                                                                it.errorMessage);
//                                                          });
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .TOKEN_EXPIRED) {}
//                                                      });
//                                                    }
//                                                    if (calvingList.length >
//                                                        0) {
//                                                      //send calving
//                                                      this
//                                                          .calvingBloc
//                                                          .sendCalvingData(
//                                                              calvingList)
//                                                          .listen((it) {
//                                                        if (it.status ==
//                                                            Status.SUCCESS) {
//                                                          this.state.showToast(
//                                                              "Succesfully sync Calving data to server.");
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .NO_INTERNET_AVAILABLE) {
//                                                          progressBar.dismiss(
//                                                              this
//                                                                  .state
//                                                                  .context);
//                                                          this.state.showToast(
//                                                              "No Internet Connection");
//                                                        } else if (it.status ==
//                                                            Status.ERROR) {
//                                                          new Future.delayed(
//                                                                  new Duration(
//                                                                      milliseconds:
//                                                                          3000))
//                                                              .then((_) {
//                                                            progressBar.dismiss(
//                                                                this
//                                                                    .state
//                                                                    .context);
//                                                            this.state.showToast(
//                                                                it.errorMessage);
//                                                          });
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .TOKEN_EXPIRED) {}
//                                                      });
//                                                    }
//                                                    if (exitRecordList.length >
//                                                        0) {
//                                                      //send exit
//                                                      this
//                                                          .exitRecordBloc
//                                                          .sendExitRecordData(
//                                                              exitRecordList)
//                                                          .listen((it) {
//                                                        if (it.status ==
//                                                            Status.SUCCESS) {
//                                                          this.state.showToast(
//                                                              "Succesfully sync Exit Record data to server.");
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .NO_INTERNET_AVAILABLE) {
//                                                          progressBar.dismiss(
//                                                              this
//                                                                  .state
//                                                                  .context);
//                                                          this.state.showToast(
//                                                              "No Internet Connection");
//                                                        } else if (it.status ==
//                                                            Status.ERROR) {
//                                                          new Future.delayed(
//                                                                  new Duration(
//                                                                      milliseconds:
//                                                                          3000))
//                                                              .then((_) {
//                                                            progressBar.dismiss(
//                                                                this
//                                                                    .state
//                                                                    .context);
//                                                            this.state.showToast(
//                                                                it.errorMessage);
//                                                          });
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .TOKEN_EXPIRED) {}
//                                                      });
//                                                    }
//                                                    if (semenList.length > 0) {
//                                                      //send semen
//                                                      this
//                                                          .semenBloc
//                                                          .sendSemenData(
//                                                              semenList)
//                                                          .listen((it) {
//                                                        Logger.v(
//                                                            "inside logic send semen");
//                                                        if (it.status ==
//                                                            Status.SUCCESS) {
//                                                          this.state.showToast(
//                                                              "Sync semen data to server successfully");
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .NO_INTERNET_AVAILABLE) {
//                                                          progressBar.dismiss(
//                                                              this
//                                                                  .state
//                                                                  .context);
//                                                          this.state.showToast(
//                                                              "No Internet Connection");
//                                                        } else if (it.status ==
//                                                            Status.ERROR) {
//                                                          new Future.delayed(
//                                                                  new Duration(
//                                                                      milliseconds:
//                                                                          3000))
//                                                              .then((_) {
//                                                            progressBar.dismiss(
//                                                                this
//                                                                    .state
//                                                                    .context);
//                                                            this.state.showToast(
//                                                                it.errorMessage);
//                                                          });
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .TOKEN_EXPIRED) {}
//                                                      });
//                                                    }
//                                                    if (liquidNitrogenList
//                                                            .length >
//                                                        0) {
//                                                      //send ln
//                                                      this
//                                                          .nitrogenBloc
//                                                          .sendNitrogenData(
//                                                              liquidNitrogenList)
//                                                          .listen((it) {
//                                                        if (it.status ==
//                                                            Status.SUCCESS) {
//                                                          this.state.showToast(
//                                                              "Successfully sync nitrogen data to server ");
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .NO_INTERNET_AVAILABLE) {
//                                                          progressBar.dismiss(
//                                                              this
//                                                                  .state
//                                                                  .context);
//                                                          this.state.showToast(
//                                                              "No Internet Connection");
//                                                        } else if (it.status ==
//                                                            Status.ERROR) {
//                                                          new Future.delayed(
//                                                                  new Duration(
//                                                                      milliseconds:
//                                                                          3000))
//                                                              .then((_) {
//                                                            progressBar.dismiss(
//                                                                this
//                                                                    .state
//                                                                    .context);
//                                                            this.state.showToast(
//                                                                it.errorMessage);
//                                                          });
//                                                        } else if (it.status ==
//                                                            Status
//                                                                .TOKEN_EXPIRED) {}
//                                                      });
//                                                    }
//                                                  }
//                                                } else if (it.status ==
//                                                    Status.ERROR) {
//                                                  new Future.delayed(
//                                                          new Duration(
//                                                              milliseconds:
//                                                                  3000))
//                                                      .then((_) {
//                                                    progressBar.dismiss(
//                                                        this.state.context);
//                                                    this.state.showToast(
//                                                        it.errorMessage);
//                                                  });
//                                                } else if (it.status ==
//                                                    Status.TOKEN_EXPIRED) {}
//                                              });
//                                            }
//                                          } else if (it.status ==
//                                              Status.ERROR) {
//                                            new Future.delayed(new Duration(
//                                                    milliseconds: 3000))
//                                                .then((_) {
//                                              progressBar
//                                                  .dismiss(this.state.context);
//                                              this
//                                                  .state
//                                                  .showToast(it.errorMessage);
//                                            });
//                                          } else if (it.status ==
//                                              Status.TOKEN_EXPIRED) {}
//                                        });
//                                      }
//                                    } else if (it.status == Status.ERROR) {
//                                      new Future.delayed(
//                                              new Duration(milliseconds: 3000))
//                                          .then((_) {
//                                        progressBar.dismiss(this.state.context);
//                                        this.state.showToast(it.errorMessage);
//                                      });
//                                    } else if (it.status ==
//                                        Status.TOKEN_EXPIRED) {}
//                                  });
//                                }
//                              } else if (it.status == Status.ERROR) {
//                                new Future.delayed(
//                                        new Duration(milliseconds: 3000))
//                                    .then((_) {
//                                  progressBar.dismiss(this.state.context);
//                                  this.state.showToast(it.errorMessage);
//                                });
//                              } else if (it.status == Status.TOKEN_EXPIRED) {}
//                            });
//                          }
//                        } else if (it.status == Status.ERROR) {
//                          new Future.delayed(new Duration(milliseconds: 3000))
//                              .then((_) {
//                            progressBar.dismiss(this.state.context);
//                            this.state.showToast(it.errorMessage);
//                          });
//                        } else if (it.status == Status.TOKEN_EXPIRED) {}
//                      });
//                    }
//                  } else if (it.status == Status.ERROR) {
//                    new Future.delayed(new Duration(milliseconds: 3000))
//                        .then((_) {
//                      progressBar.dismiss(this.state.context);
//                      this.state.showToast(it.errorMessage);
//                    });
//                  } else if (it.status == Status.TOKEN_EXPIRED) {}
//                });
//              }
//            } else if (it.status == Status.ERROR) {
//              progressBar.dismiss(this.state.context);
//              Logger.v("AnimalList error: " + it.errorMessage);
//              new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//                this.state.showToast(it.errorMessage);
//              });
//            } else if (it.status == Status.TOKEN_EXPIRED) {}
//          });
//        }
//      } else if (it.status == Status.ERROR) {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          progressBar.dismiss(this.state.context);
//          this.state.showToast(it.errorMessage);
//        });
//      } else if (it.status == Status.TOKEN_EXPIRED) {}
//    });
//  }
//end of single button sync if only

}
