import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/bloc/pdBloc/PDBloc.dart';
import 'package:nbis/features/home/PDPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/SplitDate.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:nepali_utils/nepali_utils.dart';

class PDPageLogic {
  PDPageState state;
  PDBloc pdBloc;
  Animal animal = Animal();
  Farmer farmer = Farmer();
  AnimalBloc animalBloc;
  FarmerBloc farmerBloc;
  PregnancyDiagnosis pd = PregnancyDiagnosis();
  int speciesId;
  int breedId;

  //check ai date
  List<ArtificialInsemination> aiList = [];
  AIBloc aiBloc;

  //
  bool isVisible = true;

  PDPageLogic(
      this.state, this.pdBloc, this.animalBloc, this.farmerBloc, this.aiBloc);

  void onPostState() {
    if (this.state.isEdit) {
//      new Future.delayed(new Duration(milliseconds: 0)).then((_) {
//        this.state.setAppBar(
//              AppTranslations.of(state.context).text("pd_form_edit"),
//            );
//      });
        this.state.setAppBar('Edit Pregnancy Diagnosis');

      this.pd = state.pd;
      //get farmer update
      this.farmerBloc.getFarmerByFId(this.pd.mFarmerId).listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          farmer = it.data;
          state.tfMobileNumber.controller.text = farmer.mobileNumber;

          //get speces code update
          this.animalBloc.getAnimalByaId(this.pd.mAnimalId).listen((it) {
            if (it == null) return;
            if (it.status == Status.SUCCESS) {
              animal = it.data;
              state.tfSpeciesCode.controller.text = it.data.speciesCode;
//                  it.data.speciesCode.split("-")[1];
            } else if (it.status == Status.ERROR) {
              ToastUtils.show(it.errorMessage);
            } else {
              new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                Navigator.of(this.state.context).pop();
              });
            }
          });
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
            Navigator.of(this.state.context).pop();
          });
        }
      });

      //diplay radio button value from db
      if (pd.pdResult) {
        state.selected = 0;
      } else {
        state.selected = 1;
      }

      state.pdDateController.text = this.pd.pdDate;
      state.calvingDate.text = this.pd.expectedDeliveryDate;
    } else {
//      new Future.delayed(new Duration(milliseconds: 0)).then((_) {
//        this.state.setAppBar(
//              AppTranslations.of(state.context).text("pd_form_add_pd"),
//            );
//      });
            this.state.setAppBar('Add Pregnancy Diagnosis');

//      String pdDates = state.pdDateController.text;
//
//      DateTime dt = DateTime.parse(pdDates);

//      DateTime today = new DateTime.now();
//      DateTime todayDate =
//          new DateTime(today.year + 56, today.month + 8, today.day + 17);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
//      state.pdDateController.text = todayNepaliDate.toString();

      NepaliDateTime currentTime = NepaliDateTime.now();
      String todayDateNepali =  currentTime.toIso8601String().split("T").first;
      state.pdDateController.text = todayDateNepali;

      //get ai list for ai date to calculated expected calving date
      this.animal = state.animal;
      if (animal != null) {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          this.aiBloc.getAIListByAnimal(animal).listen((it) {
            Logger.v("animalIds:" + animal.aId.toString());

            if (it == null) return;
            Logger.v("inside listener it");
            if (it.status == Status.SUCCESS) {
              this.state.setState(() {
                this.aiList = it.data;
                Logger.v(" new List is " + it.data.toString());
                for (ArtificialInsemination ai in aiList) {
                  Logger.v("aiDate:" + ai.englishAiDate);
                  if (animal.speciesName == "Buffalo") {
                    DateTime aiBuffaloDate = DateTime.tryParse(ai.englishAiDate);
                    Logger.v(
                        "aiDate Display: " + aiBuffaloDate.toIso8601String());

                    DateTime todayBuffaloDate = new DateTime(aiBuffaloDate.year,
                        aiBuffaloDate.month, aiBuffaloDate.day + 300);

                    NepaliDateTime convertedCalvingDate = DateConverter.toBS(todayBuffaloDate);
//                    String todayNepaliBuffaloDate =
//                        DateFormat('yyyy-MM-dd').format(convertedCalvingDate);

                    Logger.v(
                        "getdifer" + todayBuffaloDate.difference(aiBuffaloDate).inDays.toString());

                    state.calvingDate.text = SplitDate.splitNepali(convertedCalvingDate);
//                    state.calvingDate.text = convertedCalvingDate.toIso8601String().split("T").first;
                  } else if (animal.speciesName == "Cattle") {
                    DateTime aiCattleDate = DateTime.tryParse(ai.englishAiDate);
                    Logger.v(
                        "aiDate Display: " + aiCattleDate.toIso8601String());

                    DateTime todayCattleDate = new DateTime(aiCattleDate.year,
                        aiCattleDate.month, aiCattleDate.day + 285);

                    NepaliDateTime convertedCalvingDate = DateConverter.toBS(todayCattleDate);
//                    String todayNepaliCattleDate =
//                        DateFormat('yyyy-MM-dd').format(todayCattleDate);
                    state.calvingDate.text = convertedCalvingDate.toIso8601String().split("T").first;
                  } else if (animal.speciesName == "Goat") {
                    DateTime aiGoatDate = DateTime.tryParse(ai.englishAiDate);
                    Logger.v("aiDate Display: " + aiGoatDate.toIso8601String());

                    DateTime todayGoatDate = new DateTime(aiGoatDate.year,
                        aiGoatDate.month, aiGoatDate.day + 145);
//                    String todayNepaliGoatDate =
//                        DateFormat('yyyy-MM-dd').format(todayGoatDate);
                    NepaliDateTime convertedCalvingDate = DateConverter.toBS(todayGoatDate);
                    state.calvingDate.text = convertedCalvingDate.toIso8601String().split("T").first;
                  } else if (animal.speciesName == "Pig") {
                    DateTime aiPigDate = DateTime.tryParse(ai.englishAiDate);
                    Logger.v("aiDate Display: " + aiPigDate.toIso8601String());

                    DateTime todayPigDate = new DateTime(
                        aiPigDate.year, aiPigDate.month, aiPigDate.day + 118);
//                    String todayNepaliPigDate =
//                        DateFormat('yyyy-MM-dd').format(todayPigDate);
                    NepaliDateTime convertedCalvingDate = DateConverter.toBS(todayPigDate);
                    state.calvingDate.text = convertedCalvingDate.toIso8601String().split("T").first;
                  }
                }
              });
            } else {
              new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                Navigator.of(state.context).pop();
              });
            }
          });
        });
      }

      //

      Logger.v('inside ' + this.state.isEdit.toString());
    }
  }

  savePD() async {
    speciesId = animal.speciesId;
    breedId = animal.breedId;
    if (state.selected < 0) {
      this.state.showToast("Please select report");
    }else if(state.speciesController.text =="Goat" || state.speciesController.text =="Pig" ){
      this.state.showToast("PD can't be carried out for Goat and Pig");

    } else {
      this
          .pdBloc
          .savePD(this.state.context, this.pd, this.farmer, this.animal,
              speciesId, breedId)
          .listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          ToastUtils.show(it.data);
//          state.setState(() {
//            Navigator.pop(this.state.context);
//          });
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(this.state.context).pop();
//        });
        }
      });
    }
  }

  updatePD() async {
    this
        .pdBloc
        .updatePD(this.state.context, this.pd, this.farmer, this.animal)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        Navigator.pop(this.state.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  selectedReport(int value) {
    state.setState(() {
      state.selected = value;
    });
  }

  selectedPositive() {
    if (state.selected == 1) {
      return isVisible = false;
    } else {
      return isVisible = true;
    }
  }

  //date picker

  String calvingDate;
  String pdDate;

//  Future<Null> showDatePickers() async {
//    final DateTime picked = await showDatePicker(
//        context: state.context,
//        initialDate: DateTime.now(),
//        firstDate: DateTime(2010),
//        lastDate: DateTime(2100));
//
//    if (picked != null) {
//      state.setState(() {
//        DateTime dates = picked;
//        calvingDate = DateFormat('yyyy-MM-dd').format(picked);
//
//        state.setState(() {
//          state.tfCalvingDate.controller.text = calvingDate.toString();
//        });
//      });
//    }
//  }

  showDatePickers()async {

    state.selectedDateTime = await showNepaliDatePicker(
      context: state.context,
      initialDate: NepaliDateTime.now(),
      firstDate: NepaliDateTime(2050),
      lastDate: NepaliDateTime(2090),
      language: state.language,
    );
    state.setState(() {
      if (state.selectedDateTime != null){
        state.calvingDate.text =state.selectedDateTime.toIso8601String().split("T").first;

      }else{
        state.calvingDate.text ="";

      }
    });
//    NepaliDatePicker.showPicker(
//        context: state.context,
//        startYear: 2052,
//        endYear: 2100,
//        color: Colors.blue,
//        barrierDismissible: false,
//        onPicked: (DateTime picked) {
//          if (picked != null) {
//            state.setState(() {
//              DateTime dates = picked;
//              state.setState(() {
//                state.tfCalvingDate.controller.text =
//                    picked.toIso8601String().split("T").first;
//              });
//            });
//          }
//        });
  }

//  Future<Null> showDatePickerPD() async {
//    final DateTime picked = await showDatePicker(
//        context: state.context,
//        initialDate: DateTime.now(),
//        firstDate: DateTime(2010),
//        lastDate: DateTime(2100));
//
//    if (picked != null) {
//      state.setState(() {
//        DateTime dates = picked;
//
//        pdDate = DateFormat('yyyy-MM-dd').format(picked);
//        state.setState(() {
//          state.tfDatePD.controller.text = pdDate.toString();
//        });
//      });
//    }
//  }

  showDatePickerPD()async {

    state.selectedDateTime = await showNepaliDatePicker(
      context: state.context,
      initialDate: NepaliDateTime.now(),
      firstDate: NepaliDateTime(2050),
      lastDate: NepaliDateTime(2100),
      language: state.language,
    );
    state.setState(() {
      if (state.selectedDateTime != null){
        state.pdDateController.text =state.selectedDateTime.toIso8601String().split("T").first;

      }else{
        state.pdDateController.text ="";

      }
    });

  }

  getFarmerByMobileNumber(String mblNumber) async {
    this
        .farmerBloc
        .getFarmerByMobileNumber(this.state.context, mblNumber)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        farmer = it.data;

        // ToastUtils.show(it.data.toString());
        state.tfFarmer.controller.text = it.data.farmerName.toString();
        state.tfAddress.controller.text = it.data.municipality.toString();
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfFarmer.controller.text = "";
        state.tfAddress.controller.text = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }

  //get animal

  getAnimalBySpeciesCode(String speciesCode) async {
    this
        .animalBloc
        .getAnimalBySpeciesCode(this.state.context, speciesCode)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        animal = it.data;
        // ToastUtils.show(it.data.toString());
        state.tfSpecies.controller.text = it.data.speciesName.toString();
        state.tfBreed.controller.text = it.data.breedName.toString();
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfSpecies.controller.text = "";
        state.tfBreed.controller.text = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }
}
