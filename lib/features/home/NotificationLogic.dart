
import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/notificationBloc/NotificationBloc.dart';
import 'package:nbis/features/home/NotificationPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';
//import 'package:nbis/s/DsonUtils.dart';
//import 'package:nbis/s/Logger.dart';

class NotificationLogic{
  NotificationPageState state;
  NotificationBloc notificationBloc;
  List<NotificationRequest> notification = [];
  static NotificationRequest notificationRequest = NotificationRequest();

  NotificationLogic(this.state, this.notificationBloc);


  void onPostInit() {
    getNotificationList();
  }

  getNotificationList() {
    this.notificationBloc.getNotificationList().listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        this.state.setState(() {
          this.notification = it.data;
//          Logger.v("getNotification"+DsonUtils.toJsonString(notification));
//           distinctNotification = notification.toSet().toList();
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

}
