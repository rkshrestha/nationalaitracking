import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/pdBloc/PDBloc.dart';
import 'package:nbis/features/home/PDPage.dart';
import 'package:nbis/features/home/PDPageList.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class PDPageListLogic {
  PDPageListState state;
  PDBloc pdBloc;
  List<PregnancyDiagnosis> pdList = [];
  List<PregnancyDiagnosis> pdUnSyncList = [];
  List<PregnancyDiagnosis> pdSyncList = [];
  static PregnancyDiagnosis pd = PregnancyDiagnosis();
  String results;

  PDPageListLogic(this.state, this.pdBloc);

  void onPostInit(Animal animal) {
    if (animal != null) {
      getPdListByAnimal(animal);
//      getUnSyncPdListByAnimal(animal);
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//        getSyncPdListByAnimal(animal);
//      });
    } else {
      getPDList();
//      getPDUnSyncList();
//      new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
//        getPDSyncList();
//      });
    }
  }

  getPDData() {
    if (state.pd.pdResult = true) {
      return results = "Positive";
    }else if(state.pd.pdResult = false){
      return results= "Negative";
    }
  }

  getPdListByAnimal(animal) {
    this.pdBloc.getPDListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.pdList = it.data;
            });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncPdListByAnimal(animal) {
    this.pdBloc.getUnSyncPDListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.pdUnSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncPdListByAnimal(animal) {
    this.pdBloc.getSyncPDListByAnimal(animal).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v(" list is " + it.data.toString());
        this.state.setState(() {
          this.pdSyncList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getPDList() {
    this.pdBloc.getPdList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("pd list is " + it.data.toString());
        this.state.setState(() {
          this.pdList = it.data;

        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }


  getPDUnSyncList() {
    this.pdBloc.getUnSyncPdList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("pd list is " + it.data.toString());
        this.state.setState(() {
          this.pdUnSyncList = it.data;

        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }


  getPDSyncList() {
    this.pdBloc.getSyncPdList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("pd list is " + it.data.toString());
        this.state.setState(() {
          this.pdSyncList = it.data;

        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }


  onEditPD(PregnancyDiagnosis pd, int position)async {
    Logger.v('get Data:' + pdList[position].pdResult.toString());
  await  Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return PDPage(pd: pd, isEdit: true);
    }));
    state.setState(() {
      state.refreshPDList();
    });
  }

  onAddPD(Animal animal)async {
   await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return PDPage(animal: animal);

    }));

    state.setState(() {
      state.refreshPDList();
    });
  }

  void confirmationDelete(PregnancyDiagnosis pd,Animal animal, int position) {
    DialogUtils.showAlertDialog(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "Are you sure,you want to delete?", (it) {
//        AppTranslations.of(state.context).text("delete_warning"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.pop(this.state.context, true);

            if (animal != null) {
              getPdListByAnimal(animal);

            } else {
              getPDList();
            }

            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Logger.v('position ' + position.toString());

            this.pdBloc.deletePD(this.state.context,pd).listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {
//                state.setState(() {
//                  pd.syncToServer == true
//                      ? this.pdSyncList.removeAt(position)
//                      : this.pdUnSyncList.removeAt(position);
//                });

//                state.setState(() {
//                       this.pdList.removeAt(position);
//                });

                Navigator.pop(this.state.context, true);
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(state.context).pop();
                });
              }
            });

            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }
}
