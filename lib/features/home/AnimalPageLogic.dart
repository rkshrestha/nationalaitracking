import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/UserBloc/UserBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/AnimalPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/Breed.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/Species.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class AnimalPageLogic {
  AnimalPageState state;
  AnimalBloc animalBloc;
  FarmerBloc farmerBloc;
  UserBloc userBloc;
  List<Species> speciesList = [];
  List<Breed> breedList = [];

  // User user = User();
  Animal animal = Animal();
  Farmer farmer = Farmer();
  Species selectedSpecies;
  Breed selectedBreed;
  int i;
  int j;
  int speciesIds;

  String farmerId;

  AnimalPageLogic(this.state, this.userBloc, this.animalBloc, this.farmerBloc);

  //  AnimalPageLogic(AnimalPageState state, UserBloc userBloc, AnimalBloc animalBloc,
//      FarmerBloc farmerBloc) {
//    this.state = state;
//    this.animalBloc = animalBloc;
//    this.farmerBloc = farmerBloc;
//    this.userBloc = userBloc;
//    this.user = this.userBloc.getSession().user;
//  }

  void onPostState() {
    if (this.state.isEdit) {
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
        this.state.setAppBar('Edit Animals');
//        this.state.setAppBar(
//          AppTranslations.of(state.context).text("animal_form_edit"),
//        );
//      });
      this.animal = state.animal;
      Logger.v('inside ' + this.state.isEdit.toString());
//      new Future.delayed(new Duration(milliseconds: 2000)).then((_) {
      state.mblController.text =
      this.animal == null ? "" : this.animal.mobileNumber;
//      state.mblController.text =
//      animal == null ? "" : animal.speciesCode.split("-")[0];
//      });
      state.aCodeController.text =
      this.animal == null ? "" : this.animal.speciesCode;
//          this.animal == null ? "" : this.animal.speciesCode.split("-")[1];
      this.animalBloc.getSpeciesList().listen((it) {
        if (it == null) return;
        Logger.v("inside listener it");
        if (it.status == Status.SUCCESS) {
          Logger.v("Species list is " + it.data.toString());
          this.state.setState(() {
            this.speciesList = it.data;

            for (Species species in speciesList) {
              if (species.name.contains(animal.speciesName)) {
                state.setState(() {
                  Logger.v("Species: " + animal.speciesName);
                  var stateNameFromList = species.name;
                  Logger.v("stateNamefromList: " + stateNameFromList);
                  i = speciesList.indexOf(species);
                  speciesIds = species.ids;
                  //display in dropdown
                  selectedSpecies = speciesList[i];
                });
                Logger.v("speciesIds" + speciesIds.toString());

                this.animalBloc.getBreedList(speciesIds).listen((it) {
                  if (it == null) return;
                  if (it.status == Status.SUCCESS) {
                    Logger.v("Breed list is " + it.data.toString());
                    this.state.setState(() {
                      this.breedList = it.data;

                      //display breed for edit
                      for (Breed breeds in breedList) {
                        if (breeds.name.contains(animal.breedName)) {
                          Logger.v("Breedsss: " + animal.breedName);
                          state.setState(() {
                            var stateNamebreed = breeds.name;
                            Logger.v("stateNamefromList: " + stateNamebreed);
                            j = breedList.indexOf(breeds);
                            //display in dropdown
                            selectedBreed = breedList[j];
                          });
                        }
                      }
                    });
                  }
                });
              }
            }
          });
        }
      });
    } else {
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//          AppTranslations.of(state.context).text("animal_form_add_title"),
//        );
        this.state.setAppBar('Add Animals');
//      });
      Logger.v('inside ' + this.state.isEdit.toString());
    }



        new Future.delayed(new Duration(milliseconds: 1000)).then((_) {
    getSpeciesList();
    });
  }

  bool onValid() {
    if (selectedSpecies == null) {
//      ToastUtils.show(
//          AppTranslations.of(state.context).text("species_validate"));
      ToastUtils.show(" Please select Animal !!!");
      return false;
    } else if (selectedBreed == null) {
//      ToastUtils.show(AppTranslations.of(state.context).text("breed_validate"));
      ToastUtils.show("Please select Breed!");
      return false;
    }

    return true;
  }

  saveAnimal() async {
    if (onValid()) {
      this
          .animalBloc
          .saveAnimal(this.state.context, this.animal, this.farmer,
          selectedSpecies, selectedBreed)
          .listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          ToastUtils.show(it.data);
//          state.setState((){
//            Navigator.pop(this.state.context);
//
//          });

        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
//          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//            Navigator.of(this.state.context).pop();
//          });
        }
      });
    }
  }

  updateAnimal() async {
    this
        .animalBloc
        .updateAnimal(this.state.context, this.animal, this.farmer,
        selectedSpecies, selectedBreed)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        Navigator.pop(this.state.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getFarmerByMobileNumber(String mblNumber) async {
    this
        .farmerBloc
        .getFarmerByMobileNumber(this.state.context, mblNumber)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        farmer = it.data;

        Logger.v('getFarmerId: ' + it.data.fId.toString());
        state.tfFarmer.controller.text =
        it.data == null ? "" : it.data.farmerName;

//        farmerId = it.data.fId;
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfFarmer.controller.text = "";
      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(this.state.context).pop();
//        });
      }
    });
  }

  //check for mobile and farmer by id
  getFarmerMobileNumberById(String fId) async {
    this.farmerBloc.getFarmerMobileNumberById(fId).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        farmer = it.data;

        //   Logger.v('getFarmerId: ' + it.data.fId.toString());
        Logger.v('clicked getFarmerMobileNumberById: ' +
            it.data.mobileNumber.toString());
        // ToastUtils.show(it.data.toString());
        state.mblController.text = it.data.mobileNumber.toString();
        state.tfFarmer.controller.text = it.data.farmerName.toString();
        farmerId = it.data.fId;

        //Navigator.pop(this.state.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }

  //radio selective button
  selectedAnimal(int value) {
    state.setState(() {
      state.selected = value;
    });
  }

//  onDelayed() {
//    new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//      syncSpeciesList();
//    });
//  }

  getSpeciesList() {
    this.animalBloc.getSpeciesList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("Species list is " + it.data.toString());
        this.state.setState(() {
          this.speciesList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getBreedList(int speciesId) {
    this.animalBloc.getBreedList(speciesId).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("Breed list is " + it.data.toString());
        this.state.setState(() {
          this.breedList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }
}
