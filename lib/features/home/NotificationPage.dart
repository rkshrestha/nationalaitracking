//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/notificationBloc/NotificationBloc.dart';
import 'package:nbis/features/home/SyncServerPageLogic.dart';
import 'package:nbis/features/repo/AIRepo.dart';
import 'package:nbis/features/repo/NotificationRepo.dart';
import 'package:nbis/features/repo/NotificationRequest.dart';

import 'NotificationLogic.dart';

class NotificationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationPageState();
  }
}

class NotificationPageState extends State<NotificationPage> {
//  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
  final List<NotificationRequest> messages = [];
  SyncServerPageLogic logic;
  NotificationLogic notificationLogic;
  AIBloc aiBloc;
  NotificationBloc notificationBloc;
  var refreshNotificationListKey = GlobalKey<RefreshIndicatorState>();
  BuildContext context;
  NotificationRequest nr;

  @override
  void initState() {
    super.initState();
    this.aiBloc = AIBloc(AIRepo(context));
    notificationBloc = NotificationBloc(NotificationRepo(context));
    notificationLogic = NotificationLogic(this, notificationBloc);
    this.logic = SyncServerPageLogic.makes(aiBloc);
    refreshNotificationList();


//    firebaseMessaging.configure(
//      onMessage: (Map<String, dynamic> message) {
//        print('on message $message');
////        final notification = message['notification'];
//        final notification = message['data'];
//        setState(() {
//          messages.add(NotificationRequest.make(
//              title: notification['title'], body: notification['body']));
//        });
//      },
//      onResume: (Map<String, dynamic> message) {
//        print('on resume $message');
//        final notification = message['data'];
//        setState(() {
//          messages.add(NotificationRequest.make(
//              title: notification['title'], body: notification['body']));
//        });
//      },
//      onLaunch: (Map<String, dynamic> message) {
//        print('on launch $message');
//
//        final notification = message['data'];
//        setState(() {
//          messages.add(NotificationRequest.make(
//              title: notification['title'], body: notification['body']));
//        });
//      },
//    );
//    firebaseMessaging.requestNotificationPermissions(
//        const IosNotificationSettings(sound: true, badge: true, alert: true));
//    firebaseMessaging.getToken().then((token) {
//      print(token);
//    });
  }

//  update(String token) {
//    print(token);
//    DatabaseReference databaseReference = new FirebaseDatabase().reference();
//    databaseReference.child('fcm-token/${token}').set({"token": token});
////    textValue = token;
//    setState(() {});
//  }

  Future<Null> refreshNotificationList() async {
    refreshNotificationListKey.currentState?.show(atTop: false);
//    await Future.delayed(Duration(seconds: 1));

    setState(() {
      this.notificationLogic.onPostInit();
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Notification"),
        ),
////      body: MessagingWidget(),
//      body: ListView(
//        shrinkWrap: true,
//        children: []..addAll(messages.map(buildMessage).toList()),
//      ),

        body: BlocProvider(
            child: SafeArea(
          child: Column(children: <Widget>[
            Expanded(
              child:
                  this.notificationLogic.notification.isEmpty ? noData() : getNotificationList(),
            ),
          ]),
        )));
  }

//  Widget buildMessage(NotificationRequest request) => Card(
//        elevation: 2,
//        child: ListTile(
//          title: Text(request.title == null ? "" : request.title),
//          subtitle: Text(request.body == null ? "" : request.body),
//        ),
//      );

  //list

  Widget noData() {
    return Container(
      child: Center(
        child: Text("No any notification"),
//        child: Text("No Artificial Insemination Data has been found."),
      ),
    );
  }

  ListView getNotificationList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: notificationLogic.notification.length,
        itemBuilder: (BuildContext context, int position) {
          nr = this.notificationLogic.notification[position];

          return Container(
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
//                leading: Container(
//                  width: 30.0,
//                  height: 30.0,
//                  child: nr.syncToServer == true
//                      ? Image.asset(
//                    "images/xxhdpi/pregnancydiagnosis.png",
//                    color: Colors.green,
//                  )
//                      : Image.asset(
//                    "images/xxhdpi/pregnancydiagnosis.png",
//                    color: Colors.red,
//                  ),
//                ),
//                leading: Container(
//                    width: 40.0,
//                    height: 40.0,
//                    decoration: new BoxDecoration(
//                        color: Colors.white,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.fill,
//                            image: new AssetImage(
//                                "images/xxhdpi/pregnancydiagnosis.png")))),

                title: Text("Tag: "+ nr.title),
                subtitle: Text(nr.body),
//                trailing: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditAI(this.logic.aiList[position], position);
//                  },
//                ),
//                onTap: () {
//                  this.logic.onEditAI(this.logic.aiList[position], position);
//                },
              ),
            ),
          );
        });
  }
}
