import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/nitrogenBloc/NitrogenBloc.dart';
import 'package:nbis/features/home/NitrogenPageListLogic.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/features/repo/LiquidNitrogenRepo.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class NitrogenPageList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NitrogenPageListState();
  }
}

class NitrogenPageListState extends State<NitrogenPageList> {
  NitrogenPageListLogic logic;
  NitrogenBloc nitrogenBloc;
  LiquidNitrogen nitrogen;
  var refreshNitrogenListKey = GlobalKey<RefreshIndicatorState>();

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  void initState() {
    super.initState();
    nitrogenBloc = NitrogenBloc(LiquidNitrogenRepo(context));
    logic = NitrogenPageListLogic(this, nitrogenBloc);
//    logic.onPostInit();
    refreshNitrogenList();
  }

  Future<Null> refreshNitrogenList() async {
    refreshNitrogenListKey.currentState?.show(atTop: false);
//    await Future.delayed(Duration(seconds: 1));

    setState(() {
      this.logic.onPostInit();
    });

    return null;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    this
        .logic
        .nitrogenList
        .sort((a, b) => b.receivedDate.compareTo(a.receivedDate));
    final tabSync = <Tab>[
      Tab(text: "Not Sync", icon: new Icon(Icons.sync)),
      Tab(text: " Sync", icon: new Icon(Icons.sync))
    ];

    return DefaultTabController(
      length: tabSync.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
          "Liquid Nitrogen",
//            AppTranslations.of(context).text("nitrogen_registered_title"),
          ),
//          bottom: TabBar(tabs: tabSync),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 45.0,
            color: Theme.of(context).primaryColor,
            child: GestureDetector(
              onTap: () {
                logic.onAddNitrogen();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  Divider(
                    indent: 2.0,
                  ),
                  Text(
//                          AppTranslations.of(context)
//                              .text("nitrogen_register_button"),
                    "Add Liquid Nitrogen",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),

                ]),

//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 3,
//                    child: Text(''),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: Row(
//                      children: <Widget>[
//                        Icon(
//                          Icons.add,
//                          color: Colors.white,
//                        ),
//                        Divider(
//                          indent: 2.0,
//                        ),
//                        Text(
////                          AppTranslations.of(context)
////                              .text("nitrogen_register_button"),
//                        "Add Liquid Nitrogen",
//                          style: TextStyle(
//                              color: Colors.white, fontWeight: FontWeight.bold),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Text(''),
//                  ),
//                ],
//              ),
            ),
          ),
        ),
        body: BlocProvider(
          child: SafeArea(
            child: Column(
              children: <Widget>[
//                Expanded(
//                  child: TabBarView(
//                    children: <Widget>[
//                      logic.unSyncNitrogenList.isEmpty
//                          ? noData()
//                          : getUnSyncNitrogenList(),
//                      logic.syncNitrogenList.isEmpty
//                          ? noData()
//                          : getSyncNitrogenList(),
//                    ],
//                  ),
//                ),

                Expanded(
                  child: RefreshIndicator(
                    key: refreshNitrogenListKey,
                    child: this.logic.nitrogenList.isEmpty
                        ? noData()
                        : getNitrogenList(),
                    onRefresh: refreshNitrogenList,
                  ),
                ),
                //  getPropertyList(),
              ],
            ),
          ),
        ),
//      floatingActionButton: Padding(
//        padding: EdgeInsets.only(bottom: 1.0),
//        child: FloatingActionButton(
//            child: Icon(Icons.add),
//            tooltip: 'Add Nitrogen',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              logic.onAddNitrogen();
//            }),
//      ),

//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//      floatingActionButton: Column(
//        crossAxisAlignment: CrossAxisAlignment.stretch,
//        mainAxisSize: MainAxisSize.min,
//        children: <Widget>[
//          FloatingActionButton.extended(
//            isExtended: true,
//            label: Text("Add Nitrogen"),
//            icon: Image.asset("images/hdpi/add.png",color: Colors.white,),
//            tooltip: 'Add Nitrogen',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              this.logic.onAddNitrogen();
//            },
//          ),
//        ],
//      ),
      ),
    );
  }

  Widget noData() {
    return Container(
      child: Center(
//        child: Text(
//          AppTranslations.of(context).text("nitrogen_noData"),
//        ),
        child: Text("No Liquid Nitrogen Data has been found !!"),
      ),
    );
  }

  //list

  ListView getNitrogenList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.nitrogenList.length,
        itemBuilder: (BuildContext context, int position) {
          nitrogen = this.logic.nitrogenList[position];

          editButton() {
            DateTime lastestDate = DateTime.now();


            if (nitrogen.syncDate != null) {
//              String nitrogenSyncDate = nitrogen.syncDate==null?"":nitrogen.syncDate;
              String nitrogenSyncDate = nitrogen.syncDate==null?"":nitrogen.syncDate.split(".").first;
              DateTime syncDate = DateTime.tryParse(nitrogenSyncDate);
              DateTime addedDate =syncDate.add(new Duration(hours: 24));
              if (lastestDate.compareTo(addedDate)==1) {
//              if (lastestDate.compareTo(addedDate)==1 && nitrogen.syncToServer == true) {
//              if (addedDate.difference(syncDate).inHours >= 24 && nitrogen.syncToServer == true) {
//              if (syncDate.add(new Duration(hours: 24)) == lastestDate && nitrogen.syncToServer == true) {
                return Container(
                  child: Text(""),
                );
              } else {
                return GestureDetector(
                  child: Container(
                    width: 40.0,
                    height: 40.0,
                    child: Card(
                      color: Colors.white70,
                      elevation: 3.0,
                      child: Image.asset(
                        "images/edits.png",
                      ),

                    ),

                  ),
                  onTap: () {
                    logic.onEditNitrogen(this.logic.nitrogenList[position]);
                  },
                );



//                return Container(
//                  child: GestureDetector(
//                    child: Icon(
//                      Icons.edit,
//                      size: 25.0,
//                      color: Colors.black,
//                    ),
//                    onTap: () {
//                      logic.onEditNitrogen(this.logic.nitrogenList[position]);
//                    },
//                  ),
//                );
              }
            } else {

              return GestureDetector(
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: Card(
                    color: Colors.white70,
                    elevation: 3.0,
                    child: Image.asset(
                      "images/edits.png",
                  ),

                ),

              ),
                onTap: () {
            logic.onEditNitrogen(this.logic.nitrogenList[position]);
            },
              );



//              return Container(
//                child: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 25.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    logic.onEditNitrogen(this.logic.nitrogenList[position]);
//                  },
//                ),
//              );
            }
          }

          if(nitrogen.syncDate !=null){
//          if(nitrogen.syncToServer ==true){
            return Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 30.0,
                    height: 30.0,
                    child: nitrogen.syncToServer == true
                        ? Image.asset(
                      "images/kit.png",
//                      "images/xxhdpi/liquidnitrogen.png",
                      color: Colors.green,
                    )
                        : Image.asset(
                      "images/kit.png",
//                      "images/xxhdpi/liquidnitrogen.png",
                      color: Colors.red,
                    ),
                  ),

                  title: Text("Received Date: "+  nitrogen.receivedDate),
                  subtitle: Text(
                      "Quantity : " + nitrogen.quantity.toString() + " litre"),
                  trailing: editButton(),

                ),
              );
          }else{
            return Dismissible(
              key: Key(this.logic.nitrogenList[position].lnId.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (DismissDirection direction) {
                LiquidNitrogen nitrogens = this.logic.nitrogenList[position];

                setState((){
                  this.logic.nitrogenList.removeAt(position);
                  this.logic.confirmationDelete(nitrogens, position);

                });
              },
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 30.0,
                    height: 30.0,
                    child: nitrogen.syncToServer == true
                        ? Image.asset(
                      "images/kit.png",
//                      "images/xxhdpi/liquidnitrogen.png",
                      color: Colors.green,
                    )
                        : Image.asset(
                      "images/kit.png",
//                      "images/xxhdpi/liquidnitrogen.png",
                      color: Colors.red,
                    ),
                  ),
//                leading: Image.asset(
//                  "images/xxhdpi/liquidnitrogen.png",
//                  width: 30.0,
//                  height: 30.0,
//                ),
                  title: Text("Received Date: "+ nitrogen.receivedDate),
                  subtitle: Text(
                      "Quantity : " + nitrogen.quantity.toString() + " litre"),
                  trailing: editButton(),
//                onTap: () {
//                  logic.onEditNitrogen(this.logic.nitrogenList[position]);
//                },
                ),
              ),
            );
          }


        });
  }

  ListView getUnSyncNitrogenList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.unSyncNitrogenList.length,
        itemBuilder: (BuildContext context, int position) {
          nitrogen = this.logic.unSyncNitrogenList[position];
          return Dismissible(
            key: Key(this.logic.unSyncNitrogenList[position].lnId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              LiquidNitrogen nitrogens =
                  this.logic.unSyncNitrogenList[position];
              this.logic.confirmationDelete(nitrogens, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Image.asset(
                  "images/xxhdpi/liquidnitrogen.png",
                  width: 30.0,
                  height: 30.0,
                ),
                title: Text(nitrogen.receivedDate),
                subtitle: Text(
                    "Quantity : " + nitrogen.quantity.toString() + " litre"),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.edit,
                    size: 25.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    logic.onEditNitrogen(
                        this.logic.unSyncNitrogenList[position]);
                  },
                ),
//                onTap: () {
//                  logic.onEditNitrogen(this.logic.nitrogenList[position]);
//                },
              ),
            ),
          );
        });
  }

  //sync
  ListView getSyncNitrogenList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.syncNitrogenList.length,
        itemBuilder: (BuildContext context, int position) {
          nitrogen = this.logic.syncNitrogenList[position];
          return Dismissible(
            key: Key(this.logic.syncNitrogenList[position].lnId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
              LiquidNitrogen nitrogens = this.logic.syncNitrogenList[position];
              this.logic.confirmationDelete(nitrogens, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Image.asset(
                  "images/xxhdpi/liquidnitrogen.png",
                  width: 30.0,
                  height: 30.0,
                ),
                title: Text(nitrogen.receivedDate),
                subtitle: Text(
                    "Quantity : " + nitrogen.quantity.toString() + " litre"),
//                trailing: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 25.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    logic.onEditNitrogen(this.logic.syncNitrogenList[position]);
//                  },
//                ),
//                onTap: () {
//                  logic.onEditNitrogen(this.logic.nitrogenList[position]);
//                },
              ),
            ),
          );
        });
  }
}
