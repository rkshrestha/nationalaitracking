import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class AboutPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AboutStatePage();
  }
}

class AboutStatePage extends State<AboutPage> {
  String appName;
  String packageName;
  String version;
  String buildNumber;

  package() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    appName = packageInfo.appName;
    packageName = packageInfo.packageName;
    version = packageInfo.version;
    buildNumber = packageInfo.buildNumber;
  }

  @override
  void initState() {
    super.initState();
    new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
      setState((){
        package();

      });
    });


  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About"),
//        title: Text( AppTranslations.of(context).text("tab_about")),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 55.0,
//          color: Theme.of(context).primaryColor,
//        color:Colors.green,
          child: GestureDetector(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Text(''),
                ),
                Expanded(
                  flex: 6,
                  child: Image.asset(
                    'images/pathway_logo.png',
//                    width: 50.0,
//                    height: 50.0,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(''),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        child: Center(
          child: Text("App Version: " + version.toString()),
        ),
      ),
    );
  }
}
