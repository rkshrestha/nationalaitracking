import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/AIPageLogic.dart';
import 'package:nbis/features/repo/AIRepo.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/ArtificialInsemination.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nepali_utils/nepali_utils.dart';

class AIPage extends StatefulWidget {
  bool isEdit;
  Farmer farmer;
  Animal animal;
  ArtificialInsemination ai;

  AIPage({this.ai, this.farmer, this.animal, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return AIPageState(
        ai: this.ai,
        farmer: this.farmer,
        animal: this.animal,
        isEdit: this.isEdit);
  }
}

class AIPageState extends State<AIPage> {
  AIPageLogic logic;
  AIBloc aiBloc;
  bool isEdit;
  AnimalBloc animalBloc;
  FarmerBloc farmerBloc;
  ArtificialInsemination ai;
  TextFormField tfDateAI;
  TextFormField tfTentativeDatePD;
  TextFormField tfMobileNumber;
  TextFormField tfFarmer;
  TextFormField tfAddress;
  TextFormField tfSpeciesCode;
  TextFormField tfSpecies;
  TextFormField tfBreed;
  AppBar appBar;
  Farmer farmer;
  Animal animal;
  TextEditingController dateAIController = TextEditingController();
  TextEditingController tentativePdDateController = TextEditingController();
  TextEditingController mblController = TextEditingController();
  TextEditingController farmerController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController aCodeController = TextEditingController();
  TextEditingController speciesController = TextEditingController();
  TextEditingController breedController = TextEditingController();
  FocusNode textFocus = new FocusNode();
  FocusNode textFocusAnimal = new FocusNode();

  NepaliDateTime selectedDateTime;
  Language language = Language.ENGLISH;

  AIPageState({this.ai, this.farmer, this.animal, this.isEdit = false});

  GlobalKey<FormState> aiFormKey = GlobalKey<FormState>();

//  static var farmers = ['Select Farmer', 'Abd', 'cde'];
//  static var parity = ['Select Current Parity', 'Heifer', '1', '2', '3'];
//  static var aiSerial = ['Select AI Serial', '1', '2', '3', '4'];

  void onNumberChanged() {
    String mobileText = mblController.text;
//    bool hasFocus = textFocus.hasFocus;
    Logger.v(mobileText);
    setState(() {
      mblController.selection = new TextSelection(
          baseOffset: mobileText.length, extentOffset: mobileText.length);
    });

    if (mobileText.length == 10) {
      this.logic.getFarmerByMobileNumber(mobileText);
    }
  }

  void onDateChanged() {
    String date = dateAIController.text;
//    bool hasFocus = textFocus.hasFocus;
    setState(() {
      dateAIController.selection =
          new TextSelection(baseOffset: date.length, extentOffset: date.length);
    });

    this.logic.tentativePdDate(date);
  }

  void onAnimalChanged() {
    String animalText = aCodeController.text;
    Logger.v('getCOde:' + aCodeController.text);
//    bool hasFocus = textFocusAnimal.hasFocus;
    Logger.v(animalText);

    setState(() {
      aCodeController.selection = new TextSelection(
          baseOffset: animalText.length, extentOffset: animalText.length);
    });

    this.logic.getAnimalBySpeciesCode(animalText);
  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  @override
  void initState() {
    super.initState();
    this.aiBloc = AIBloc(AIRepo(context));
    this.animalBloc = AnimalBloc(AnimalRepo(context));
    this.farmerBloc = FarmerBloc(FarmerRepo(context));
    this.logic = AIPageLogic(this, aiBloc, farmerBloc, animalBloc);
//    mblController.text = farmer == null ? "" : farmer.mobileNumber;

    mblController.text = animal == null ? "" : animal.mobileNumber;
    aCodeController.text = animal == null ? "" : animal.speciesCode;
    logic.onPostState();
    initUIController();

//
    if (animal != null) {
      Logger.v("animal" + animal.speciesCode);
      Logger.v("farmer number: " + animal.mobileNumber);

      if (animal.mobileNumber.length == 10) {
        Logger.v("farmer number inside: " + animal.mobileNumber);
        this.logic.getFarmerByMobileNumber(animal.mobileNumber);
      }
//      setState((){
//
//      });
      this.logic.getAnimalBySpeciesCode(animal.speciesCode);
    }

//
  }

  void initUIController() {
    mblController.addListener(onNumberChanged);
    textFocus.addListener(onNumberChanged);
    aCodeController.addListener(onAnimalChanged);
    textFocusAnimal.addListener(onNumberChanged);
    dateAIController.addListener(onDateChanged);
  }

  @override
  void dispose() {
    mblController.removeListener(onNumberChanged);
    aCodeController.removeListener(onAnimalChanged);
    dateAIController.removeListener(onDateChanged);
    mblController.dispose();
    aCodeController.dispose();
    dateAIController.dispose();
    textFocus.dispose();
    textFocusAnimal.dispose();
    super.dispose();
  }

  Widget getAIForm() {
    ArtificialInsemination ai = this.logic.ai;
    //farmer dropdown
//    DropdownButton dbFarmer = DropdownButton(
//        items: farmers.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        value: 'Select Farmer',
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

    //farmer name tf
    tfFarmer = TextFormField(
        controller: farmerController,
        enabled: false,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Farmer's Name",
//          labelText: AppTranslations.of(context).text("ai_form_farmer_name"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String farmer) {
          ai.farmerName = farmer;
        },
        validator: (farmer) {
          if (farmer.isEmpty) {
            return "Farmer Name can't be empty";
          }
        });

    //address tf
    tfAddress = TextFormField(
        enabled: false,
        controller: addressController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Address",
//          labelText: AppTranslations.of(context).text("ai_form_address"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String address) {},
        validator: (address) {
          if (address.isEmpty) {
            return "Address can't be empty";
          }
        });

    //animal code

    tfSpeciesCode = TextFormField(
        controller: aCodeController,
        focusNode: textFocusAnimal,
        keyboardType: TextInputType.number,
//        autofocus: true,

        decoration: InputDecoration(
          labelText: "Animal Code",
//          labelText:  AppTranslations.of(context).text("ai_form_code"),
          prefixText: mblController.text + "-",
        ),
        onSaved: (String animalsCode) {
//          String animalCode = mblController.text + "-" + animalsCode;
          ai.speciesCode = animalsCode;
        },
        validator: (animalCode) {
          if (animalCode.isEmpty) {
//            return AppTranslations.of(context).text("code_validate");
            return " Animal code should not be empty !!";
          }
        });

    //species tf
    tfSpecies = TextFormField(
        controller: speciesController,
        enabled: false,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Animal",
//          labelText: AppTranslations.of(context).text("ai_form_species"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String species) {},
        validator: (species) {
          if (species.isEmpty) {
            return "Species can't be empty";
          }
        });

    //breed
    tfBreed = TextFormField(
        controller: breedController,
        enabled: false,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Breed",
//          labelText:  AppTranslations.of(context).text("ai_form_breed"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String breed) {},
        validator: (breed) {
          if (breed.isEmpty) {
            return "Breed can't be empty";
          }
        });

    //ai date

    tfDateAI = TextFormField(
        keyboardType: TextInputType.number,
        controller: dateAIController,
        decoration: InputDecoration(
          labelText: "Date of AI",
//          labelText: AppTranslations.of(context).text("ai_form_date"),
//          hintText: "Select date",
        ),
        onSaved: (String aiDate) {
          ai.aiDate = aiDate;
        },
        validator: (aiDate) {
          if (aiDate.isEmpty) {
//            return AppTranslations.of(context).text("aiDate_validate");
            return "Date of AI should not be empty !!";
          }
        });

    tfTentativeDatePD = TextFormField(
        keyboardType: TextInputType.number,
//        enabled: false,
        controller: tentativePdDateController,
        decoration: InputDecoration(
          labelText: "Tentative Date of PD",
//          labelText: AppTranslations.of(context).text("tentative_date_pd_form_date"),
//          hintText: "Select date",
        ),
        onSaved: (String tentativePd) {
          ai.tentativePDDate = tentativePd;
        },
        validator: (tentativePd) {
//          if (tentativePd.isEmpty) {
//            return AppTranslations.of(context).text("aiDate_validate");
////            return "Date of AI can't be empty";
//          }
        });

    //mobile number tf
    tfMobileNumber = TextFormField(
        controller: mblController,
        keyboardType: TextInputType.phone,
        maxLength: 10,
        focusNode: textFocus,
//        autofocus: true,
        decoration: InputDecoration(
          labelText: "Farmer's Mobile Number",
//          labelText:  AppTranslations.of(context).text("ai_form_mobile_number"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String mbl) {
          ai.mobileNumber = mbl;
        },
        validator: (mblNumber) {
          if (mblNumber.isEmpty) {
//            return AppTranslations.of(context).text("mobile_number_validate");
            return "Mobile Number can't be empty !!";
          } else if (mblNumber.length != 10) {
//            return AppTranslations.of(context).text("mobile_number_ten_digit_validate");
            return "Please enter ten digits number !!";
          }
        });

    //tf parity
    TextFormField tfCurrentParity = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 2,
        initialValue: this.ai == null ? "" : this.ai.currentParity.toString(),
        inputFormatters: [
          new BlacklistingTextInputFormatter(new RegExp('[\\.|\\,|\\-|\\*|\\+|\\#]')),
        ],
        decoration: InputDecoration(
          labelText: "Current Parity",
//          labelText:  AppTranslations.of(context).text("ai_form_parity"),
        ),
        onSaved: (parity) {
          ai.currentParity = int.tryParse(parity);
        },
        validator: (parity) {
          if (parity.isEmpty) {
//            return AppTranslations.of(context).text("parity_validate");
            return "Current Parity can't be empty";
          }
        });

    //tf serial

    TextFormField tfAISerial = TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 2,
        initialValue: this.ai == null ? "" : this.ai.aiSerial.toString(),
        inputFormatters: [
          new BlacklistingTextInputFormatter(new RegExp('[\\.|\\,|\\-|\\*|\\+|\\#]')),
        ],
        decoration: InputDecoration(
          labelText: "AI Serial",
//          labelText:  AppTranslations.of(context).text("ai_form_serial"),
        ),
        onSaved: (serial) {
          ai.aiSerial = int.tryParse(serial);
        },
        validator: (serial) {
          if (serial.isEmpty) {
//            return AppTranslations.of(context).text("serial_validate");
            return "AI Serial can't be empty";
          }
        });

    TextFormField tfShireId = TextFormField(
        keyboardType: TextInputType.text,
        initialValue: this.ai == null ? "" : this.ai.shireId.toString(),
        textCapitalization: TextCapitalization.characters,
        decoration: InputDecoration(
          labelText: "Sire Id",
//          labelText:  AppTranslations.of(context).text("ai_form_shire"),
        ),
        onSaved: (shire) {
          ai.shireId = shire.toUpperCase();
        },
        validator: (shire) {
          if (shire.isEmpty) {
//            return AppTranslations.of(context).text("shire_validate");
            return "Sire Id can't be empty";
          }
        });

//remarks
    TextFormField tfRemarks = TextFormField(
      keyboardType: TextInputType.multiline,
      initialValue: this.ai == null ? "" : this.ai.remarks,
      maxLines: null,
      decoration: InputDecoration(
        labelText: "Remarks",
//        labelText:  AppTranslations.of(context).text("ai_form_remarks"),
      ),
      onSaved: (String remarks) {
        ai.remarks = remarks;
      },
//        validator: (remarks) {
//          if (remarks.isEmpty) {
//            return " Remarks can't be empty";
//          }
//        }
//
    );

    //parity dropdown
//    DropdownButton dbParity = DropdownButton(
//        items: parity.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        value: 'Select Current Parity',
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

//    DropdownButton dbSerial = DropdownButton(
//        items: aiSerial.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        value: 'Select AI Serial',
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

    //Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text(
        "Save",
//        AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (aiFormKey.currentState.validate()) {
          aiFormKey.currentState.save();
          this.logic.saveAI();
        }
      },
    );

    //Raised Update Button
    RaisedButton rbUpdate = RaisedButton(
      child: Text(
        "Update",
//        AppTranslations.of(context).text("form_update"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (aiFormKey.currentState.validate()) {
          aiFormKey.currentState.save();
          this.logic.updateAI();
        }
      },
    );

    //astrick
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: aiFormKey,
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfMobileNumber,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfFarmer,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfAddress,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfSpeciesCode,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfSpecies,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfBreed,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: InkWell(
                      onTap: () {
                        SingleChildScrollView(
                          child: ConstrainedBox(
                              constraints: BoxConstraints(),
                              child: this.logic.showDatePickers()),
                        );
                      },
                      child: IgnorePointer(
                        child: tfDateAI,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfCurrentParity,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfAISerial,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfShireId,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
//dropdown of parity and serial
//          Padding(
//            padding: EdgeInsets.only(left: 12.0),
//            child: ListTile(
//              title: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 7,
//                    child: dbParity,
//                  ),
//                  Expanded(
//                    flex: 1,
//                    child: astrickText,
//                  ),
//                ],
//              ),
//            ),
//          ),
//          Padding(
//            padding: EdgeInsets.only(left: 12.0),
//            child: ListTile(
//              title: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 7,
//                    child: dbSerial,
//                  ),
//                  Expanded(
//                    flex: 1,
//                    child: astrickText,
//                  ),
//                ],
//              ),
//            ),
//          ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfRemarks,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
                  ),
                ],
              ),
            ),

            ListTile(
              title: Visibility(
                visible: this.logic.selectedGoatOrPig(),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 19,
                          child: InkWell(
                            onTap: () {
//                              this.logic.showDatePickers();
                            },
                            child: IgnorePointer(
                              child: tfTentativeDatePD,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text(''),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 19,
                          child: Visibility(
                            visible: !this.isEdit,
                            child: rbSave,
                            replacement: rbUpdate,
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text(''),
                        ),
                      ],
                    ),
                  ],
                ),
                replacement: ListTile(
                  title: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 19,
                        child: Visibility(
                          visible: !this.isEdit,
                          child: rbSave,
                          replacement: rbUpdate,
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(''),
                      ),
                    ],
                  ),
                ),
              ),
            ),

//            ListTile(
//
//              title: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 19,
//
//                    child: InkWell(
//
//                      onTap: () {
////                        this.logic.showDatePickers();
//                      },
//                      child: IgnorePointer(
//                        child: tfTentativeDatePD,
//                      ),
//                    ),
//                  ),
//                  Expanded(
//                    flex: 1,
//                    child: Text(""),
////                    child: astrickText,
//                  ),
//                ],
//              ),
//            ),

//            ListTile(
//              title: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 19,
//                    child: Visibility(
//                      visible: !this.isEdit,
//                      child: rbSave,
//                      replacement: rbUpdate,
//                    ),
//                  ),
//                  Expanded(
//                    flex: 1,
//                    child: Text(''),
//                  ),
//                ],
//              ),
//            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: this.appBar,
      body: SafeArea(
        child: BlocProvider(
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      //   Log.v("scrolled");
                    },
                    child: SingleChildScrollView(
                      child: getAIForm(),
                    ),
                  ),

                  //
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
