import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/ExitRecordPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ExitReason.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/SplitDate.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:nepali_utils/nepali_utils.dart';

class ExitRecordPageLogic {
  ExitRecordPageState state;
  ExitRecordBloc exitRecordBloc;
  FarmerBloc farmerBloc;
  AnimalBloc animalBloc;
  String formattedDate;
  Animal animal = Animal();
  Farmer farmer = Farmer();
  List<ExitReason> reasonList = [];

  ExitRecord exitRecord = ExitRecord();
  ExitReason selectedReason;

  ExitRecordPageLogic(
      this.state, this.exitRecordBloc, this.farmerBloc, this.animalBloc);

  void onPostState() {
    if (this.state.isEdit) {
//      new Future.delayed(new Duration(milliseconds: 0)).then((_) {
//        this.state.setAppBar(
//        AppTranslations.of(state.context).text("exit_form_edit"),
//      );
//    });
      this.state.setAppBar('Edit Exit Animals');
//      this.state.setAppBar('Edit Exit Cattle/Buffalo');
      this.exitRecord = state.exitRecord;

      this.exitRecordBloc.getExitReasonList().listen((it) {
        if (it == null) return;
        Logger.v("inside listener it");
        if (it.status == Status.SUCCESS) {
          Logger.v("reason list is " + it.data.toString());
          this.state.setState(() {
            this.reasonList = it.data;
            for (ExitReason exit in reasonList) {
              if (exit.name.contains(exitRecord.exitReason)) {
                state.setState(() {
                 int i = reasonList.indexOf(exit);
                  //display in dropdown
                  selectedReason = reasonList[i];
                });
              }
            }
          });
        } else {
          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
            Navigator.of(state.context).pop();
          });
        }
      });



      //
      //get farmer update
      this.farmerBloc.getFarmerByFId(this.exitRecord.mFarmerId).listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          farmer = it.data;
          state.tfMobileNumber.controller.text = it.data.mobileNumber;

          //get speces code update
          this.animalBloc.getAnimalByaId(this.exitRecord.mAnimalId).listen((it) {
            if (it == null) return;
            if (it.status == Status.SUCCESS) {
              animal = it.data;
              state.tfSpeciesCode.controller.text =
              it.data.speciesCode;
//              it.data.speciesCode.split("-")[1];
            } else if (it.status == Status.ERROR) {
              ToastUtils.show(it.errorMessage);
            } else {
              new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                Navigator.of(this.state.context).pop();
              });
            }
          });
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
            Navigator.of(this.state.context).pop();
          });
        }
      });


      state.exitDateController.text = this.exitRecord.dateOfExit;
    } else {
//      new Future.delayed(new Duration(milliseconds: 0)).then((_) {
//        this.state.setAppBar(
//              AppTranslations.of(state.context).text("exit_form_add_exit"),
//            );
//      });
      this.state.setAppBar('Add Exit Animals');
//      this.state.setAppBar('Add Exit Cattle/Buffalo');

//      DateTime today = new DateTime.now();
//      DateTime todayDate =
//      new DateTime(today.year + 56, today.month + 8, today.day + 17);
//      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
//      state.exitDateController.text = todayNepaliDate.toString();
      NepaliDateTime currentTime = NepaliDateTime.now();
//      String todayDateNepali =  currentTime.toIso8601String().split("T").first;
      String todayDateNepali =  SplitDate.splitNepali(currentTime);
      state.exitDateController.text = todayDateNepali;

      Logger.v('inside ' + this.state.isEdit.toString());
    }
    new Future.delayed(new Duration(milliseconds: 0)).then((_) {
      getExitReasonList();
    });
  }

  bool onValid() {
    if (selectedReason == null) {
//      ToastUtils.show(
//        AppTranslations.of(state.context).text("exitReason_validate"),
//      );
      ToastUtils.show("Please select exit reason!!");
      return false;
    }
    return true;
  }

  saveExit() async {
    if (onValid()) {
      this
          .exitRecordBloc
          .saveExit(this.state.context, this.exitRecord, this.farmer,
              this.animal, selectedReason)
          .listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          ToastUtils.show(it.data);
          state.setState(() {
//            Navigator.pop(this.state.context);
          });
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
//          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//            Navigator.of(this.state.context).pop();
//          });
        }
      });
    }
  }

  updateExitRecord() async {
    if (onValid()) {
      this
          .exitRecordBloc
          .updateExitRecord(this.state.context, this.exitRecord, this.farmer,
              this.animal, selectedReason)
          .listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          ToastUtils.show(it.data);
//          Navigator.pop(this.state.context);
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
            Navigator.of(state.context).pop();
          });
        }
      });
    }
  }

  selectedExitReason(int value) {
    state.setState(() {
      state.selected = value;
    });
  }

  getExitReasonList() {
    this.exitRecordBloc.getExitReasonList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("reason list is " + it.data.toString());
        this.state.setState(() {
          this.reasonList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  //date picker

//
//  Future<Null> showDatePickers() async {
//    final DateTime picked = await showDatePicker(
//        context: state.context,
//        // initialValue: DateTime.now(),
//        initialDate: DateTime.now(),
//        firstDate: DateTime(2010),
//        lastDate: DateTime(2100));
//
//    if (picked != null) {
//      state.setState(() {
//        DateTime dates = picked;
//
//        formattedDate = DateFormat('yyyy-MM-dd').format(picked);
//        state.setState(() {
//          state.tfDateExit.controller.text = formattedDate.toString();
//        });
//      });
//    }
//  }

  showDatePickers()async {

    state.selectedDateTime = await showNepaliDatePicker(
      context: state.context,
      initialDate: NepaliDateTime.now(),
      firstDate: NepaliDateTime(2050),
      lastDate: NepaliDateTime(2090),
      language: state.language,
    );
    state.setState(() {
      if (state.selectedDateTime != null){
//        state.exitDateController.text =SplitDate.splitDateString(state.selectedDateTime);
        state.exitDateController.text =state.selectedDateTime.toIso8601String().split("T").first;

      }else{
        state.exitDateController.text ="";

      }
    });
//    NepaliDatePicker.showPicker(
//        context: state.context,
//        startYear: 2052,
//        endYear: 2100,
//        color: Colors.blue,
//        barrierDismissible: false,
//        onPicked: (DateTime picked) {
//          if (picked != null) {
//            state.setState(() {
//              DateTime dates = picked;
//              state.setState(() {
//                state.tfDateExit.controller.text =
//                    picked.toIso8601String().split("T").first;
//              });
//            });
//          }
//        });
  }

  getFarmerByMobileNumber(String mblNumber) async {
    this
        .farmerBloc
        .getFarmerByMobileNumber(this.state.context, mblNumber)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        farmer = it.data;

        state.tfFarmer.controller.text = it.data.farmerName.toString();
        state.tfAddress.controller.text = it.data.municipality.toString();
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfFarmer.controller.text = "";
        state.tfAddress.controller.text = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }

  //get animal

  getAnimalBySpeciesCode(String speciesCode) async {
    this
        .animalBloc
        .getAnimalBySpeciesCode(this.state.context, speciesCode)
        .listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        animal = it.data;
        state.tfSpecies.controller.text = it.data.speciesName.toString();
        state.tfBreed.controller.text = it.data.breedName.toString();
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
        state.tfSpecies.controller.text = "";
        state.tfBreed.controller.text = "";
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(this.state.context).pop();
        });
      }
    });
  }
}
