import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/nitrogenBloc/NitrogenBloc.dart';
import 'package:nbis/features/home/NitrogenPage.dart';
import 'package:nbis/features/home/NitrogenPageList.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/LiquidNitrogen.dart';
import 'package:nbis/s/DialogUtils.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class NitrogenPageListLogic {
  NitrogenPageListState state;
  NitrogenBloc nitrogenBloc;
  List<LiquidNitrogen> nitrogenList = [];
  List<LiquidNitrogen> syncNitrogenList = [];
  List<LiquidNitrogen> unSyncNitrogenList = [];
  static LiquidNitrogen nitrogen = LiquidNitrogen();

  NitrogenPageListLogic(this.state, this.nitrogenBloc);

  void onPostInit() {
    getNitrogenList();
  }

  getNitrogenList() {
    this.nitrogenBloc.getLiquidNitrogenList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("nitrogen list is " + it.data.toString());
        this.state.setState(() {
          this.nitrogenList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getSyncNitrogenList() {
    this.nitrogenBloc.getSyncLiquidNitrogenList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("nitrogen list is " + it.data.toString());
        this.state.setState(() {
          this.syncNitrogenList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getUnSyncNitrogenList() {
    this.nitrogenBloc.getUnSyncLiquidNitrogenList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("nitrogen list is " + it.data.toString());
        this.state.setState(() {
          this.unSyncNitrogenList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  onEditNitrogen(LiquidNitrogen liquidNitrogen) async {
    await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return NitrogenPage(nitrogen: liquidNitrogen, isEdit: true);
    }));

    state.setState(() {
      state.refreshNitrogenList();
    });
  }

  onAddNitrogen()async {
 await Navigator.push(state.context, MaterialPageRoute(builder: (context) {
      return NitrogenPage();
    }));

 state.setState(() {
   state.refreshNitrogenList();
 });
  }

  void confirmationDelete(LiquidNitrogen nitrogens, int position) {
    DialogUtils.showAlertDialog(
        this.state.context,
        "Alert",
//        AppTranslations.of(state.context).text("alert"),
        "Are you sure,you want to delete?", (it) {
//        AppTranslations.of(state.context).text("delete_warning"), (it) {
      switch (it.eventType) {
        case DialogUtils.ON_CANCELABLE:
          {
            Logger.v('No clicked');
            Navigator.pop(this.state.context, true);
            getNitrogenList();

            break;
          }
        case DialogUtils.ON_CONFIRM:
          {
            Logger.v('position nitrogen' + position.toString());

            this
                .nitrogenBloc
                .deleteNitrogen(this.state.context, nitrogens)
                .listen((it) {
              if (it == null) return;
              if (it.status == Status.SUCCESS) {


//                state.setState(() {
//                       this.nitrogenList.removeAt(position);
//                });

                Navigator.pop(this.state.context, true);
                ToastUtils.show(it.data);
              } else if (it.status == Status.ERROR) {
                ToastUtils.show(it.errorMessage);
              } else {
                new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
                  Navigator.of(state.context).pop();
                });
              }
            });

            Logger.v('Yes clicked');
            break;
          }
      }
    }, "No", "Yes");
  }
}
