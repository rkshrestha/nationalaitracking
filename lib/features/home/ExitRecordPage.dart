import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/ExitRecordPageLogic.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/ExitReason.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/ExitRecordRepo.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/s/Logger.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_utils/nepali_utils.dart';

class ExitRecordPage extends StatefulWidget {
  bool isEdit;
  ExitRecord exitRecord;
  Animal animal;

  ExitRecordPage({this.exitRecord, this.animal, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return ExitRecordPageState(
        exitRecord: this.exitRecord, animal: this.animal, isEdit: this.isEdit);
  }
}

class ExitRecordPageState extends State<ExitRecordPage> {
  bool isEdit;
  ExitRecord exitRecord;
  ExitRecordBloc exitRecordBloc;
  ExitRecordPageLogic logic;
  TextFormField tfDateExit;
  FarmerBloc farmerBloc;
  AnimalBloc animalBloc;
  TextEditingController exitDateController = TextEditingController(text: " ");
  Animal animal;
  TextFormField tfMobileNumber;
  TextFormField tfFarmer;
  TextFormField tfAddress;
  TextFormField tfSpeciesCode;
  TextFormField tfSpecies;
  TextFormField tfBreed;
  AppBar appBar;
  NepaliDateTime selectedDateTime;
  Language language = Language.ENGLISH;
  DropdownButton dbExitReason;
  TextEditingController mblController = TextEditingController();
  TextEditingController farmerController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController aCodeController = TextEditingController();
  TextEditingController speciesController = TextEditingController();
  TextEditingController breedController = TextEditingController();
  FocusNode textFocus = new FocusNode();

  ExitRecordPageState({this.exitRecord, this.animal, this.isEdit = false});

  GlobalKey<FormState> exitFormKey = GlobalKey<FormState>();
  static var farmer = ['Select Farmer', 'Abd', 'cde'];
  int selected;

  //get number and animal

  void onNumberChanged() {
    String mobileText = mblController.text;
//    bool hasFocus = textFocus.hasFocus;
    Logger.v(mobileText);
    setState(() {
      mblController.selection = new TextSelection(
          baseOffset: mobileText.length, extentOffset: mobileText.length);
    });

    if (mobileText.length == 10) {
//      this.logic.getFarmerByMobileNumber(int.tryParse(mobileText));
      this.logic.getFarmerByMobileNumber(mobileText);
    }
  }

  void onAnimalChanged() {
    String animalText = aCodeController.text;
    Logger.v('getCOde:' + aCodeController.text);
//    bool hasFocus = textFocus.hasFocus;
    Logger.v(animalText);

    setState(() {
      aCodeController.selection = new TextSelection(
          baseOffset: animalText.length, extentOffset: animalText.length);
    });

//    this.logic.getAnimalBySpeciesCode(mblController.text + "-" + animalText);
    this.logic.getAnimalBySpeciesCode(animalText);

  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  @override
  initState() {
    super.initState();
    this.exitRecordBloc = ExitRecordBloc(ExitRecordRepo(context));
    this.animalBloc = AnimalBloc(AnimalRepo(context));
    this.farmerBloc = FarmerBloc(FarmerRepo(context));
    this.logic =
        ExitRecordPageLogic(this, exitRecordBloc, farmerBloc, animalBloc);

    mblController.text = animal == null ? "" : animal.mobileNumber;
//    mblController.text = animal == null ? "" : animal.speciesCode.split("-")[0];
    aCodeController.text =
        animal == null ? "" : animal.speciesCode;
//        animal == null ? "" : animal.speciesCode.split("-")[1];
    logic.onPostState();
    initUIController();


    if(animal != null){
      Logger.v("animal"+animal.speciesCode);
      Logger.v("farmer number: "+animal.mobileNumber);

      if (animal.mobileNumber.length == 10) {
        Logger.v("farmer number inside: "+animal.mobileNumber);
        this.logic.getFarmerByMobileNumber(animal.mobileNumber);
      }
//      setState((){
//
//      });
      this.logic.getAnimalBySpeciesCode(animal.speciesCode);
    }


  }

  void initUIController() {
    // aCodeController = TextEditingController(text: "");
    mblController.addListener(onNumberChanged);
    aCodeController.addListener(onAnimalChanged);
  }

  @override
  void dispose() {
    mblController.removeListener(onNumberChanged);
    aCodeController.removeListener(onAnimalChanged);
    mblController.dispose();
    textFocus.dispose();
    super.dispose();
  }

  Widget getExitForm() {
    ExitRecord exitRecord = this.logic.exitRecord;
    //mobile number tf
    tfMobileNumber = TextFormField(
        keyboardType: TextInputType.phone,
        controller: mblController,
        maxLength: 10,
//        autofocus: true,
        decoration: InputDecoration(
          labelText:   "Farmer's Mobile Number",
//          labelText:    AppTranslations.of(context).text("exit_form_mobile_number"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String mbl) {
          exitRecord.mobileNumber=mbl;
        },
        validator: (mblNumber) {
          if (mblNumber.isEmpty) {
//            return AppTranslations.of(context).text("mobile_number_validate");
            return "Mobile Number can't be empty !!";
          } else if (mblNumber.length != 10) {
//            return AppTranslations.of(context).text("mobile_number_ten_digit_validate");
            return "Please enter ten digits number !!";
          }
        });

    //farmer name tf
    tfFarmer = TextFormField(
        enabled: false,
        controller: farmerController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Farmer's Name",
//          labelText: AppTranslations.of(context).text("exit_form_farmer_name"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String farmer) {
          exitRecord.farmerName= farmer;
        },
        validator: (farmer) {
          if (farmer.isEmpty) {
            return "Farmer Name can't be empty";
          }
        });

    //address tf
    tfAddress = TextFormField(
        enabled: false,
        controller: addressController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Address",
//          labelText: AppTranslations.of(context).text("exit_form_address"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String address) {},
        validator: (address) {
          if (address.isEmpty) {
            return "Address can't be empty";
          }
        });

    //species tf
    tfSpecies = TextFormField(
        enabled: false,
        controller: speciesController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
//          labelText:  AppTranslations.of(context).text("exit_form_species")
          labelText:  "Animal"
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String species) {},
        validator: (species) {
          if (species.isEmpty) {
            return "Species can't be empty";
          }
        });

    //breed
    tfBreed = TextFormField(
        enabled: false,
        controller: breedController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Breed"
//          labelText: AppTranslations.of(context).text("exit_form_breed")
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String breed) {},
        validator: (breed) {
          if (breed.isEmpty) {
            return "Breed can't be empty";
          }
        });

//    DropdownButton dbFarmer = DropdownButton(
//        items: farmer.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        value: 'Select Farmer',
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

    //animal code

    tfSpeciesCode = TextFormField(
        keyboardType: TextInputType.number,
        controller: aCodeController,
//        autofocus: true,
        decoration: InputDecoration(
          //     icon: Icon(Icons.confirmation_number),
          labelText: "Animal Code",
//          labelText: AppTranslations.of(context).text("exit_form_code"),
          prefixText: mblController.text + "-",
          //    hintText: "XXXX-XXXXX-XXXXXX",
        ),
        onSaved: (String animalCode) {
//          String animalsCode = mblController.text +"-"+animalCode;
          exitRecord.speciesCode = animalCode;
        },
        validator: (animalCode) {
          if (animalCode.isEmpty) {
            return " Animal Code can't be empty !!";
//            return AppTranslations.of(context).text("code_validate");
          }
        });

    //date test

    tfDateExit = TextFormField(
        keyboardType: TextInputType.number,
        controller: exitDateController,
        decoration: InputDecoration(
          //  icon: Icon(Icons.date_range),
          labelText:  "Date of Exit",
//          labelText:  AppTranslations.of(context).text("exit_form_date")
//          hintText: "Select date",
        ),
        onSaved: (String exitDate) {
          exitRecord.dateOfExit = exitDate;
        },
        validator: (exitDate) {
          if (exitDate.isEmpty) {
//            return AppTranslations.of(context).text("exitDate_validate");
            return "Date of Exit can't be empty !!";
          }
        });

    //sold /dead radio button

//    radioExitReason() {
//      List<Widget> radioExitList = new List<Widget>();

//      radioExitList.add(new Row(
//        children: <Widget>[
//          Row(
//            children: <Widget>[
//              Radio(
//                  value: 0,
//                  groupValue: selected,
//                  onChanged: (int value) {
//                    logic.selectedExitReason(value);
//                    exitRecord.exitReason = "Sold";
//                  }),
//              Text('Sold'),
//            ],
//          ),
//          Row(
//            children: <Widget>[
//              Radio(
//                  value: 1,
//                  groupValue: selected,
//                  onChanged: (int value) {
//                    logic.selectedExitReason(value);
//                    exitRecord.exitReason = "Dead";
//                  }),
//              Text('Dead'),
//            ],
//          ),
//          Text(
//            '*',
//            style: TextStyle(
//              color: Colors.red,
//              fontWeight: FontWeight.bold,
//              fontSize: 18.0,
//            ),
//          ),
//        ],
//      ));
//      return radioExitList;
//    }

     dbExitReason = DropdownButton<ExitReason>(

      hint: Text( "Select Exit Reason"),
//      hint: Text( AppTranslations.of(context).text("exit_form_reason")),
      items: logic.reasonList.map((exitReasonSelected) {
        return DropdownMenuItem<ExitReason>(
          child: new Text(exitReasonSelected.name),
          value: exitReasonSelected,
        );
      }).toList(),
       value: logic.selectedReason,
      isExpanded: true,
      onChanged: (changedExitReason) {
        if (changedExitReason == null) {
          return;
        }
        setState(() {
          this.logic.selectedReason = changedExitReason;
        });
      },
    );

//    Radio soldRadio = Radio(
//      value: 0,
//      groupValue: radioValue,
//      //onChanged: _handleRadioValueChange,
//    );
//
//    //dead radio button
//    Radio deadRadio = Radio(
//      value: 1,
//      groupValue: radioValue,
//      //onChanged: ,
//    );

    //Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text(
          "Save",
//          AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (exitFormKey.currentState.validate()) {
          exitFormKey.currentState.save();
          this.logic.saveExit();
        }
      },
    );

    //Raised Update Button
    RaisedButton rbUpdate = RaisedButton(
      child: Text( "Update",
//      child: Text( AppTranslations.of(context).text("form_update"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (exitFormKey.currentState.validate()) {
          exitFormKey.currentState.save();
          this.logic.updateExitRecord();
        }
      },
    );

    //astrick
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: exitFormKey,
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
//            Padding(
////              padding: EdgeInsets.only(top: 6.0),
////              child: ListTile(
////                title: Row(
////                  children: <Widget>[
////                   // Icon(Icons.select_all),
////                    Padding(
////                      padding: EdgeInsets.only(left: 12.0),
////                    ),
////                    Expanded(
////                      flex: 6,
////                      child: dbFarmer,
////                    ),
////                    Expanded(
////                      flex: 1,
////                      child: astrickText,
////                    ),
////                  ],
////                ),
////              ),
////            ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfMobileNumber,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfFarmer,
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(""),
//                      child: astrickText,
                    ),
                  ],
                ),
              ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfAddress,
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(""),
//                      child: astrickText,
                    ),
                  ],
                ),
              ),

              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfSpeciesCode,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),

              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfSpecies,
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(""),
//                      child: astrickText,
                    ),
                  ],
                ),
              ),

              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: tfBreed,
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(""),
//                      child: astrickText,
                    ),
                  ],
                ),
              ),

              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: InkWell(
                        onTap: () {
                          this.logic.showDatePickers();
                        },
                        child: IgnorePointer(
                          child: tfDateExit,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),
//              Padding(
//                padding: EdgeInsets.only(left: 14.0, top: 8.0),
//                child: Text(
//                  'Exit Reason:',
//                  //  textAlign: TextAlign.left,
//                ),
//              ),
              ListTile(
                title: Row(
                  //      children: radioExitReason(),
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: dbExitReason,
                    ),
                    Expanded(
                      flex: 1,
                      child: astrickText,
                    ),
                  ],
                ),
              ),
              ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: Visibility(
                        visible: !this.isEdit,
                        child: rbSave,
                        replacement: rbUpdate,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(''),
                    ),
                  ],
                ),
              ),
            ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: this.appBar,
      body: SafeArea(
        child: BlocProvider(
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      //   Log.v("scrolled");
                    },
                    child: SingleChildScrollView(
                      child: getExitForm(),
                    ),
                  ),

                  //
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
