import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/bloc/nbisBloc/NbisBloc.dart';
import 'package:nbis/features/home/FarmerPageLogic.dart';
import 'package:nbis/features/repo/District.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/features/repo/Municipality.dart';
import 'package:nbis/features/repo/NbisRepo.dart';
import 'package:nbis/features/repo/Province.dart';
import 'package:nbis/s/Logger.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class FarmerPage extends StatefulWidget {
  bool isEdit;
  Farmer farmer;

  FarmerPage({this.farmer, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return FarmerPageState(farmer: this.farmer, isEdit: this.isEdit);
  }
}

class FarmerPageState extends State<FarmerPage> {
  bool isEdit;
  Farmer farmer;
  Province province;
  FarmerBloc farmerBloc;
  NbisBloc nbisBloc;
  AppBar appBar;
  GlobalKey<FormState> farmerFormKey = GlobalKey<FormState>();

//  static var district = ['Abd', 'cde'];
//  static var municipality = ['Abdss', 'cdess'];

  int provinceId;
  int districtId;
  int municipalityId;

  FarmerPageLogic logic;
  TextFormField tfFarmerName;
  TextFormField tfMobileNumber;
  DropdownButton dbProvince;
  DropdownButton dbDistrict;
  DropdownButton dbMunicipality;
  TextFormField tfWard;
  TextFormField tfVillage;
  int farmerIndex;
  TextEditingController mblController ;
  TextEditingController farmerController ;

//  FocusNode mblNumberFocus;
//  FocusNode farmerNameFocus;
//  FocusNode wardFocus;
//  FocusNode villageFocus;

  FarmerPageState({this.farmer, this.isEdit = false});

  @override
  void initState() {
    super.initState();
    this.farmerBloc = FarmerBloc(FarmerRepo(context));
    this.nbisBloc = NbisBloc(NbisRepo(context));
    this.logic = FarmerPageLogic(this, farmerBloc, nbisBloc);

     mblController = TextEditingController(text: "");
     farmerController = TextEditingController(text: "");
    logic.onPostInit();
    logic.onPostState();

//     mblNumberFocus =FocusNode();
//     farmerNameFocus=FocusNode();
//     wardFocus=FocusNode();
//     villageFocus=FocusNode();
  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }


  @override
  void dispose() {
//    mblNumberFocus.dispose();
//    farmerNameFocus.dispose();
//    wardFocus.dispose();
//    villageFocus.dispose();
    mblController.dispose();
    farmerController.dispose();
    super.dispose();
  }


  Widget farmerForm() {
    Farmer farmer = this.logic.farmer;

    //farmer code textformfield
//    TextFormField tfFarmerCode = TextFormField(
//        decoration: InputDecoration(
//          labelText: "Farmer's Code",
//          hintText: "XXXX-XXXXX",
//          //  icon: Icon(Icons.confirmation_number),
//        ),
//        onSaved: (String name) {},
//        validator: (farmerCode) {
//          if (farmerCode.isEmpty) {
//            return "Farmer Code can't be empty";
//          }
//        });

    //Farmer Name tf


    tfFarmerName = TextFormField(
        controller: farmerController,
//        focusNode: farmerNameFocus,
        keyboardType: TextInputType.text,
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          labelText:
//          AppTranslations.of(context).text("farmer_name"),
          "Farmer Name",
          hintText: "Full Name",
          //    icon: Icon(Icons.person)
        ),
        onSaved: (String name) {
          farmer.farmerName = name;
        },
        validator: (farmerName) {
          String patttern = r'(^[a-zA-Z ]*$)';
          RegExp regExp = new RegExp(patttern);
          if (farmerName.isEmpty) {
//            return AppTranslations.of(context).text("farmer_name_validate");
            return "Farmer Name can't be empty";
          }else if (!regExp.hasMatch(farmerName)) {
            return "Name must be a-z and A-Z";
          }else{}
        });

    //mobile number tf
    tfMobileNumber = TextFormField(
        controller: mblController,
//        focusNode: mblNumberFocus,
        keyboardType: TextInputType.phone,
//        textInputAction: TextInputAction.next,
        inputFormatters: [
          new BlacklistingTextInputFormatter(new RegExp('[\\.|\\,|\\-|\\*|\\+|\\#]')),
        ],
        maxLength: 10,
        decoration: InputDecoration(
          labelText:
//              AppTranslations.of(context).text("farmer_form_mobile_number"),
            "Farmer's Mobile Number",
          suffixIcon: IconButton(
            icon: Icon(Icons.contact_phone),
            onPressed: () {
              logic.getContact();
            },
          ),
        ),
        onSaved: (mbl) {
          //  farmer.mobileNumber = int.tryParse(mbl);
          farmer.mobileNumber = mbl;
        },
        validator: (mbl) {
          String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
          RegExp regExp = new RegExp(patttern);
          if (mbl.isEmpty) {
//            return AppTranslations.of(context).text("mobile_number_validate");
            return "Mobile Number can't be empty";
          } else if (mbl.length != 10) {
//            return AppTranslations.of(context).text("mobile_number_ten_digit_validate");
            return "Please enter ten digits number";
          }else if (!regExp.hasMatch(mbl)) {
            return 'Please enter valid mobile number';
          }
        });

    // tf village/Street
    tfVillage = TextFormField(
        initialValue: farmer.village == null ? "" : farmer.village,
        keyboardType: TextInputType.text,
//        focusNode: villageFocus,
        decoration: InputDecoration(
//          labelText: AppTranslations.of(context).text("farmer_form_village"),
          labelText: "Village",
          //  icon: Icon(Icons.add_location),
        ),
        onSaved: (String village) {
          farmer.village = village;
        },
        validator: (village) {
          if (village.isEmpty) {
//            return AppTranslations.of(context).text("village_validate");
            return "Village/Street can't be empty";
          }
        });

    // tf district
//    TextFormField tfDistrict = TextFormField(
//        keyboardType: TextInputType.text,
//        decoration: InputDecoration(
//          labelText: "District",
//          //    icon: Icon(Icons.select_all),
//        ),
//        onSaved: (String district) {
//          //   farmer.district = district;
//        },
//        validator: (district) {
//          if (district.isEmpty) {
//            return "District can't be empty";
//          }
//        });

    // tf municipality
//    TextFormField tfMunicipality = TextFormField(
//        keyboardType: TextInputType.text,
//        decoration: InputDecoration(
//          labelText: "Municipality",
//          //   icon: Icon(Icons.select_all),
//        ),
//        onSaved: (String municipality) {
//          //  farmer.municipality = municipality;
//        },
//        validator: (municipality) {
//          if (municipality.isEmpty) {
//            return "Municipality can't be empty";
//          }
//        });

    //tf ward no
    tfWard = TextFormField(
        initialValue: farmer.ward == null ? "" : farmer.ward.toString(),
        keyboardType: TextInputType.number,
        inputFormatters: [
          new BlacklistingTextInputFormatter(new RegExp('[\\.|\\,]')),
        ],
        maxLength: 2,
//        focusNode: wardFocus,
        decoration: InputDecoration(
          labelText: "Ward No.",
//          labelText: AppTranslations.of(context).text("farmer_form_ward"),
          //   icon: Icon(Icons.whatshot),
        ),
        onSaved: (ward) {
          farmer.ward = int.tryParse(ward);
        },
        validator: (ward) {
          if (ward.isEmpty) {
//            return AppTranslations.of(context).text("ward_validate");
            return " Ward No. can't be empty";
          } else if (ward.length > 35) {
//            return AppTranslations.of(context).text("ward_number_validate");
            return "Ward No. must be below 36";
          }
        });

    //db province
    dbProvince = DropdownButton<Province>(
      isDense: true,
      hint: Text(
        "Select State",
//        AppTranslations.of(context).text("farmer_form_state"),
      ),
      items: logic.provinceList.map((provinceSelected) {
        return DropdownMenuItem<Province>(
          child: new Text(provinceSelected.state),
          value: provinceSelected,
          // value: provinceSelected.provinceid.toString(),
        );
      }).toList(),
      value: logic.selectedProvince,
      isExpanded: true,
      onChanged: (changedProvince) {

        if (changedProvince == null) {
          return;
        } else {
          setState(() {
            FocusScope.of(context).requestFocus(new FocusNode());
            logic.selectedDistrict = null;
            logic.selectedMunicipality = null;
            this.logic.selectedProvince = changedProvince;
//            farmerIndex = logic.provinceList.indexOf(changedProvince);
            Logger.v(
                "changedProvince  " + logic.selectedProvince.state.toString());
            //    provinceId = int.tryParse(changedProvince.provinceid.toString());
            provinceId = changedProvince.provinceid;
            this.logic.getDistrictList(provinceId);

          });
        }
      },
    );

    //db province
    dbDistrict = DropdownButton<District>(
      isDense: true,
      hint: Text(
       "Select District"
//        AppTranslations.of(context).text("farmer_form_district"),
      ),
      items: logic.districtList.map((districtSelected) {
        return DropdownMenuItem<District>(
          child: new Text(districtSelected.name),
          value: districtSelected,
        );
      }).toList(),
      value: logic.selectedDistrict,
      isExpanded: true,
      onChanged: (changedDistrict) {
        if (changedDistrict == null) {
          return;
        } else {
          setState(() {
            logic.selectedMunicipality = null;
            this.logic.selectedDistrict = changedDistrict;
//            Logger.v(
//                "changedDistrict  " + logic.selectedDistrict.dId.toString());
//            Logger.v(
//                "changedDistrict  " + logic.selectedDistrict.name.toString());
            //    provinceId = int.tryParse(changedProvince.provinceid.toString());
            districtId = changedDistrict.districtid;

            this.logic.getMunicipalityList(districtId);
          });
        }
      },
    );

    //db municipality

    dbMunicipality = DropdownButton<Municipality>(
      hint: Text(
       "Select Municipality"
//        AppTranslations.of(context).text("farmer_form_municipality"),
      ),
      items: logic.municipalityList.map((municipalitySelected) {
        return DropdownMenuItem<Municipality>(
          child: new Text(municipalitySelected.name),
          value: municipalitySelected,
        );
      }).toList(),
      value: logic.selectedMunicipality,
      isExpanded: true,
      onChanged: (changedMunicipality) {
        if (changedMunicipality == null) {
          return;
        } else {
          setState(() {
            this.logic.selectedMunicipality = changedMunicipality;
            Logger.v("changedMunicipality" +
                logic.selectedMunicipality.name.toString());
            //    provinceId = int.tryParse(changedProvince.provinceid.toString());
            //districtId = changedDistrict.districtid;

            //  this.logic.getMunicipalityList(districtId);
          });
        }
      },
    );

    //province dropdown
//    DropdownButton test = DropdownButton(
//        items: municipality.map((String provinceSelected) {
//          return DropdownMenuItem<String>(
//            value: provinceSelected,
//            child: Text(provinceSelected),
//          );
//        }).toList(),
//        hint: Text("Select Province"),
//        //value: ,
//        elevation: 2,
//        onChanged: (getProvince) {
//          setState(() {
//            selectedProvince = getProvince;
//            //  updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

    //district dropdown
//    DropdownButton dbDistrict = DropdownButton(
//        items: district.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        hint: Text("Select district"),
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            selectedDistrict = valueSelectedByUser;
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });
//
//    //municipality dropdown
//
//    DropdownButton dbMunicipality = DropdownButton(
//        items: municipality.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        hint: Text("Select Municipality"),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //  updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

    //Raised Save Button
    MaterialButton rbSave = MaterialButton(
      child: Text(
     "Save",
//        AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (farmerFormKey.currentState.validate()) {
          farmerFormKey.currentState.save();
          this.logic.saveFarmer();
        }
      },
    );

    //Raised Update Button
    MaterialButton rbUpdate = MaterialButton(
      child: Text(
        "Update",
//        AppTranslations.of(context).text("form_update"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (farmerFormKey.currentState.validate()) {
          farmerFormKey.currentState.save();
          setState(() {
            this.logic.updateFarmer();
          });
        }
      },
    );

    //astrick
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: farmerFormKey,
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfMobileNumber,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
//          ListTile(
//            title: Row(
//              children: <Widget>[
//                Expanded(
//                  flex: 7,
//                  child: tfFarmerCode,
//                ),
//                Expanded(
//                  flex: 1,
//                  child: astrickText,
//                ),
//              ],
//            ),
//          ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfFarmerName,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.fromLTRB(15.0, 8.0, 0, 0),
              child: Text(
                "Address: ",
//                AppTranslations.of(context).text("ai_form_address"),
                style: TextStyle(fontSize: 16.0),
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  // Icon(Icons.select_all),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                  ),
                  Expanded(
                    flex: 19,
                    child: dbProvince,
                    //      child: getProvinceList(),
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  // Icon(Icons.select_all),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                  ),
                  Expanded(
                    flex: 19,
                    child: dbDistrict,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  //   Icon(Icons.select_all),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                  ),
                  Expanded(
                    flex: 19,
                    child: dbMunicipality,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfWard,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfVillage,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            Align(
              alignment: FractionalOffset.bottomCenter,
              child: ListTile(
                title: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 19,
                      child: Visibility(
                        visible: !this.isEdit,
                        child: rbSave,
                        replacement: rbUpdate,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(''),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//resizeToAvoidBottomInset: false,
//    resizeToAvoidBottomPadding: false,
      appBar: this.appBar,
      body:
//        SafeArea(
//        child:
        BlocProvider(
          child: Column(
            children: <Widget>[
              Expanded(
                child: NotificationListener<ScrollNotification>(
                  onNotification: (scrollNotification) {
                    //   Log.v("scrolled");
                  },
                  child: SingleChildScrollView(
                    child: farmerForm(),
                  ),
                ),

                //
              ),
            ],
          ),
        ),
//      ),
    );
  }

  //test
//  ListView getProvinceList() {
//    return ListView.builder(
//        shrinkWrap: true,
//        scrollDirection: Axis.vertical,
//        itemCount: logic.provinceList.length,
//        itemBuilder: (BuildContext context, int position) {
//          province = this.logic.provinceList[position];
//
//          return new DropdownButton<String>(
//            isExpanded: true,
//            onChanged: (value) {
//              setState(() {
//                province.state = value;
//              });
//            },
//            hint: new Text('Select Province'),
//            value: province.state == null ? "" : province.state,
//            items: logic.provinceList.map((value) {
//              return new DropdownMenuItem<String>(
//                value: value.state,
//                child: new Text(value.state),
//              );
//            }).toList(),
//          );
//        });
//  }
}
