import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/UserBloc/ChangePasswordBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/home/ProfilePageLogic.dart';
import 'package:nbis/features/repo/ChangePasswordModel.dart';
import 'package:nbis/features/repo/ChangePasswordRepo.dart';
import 'package:nbis/features/repo/SQFliteDatabase.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfilePageState();
  }
}

class ProfilePageState extends State<ProfilePage> {
  SessionManager sessionManager;
  bool obscureText = true;
  SQFliteDatabase sqFliteDatabase;
  ProfilePageLogic logic;
  ChangePasswordModel change;
  ChangePasswordBloc changePasswordBloc;

  @override
  void initState() {
    super.initState();
    changePasswordBloc = ChangePasswordBloc(ChangePasswordRepo(this.context));
    logic = ProfilePageLogic(this, changePasswordBloc);
    init();
  }

  void showToast(String message) {
    ToastUtils.show(message);
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(context);
    sqFliteDatabase = SQFliteDatabase.getDB();
  }

  @override
  Widget build(BuildContext context) {
    var user = this.sessionManager.findOne().user;
    Logger.v("ShowDatas: " + user.username);
    return Scaffold(
      appBar: AppBar(
          title: Text(
   "Technician Profile",
//        AppTranslations.of(context).text("profile_title"),
      )),
      body: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(color: Colors.blue ,  child:Row(
                children: <Widget>[
                  Expanded(
                    child: Text(""),
                    flex: 2,
                  ),
                  Expanded(
                    child: Container(
                        width: 180.0,
                        height: 150.0,
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.none,
                                image: new AssetImage(
                                    "images/xxhdpi/farmer.png")))),
                    flex: 6,
                  ),
                  Expanded(
                    child: Text(""),
                    flex: 2,
                  ),
                ],
              ),),

            ),
            Expanded(
                flex: 5,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: NotificationListener<ScrollNotification>(
                        onNotification: (scrollNotification) {
                          //   Log.v("scrolled");
                        },
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  "Username: "
//                                    AppTranslations.of(context).text("username")
                                    + user.username),
                              ),
                              ListTile(
                                title: Text(
                                  "Mobile Number: "
//                                    AppTranslations.of(context).text("mobile_number")
                                        + user.mobileno),
                              ),
                              ListTile(
                                title: Text(
                                  "Email: "
//                                    AppTranslations.of(context).text("email")
                                    + user.email),
                              ),
//                              ListTile(
//                                title: Text("address" + user.address==null?"":user.address),
//                              ),
                              ListTile(
                                title: Container(
                                  child: FlatButton(
                                      color: Theme.of(context).primaryColor,
                                      onPressed: () {
                                        changePasswordDialog();
                                      },
                                      child: Text(
                                        "Change Password",
//                                        AppTranslations.of(context).text("change_password"),
                                        style: TextStyle(color: Colors.white),
                                      )),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  changePasswordDialog() {
    TextEditingController oldController = TextEditingController();
    TextEditingController newController = TextEditingController();
    TextEditingController confirmController = TextEditingController();

    change = ChangePasswordModel();

    GlobalKey<FormState> changeFormKey = GlobalKey<FormState>();
    TextFormField tfOldPassword = TextFormField(
        controller: oldController,

        decoration: InputDecoration(
          labelText:  "Old Password",
//          labelText:  AppTranslations.of(context).text("old_password"),

//            suffixIcon: new GestureDetector(
//              onTap: () {
//                setState(() {
//                  obscureText = !obscureText;
//                });
//              },
//              child:
//              Icon(obscureText ? Icons.visibility : Icons.visibility_off),
//            )

          //  icon: Icon(Icons.confirmation_number),
        ),
        obscureText: true,
        onSaved: (String old) {
          change.password = old;
        },
        validator: (old) {
          if (old.isEmpty) {
            return "Old Password can't be empty";
//            return  AppTranslations.of(context).text("old_password_validate");
          }
        });

    TextFormField tfNewPassword = TextFormField(
        controller: newController,
        obscureText: true,
        decoration: InputDecoration(
          labelText: "New Password",
//          labelText:  AppTranslations.of(context).text("new_password"),
          //  icon: Icon(Icons.confirmation_number),
        ),
        onSaved: (String newPassword) {},
        validator: (newPassword) {
          if (newPassword.isEmpty) {
            return  "New Password can't be empty";
//            return  AppTranslations.of(context).text("new_password_validate");
          }else if(newPassword.length < 5){
            return "Password must be aleast five characters";
          }
        });

    TextFormField tfConfirmPassword = TextFormField(
        controller: confirmController,
        obscureText: true,
        decoration: InputDecoration(
          labelText:  "Confirm Password",
//          labelText:  AppTranslations.of(context).text("confirm_password"),
          //  icon: Icon(Icons.confirmation_number),
        ),
        onSaved: (String confirm) {
          change.newPassword = confirm;
        },
        validator: (confirm) {
          if (confirm.isEmpty) {
            return  "Confirm New Password can't be empty";
//            return  AppTranslations.of(context).text("confirm_password_validate");
          } else if (confirmController.text != newController.text) {
            return  "Not matched with new password";
//            return  AppTranslations.of(context).text("confirmation_not_match");
          }
        });

    MaterialButton rbSave = MaterialButton(
      child: Text(
        "Save",
//        AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (changeFormKey.currentState.validate()) {
          changeFormKey.currentState.save();
          var user = this.sessionManager.findOne().user;
          change.userId = user.userid;
          Logger.v("userId:" + user.userid.toString());
          this.logic.sendChangePasswordRequest(change);
        }
      },
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Change Password",
//              AppTranslations.of(context).text("change_password"),
              textAlign: TextAlign.center,
            ),
            content: SafeArea(
              child: BlocProvider(
                child: Container(
                  height: 250.0,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: NotificationListener<ScrollNotification>(
                          onNotification: (scrollNotification) {
                            //   Log.v("scrolled");
                          },
                          child: SingleChildScrollView(
                            child: Form(
                              key: changeFormKey,
                              child: Container(
                                child: Column(
                                  children: <Widget>[
                                    tfOldPassword,
                                    tfNewPassword,
                                    tfConfirmPassword,
                                    Container(
                                      height: 20.0,
                                    ),
                                    rbSave,
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),

                        //
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
