import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
import 'package:nbis/features/bloc/semenBloc/SemenBloc.dart';
import 'package:nbis/features/home/SemenPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:nepali_utils/nepali_utils.dart';

class SemenPageLogic {
  SemenPageState state;
  SemenBloc semenBloc;

  Semen semen = Semen();

  SemenPageLogic(this.state, this.semenBloc);

  void onPostState() {
    if (this.state.isEdit) {
      this.state.setAppBar('Edit Semen');
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//          AppTranslations.of(state.context).text("semen_form_edit"),);
//      });
      this.semen = state.semen;
      state.dateSemenController.text = this.semen.receivedDate;

      Logger.v('inside ' + this.state.isEdit.toString());
      Logger.v('inside ' + semen.receivedDate);
    } else {
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//          AppTranslations.of(state.context).text("semen_form_add_semen"),);
//      });

      this.state.setAppBar('Add Semen');

//      DateTime today = new DateTime.now();

      NepaliDateTime currentTime = NepaliDateTime.now();
      String todayDateNepali =  currentTime.toIso8601String().split("T").first;
      state.dateSemenController.text = todayDateNepali;
//      DateTime todayDate =
////      new DateTime(today.year + 56, today.month + 8, today.day + 17);
////      String todayNepaliDate = DateFormat('yyyy-MM-dd').format(todayDate);
////      state.dateSemenController.text = todayNepaliDate.toString();
      Logger.v('inside ' + this.state.isEdit.toString());
    }
  }

  saveSemen() async {
    this.semenBloc.saveSemen(this.state.context,this.semen).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        state.setState(() {
//          Navigator.of(state.context, rootNavigator: true).pop();
//
//        });
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(state.context).pop();
//        });
      }
    });
  }

  updateSemen() async {
    Logger.v("getSID: " + semen.msId.toString());
    this.semenBloc.updateSemen(this.state.context,this.semen).listen((it) {
      if (it == null) return;
      if (it.status == Status.SUCCESS) {
        ToastUtils.show(it.data);
//        Navigator.pop(this.state.context);
      } else if (it.status == Status.ERROR) {
        ToastUtils.show(it.errorMessage);
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  String formattedDate;

//  Future<Null> showDatePickers() async {
//    final DateTime picked = await showDatePicker(
//        context: state.context,
//        initialDate: DateTime.now(),
//        firstDate: DateTime(2010),
//        lastDate: DateTime(2100));
//
//    if (picked != null) {
//      state.setState(() {
//        DateTime dates = picked;
//
//        formattedDate = DateFormat('yyyy-MM-dd').format(picked);
//        state.setState(() {
//          state.tfSemenDate.controller.text = formattedDate.toString();
//        });
//      });
//    }
//  }

  showDatePickers() async{

    state.selectedDateTime = await showNepaliDatePicker(
      context: state.context,
      initialDate: NepaliDateTime.now(),
      firstDate: NepaliDateTime(2050),
      lastDate: NepaliDateTime(2090),
      language: state.language,
    );
    state.setState(() {
      if (state.selectedDateTime != null){
        state.dateSemenController.text =state.selectedDateTime.toIso8601String().split("T").first;

      }else{
        state.dateSemenController.text ="";

      }
    });
//    NepaliDatePicker.showPicker(
//        context: state.context,
//        startYear: 2052,
//        endYear: 2100,
//        color: Colors.blue,
//        barrierDismissible: false,
//        onPicked: (DateTime picked) {
//          if (picked != null) {
//            state.setState(() {
//              DateTime dates = picked;
//              state.setState(() {
//                state.tfSemenDate.controller.text =
//                    picked.toIso8601String().split("T").first;
//              });
//            });
//          }
//
//        });
  }



}
