import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/UserBloc/UserBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/nbisBloc/NbisBloc.dart';
import 'package:nbis/features/home/HomePage.dart';
import 'package:nbis/features/home/LoginPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/LoginRequest.dart';
import 'package:nbis/features/sharedPreferences/Session.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ProgressBar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPageLogic {
  LoginPageState state;
  UserBloc userBloc;
  AnimalBloc animalBloc;
  NbisBloc nbisBloc;
  ExitRecordBloc exitRecordBloc;
  var timers;
  var username;
  var techId;

  LoginPageLogic(this.state, this.userBloc, this.animalBloc, this.nbisBloc,
      this.exitRecordBloc);

  ProgressBar progressBar = ProgressBar();


  //get all province, district, municipality, species, breed, exit reason

  syncSpeciesList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.animalBloc.syncSpeciesList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
        //   progressBar.dismiss(this.state.context);
        //  this.state.showToast(it.data);
        //
        //
        syncBreedList();
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
          goBackToLoginPage();
        });
      }
    });
  }

  //sync breed
  syncBreedList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.animalBloc.syncBreedList().listen((it) {
      if (it == null) return;
      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
        // progressBar.dismiss(this.state.context);
        //  this.state.showToast(it.data);

        syncProvinceList();
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
          goBackToLoginPage();
        });
      }
    });
  }

  //
  syncProvinceList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.nbisBloc.syncProvinceList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
        //   progressBar.dismiss(this.state.context);
        //  this.state.showToast(it.data);

        syncDistrictList();
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);

          goBackToLoginPage();
        });
      }
    });
  }

  //district

  syncDistrictList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.nbisBloc.syncDistrictList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        // this.state.showToast(it.data);
        syncMunicipalityList();
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
          goBackToLoginPage();
        });
      }
    });
  }

  //Municipality

  syncMunicipalityList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.nbisBloc.syncMunicipalityList().listen((it) {
      if (it == null) return;

      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
//        progressBar.dismiss(this.state.context);
        // this.state.showToast(it.data);
        syncExitReasonList();
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
          goBackToLoginPage();
        });
      }
    });
  }

  //exit reason

  syncExitReasonList() async {
//    ProgressBar progressBar = ProgressBar();
//    progressBar.show(this.state.context);

    this.exitRecordBloc.syncExitReasonList().listen((it) {
      if (it == null) return;
      Logger.v("inside listenere");
      if (it.status == Status.SUCCESS) {
        progressBar.dismiss(this.state.context);
        //   this.state.showToast(it.data);
//        sendLoginRequest(state.loginRequest);

//        gotoHomePage();
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          progressBar.dismiss(this.state.context);
          this.state.showToast(it.errorMessage);
          goBackToLoginPage();
        });
      }
    });
  }

  gotoHomePage() {
    Navigator.pushAndRemoveUntil(
      state.context,
      MaterialPageRoute(builder: (context) => HomePage()),
      (Route<dynamic> route) => false,
    );

    //    Navigator.pushReplacement(
//        state.context, MaterialPageRoute(builder: (context) => HomePage()));
  }

  goBackToLoginPage() {
    Navigator.pushAndRemoveUntil(
      state.context,
      MaterialPageRoute(builder: (context) => LoginPage()),
      (Route<dynamic> route) => false,
    );
//    Navigator.pushReplacement(
//        state.context, MaterialPageRoute(builder: (context) => LoginPage()));
  }

//  sendLoginRequest(LoginRequest loginRequest) async {
//    Logger.v('show daatas');
////    ProgressBar progressBar = ProgressBar();
////    Future.delayed(Duration.zero, () => progressBar.show(this.state.context));
//    progressBar.show(this.state.context);
//    this.userBloc.sendLoginRequest(loginRequest).listen((it) {
//      if (it == null) return;
//      Logger.v("inside logic sendLogicRequest");
//
//      if (it.status == Status.SUCCESS) {
//        //  progressBar.dismiss(this.state.context);
//
//        var sessionManager = SessionManager.getInstance(state.context);
//        Session session = Session();
//        session.isSessionActive = true;
//        session.user = it.data;
//        sessionManager.persist(session);
//
//        syncSpeciesList();
//
//      } else if (it.status == Status.TOKEN_EXPIRED) {} else
//      if (it.status == Status.NO_INTERNET_AVAILABLE) {
//        progressBar.dismiss(this.state.context);
//        this.state.showToast("No Internet Connection");
//        goBackToLoginPage();
//      } else {
//        //test
////         timers = Timer(const Duration(milliseconds: 5000), () {
////          progressBar.dismiss(this.state.context);
////          this.state.showToast(it.errorMessage);
////        });
//
//
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          state.setState(() {
//            progressBar.dismiss(this.state.context);
//            this.state.showToast(it.errorMessage);
//            goBackToLoginPage();
//          });
//        });
//      }
//    });
//  }

//send with fcm
  sendLoginRequest(LoginRequest loginRequest) async {
    progressBar.show(this.state.context);
    this.userBloc.sendLoginRequest(loginRequest).listen((it)async {
      if (it == null) return;
      Logger.v("inside logic sendLogicRequest");

      if (it.status == Status.SUCCESS) {
        var sessionManager = SessionManager.getInstance(state.context);
        Session session = Session();
        session.isSessionActive = true;
        session.user = it.data;
        sessionManager.persist(session);


//        techId = it.data.technicianId;
//        Logger.v("TechId:"+techId);
//
        if(session.isSessionActive = true){
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('technicianId', loginRequest.technicianId);

        }else{
//          techId = "";
        }


//        syncSpeciesList();

        gotoHomePage();
      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismiss(this.state.context);
        this.state.showToast("No Internet Connection");
        goBackToLoginPage();
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          state.setState(() {
            progressBar.dismiss(this.state.context);
            this.state.showToast(it.errorMessage);
            goBackToLoginPage();
          });
        });
      }
    });
  }

  sendForgotPasswordRequest(String technicianId) async {
    progressBar.show(this.state.context);
    this.userBloc.sendForgotPasswordRequest(technicianId).listen((it) {
      if (it == null) return;
      Logger.v("inside logic sendLogicRequest");
      Logger.v("technicianID" + technicianId);

      if (it.status == Status.SUCCESS) {
        progressBar.dismissToLogin(this.state.context);
        this.state.showToast(it.data);

      } else if (it.status == Status.TOKEN_EXPIRED) {
      } else if (it.status == Status.NO_INTERNET_AVAILABLE) {
        progressBar.dismissToLogin(this.state.context);
        this.state.showToast("No Internet Connection");
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          state.setState(() {
            progressBar.dismissToLogin(this.state.context);
            this.state.showToast(it.errorMessage);
          });
        });
      }
    });
  }


//  void onRememberMeChanged(bool newValue) => state.setState(() {
//    state.rememberMe = newValue;
//
//    if (state.rememberMe) {
//      // TODO: Here goes your functionality that remembers the user.
//    } else {
//      // TODO: Forget the user
//    }
//  });
//
//  saveLoginDetails(LoginRequest loginRequest)async{
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.setString('technicianId', loginRequest.technicianId);
//  }


}
