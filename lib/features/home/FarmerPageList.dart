import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/AnimalPageList.dart';
import 'package:nbis/features/home/FarmerPageListLogic.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/s/ToastUtils.dart';

class FarmerPageList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FarmerPageListState();
  }
}

class FarmerPageListState extends State<FarmerPageList> {
  FarmerPageListLogic logic;
  FarmerBloc farmerBloc;
  AnimalBloc animalBloc;
  Farmer farmer;
  int count = 0;
  var refreshFarmerListKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    farmerBloc = FarmerBloc(FarmerRepo(context));
    animalBloc = AnimalBloc(AnimalRepo(context));
    logic = FarmerPageListLogic(this, farmerBloc, animalBloc);
    refreshFarmerList();

//    this.logic.onPostInit();
  }

  Future<Null> refreshFarmerList() async {
    refreshFarmerListKey.currentState?.show(atTop: false);

    setState(() {
      this.logic.onPostInit();
    });

    return null;
  }

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  Widget build(BuildContext context) {
    final tabSync = <Tab>[
      Tab(text: "Not Sync", icon: new Icon(Icons.sync)),
      Tab(text: " Sync", icon: new Icon(Icons.sync))
    ];

    return DefaultTabController(
      length: tabSync.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Registered Farmer",
//            AppTranslations.of(context).text("farmer_registered_title"),
          ),
//          bottom: TabBar(tabs: tabSync),
//        actions: <Widget>[
//          IconButton(
//            icon: Icon(Icons.search),
//            onPressed: () {
//              showSearch(
//                context: context,
////                delegate: CustomSearchDelegate(),
//              );
//            },
//          ),
//        ],
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 45.0,
            color: Theme.of(context).primaryColor,
            child: GestureDetector(
              onTap: () {
                logic.onAddFarmer();
              },
              child: Row(
//                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.person_add,
                    color: Colors.white,
                  ),
                  Divider(
                    indent: 4.0,
                  ),
                  Text(
                    "Register Farmer",
//                          AppTranslations.of(context)
//                              .text("farmer_register_button"),
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),


//                  Expanded(
//                    flex: 4,
//                    child: Text(''),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: Row(
//                      children: <Widget>[
//                        Icon(
//                          Icons.person_add,
//                          color: Colors.white,
//                        ),
//                        Divider(
//                          indent: 12.0,
//                        ),
//                        Text(
//                          "Register Farmer",
////                          AppTranslations.of(context)
////                              .text("farmer_register_button"),
//                          style: TextStyle(
//                              color: Colors.white, fontWeight: FontWeight.bold),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Text(''),
//                  ),
                ],
              ),
            ),
          ),
        ),
        body: BlocProvider(
          child: SafeArea(
            child: Column(
              children: <Widget>[
//                Expanded(
//                  child: TabBarView(
//                    children: <Widget>[
//                      logic.farmerUnSyncList.isEmpty
//                          ? noData()
//                          : getUnSyncFarmerList(),
//                      logic.farmerSyncList.isEmpty
//                          ? noData()
//                          : getSyncFarmerList(),
//                    ],
//                  ),
//                ),
                Expanded(
                  child: RefreshIndicator(
                    key: refreshFarmerListKey,
                    child: this.logic.farmerList.isEmpty
                        ? noData()
                        : getFarmerList(),
                    onRefresh: refreshFarmerList,
                  ),
                ),
              ],
            ),
          ),
        ),

//floating action button
//      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
//      floatingActionButton: Column(
//        //  crossAxisAlignment: CrossAxisAlignment.stretch,
//        mainAxisSize: MainAxisSize.min,
//
//        children: <Widget>[
//          FloatingActionButton.extended(
//            isExtended: true,
//            label: Text("Register Farmer"),
//            icon: Icon(Icons.person_add),
//            tooltip: 'Add Farmer',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              this.logic.onAddFarmer();
//            },
//          ),
//        ],
//      ),
      ),
    );
  }

  Widget noData() {
    return Container(
      child: Center(
//        child: Text(
//          AppTranslations.of(context).text("farmer_noData"),
//        ),
        child: Text("No Farmer Data has been found !!"),
      ),
    );
  }

  ListView getFarmerList() {
    return ListView.builder(
        addAutomaticKeepAlives: true,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.farmerList.length,
        itemBuilder: (BuildContext context, int position) {
          farmer = this.logic.farmerList[position];

          editButton() {
            DateTime lastestDate = DateTime.now();

            if (farmer.syncDate != null) {
              String farmerSyncDate = farmer.syncDate == null
                  ? ""
                  : farmer.syncDate.split(".").first;

              DateTime syncDate = DateTime.tryParse(farmerSyncDate);

              DateTime addedDate = syncDate.add(new Duration(hours: 24));
//              if (syncDate.add(new Duration(hours: 24)) == lastestDate && farmer.syncToServer == true) {

              if (lastestDate.compareTo(addedDate) == 1) {
//                  && farmer.syncToServer == true) {
//              if (addedDate.difference(syncDate).inHours >= 24 && farmer.syncToServer == true) {
//              if (lastestDate.difference(syncDate).inHours >= 24 && farmer.syncToServer == true) {
                return Container(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(50.0, 0, 0, 0),
                    child: Text(""),
                  ),
//                  child: Text("              "),
                );
              } else {
                return GestureDetector(

                    child: Container(
                      width: 40.0,
                      height: 40.0,
                      child: Card(
                        color: Colors.white70,
                        elevation: 3.0,
                        child: Image.asset(

                          "images/edits.png",
//                          color: Colors.black,
                        ),
//                  child: IconButton(
//                      icon: Icon(
//                        Icons.edit,
//                        color: Colors.black,
//                      ),
//                      onPressed: () {
//                        this.logic.onEditFarmer(
//                            this.logic.farmerList[position], position);
//                      }),
                      ),

                    ),
                    onTap: () {
                  this.logic.onEditFarmer(
                      this.logic.farmerList[position], position);
                },



//                  child: IconButton(
//                      icon: Icon(
//                        Icons.edit,
//                        color: Colors.black,
//                      ),
//                      onPressed: () {
//                        this.logic.onEditFarmer(
//                            this.logic.farmerList[position], position);
//                      }),
                    );
              }
            } else {
              return GestureDetector(

                  child: Container(
                    width: 40.0,
                    height: 40.0,
                    child: Card(
                      color: Colors.white70,
                      elevation: 3.0,
                      child: Image.asset(
                        "images/edits.png",
//                        color: Colors.black,
                      ),
//                  child: IconButton(
//                      icon: Icon(
//                        Icons.edit,
//                        color: Colors.black,
//                      ),
//                      onPressed: () {
//                        this.logic.onEditFarmer(
//                            this.logic.farmerList[position], position);
//                      }),
                    ),

                  ),
                onTap: () {
                  this.logic.onEditFarmer(
                      this.logic.farmerList[position], position);
                },
              );
            }
          }

          if (farmer.syncDate != null) {
//          if (farmer.syncToServer == true) {
            return Card(
              elevation: 2.0,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Container(
                          width: 35.0,
                          height: 35.0,
                          child: farmer.syncToServer == true
                              ? Image.asset(
                                  'images/farmers.png',
//                                  "images/xxhdpi/farmer.png",
                                  color: Colors.green,
                                )
                              : Image.asset(
                                  'images/farmers.png',
//                                  "images/xxhdpi/farmer.png",
                                  color: Colors.red,
                                ),
                        ),
                      ),
                      Expanded(
                        flex: 5,
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                farmer.farmerName,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 15.0),
                              ),
                              Text(
                                farmer.mobileNumber,
                                style: TextStyle(fontSize: 14.0),
                              ),
                              Text(
                                farmer.province +
                                    "-" +
                                    farmer.district +
                                    "-" +
                                    farmer.municipality,
                                style: TextStyle(fontSize: 13.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: GestureDetector(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              editButton(),
                              GestureDetector(
                                child: Container(
                                  width: 40.0,
                                  height: 40.0,
                                  child: Card(
                                    color: Colors.white70,
                                    elevation: 3.0,
                                    child: Image.asset(
                                      "images/cow.png",
                                      color: Colors.black,
                                    ),
                                  ),
//                                      Image.asset('images/xxhdpi/animals.png'),
                                ),
                                onTap: () {
                                  setState(() {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                AnimalPageList(
                                                    farmer:
                                                        this.logic.farmerList[
                                                            position])));
                                  });
                                },
                              ),
                            ],
                          ),
//
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(flex: 2, child: Text("")),
                      Expanded(
                        flex: 8,
                        child: Column(
//                        child: Row(
                          children: <Widget>[
                            //two line
                            Row(
                              children: <Widget>[
                                Text(
                                    "Buffalo: " +
                                        logic.animalList
                                            .where((item) =>
                                        item.speciesName == "Buffalo" &&
                                            item.mfarmerId == farmer.fId)
                                            .length
                                            .toString(),
                                    style: TextStyle(fontSize: 13.0)),
                                Text(
                                    "    Cattle: " +
                                        logic.animalList
                                            .where((item) =>
                                        item.speciesName == "Cattle" &&
                                            item.mfarmerId == farmer.fId)
                                            .length
                                            .toString(),
                                    style: TextStyle(fontSize: 13.0)),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                    "Goat: " +
                                        logic.animalList
                                            .where((item) =>
                                        item.speciesName == "Goat" &&
                                            item.mfarmerId == farmer.fId)
                                            .length
                                            .toString(),
                                    style: TextStyle(fontSize: 13.0)),
                                Text(
                                    "         Pig: " +
                                        logic.animalList
                                            .where((item) =>
                                        item.speciesName == "Pig" &&
                                            item.mfarmerId == farmer.fId)
                                            .length
                                            .toString(),
                                    style: TextStyle(fontSize: 13.0)),
                              ],
                            ),

                            //one line
//                            Text(
//                                "Buffalo: " +
//                                    logic.animalList
//                                        .where((item) =>
//                                            item.speciesName == "Buffalo" &&
//                                            item.mfarmerId == farmer.fId)
//                                        .length
//                                        .toString(),
//                                style: TextStyle(fontSize: 13.0)),
//                            Text(
//                                " Cattle: " +
//                                    logic.animalList
//                                        .where((item) =>
//                                            item.speciesName == "Cattle" &&
//                                            item.mfarmerId == farmer.fId)
//                                        .length
//                                        .toString(),
//                                style: TextStyle(fontSize: 13.0)),
//                            Text(
//                                " Goat: " +
//                                    logic.animalList
//                                        .where((item) =>
//                                            item.speciesName == "Goat" &&
//                                            item.mfarmerId == farmer.fId)
//                                        .length
//                                        .toString(),
//                                style: TextStyle(fontSize: 13.0)),
//                            Text(
//                                " Pig: " +
//                                    logic.animalList
//                                        .where((item) =>
//                                            item.speciesName == "Pig" &&
//                                            item.mfarmerId == farmer.fId)
//                                        .length
//                                        .toString(),
//                                style: TextStyle(fontSize: 13.0)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),

//              child: ListTile(
//                leading: Container(
//                  width: 30.0,
//                  height: 30.0,
//                  child: farmer.syncToServer == true
//                      ? Image.asset(
//                          "images/xxhdpi/farmer.png",
//                          color: Colors.green,
//                        )
//                      : Image.asset(
//                          "images/xxhdpi/farmer.png",
//                          color: Colors.red,
//                        ),
//                ),
//                title:
//                Container(
//
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      Text(farmer.farmerName),
//                      Text(
//                        farmer.mobileNumber,
//                        style: TextStyle(fontSize: 13.0),
//                      ),
//                      Text(
//                        farmer.province +
//                            "-" +
//                            farmer.district +
//                            "-" +
//                            farmer.municipality,
//                        style: TextStyle(fontSize: 13.0),
//                      ),
//                      Row(
////                        crossAxisAlignment: CrossAxisAlignment.center,
////                      mainAxisAlignment: MainAxisAlignment.start,
////                        mainAxisSize: MainAxisSize.max,
//                        children: <Widget>[
//                          Text("Buffalo: "+"999",
////                              logic.animalList
////                                  .where((item) =>
////                                      item.speciesName == "Buffalo" &&
////                                      item.mfarmerId == farmer.fId)
////                                  .length
////                                  .toString(),
//                              style: TextStyle(fontSize: 12.0)
//                              ),
//
//                          Text(" Cattle: "+"999",
////                              logic.animalList
////                                  .where((item) =>
////                              item.speciesName == "Cattle" &&
////                                  item.mfarmerId == farmer.fId)
////                                  .length
////                                  .toString(),
//                              style: TextStyle(fontSize: 12.0)
//                          ),
//
//                          Text(" Goat: "+"999",
////                              logic.animalList
////                                  .where((item) =>
////                              item.speciesName == "Goat" &&
////                                  item.mfarmerId == farmer.fId)
////                                  .length
////                                  .toString(),
//                              style: TextStyle(fontSize: 12.0)
//                          ),
//
//                          Text(" Pig: "+"999",
////                              logic.animalList
////                                  .where((item) =>
////                              item.speciesName == "Pig" &&
////                                  item.mfarmerId == farmer.fId)
////                                  .length
////                                  .toString(),
//                              style: TextStyle(fontSize: 12.0)
//                          ),
//
//
//                        ],
//                      ),
//                    ],
//                  ),
//                ),
//                trailing: GestureDetector(
//                  child: Row(
//                    mainAxisSize: MainAxisSize.min,
//                    children: <Widget>[
//                      editButton(),
//                      GestureDetector(
//                        child: Container(
//                          width: 30.0,
//                          height: 30.0,
//                          child: Image.asset('images/xxhdpi/animals.png'),
//                        ),
//                        onTap: () {
//                          setState(() {
//                            Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                    builder: (context) => AnimalPageList(
//                                        farmer:
//                                            this.logic.farmerList[position])));
//                          });
//                        },
//                      ),
//                    ],
//                  ),
////
//                ),
//
//              ),
            );
          } else {
            return Dismissible(
              key: Key(this.logic.farmerList[position].fId.toString()),
//              key:Key(farmer.mobileNumber + logic.farmerList.length.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (DismissDirection direction) {
                Farmer farmers = this.logic.farmerList[position];
                setState(() {
                  this.logic.farmerList.removeAt(position);
                  this.logic.confirmationForDeletion(farmers, position);
                });
              },
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Container(
                            width: 35.0,
                            height: 35.0,
                            child: farmer.syncToServer == true
                                ? Image.asset(
                                    'images/farmers.png',
//                                    "images/xxhdpi/farmer.png",
                                    color: Colors.green,
                                  )
                                : Image.asset(
                                    'images/farmers.png',
//                                    "images/xxhdpi/farmer.png",
                                    color: Colors.red,
                                  ),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  farmer.farmerName,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15.0),
                                ),
                                Text(
                                  farmer.mobileNumber,
                                  style: TextStyle(fontSize: 14.0),
                                ),
                                Text(
                                  farmer.province +
                                      "-" +
                                      farmer.district +
                                      "-" +
                                      farmer.municipality,
                                  style: TextStyle(fontSize: 13.0),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: GestureDetector(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                editButton(),

//                                IconButton(
//                                    icon: Icon(
//                                      Icons.delete,
//                                      color: Colors.black,
//                                    ),
//                                    onPressed: () {
//                                      this.logic.confirmationForDeletion(
//                                          this.logic.farmerList[position], position);
//                                    }),


                                GestureDetector(
                                  child: Container(
                                    width: 40.0,
                                    height: 40.0,
                                    child: Card(
                                      color: Colors.white70,
                                      elevation: 3.0,
                                      child: Image.asset(
                                        "images/cow.png",
                                        color: Colors.black,
                                      ),
                                    ),
//                                      Image.asset('images/xxhdpi/animals.png'),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AnimalPageList(
                                                      farmer:
                                                      this.logic.farmerList[
                                                      position])));
                                    });
                                  },
                                ),

//                                GestureDetector(
//                                  child: Container(
//                                    width: 30.0,
//                                    height: 30.0,
//                                    child: Image.asset(
//                                      "images/cow.png",
//                                      color: Colors.black,
////                                        'images/xxhdpi/animals.png'
//                                    ),
//                                  ),
//                                  onTap: () {
//                                    setState(() {
//                                      Navigator.push(
//                                          context,
//                                          MaterialPageRoute(
//                                              builder: (context) =>
//                                                  AnimalPageList(
//                                                      farmer:
//                                                          this.logic.farmerList[
//                                                              position])));
//                                    });
//                                  },
//                                ),
                              ],
                            ),
//
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(flex: 2, child: Text("")),
                        Expanded(
                          flex: 8,
                          child: Column(
//                          child: Row(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                      "Buffalo: " +
                                          logic.animalList
                                              .where((item) =>
                                          item.speciesName == "Buffalo" &&
                                              item.mfarmerId == farmer.fId)
                                              .length
                                              .toString(),
                                      style: TextStyle(fontSize: 13.0)),
                                  Text(
                                      "    Cattle: " +
                                          logic.animalList
                                              .where((item) =>
                                          item.speciesName == "Cattle" &&
                                              item.mfarmerId == farmer.fId)
                                              .length
                                              .toString(),
                                      style: TextStyle(fontSize: 13.0)),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                      "Goat: " +
                                          logic.animalList
                                              .where((item) =>
                                          item.speciesName == "Goat" &&
                                              item.mfarmerId == farmer.fId)
                                              .length
                                              .toString(),
                                      style: TextStyle(fontSize: 13.0)),
                                  Text(
                                      "         Pig: " +
                                          logic.animalList
                                              .where((item) =>
                                          item.speciesName == "Pig" &&
                                              item.mfarmerId == farmer.fId)
                                              .length
                                              .toString(),
                                      style: TextStyle(fontSize: 13.0)),
                                ],
                              ),


//two line
//                              Text(
//                                  "Buffalo: " +
//                                      logic.animalList
//                                          .where((item) =>
//                                              item.speciesName == "Buffalo" &&
//                                              item.mfarmerId == farmer.fId)
//                                          .length
//                                          .toString(),
//                                  style: TextStyle(fontSize: 13.0)),
//                              Text(
//                                  " Cattle: " +
//                                      logic.animalList
//                                          .where((item) =>
//                                              item.speciesName == "Cattle" &&
//                                              item.mfarmerId == farmer.fId)
//                                          .length
//                                          .toString(),
//                                  style: TextStyle(fontSize: 13.0)),
//                              Text(
//                                  " Goat: " +
//                                      logic.animalList
//                                          .where((item) =>
//                                              item.speciesName == "Goat" &&
//                                              item.mfarmerId == farmer.fId)
//                                          .length
//                                          .toString(),
//                                  style: TextStyle(fontSize: 13.0)),
//                              Text(
//                                  " Pig: " +
//                                      logic.animalList
//                                          .where((item) =>
//                                              item.speciesName == "Pig" &&
//                                              item.mfarmerId == farmer.fId)
//                                          .length
//                                          .toString(),
//                                  style: TextStyle(fontSize: 13.0)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),

//              Card(
//                elevation: 2.0,
//                color: Colors.white,
//                child: ListTile(
//                  leading: Container(
//                    width: 30.0,
//                    height: 30.0,
//                    child: farmer.syncToServer == true
//                        ? Image.asset(
//                            "images/xxhdpi/farmer.png",
//                            color: Colors.green,
//                          )
//                        : Image.asset(
//                            "images/xxhdpi/farmer.png",
//                            color: Colors.red,
//                          ),
//                  ),
//
////                leading: Container(
////                    width: 40.0,
////                    height: 40.0,
////                    decoration: new BoxDecoration(
////                        color: Colors.red,
////                        shape: BoxShape.circle,
////                        image: new DecorationImage(
////                            fit: BoxFit.fill,
////                            image:
////                                new AssetImage("images/xxhdpi/farmer.png")
////                        )
////
////
////                    )),
////                title: Text(
////                    farmer.farmerName + "\n" + farmer.mobileNumber.toString()),
//                  // subtitle: Text("Cattle: " + "100" + "Buffalos:" + "100"),
//
//                  title: Container(
//                    child: Column(
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Text(farmer.farmerName),
//                        Text(
//                          farmer.mobileNumber,
//                          style: TextStyle(fontSize: 13.0),
//                        ),
//                        Text(
//                          farmer.province +
//                              "-" +
//                              farmer.district +
//                              "-" +
//                              farmer.municipality,
//                          style: TextStyle(fontSize: 13.0),
//                        ),
//
////                        Row(
////                          children: <Widget>[
////                            Text(logic.animalBuffaloCount == null
////                                ? "0"
////                                : "Buffalo: "+ logic.animalBuffaloCount.toString(), style: TextStyle(fontSize: 13.0),),
////                            Text(logic.animalCattleCount == null
////                                ? "0"
////                                : " "+ "Cattle: "+ logic.animalCattleCount.toString(), style: TextStyle(fontSize: 13.0),),
////                            Text(logic.animalGoatCount == null
////                                ? "0"
////                                : " "+ "Goat: "+ logic.animalGoatCount.toString(),  style: TextStyle(fontSize: 13.0),),
////                            Text(logic.animalPigCount == null
////                                ? "0"
////                                : " "+ "Pig: "+ logic.animalPigCount.toString(),  style: TextStyle(fontSize: 13.0),),
////                          ],
////                        ),
//                      ],
//                    ),
//                  ),
//                  trailing: GestureDetector(
//                    child: Row(
//                      mainAxisSize: MainAxisSize.min,
//                      children: <Widget>[
//                        editButton(),
//                        GestureDetector(
//                          child: Container(
//                            width: 30.0,
//                            height: 30.0,
//                            child: Image.asset('images/xxhdpi/animals.png'),
//                          ),
//                          onTap: () {
//                            setState(() {
//                              Navigator.push(
//                                  context,
//                                  MaterialPageRoute(
//                                      builder: (context) => AnimalPageList(
//                                          farmer: this
//                                              .logic
//                                              .farmerList[position])));
//                            });
//                          },
//                        ),
//                      ],
//                    ),
////                  child: Icon(
////                    Icons.add,
////                    size: 30.0,
////                    color: Colors.black,
////                  ),
////                  onTap: () {
////                    Navigator.push(
////                        context,
////                        MaterialPageRoute(
////                            builder: (context) => AnimalPageList(farmer: this.logic.farmerList[position])));
////                  },
//                  ),
////                onTap: () {
////                  this
////                      .logic
////                      .onEditFarmer(this.logic.farmerList[position], position);
////                },
//                ),
//              ),
            );
          }
        });
  }

//  ListView getUnSyncFarmerList() {
//    return ListView.builder(
//        shrinkWrap: true,
//        scrollDirection: Axis.vertical,
//        itemCount: logic.farmerUnSyncList.length,
//        itemBuilder: (BuildContext context, int position) {
//          farmer = this.logic.farmerUnSyncList[position];
//          return Dismissible(
//            key: Key(this.logic.farmerUnSyncList[position].fId.toString()),
//            direction: DismissDirection.endToStart,
//            onDismissed: (DismissDirection direction) {
//              Farmer farmers = this.logic.farmerUnSyncList[position];
//              this.logic.confirmationDelete(farmers, position);
//            },
//            background: Container(
//              alignment: AlignmentDirectional.centerEnd,
//              color: Colors.red,
//              child: Padding(
//                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
//                child: Icon(
//                  Icons.delete,
//                  color: Colors.white,
//                ),
//              ),
//            ),
//            child: Card(
//              elevation: 2.0,
//              color: Colors.white,
//              child: ListTile(
//                leading: Container(
//                    width: 40.0,
//                    height: 40.0,
//                    decoration: new BoxDecoration(
//                        color: Colors.white,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.fill,
//                            image:
//                            new AssetImage("images/xxhdpi/farmer.png")))),
////                title: Text(
////                    farmer.farmerName + "\n" + farmer.mobileNumber.toString()),
//                // subtitle: Text("Cattle: " + "100" + "Buffalos:" + "100"),
//
//                title: Container(
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      Text(farmer.farmerName),
//                      Text(farmer.mobileNumber),
////                     Row(
////                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
////                       children: <Widget>[
////                         Text("Cattle: "+"100",style: TextStyle(fontSize: 12.0),),
////                         Text(""),
////                         Text("Buffalo: "+"100",style: TextStyle(fontSize: 12.0),),
////                       ],
////                     ),
//                    ],
//                  ),
//                ),
//                trailing: GestureDetector(
//                  child: Row(
//                    mainAxisSize: MainAxisSize.min,
//                    children: <Widget>[
//                      IconButton(
//                          icon: Icon(
//                            Icons.edit,
//                            color: Colors.black,
//                          ),
//                          onPressed: () {
//                            this.logic.onEditFarmer(
//                                this.logic.farmerUnSyncList[position],
//                                position);
//                          }),
//                      IconButton(
//                          icon: Icon(
//                            Icons.add,
//                            color: Colors.black,
//                          ),
//                          onPressed: () {
//                            setState(() {
//                              Navigator.push(
//                                  context,
//                                  MaterialPageRoute(
//                                      builder: (context) =>
//                                          AnimalPageList(
//                                              farmer: this
//                                                  .logic
//                                                  .farmerUnSyncList[position])));
//                            });
//                          }),
//                    ],
//                  ),
////                  child: Icon(
////                    Icons.add,
////                    size: 30.0,
////                    color: Colors.black,
////                  ),
////                  onTap: () {
////                    Navigator.push(
////                        context,
////                        MaterialPageRoute(
////                            builder: (context) => AnimalPageList(farmer: this.logic.farmerList[position])));
////                  },
//                ),
////                onTap: () {
////                  this
////                      .logic
////                      .onEditFarmer(this.logic.farmerList[position], position);
////                },
//              ),
//            ),
//          );
//        });
//  }

//sync farmer
//  ListView getSyncFarmerList() {
//    return ListView.builder(
//        shrinkWrap: true,
//        scrollDirection: Axis.vertical,
//        itemCount: logic.farmerSyncList.length,
//        itemBuilder: (BuildContext context, int position) {
//          farmer = this.logic.farmerSyncList[position];
//          return Dismissible(
//            key: Key(this.logic.farmerSyncList[position].fId.toString()),
//            direction: DismissDirection.endToStart,
//            onDismissed: (DismissDirection direction) {
//              Farmer farmers = this.logic.farmerSyncList[position];
//              this.logic.confirmationDelete(farmers, position);
//            },
//            background: Container(
//              alignment: AlignmentDirectional.centerEnd,
//              color: Colors.red,
//              child: Padding(
//                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
//                child: Icon(
//                  Icons.delete,
//                  color: Colors.white,
//                ),
//              ),
//            ),
//            child: Card(
//              elevation: 2.0,
//              color: Colors.white,
//              child: ListTile(
//                leading: Container(
//                    width: 40.0,
//                    height: 40.0,
//                    decoration: new BoxDecoration(
//                        color: Colors.white,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.fill,
//                            image:
//                            new AssetImage("images/xxhdpi/farmer.png")))),
////                title: Text(
////                    farmer.farmerName + "\n" + farmer.mobileNumber.toString()),
//                // subtitle: Text("Cattle: " + "100" + "Buffalos:" + "100"),
//
//                title: Container(
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      Text(farmer.farmerName),
//                      Text(farmer.mobileNumber),
////                     Row(
////                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
////                       children: <Widget>[
////                         Text("Cattle: "+"100",style: TextStyle(fontSize: 12.0),),
////                         Text(""),
////                         Text("Buffalo: "+"100",style: TextStyle(fontSize: 12.0),),
////                       ],
////                     ),
//                    ],
//                  ),
//                ),
//                trailing: GestureDetector(
//                  child: Row(
//                    mainAxisSize: MainAxisSize.min,
//                    children: <Widget>[
////                      IconButton(
////                          icon: Icon(
////                            Icons.edit,
////                            color: Colors.black,
////                          ),
////                          onPressed: () {
////                            this.logic.onEditFarmer(
////                                this.logic.farmerSyncList[position], position);
////                          }),
//                      IconButton(
//                          icon: Icon(
//                            Icons.add,
//                            color: Colors.black,
//                          ),
//                          onPressed: () {
//                            setState(() {
//                              Navigator.push(
//                                  context,
//                                  MaterialPageRoute(
//                                      builder: (context) =>
//                                          AnimalPageList(
//                                              farmer: this
//                                                  .logic
//                                                  .farmerSyncList[position])));
//                            });
//                          }),
//                    ],
//                  ),
////                  child: Icon(
////                    Icons.add,
////                    size: 30.0,
////                    color: Colors.black,
////                  ),
////                  onTap: () {
////                    Navigator.push(
////                        context,
////                        MaterialPageRoute(
////                            builder: (context) => AnimalPageList(farmer: this.logic.farmerList[position])));
////                  },
//                ),
////                onTap: () {
////                  this
////                      .logic
////                      .onEditFarmer(this.logic.farmerList[position], position);
////                },
//              ),
//            ),
//          );
//        });
//  }
}
