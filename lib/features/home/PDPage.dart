import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/bloc/pdBloc/PDBloc.dart';
import 'package:nbis/features/home/PDPageLogic.dart';
import 'package:nbis/features/repo/AIRepo.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/features/repo/PDRepo.dart';
import 'package:nbis/features/repo/PregnancyDiagnosis.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';
import 'package:nepali_utils/nepali_utils.dart';

class PDPage extends StatefulWidget {
  bool isEdit;
  PregnancyDiagnosis pd;
  Animal animal;

  PDPage({this.pd, this.animal, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return PDPageState(pd: this.pd, animal: this.animal, isEdit: this.isEdit);
  }
}

class PDPageState extends State<PDPage> {
  bool isEdit;
  PregnancyDiagnosis pd;
  PDBloc pdBloc;
  PDPageLogic logic;
  TextFormField tfCalvingDate;
  TextFormField tfDatePD;
  AnimalBloc animalBloc;
  FarmerBloc farmerBloc;
  AIBloc aiBloc;
  Animal animal;

  TextFormField tfMobileNumber;
  TextFormField tfFarmer;
  TextFormField tfAddress;
  TextFormField tfSpeciesCode;
  TextFormField tfSpecies;
  TextFormField tfBreed;
  AppBar appBar;
  NepaliDateTime selectedDateTime;
  Language language = Language.ENGLISH;
  TextEditingController calvingDate = TextEditingController(text: " ");
  TextEditingController pdDateController = TextEditingController(text: " ");

  TextEditingController mblController = TextEditingController();
  TextEditingController farmerController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController aCodeController = TextEditingController();
  TextEditingController speciesController = TextEditingController();
  TextEditingController breedController = TextEditingController();
  FocusNode textFocus = new FocusNode();

  PDPageState({this.pd, this.animal, this.isEdit = false});

  GlobalKey<FormState> pdFormKey = GlobalKey<FormState>();
  static var farmer = ['Select Farmer', 'Abd', 'cde'];
  int selected = -1;

  void showToast(String message) {
    ToastUtils.show(message);
  }

  void onNumberChanged() {
    String mobileText = mblController.text;
//    bool hasFocus = textFocus.hasFocus;
    Logger.v(mobileText);
    setState(() {
      mblController.selection = new TextSelection(
          baseOffset: mobileText.length, extentOffset: mobileText.length);
    });

    if (mobileText.length == 10) {
      //   this.logic.getFarmerByMobileNumber(int.tryParse(mobileText));
      this.logic.getFarmerByMobileNumber(mobileText);
    }
  }

  void onAnimalChanged() {
    String animalText = aCodeController.text;
    Logger.v('getCOde:' + aCodeController.text);
//    bool hasFocus = textFocus.hasFocus;
    Logger.v(animalText);

    setState(() {
      aCodeController.selection = new TextSelection(
          baseOffset: animalText.length, extentOffset: animalText.length);
    });

//    this.logic.getAnimalBySpeciesCode(mblController.text + "-" + animalText);
    this.logic.getAnimalBySpeciesCode( animalText);
  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

  @override
  initState() {
    super.initState();
    this.pdBloc = PDBloc(PDRepo(context));
    this.animalBloc = AnimalBloc(AnimalRepo(context));
    this.farmerBloc = FarmerBloc(FarmerRepo(context));
    this.aiBloc = AIBloc(AIRepo(context));
    this.logic = PDPageLogic(this, pdBloc, animalBloc, farmerBloc, aiBloc);
    mblController.text = animal == null ? "" : animal.mobileNumber;
//    mblController.text = animal == null ? "" : animal.speciesCode.split("-")[0];
    aCodeController.text =
//        animal == null ? "" : animal.speciesCode.split("-")[1];
        animal == null ? "" : animal.speciesCode;

    logic.onPostState();
    initUIController();

    if(animal != null){
      Logger.v("animal"+animal.speciesCode);
      Logger.v("farmer number: "+animal.mobileNumber);

      if (animal.mobileNumber.length == 10) {
        Logger.v("farmer number inside: "+animal.mobileNumber);
        this.logic.getFarmerByMobileNumber(animal.mobileNumber);
      }
      setState((){

      });
      this.logic.getAnimalBySpeciesCode(animal.speciesCode);
    }

  }

  void initUIController() {
    mblController.addListener(onNumberChanged);
    aCodeController.addListener(onAnimalChanged);
  }

  @override
  void dispose() {
    mblController.removeListener(onNumberChanged);
    aCodeController.removeListener(onAnimalChanged);
    mblController.dispose();
    textFocus.dispose();
    super.dispose();
  }

  Widget getPDForm() {
    PregnancyDiagnosis pd = this.logic.pd;

    //mobile number tf
    tfMobileNumber = TextFormField(
        keyboardType: TextInputType.phone,
        controller: mblController,
//        autofocus: true,
        maxLength: 10,
        decoration: InputDecoration(
          labelText: "Farmer's Mobile Number",
//          labelText: AppTranslations.of(context).text("pd_form_mobile_number"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String mbl) {
          pd.mobileNumber = mbl;
        },
        validator: (mblNumber) {
          if (mblNumber.isEmpty) {
//            return AppTranslations.of(context).text("mobile_number_validate");
            return "Mobile Number can't be empty !!";
          } else if (mblNumber.length != 10) {
//            return AppTranslations.of(context)
//                .text("mobile_number_ten_digit_validate");
            return "Please enter ten digits number !!";
          }
        });

    //farmer name tf
    tfFarmer = TextFormField(
        enabled: false,
        controller: farmerController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText:"Farmer's Name",
//          labelText: AppTranslations.of(context).text("pd_form_farmer_name"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String farmer) {
          pd.farmerName = farmer;
        },
        validator: (farmer) {
          if (farmer.isEmpty) {
            return "Farmer Name can't be empty";
          }
        });

    //address tf
    tfAddress = TextFormField(
        enabled: false,
        controller: addressController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: "Address",
//          labelText: AppTranslations.of(context).text("pd_form_address"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String address) {},
        validator: (address) {
          if (address.isEmpty) {
            return "Address can't be empty";
          }
        });

    //species tf
    tfSpecies = TextFormField(
        enabled: false,
        controller: speciesController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
         labelText: "Animal",
//          labelText: AppTranslations.of(context).text("pd_form_species"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String species) {},
        validator: (species) {
          if (species.isEmpty) {
            return "Species can't be empty";
          }
        });

    //breed
    tfBreed = TextFormField(
        enabled: false,
        controller: breedController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText:"Breed",
//          labelText: AppTranslations.of(context).text("pd_form_breed"),
          //  icon: Icon(Icons.mobile_screen_share),
        ),
        onSaved: (String breed) {},
        validator: (breed) {
          if (breed.isEmpty) {
            return "Breed can't be empty";
          }
        });

//    DropdownButton dbFarmer = DropdownButton(
//        items: farmer.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        value: 'Select Farmer',
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

    //animal code

    tfSpeciesCode = TextFormField(
        keyboardType: TextInputType.number,
        controller: aCodeController,
//        autofocus: true,
        decoration: InputDecoration(
          //   icon: Icon(Icons.confirmation_number),
          labelText: "Animal Code",
//          labelText: AppTranslations.of(context).text("pd_form_code"),
          prefixText: mblController.text + "-",
          //    hintText: "XXXX-XXXXX-XXXXXX",
        ),
        onSaved: (String animalCode) {
//          String animalsCode = mblController.text+"-"+animalCode;
          pd.speciesCode = animalCode;
        },
        validator: (animalCode) {
          if (animalCode.isEmpty) {
//            return AppTranslations.of(context).text("code_validate");
            return " Animal Code can't be empty !!";
          }
        });

    //date test

    tfDatePD = TextFormField(
        keyboardType: TextInputType.number,
        controller: pdDateController,
        decoration: InputDecoration(
          labelText: "Date of PD",
//          labelText: AppTranslations.of(context).text("pd_form_date"),
//          hintText: "Select date",
        ),
        onSaved: (String pdDate) {
          pd.pdDate = pdDate;
        },
        validator: (pdDate) {
          if (pdDate.isEmpty) {
//            return AppTranslations.of(context).text("pdDate_validate");
            return "Date of AI can't be empty !!";
          }
        });

    // calving date

    tfCalvingDate = TextFormField(
      keyboardType: TextInputType.number,
      controller: calvingDate,
      decoration: InputDecoration(
        //   icon: Icon(Icons.date_range),
        labelText:
          "Expected Calving Date",
//            AppTranslations.of(context).text("pd_form_expected_calving_date"),
//        hintText: "Select date",
      ),
      onSaved: (String date) {
        pd.expectedDeliveryDate = date;
      },
//        validator: (date) {
//          if (date.isEmpty) {
//            return "Expected calving date can't be empty";
//          }
//        }
//
    );

//    //positive radio button
//    Radio positiveRadio = Radio(
//      value: 0,
//      groupValue: radioValue,
//      //onChanged: _handleRadioValueChange,
//    );
//
//    //negative radio button
//    Radio negativeRadio = Radio(
//      value: 1,
//      groupValue: radioValue,
//      //onChanged: ,
//    );

    radioPDReport() {
      List<Widget> radioPdList = new List<Widget>();
      radioPdList.add(new Row(
        children: <Widget>[
          Row(
            children: <Widget>[
              Radio(

                  value: 0,
                  groupValue: selected,
                  onChanged: (int value) {
                    logic.selectedReport(value);
                    pd.pdResult = true;
                  }),
              Text('+Ve'),
            ],
          ),
          Row(
            children: <Widget>[
              Radio(
                  value: 1,
                  groupValue: selected,
                  onChanged: (int value) {
                    logic.selectedReport(value);
                    pd.pdResult = false;
                  }),
              Text('-Ve'),
            ],
          ),
        ],
      ));
      return radioPdList;
    }

    //remarks
    TextFormField tfRemarks = TextFormField(
      keyboardType: TextInputType.multiline,
      initialValue: this.pd == null ? "" : this.pd.remarks,
      maxLines: null,
      decoration: InputDecoration(
        //   icon: Icon(Icons.ac_unit),
        labelText: "Remarks",
//        labelText: AppTranslations.of(context).text("pd_form_remarks"),
      ),
      onSaved: (String remarks) {
        pd.remarks = remarks;
      },
//        validator: (remarks) {
//          if (remarks.isEmpty) {
//            return " Remarks can't be empty";
//          }
//        }
    );
//Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text(
     "Save",
//        AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (pdFormKey.currentState.validate()) {
          pdFormKey.currentState.save();
          this.logic.savePD();
        }
      },
    );

    //Raised Update Button
    RaisedButton rbUpdate = RaisedButton(
      child: Text(
        "Update",
//        AppTranslations.of(context).text("form_update"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (pdFormKey.currentState.validate()) {
          pdFormKey.currentState.save();
          this.logic.updatePD();
        }
      },
    );

    //astrick
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: pdFormKey,
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
//          Padding(
//            padding: EdgeInsets.only(top: 6.0),
//            child: ListTile(
//              title: Row(
//                children: <Widget>[
//                  //  Icon(Icons.select_all),
//                  Padding(
//                    padding: EdgeInsets.only(left: 12.0),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: dbFarmer,
//                  ),
//                  Expanded(
//                    flex: 1,
//                    child: astrickText,
//                  ),
//                ],
//              ),
//            ),
//          ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfMobileNumber,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfFarmer,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfAddress,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfSpeciesCode,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfSpecies,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),

            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfBreed,
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(""),
//                    child: astrickText,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: InkWell(
                      onTap: () {
                        this.logic.showDatePickerPD();
                      },
                      child: IgnorePointer(
                        child: tfDatePD,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 14.0, top: 8.0),
              child: Text(
//                AppTranslations.of(context).text("pd_form_report"),
                'Report:',
                //  textAlign: TextAlign.left,
              ),
            ),
            ListTile(
              title: Row(
                children: radioPDReport(),
//              children: <Widget>[
//                //   Icon(Icons.repeat_one),
//                positiveRadio,
//                Text('+Ve'),
//                negativeRadio,
//                Text('-Ve'),
//              ],
              ),
            ),
            ListTile(
              title: Visibility(
                visible: this.logic.selectedPositive(),
                child:Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 19,
                          child: InkWell(
                            onTap: () {
                              this.logic.showDatePickers();
                            },
                            child: IgnorePointer(
                              child: tfCalvingDate,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text(''),
                        ),
                      ],
                    ),


                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 19,
                              child: tfRemarks,

                        ),
                        Expanded(
                          flex: 1,
                          child: Text(''),
                        ),
                      ],
                    ),

                  ],
                ),


                replacement: tfRemarks,
              ),
//            title: Row(
//              children: <Widget>[
//
//
//                Expanded(
//                  flex: 7,
//                  child: tfCalvingDate,
//                ),
//                Expanded(
//                  flex: 1,
//                  child: astrickText,
//                ),
//              ],
//            ),
            ),

//            ListTile(
//              title: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 19,
//                    child: tfRemarks,
//                  ),
//                  Expanded(
//                    flex: 1,
//                    // child: astrickText,
//                    child: Text(""),
//                  ),
//                ],
//              ),
//            ),
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: Visibility(
                      visible: !this.isEdit,
                      child: rbSave,
                      replacement: rbUpdate,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(''),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: this.appBar,
      body: SafeArea(
        child: BlocProvider(
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      //   Log.v("scrolled");
                    },
                    child: SingleChildScrollView(
                      child: getPDForm(),
                    ),
                  ),

                  //
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
