import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/aiBloc/AIBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/calvingBloc/CalvingBloc.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/bloc/nbisBloc/NbisBloc.dart';
import 'package:nbis/features/bloc/nitrogenBloc/NitrogenBloc.dart';
import 'package:nbis/features/bloc/notificationBloc/NotificationBloc.dart';
import 'package:nbis/features/bloc/pdBloc/PDBloc.dart';
import 'package:nbis/features/bloc/semenBloc/SemenBloc.dart';
import 'package:nbis/features/home/SyncServerPageLogic.dart';
import 'package:nbis/features/repo/AIRepo.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/CalvingRepo.dart';
import 'package:nbis/features/repo/ExitRecordRepo.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/features/repo/LiquidNitrogenRepo.dart';
import 'package:nbis/features/repo/NbisRepo.dart';
import 'package:nbis/features/repo/NotificationRepo.dart';
import 'package:nbis/features/repo/PDRepo.dart';
import 'package:nbis/features/repo/Semen.dart';
import 'package:nbis/features/repo/SemenRepo.dart';
import 'package:nbis/s/ToastUtils.dart';

class SyncServerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SyncServerPageState();
  }
}

class SyncServerPageState extends State<SyncServerPage> {
  SyncServerPageLogic logic;
  AnimalBloc animalBloc;
  NbisBloc nbisBloc;
  ExitRecordBloc exitRecordBloc;
  FarmerBloc farmerBloc;
  AIBloc aiBloc;
  CalvingBloc calvingBloc;
  NitrogenBloc nitrogenBloc;
  PDBloc pdBloc;
  SemenBloc semenBloc;
  NotificationBloc notificationBloc;
  var firstPressSync = true;
  var firstPressRestore = true;

  @override
  void initState() {
    super.initState();
    this.animalBloc = AnimalBloc(AnimalRepo(context));
    this.exitRecordBloc = ExitRecordBloc(ExitRecordRepo(context));
    this.nbisBloc = NbisBloc(NbisRepo(context));
    this.farmerBloc = FarmerBloc(FarmerRepo(context));
    this.aiBloc = AIBloc(AIRepo(context));
    this.calvingBloc = CalvingBloc(CalvingRepo(context));
    this.nitrogenBloc = NitrogenBloc(LiquidNitrogenRepo(context));
    this.pdBloc = PDBloc(PDRepo(context));
    this.semenBloc = SemenBloc(SemenRepo(context));
    this.notificationBloc = NotificationBloc(NotificationRepo(context));
    this.logic = SyncServerPageLogic(
        this,
        animalBloc,
        exitRecordBloc,
        nbisBloc,
        farmerBloc,
        aiBloc,
        calvingBloc,
        nitrogenBloc,
        pdBloc,
        semenBloc,
        notificationBloc);

    logic.onPostInit();
  }

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  Widget build(BuildContext context) {
//    Semen semen = Semen();
    return Scaffold(
      appBar: AppBar(
        title: Text("Sync"),
      ),
      body: BlocProvider(
        child: SafeArea(
          child: NotificationListener<ScrollNotification>(
            onNotification: (scrollNotification) {
              //   Log.v("scrolled");
            },
            child: SingleChildScrollView(
              child: Column(
                //  crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  //single button to sync

//                  Container(
//                    child: Row(
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Sync To Server",
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendSemenLists();
//                                Navigator.of(context).push(PageRouteBuilder(
//                                    opaque: false,
//                                    pageBuilder:
//                                        (BuildContext context, _, __) =>
//                                            log()));
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//
//                  Container(
//                    child: Row(
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Sync From Server",
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                //this.logic.sendFarmerList();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),

                  //end single button

//              new Container(
//                  margin: new EdgeInsets.fromLTRB(55, 0, 10, 0),
//                  width: 250.0,
//                  height: 55.0,
//                  child: Card(
//                    clipBehavior: Clip.antiAlias,
//                    shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.circular(8.0)),
//                    color: Colors.white,
//                    child: Row(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: <Widget>[
//                        Container(
//                          color: Colors.blue,
//                          width: 15,
//                        ),
//                        SizedBox(
//                          width: 10.0,
//                        ),
//                        Expanded(
//                          child: GestureDetector(
//                            onTap: () {
//                              this.logic.syncSpeciesList();
//                            },
//                            child: Column(
//                              mainAxisAlignment: MainAxisAlignment.center,
//                              crossAxisAlignment: CrossAxisAlignment.center,
//                              children: [
//                                new Text(
//                                  'Species',
//                                ),
//                                new Text(
//                                  'Species',
//                                ),
//                              ],
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  )),

                  /*      Container(
                    // height: (MediaQuery.of(context).size.height) / 2,
                    //   width: MediaQuery.of(context).size.width / 2,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Card(
                            elevation: 3.0,
                            //  color: Colors.blueGrey,
                            child: ListTile(
                              title: Text(
                                "Province",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 14.0),
                              ),
                              //   subtitle: Text("Description"),
                              onTap: () {
                                this.logic.syncProvinceList();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Card(
                            elevation: 3.0,
                            //  color: Colors.blueGrey,

                            child: ListTile(
                              title: Text(
                                "District",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 14.0),
                              ),
                              // subtitle: Text("Description"),
                              onTap: () {
                                this.logic.syncDistrictList();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Card(
                            elevation: 3.0,
                            //  color: Colors.blueGrey,
                            child: ListTile(
                              title: Text(
                                "Municipality",
                                style: TextStyle(fontSize: 14.0),
                                //   textAlign: TextAlign.center,
                              ),
                              // subtitle: Text("Description"),
                              onTap: () {
                                this.logic.syncMunicipalityList();
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    // height: (MediaQuery.of(context).size.height) / 2,
                    //   width: MediaQuery.of(context).size.width / 2,
                    child: Row(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Card(
                            elevation: 4.0,
                            //  color: Colors.blueGrey,

                            child: ListTile(
                              title: Text(
                                "Species",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 14.0),
                              ),
                              //   subtitle: Text("Description"),
                              onTap: () {
                                this.logic.syncSpeciesList();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Card(
                            elevation: 4.0,
                            //  color: Colors.blueGrey,

                            child: ListTile(
                              title: Text(
                                "Breed",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 14.0),
                              ),
                              // subtitle: Text("Description"),
                              onTap: () {
                                this.logic.syncBreedList();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Card(
                            elevation: 4.0,
                            //  color: Colors.blueGrey,
                            child: ListTile(
                              title: Text(
                                "Exit Reason",
                                // textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 14.0),
                              ),
                              // subtitle: Text("Description"),
                              onTap: () {
                                this.logic.syncExitReasonList();
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  Divider(
                    height: 18.0,
                    color: Colors.red,
                  ),
*/

//                  Container(
//                    child: Row(
//                      // crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Send Farmer Data",
////                                AppTranslations.of(context).text("send_farmer_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendFarmerList();
//                              },
//                            ),
//                          ),
//                        ),
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Send Animal Data",
////                                AppTranslations.of(context).text("send_animal_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              // subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendAnimalList();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//
//                  //
//
//                  Container(
//                    child: Row(
//                      // crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Send Artificial Insemination Data",
////                                AppTranslations.of(context).text("send_ai_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendAIList();
////                                this.logic. getNotification();
//                              },
//                            ),
//                          ),
//                        ),
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Send Pregnancy Diagnosis Data",
////                                AppTranslations.of(context).text("send_pd_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              // subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendPDList();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  //
//                  Container(
//                    child: Row(
//                      // crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Send Calving Data",
////                                AppTranslations.of(context).text("send_calving_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendCalvingList();
//                              },
//                            ),
//                          ),
//                        ),
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Send Exit Record Data",
////                                AppTranslations.of(context).text("send_exit_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              // subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendExitRecordList();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//
//                  //
//                  Container(
//                    child: Row(
//                      // crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Send Nitrogen Data",
////                                AppTranslations.of(context).text("send_nitrogen_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendNitrogenList();
//                              },
//                            ),
//                          ),
//                        ),
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Send Semen Data",
////                                AppTranslations.of(context).text("send_semen_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              // subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.sendSemenList();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),

                  //get data multiple button start
//                  Divider(
//                    height: 18.0,
//                    color: Colors.red,
//                  ),
//
//                  Container(
//                    child: Row(
//                      // crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Get Farmer Data",
////                                AppTranslations.of(context).text("get_farmer_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.syncGetFarmerData();
//                                //    this.logic.getFarmerListFromServer();
//                              },
//                            ),
//                          ),
//                        ),
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Get Animal Data",
////                                AppTranslations.of(context).text("get_animal_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              // subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.syncGetAnimalData();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//
//                  //
//
//                  Container(
//                    child: Row(
//                      // crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Get Artificial Insemination Data",
////                                AppTranslations.of(context).text("get_ai_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.syncGetAIData();
//                              },
//                            ),
//                          ),
//                        ),
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Get Pregnancy Diagnosis Data",
////                                AppTranslations.of(context).text("get_pd_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              // subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.syncGetPDData();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                  //
//                  Container(
//                    child: Row(
//                      // crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Get Calving Data",
////                                AppTranslations.of(context).text("get_calving_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.syncGetCalvingData();
//                              },
//                            ),
//                          ),
//                        ),
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Get Exit Record Data",
////                                AppTranslations.of(context).text("get_exit_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              // subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.syncGetExitRecordData();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//
//                  //
//                  Container(
//                    child: Row(
//                      // crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Get Nitrogen Data",
////                                AppTranslations.of(context).text("get_nitrogen_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.syncGetLiquidNitrogenList();
//                              },
//                            ),
//                          ),
//                        ),
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            //  color: Colors.blueGrey,
//
//                            child: ListTile(
//                              title: Text(
//                                "Get Semen Data",
////                                AppTranslations.of(context).text("get_semen_data"),
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
//                              // subtitle: Text("Description"),
//                              onTap: () {
//                                this.logic.syncGetSemenList();
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),

                  //get data multiple button end

                  //sync data to server single button
                  Container(
                    child: Row(
                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            color:  firstPressSync==true?Colors.white:Colors.blueGrey ,
//                            child: ListTile(
//                              title: Text(
//                                "Sync to Server",
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0),
//                              ),
////                              subtitle: Text(logic.syncDate==null?"":this.logic.syncDate),
//                              //   subtitle: Text("Description"),
//                              onTap: () {
//                                if (firstPressSync) {
//                                  firstPressSync = false;
//                                  this.logic.getFarmerListFromDBs();
//                                }
//
//
////                                this.logic.getAllListFromDbToServer();
////                                this.logic.getFarmerListFromDB();
//
//
//                                Logger.v("button clcked");
//
//                              },
//                            ),
//                          ),
//                        ),

                        Expanded(
                          flex: 5,
                          child: InkWell(
                            highlightColor: firstPressSync == true
                                ? Colors.white
                                : Colors.blueGrey,
                            child: Card(
                              elevation: 5.0,
                              child: SizedBox(
                                height: 120.0,
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 6,
                                      //   child: Icon(Icons.home),
                                      child: Image.asset(
                                        'images/upload.png',
                                        width: 60.0,
                                        height: 60.0,
                                        color: Colors.black54,
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        "Send Data To Server",
//                                        "Sync to Server",
//                                              AppTranslations.of(context).text(""),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500),
                                      ),
//                                          child: Text('Farmer'),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            onTap: () {
                              if (firstPressSync) {
//                                firstPressSync = false;
                                this.logic.getAllDataFromDataBase();
//                                this.logic.getFarmerListFromDBs();
                              }
                            },
                          ),
                        ),

                        //restore
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                            color:  firstPressRestore==true?Colors.white:Colors.blueGrey ,
//
//                            child: ListTile(
//                              title: Text(
//                                "Restore",
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0,
////                                  color:  firstPress==true?Colors.black:Colors.blueGrey ,
//                                ),
//                              ),
////                                 subtitle: Text(logic.restoreDate==null?"":this.logic.restoreDate),
//                              onTap: () {
//
//                                if (firstPressRestore) {
//
//                                  firstPressRestore = false;
//                                  this.logic.syncGetSemenListFromServer();
//                                }
//                              },
//                            ),
//                          ),
//                        ),

                        Expanded(
                          flex: 5,
                          child: InkWell(
                            highlightColor: firstPressSync == true
                          ? Colors.white
                              : Colors.blueGrey,
                            child: Card(
                              elevation: 5.0,

                              child: SizedBox(

                                height: 120.0,
                                child: Column(

                                  children: <Widget>[
                                    Expanded(
                                      flex: 6,
                                      //   child: Icon(Icons.home),
                                      child: Image.asset(
                                        'images/download.png',
                                        width: 60.0,
                                        height: 60.0,
                                        color: Colors.black54,
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        "Get Data From Server",
//                                        "Sync From Server",
//                                              AppTranslations.of(context).text(""),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500),
                                      ),
//                                          child: Text('Farmer'),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            onTap: () {
                              if (firstPressRestore) {
                                firstPressRestore = false;
                                this.logic.syncSpeciesList();
//                                this.logic.syncGetSemenListFromServer();
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),

//                  Container(
//                    child: Row(
//                      children: <Widget>[
//                        Expanded(
//                          child: Card(
//                            elevation: 4.0,
//                              color:  firstPressRestore==true?Colors.white:Colors.blueGrey ,
//
//                            child: ListTile(
//                              title: Text(
//                                "Restore",
//                                textAlign: TextAlign.center,
//                                style: TextStyle(fontSize: 14.0,
////                                  color:  firstPress==true?Colors.black:Colors.blueGrey ,
//                                ),
//                              ),
////                                 subtitle: Text(logic.restoreDate==null?"":this.logic.restoreDate),
//                              onTap: () {
//
//                                if (firstPressRestore) {
//
//                                  firstPressRestore = false;
//                                  this.logic.syncGetSemenListFromServer();
//                                }
//                              },
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

//event of sending data to server

//  log() {
//    return Scaffold(
//      backgroundColor: Colors.black12.withOpacity(0.85),
//      body: NotificationListener<ScrollNotification>(
//        onNotification: (scrollNotification) {
//          //   Log.v("scrolled");
//        },
//        child: SingleChildScrollView(
//          child: Column(
//            children: <Widget>[
//              Container(
//                width: MediaQuery.of(context).size.width,
//                height: MediaQuery.of(context).size.height,
//                margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
//                child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>[
//                    Expanded(
//                      flex: 1,
//                      child: Text(""),
////                child: Positioned(
////                  left: 100.0,
////                  top: 10.0,
////                    child: IconButton(
////                        icon: Icon(Icons.close, size: 25.0,),
////                        onPressed: () {
////                          Navigator.of(context).pop();
////                        }))
////
//                    ),
//                    Expanded(
//                        flex: 9,
//                        child: Column(
//                          children: <Widget>[
//                            //sending semen data
//                            Text(
//                              logic.semenSend == null ? "" : logic.semenSend,
//                              style: TextStyle(color: Colors.white),
//                            ),
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//
//                            Text(
//                              logic.semenSendSuccess == null
//                                  ? ""
//                                  : logic.semenSendSuccess,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Text(
//                              logic.semenSendError == null
//                                  ? " "
//                                  : logic.semenSendError,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            // sending nitrogen data
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//
//                            Text(
//                              logic.nitrogenSend == null
//                                  ? ""
//                                  : logic.nitrogenSend,
//                              style: TextStyle(color: Colors.white),
//                            ),
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.nitrogenSendSuccess == null
//                                  ? ""
//                                  : logic.nitrogenSendSuccess,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Text(
//                              logic.nitrogenSendError == null
//                                  ? " "
//                                  : logic.nitrogenSendError,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            //sending farmer data
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.farmerSend == null ? "" : logic.farmerSend,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.farmerSendSuccess == null
//                                  ? ""
//                                  : logic.farmerSendSuccess,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Text(
//                              logic.farmerSendError == null
//                                  ? " "
//                                  : logic.farmerSendError,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            //sending animal data
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.animalSend == null ? "" : logic.animalSend,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.animalSendSuccess == null
//                                  ? ""
//                                  : logic.animalSendSuccess,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Text(
//                              logic.animalSendError == null
//                                  ? " "
//                                  : logic.animalSendError,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            //sending ai data
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.aiSend == null ? "" : logic.aiSend,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.aiSendSuccess == null
//                                  ? ""
//                                  : logic.aiSendSuccess,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Text(
//                              logic.aiSendError == null
//                                  ? " "
//                                  : logic.aiSendError,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            //sending pd data
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.pdSend == null ? "" : logic.pdSend,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.pdSendSuccess == null
//                                  ? ""
//                                  : logic.pdSendSuccess,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Text(
//                              logic.pdSendError == null
//                                  ? " "
//                                  : logic.pdSendError,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            //sending calving data
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.calvingSend == null
//                                  ? ""
//                                  : logic.calvingSend,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.calvingSendSuccess == null
//                                  ? ""
//                                  : logic.calvingSendSuccess,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Text(
//                              logic.calvingSendError == null
//                                  ? " "
//                                  : logic.calvingSendError,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            //sending exit data
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.exitSend == null ? "" : logic.exitSend,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Divider(
//                              height: 12.0,
//                              color: Colors.blue,
//                            ),
//                            Text(
//                              logic.exitSendSuccess == null
//                                  ? ""
//                                  : logic.exitSendSuccess,
//                              style: TextStyle(color: Colors.white),
//                            ),
//
//                            Text(
//                              logic.exitSendError == null
//                                  ? " "
//                                  : logic.exitSendError,
//                              style: TextStyle(color: Colors.white),
//                            ),
//                          ],
//                        )),
//                  ],
//                ),
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
}
