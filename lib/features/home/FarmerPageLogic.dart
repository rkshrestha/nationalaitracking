import 'package:contact_picker/contact_picker.dart';
import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/bloc/nbisBloc/NbisBloc.dart';
import 'package:nbis/features/home/FarmerPage.dart';
import 'package:nbis/features/network/SCResponse.dart';
import 'package:nbis/features/repo/District.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/Municipality.dart';
import 'package:nbis/features/repo/Province.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class FarmerPageLogic {
  FarmerPageState state;

  FarmerBloc farmerBloc;
  NbisBloc nbisBloc;
  List<Province> provinceList = [];
  List<String> provinceListOfString = [];
  List<District> districtList = [];
  List<Municipality> municipalityList = [];
  int i;
  int j;
  int k;
  int provinceIds;
  int districtIds;
  Farmer farmer = Farmer();

  Province selectedProvince;
  Municipality selectedMunicipality;
  District selectedDistrict;

  FarmerPageLogic(this.state, this.farmerBloc, this.nbisBloc);

  ContactPicker contactPicker = ContactPicker();
  Contact contact;
//  PhoneNumber phoneNumbers;

  void onPostInit() {
    state.setState(() {
//      getProvinceList();
    });
  }

  getContact() async {
    Contact contacts = await contactPicker.selectContact();
    contact = contacts;
    Logger.v('phoneNumber: ' + contact.fullName.toString());
    String phoneNumber = contact.phoneNumber.number.toString();
    String actualNumber =phoneNumber.replaceAll(new RegExp(r'[^\w\s]+'),'');
    String fullname = contact.fullName;
    state.setState(() {
      state.tfMobileNumber.controller.text = actualNumber.toString();
      state.tfFarmerName.controller.text = fullname;

      // state.mblController = new TextEditingController(text: "$phoneNumber");
    });
  }

  bool onValid() {
    if (selectedProvince == null) {
//      ToastUtils.show(AppTranslations.of(state.context).text("state_validate"));
      ToastUtils.show("Please select state");
      return false;
    } else if (selectedDistrict == null) {
//      ToastUtils.show(
//          AppTranslations.of(state.context).text("district_validate"));
      ToastUtils.show("Please select district!");
      return false;
    } else if (selectedMunicipality == null) {
//      ToastUtils.show(
//          AppTranslations.of(state.context).text("municipality_validate"));
      ToastUtils.show("Please select municipality!");
      return false;
    }

    return true;
  }

  saveFarmer() async {
    if (onValid()) {
      this
          .farmerBloc
          .saveFarmer(state.context, this.farmer, selectedProvince,
              selectedDistrict, selectedMunicipality)
          .listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          ToastUtils.show(it.data);
          state.setState(() {
//            Navigator.pop(state.context, true);
//            Navigator.pop(state.context, this.farmer);
//            Navigator.pop(state.context, true);

          });
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
//          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//            Navigator.of(this.state.context).pop();
//          });
        }
      });
    }
  }

  updateFarmer() async {
    if (onValid()) {
      this
          .farmerBloc
          .updateFarmer(state.context, this.farmer, selectedProvince,
              selectedDistrict, selectedMunicipality)
          .listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          ToastUtils.show(it.data);
//          Navigator.pop(this.state.context);
        } else if (it.status == Status.ERROR) {
          ToastUtils.show(it.errorMessage);
        } else {
          new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
            Navigator.of(state.context).pop();
          });
        }
      });
    }
  }

  void onPostState() {
    if (this.state.isEdit) {
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//              "Edit Farmer"
////              AppTranslations.of(state.context).text("farmer_form_edit"),
//            );
//      });

      this.state.setAppBar(
          "Edit Farmer"
      );

      this.farmer = state.farmer;
      state.mblController.text = this.farmer.mobileNumber.toString();
      state.farmerController.text = this.farmer.farmerName;
      Logger.v('inside ' + this.state.isEdit.toString());

      /*if(provinceListOfString.contains(farmer.province)){
        int i= provinceListOfString.indexOf(farmer.province);
        // set dropdown with index i
        selectedProvince = new Province.make(i, farmer.province);
        Logger.v("Province string list"+provinceListOfString.toString());

      }*/
      this.nbisBloc.getProvinceList().listen((it) {
        if (it == null) return;
        if (it.status == Status.SUCCESS) {
          this.state.setState(() {
            this.provinceList = it.data;
            for (Province item in provinceList) {
              if (item.state.contains(farmer.province)) {
                Logger.v("Province: " + farmer.province);
                var stateNameFromList = item.state;
                Logger.v("stateNamefromList: " + stateNameFromList);
                i = provinceList.indexOf(item);
                provinceIds = item.provinceid;
                //display in dropdown
                selectedProvince = provinceList[i];
                Logger.v("display: " + farmer.district);
                //district
                this.nbisBloc.getDistrictList(provinceIds).listen((it) {
                  if (it == null) return;
                  Logger.v("inside listener it");
                  if (it.status == Status.SUCCESS) {
                    Logger.v("district list is " + it.data.toString());
                    Logger.v("ids is " + provinceIds.toString());
                    this.state.setState(() {
                      this.districtList = it.data;
                      //

                      for (District items in districtList) {
                        Logger.v("items: " + items.name);
                        if (items.name.contains(farmer.district)) {
                          Logger.v("district: " + items.name);
                          Logger.v("districtSelected: " + farmer.district);
                          state.setState(() {
                            Logger.v("district: " + items.name);

                            j = districtList.indexOf(items);
                            districtIds = items.districtid;
                            selectedDistrict = districtList[j];
                          });

                          //municipality
                          this
                              .nbisBloc
                              .getMunicipalityList(districtIds)
                              .listen((it) {
                            if (it == null) return;
                            Logger.v("inside listener it");
                            if (it.status == Status.SUCCESS) {
                              this.state.setState(() {
                                this.municipalityList = it.data;

                                Logger.v("Municipality list is " +
                                    it.data.toString());
                                for (Municipality municipal
                                    in municipalityList) {
                                  if (municipal.name
                                      .contains(farmer.municipality)) {
                                    Logger.v("municipality " + municipal.name);
                                    Logger.v("municipalSelected: " +
                                        farmer.municipality);
                                    state.setState(() {
                                      k = municipalityList.indexOf(municipal);
                                      selectedMunicipality =
                                          municipalityList[k];
                                    });
                                  }
                                }
                              });
                            }
                          });
                        }
                      }
                    });
                  }
                });
              }
            }
          });
        }
      });

//      for(Province item in provinceList){
//        Logger.v("display it"+ selectedProvince.toString());
//        if(item.state.contains(farmer.province)){
//          int i = item.state.indexOf(farmer.province);
//          selectedProvince = provinceList[i];
//          Logger.v("display it"+ selectedProvince.toString());
//        }
//      }

    } else {
//      new Future.delayed(new Duration(milliseconds: 0000)).then((_) {
//        this.state.setAppBar(
//          "Add Farmer"
////              AppTranslations.of(state.context).text("farmer_form_add_title"),
//            );
//        Logger.v('inside ' + this.state.isEdit.toString());
//      });
            this.state.setAppBar("Add Farmer");

      getProvinceList();
    }
  }

  getProvinceList() {
    this.nbisBloc.getProvinceList().listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("province list is " + it.data.toString());
        this.state.setState(() {
          this.provinceList = it.data;
          /*for(Province item in it.data ){
            provinceListOfString.add(item.state);
          }*/
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

//  getProvince(int provinceId) {
//    this.nbisBloc.getProvince(provinceId).listen((it) {
//      if (it == null) return;
//      Logger.v("inside listener it");
//      if (it.status == Status.SUCCESS) {
//        Logger.v("province list is " + it.data.toString());
//        this.state.setState(() {
//          this.province = it.data;
//        });
//      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(state.context).pop();
//        });
//      }
//    });
// }

//  getDistrict(int districtId) {
//    this.nbisBloc.getDistrict(districtId).listen((it) {
//      if (it == null) return;
//      Logger.v("inside listener it");
//      if (it.status == Status.SUCCESS) {
//        Logger.v("district list is " + it.data.toString());
//        this.state.setState(() {
//          this.district = it.data;
//        });
//      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(state.context).pop();
//        });
//      }
//    });
//  }
//
//  getMunicipality(int municipalityId) {
//    this.nbisBloc.getMunicipality(municipalityId).listen((it) {
//      if (it == null) return;
//      Logger.v("inside listener it");
//      if (it.status == Status.SUCCESS) {
//        Logger.v("province list is " + it.data.toString());
//        this.state.setState(() {
//          this.municipality = it.data;
//        });
//      } else {
//        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
//          Navigator.of(state.context).pop();
//        });
//      }
//    });
//  }

  getDistrictList(int provinceId) {
    this.nbisBloc.getDistrictList(provinceId).listen((it) {
      if (it == null) return;
      Logger.v("province Id: " + provinceId.toString());
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("district list is " + it.data.toString());
        this.state.setState(() {
          this.districtList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }

  getMunicipalityList(int districtId) {
    this.nbisBloc.getMunicipalityList(districtId).listen((it) {
      if (it == null) return;
      Logger.v("inside listener it");
      if (it.status == Status.SUCCESS) {
        Logger.v("Municipality list is " + it.data.toString());
        this.state.setState(() {
          this.municipalityList = it.data;
        });
      } else {
        new Future.delayed(new Duration(milliseconds: 3000)).then((_) {
          Navigator.of(state.context).pop();
        });
      }
    });
  }
}
