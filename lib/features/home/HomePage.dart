import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/nbisBloc/NbisBloc.dart';
import 'package:nbis/features/bloc/notificationBloc/NotificationBloc.dart';
import 'package:nbis/features/home/HomePageLogic.dart';
import 'package:nbis/features/home/NotificationPage.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/ExitRecordRepo.dart';
import 'package:nbis/features/repo/NbisRepo.dart';
import 'package:nbis/features/repo/NotificationRepo.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  HomePageLogic logic;
  AnimalBloc animalBloc;
  NbisBloc nbisBloc;
  ExitRecordBloc exitRecordBloc;
  NotificationBloc notificationBloc;
  var refreshNotifyListKey = GlobalKey<RefreshIndicatorState>();
  MediaQueryData queryData;


  @override
  void initState() {
    super.initState();
    animalBloc = AnimalBloc(AnimalRepo(this.context));
    nbisBloc = NbisBloc(NbisRepo(this.context));
    exitRecordBloc = ExitRecordBloc(ExitRecordRepo(this.context));
    notificationBloc = NotificationBloc(NotificationRepo(this.context));
    logic = HomePageLogic(
        this, animalBloc, nbisBloc, exitRecordBloc, notificationBloc);

    setState((){
      refreshNotifyList();

    });
    //  logic.onPostCreate();


  }



  Future<Null> refreshNotifyList() async {
    refreshNotifyListKey.currentState?.show(atTop: false);
//    await Future.delayed(Duration(seconds: 1));

    setState(() {
      this.logic.getNotificationList();
    });

    return null;
  }

  @override
  void dispose() {
    super.dispose();
  }

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  Widget build(BuildContext context) {
    //layout width and height
    queryData = MediaQuery.of(context);

     var width= queryData.size.width;
     var height= queryData.size.height;
    var blockSize = width / 10;
    var blockSizeVertical = height / 10;
    double elementWidth = blockSize * 1.0;
//    double elementWidth = blockSize * 10.0;
    double elementHeight = blockSizeVertical * 1.62;
//    double elementHeight = blockSizeVertical * 16.0;




    //popmenu lists
    List<String> popItems = <String>[
     "Profile",
//      AppTranslations.of(context).text("profile"),
//      "Settings",
//      AppTranslations.of(context).text("settings"),
      "Logout",
//      AppTranslations.of(context).text("logout"),
    ];

    return Scaffold(
      appBar: AppBar(
        //  leading: Icon(Icons.home),
        leading: Image.asset("images/xhdpi/govLogo_small.png"),
        title: Text("NLBIS"),
//        title: Text(AppTranslations.of(context).text("home_title")),
        actions: <Widget>[

          new Stack(
            children: <Widget>[
              new IconButton(
                icon: new Icon(
                  Icons.notifications,
                  color: Colors.white,
                ),
                onPressed: () {
                 logic.notification.length = 0;
                 //
                  //
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NotificationPage()),
                  );
                },
              ),
              logic.notification.length == 0
                  ? new Container()
                  : new Positioned(
                                right: 2.0,
                      child: new Stack(
                      children: <Widget>[
                        new Icon(Icons.brightness_1,
                            size: 23.0, color: Colors.red[800]),
                        new Positioned(
                            top: 3.0,
                            right: 6.0,
                            child: new Center(
                              child: new Text(
                                logic.notification.length.toString(),
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.w500),
                              ),
                            )),
                      ],
                    )),
            ],
          ),
          //

          PopupMenuButton<String>(
              onSelected: logic.choiceOption,
              itemBuilder: (BuildContext context) {
                return popItems.map((String choice) {
//                return Constants.settings.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              }),

//          IconButton(
//              icon: Icon(Icons.settings),
//              onPressed: () {
//                setState(() {
//
//                  PopupMenuButton<String>(
//                      onSelected: logic.choiceOption,
//                      itemBuilder: (BuildContext context) {
//
//                          return Constants.settings.map((String choice) {
//                            return PopupMenuItem<String>(
//                              value: choice,
//                              child: Text(choice),
//                            );
//                          }).toList();
//
//                      });
//
//
//
//                //  FileUtils.backupDB();
//
//               //   Logger.v("back database");
//
//             //     Navigator.push(context, MaterialPageRoute(builder: (context)=> ProfilePage()));
//                });
//              }),
        ],
      ),
      body: BlocProvider(
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                child: NotificationListener<ScrollNotification>(
                  onNotification: (scrollNotification) {
                    //   Log.v("scrolled");
                  },
                  child: SingleChildScrollView(
                    child: Column(

                      children: <Widget>[

                        //two button up image and text

                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                  width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
                                          //   child: Icon(Icons.home),
                                          child: Image.asset(
                                            'images/farmers.png',
//                                            'images/xxhdpi/farmer.png',
                                            width: 50.0,
                                            height: 50.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                      "Farmers",
//                                              AppTranslations.of(context).text("tab_farmer"),
                                          style: TextStyle(fontWeight: FontWeight.w500),),
//                                          child: Text('Farmer'),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onFarmerClick();
                                },
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
                                          //child: Icon(Icons.home),
                                          child: Image.asset(
                                            "images/cow.png",
//                                            "images/xxhdpi/animals.png",
                                            width: 50.0,
                                            height: 50.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
//                                          child: Text('Cattle/Buffalo'),
                                          child: Text(
                                            "Animals",
//                                              AppTranslations.of(context).text("tab_animal"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onAnimalClick();
                                },
                              ),
                            ),
                          ],
                        ),

                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
                                          //               child: Icon(Icons.home),
                                          child: Image.asset(
                                            "images/xxhdpi/pregnancydiagnosis.png",
                                            width: 50.0,
                                            height: 50.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
//                                          child:
//                                              Text('Artificial Insemination'),

                                          child: Text(
                                            "Artificial Insemination",
//                                              AppTranslations.of(context).text("tab_ai"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onAIClick();
                                },
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
                                          //        child: Icon(Icons.home),
                                          child: Image.asset(

                                            "images/pregnancy.png",
//                                            "images/xxhdpi/pd.png",
                                            width: 50.0,
                                            height: 50.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
//                                          child: Text('Pregnancy Diagnosis'),

                                          child: Text(
                                            "Pregnancy Diagnosis",
//                                              AppTranslations.of(context)
//                                                  .text("tab_pd"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onPDClick();
                                },
                              ),
                            ),
                          ],
                        ),

//

                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
//                                          child: Icon(Icons.home),
                                          child: Image.asset(
                                            "images/calving_animal.png",
//                                            "images/xxhdpi/calving.png",
                                            width: 65.0,
                                            height: 65.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
//                                          child: Text('Calving'),
                                          child: Text(
                                            "Calving",
//                                              AppTranslations.of(context)
//                                                  .text("tab_calving"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onCalvingClick();
                                },
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
//                                          child: Icon(Icons.home),
                                          child: Image.asset(
                                            "images/xxhdpi/exit.png",
                                            width: 45.0,
                                            height: 45.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            "Exit Animals",
//                                              AppTranslations.of(context)
//                                                  .text("tab_exit"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
//                                          child: Text('Exit Animals'),
//                                          child: Text('Exit Cattle/Buffalo'),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onExitCattleClick();
                                },
                              ),
                            ),
                          ],
                        ),

                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
//                                          child: Icon(Icons.home),
                                          child: Image.asset(
                                            "images/kit.png",
//                                            "images/xxhdpi/liquidnitrogen.png",
                                            width: 50.0,
                                            height: 50.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
//                                          child: Text('Liquid Nitrogen'),
                                          child: Text(
                                            "Liquid Nitrogen",
//                                              AppTranslations.of(context)
//                                                  .text("tab_nitrogen"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onNitrogenClick();
                                },
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
//                                          child: Icon(Icons.home),
                                          child: Image.asset(
                                            "images/xxhdpi/semen.png",
                                            width: 50.0,
                                            height: 50.0,
                                            color: Colors.black54,
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            "Semen",
//                                              AppTranslations.of(context)
//                                                  .text("tab_semen"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
//                                          child: Text('Semen'),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onSemenClick();
                                },
                              ),
                            ),
                          ],
                        ),

                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            //   child: Icon(Icons.sync, size: 50.0,),
                                            child: Image.asset(
                                              "images/xxhdpi/sync.png",
                                              width: 50.0,
                                              height: 50.0,
                                              color: Colors.black54,
                                            )),
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            "Sync",
//                                              AppTranslations.of(context)
//                                                  .text("tab_sync"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
//                                          child: Text('Sync to Server'),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onSyncClick();
                                },
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: InkWell(
                                child: Card(
                                  elevation: 5.0,
                                  child: SizedBox(
//                                    height: 100.0,
                                    width: elementWidth,
                                    height:elementHeight,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
//                                          child: Icon(
//                                              Icons.perm_device_information, size: 50.0,),
                                            child: Image.asset(
                                              "images/xxhdpi/about.png",
                                              width: 40.0,
                                              height: 40.0,
                                              color: Colors.black54,
                                            )),
                                        Expanded(
                                          flex: 2,
//                                          child: Text('About'),
                                          child: Text(
                                            "About",
//                                              AppTranslations.of(context)
//                                                  .text("tab_about"),
                                            style: TextStyle(fontWeight: FontWeight.w500),),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  logic.onAboutClick();
                                },
                              ),
                            ),
                          ],
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
