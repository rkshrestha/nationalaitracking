import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/UserBloc/UserBloc.dart';
import 'package:flutter/services.dart';
import 'package:nbis/features/bloc/animalBloc/AnimalBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/bloc/farmerBloc/FarmerBloc.dart';
import 'package:nbis/features/home/AnimalPageLogic.dart';
import 'package:nbis/features/home/SyncServerPage.dart';
import 'package:nbis/features/home/SyncServerPageLogic.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/AnimalRepo.dart';
import 'package:nbis/features/repo/Breed.dart';
import 'package:nbis/features/repo/Farmer.dart';
import 'package:nbis/features/repo/FarmerRepo.dart';
import 'package:nbis/features/repo/Species.dart';
import 'package:nbis/features/repo/UserRepo.dart';
import 'package:nbis/s/Logger.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class AnimalPage extends StatefulWidget {
  bool isEdit;
  Animal animal;
  Farmer farmer;

  AnimalPage({this.animal, this.farmer, this.isEdit = false});

  @override
  State<StatefulWidget> createState() {
    return AnimalPageState(
        animal: this.animal, farmer: this.farmer, isEdit: this.isEdit);
  }
}

class AnimalPageState extends State<AnimalPage> {
  bool isEdit;
  Animal animal;
  AnimalBloc animalBloc;
  AnimalPageLogic logic;
  FarmerBloc farmerBloc;
  UserBloc userBloc;
  Farmer farmer;
  String breedSelected;
  Species species;

  int selected;
//  static var breed = ['Abd', 'cde'];
  String breedDisplayOnly = "";
  int speciesId;

  FocusNode textFocus = new FocusNode();
  TextFormField tfMobileNumber;
  TextFormField tfFarmer;
  TextFormField tfAnimalCode;
  SyncServerPageState syncState;
  AppBar appBar;
  SyncServerPageLogic syncLogic;

  TextEditingController mblController = TextEditingController();
  TextEditingController farmerController = TextEditingController();
  TextEditingController aCodeController = TextEditingController();
  GlobalKey<FormState> animalFormKey = GlobalKey<FormState>();

  AnimalPageState({this.animal, this.farmer, this.isEdit = false});

  void onChanged() {
    String newText = mblController.text;
//    bool hasFocus = textFocus.hasFocus;

    mblController.selection = new TextSelection(
        baseOffset: newText.length, extentOffset: newText.length);
    //if (this.logic != null) {
    //  this.logic.getFarmerByMobileNumber(int.tryParse(newText));
    //}

    if (newText.length == 10) {
//      this.logic.getFarmerByMobileNumber(int.tryParse(newText));
      this.logic.getFarmerByMobileNumber(newText);
  }
  }

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  void initState() {
    super.initState();
    this.animalBloc = AnimalBloc(AnimalRepo(context));
    this.farmerBloc = FarmerBloc(FarmerRepo(context));
    this.userBloc = UserBloc(UserRepo(context));
    this.logic = AnimalPageLogic(this, userBloc, animalBloc, farmerBloc);

//    new Future.delayed(new Duration(milliseconds: 2000)).then((_) {

//    });

    mblController.text = farmer == null ? "" : farmer.mobileNumber;
    mblController.addListener(onChanged);
    textFocus.addListener(onChanged);
    this.logic.onPostState();


    if(farmer != null){
      if (farmer.mobileNumber.length == 10) {
        this.logic.getFarmerByMobileNumber(farmer.mobileNumber);
      }
    }

  }

//  @override
//  void didChangeDependencies(){
//    super.didChangeDependencies();
//    this.logic.onPostState();
//  }

  setAppBar(String title) {
    appBar = AppBar(title: Text(title));
  }

//  @override
//  void didChangeDependencies() {
//    super.didChangeDependencies();
//    logic.onPostInit();
//  }

  @override
  void dispose() {
    mblController.removeListener(onChanged);
    mblController.dispose();
    textFocus.dispose();
    super.dispose();
  }

  animalForm() {
    Animal animal = this.logic.animal;

    //radio button
//
//    radioButtonAnimal() {
//      List<Widget> radioList = new List<Widget>();
//
//      radioList.add(new Row(
//        children: <Widget>[
//          Row(
//            children: <Widget>[
//              Radio(
//                  value: 0,
//                  groupValue: selected,
//                  onChanged: (int value) {
//                    logic.selectedAnimal(value);
//
//                    //   animal.speciesName = this.logic.speciesList[1].name;
//                  }),
//              Text(this.logic.speciesList[1].name == null
//                  ? ""
//                  : this.logic.speciesList[1].name),
//              //      Text("Cattle"),
//            ],
//          ),
//          Row(
//            children: <Widget>[
//              Radio(
//                  value: 1,
//                  groupValue: selected,
//                  onChanged: (int value) {
//                    logic.selectedAnimal(value);
//                    //   animal.speciesName = this.logic.speciesList[0].name;
//                  }),
//              Text(this.logic.speciesList[0].name == null
//                  ? ""
//                  : this.logic.speciesList[0].name),
////              Text("Buffalo"),
//            ],
//          ),
//          Text(
//            '*',
//            style: TextStyle(
//              color: Colors.red,
//              fontWeight: FontWeight.bold,
//              fontSize: 18.0,
//            ),
//          ),
//        ],
//      ));
//      return radioList;
//    }

//    //cattle radio button
//    Radio cattleRadio = Radio(
//      activeColor: Colors.blue,
//      value: 0,
//      groupValue: radioValue,
//      onChanged: this.logic.selectedAnimal(radioValue),
//    );
//
//    //buffalo radio button
//    Radio buffRadio = Radio(
//      activeColor: Colors.blue,
//      value: 1,
//      groupValue: radioValue,
//      onChanged: this.logic.selectedAnimal(radioValue),
//    );
//
//
//    //test
//
//
//    //
//
//    //goat radio button
//    Radio goatRadio = Radio(
//      value: 2,
//      groupValue: radioValue,
//      //onChanged: ,
//    );

    //breed
    // boar radio button
//    Radio boarRadio = Radio(
//      value: 0,
//      groupValue: radioValue,
//      //onChanged: ,
//    );
//    //holistian radio button
//    Radio holistianRadio = Radio(
//      value: 1,
//      groupValue: radioValue,
//      //onChanged: ,
//    );

    //tfmobile number
    tfMobileNumber = TextFormField(
        controller: mblController,
//        enabled: farmer == null ? true : false,
//        focusNode: textFocus,
        maxLength: 10,
        keyboardType: TextInputType.phone,
//        autofocus: true,
        decoration: InputDecoration(
          labelText:
             "Farmer's Mobile Number",
//              AppTranslations.of(context).text("animal_form_mobile_number"),
        ),
        onSaved: (mblNumber) {
          animal.mobileNumber = mblNumber;
        },
        validator: (mblNumber) {
          if (mblNumber.isEmpty) {
//            return AppTranslations.of(context).text("mobile_number_validate");
            return "Mobile Number can't be empty !!";
          } else if (mblNumber.length != 10) {
//            return AppTranslations.of(context)
//                .text("mobile_number_ten_digit_validate");
            return "Please enter ten digits number !!";
          }
        });

    //farmer name tf
    tfFarmer = TextFormField(
      controller: farmerController,
      enabled: false,
      //  initialValue: "Name",
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: "Farmer's Name",
//        labelText: AppTranslations.of(context).text("animal_form_farmer_name"),
        //  icon: Icon(Icons.mobile_screen_share),
      ),
      onSaved: (String farmers) {
        animal.farmerName = farmers;
      },
        validator: (farmer) {
          if (farmer.isEmpty) {
            return "Farmer Name can't be empty";
          }
        }
    );

    //farmer dropdown
//    DropdownButton dbFarmer = DropdownButton(
//        items: farmer.map((String dropDownStringItem) {
//          return DropdownMenuItem<String>(
//            value: dropDownStringItem,
//            child: Text(dropDownStringItem),
//          );
//        }).toList(),
//        //  value: dropDownStringItem,
//        elevation: 2,
//        value: 'Select Farmer',
//        onChanged: (valueSelectedByUser) {
//          setState(() {
//            //   updatePriorityAsInt(valueSelectedByUser);
//            //  debugPrint("show $valueSelectedByUser");
//          });
//        });

//tfanimal code
    tfAnimalCode = TextFormField(
        keyboardType: TextInputType.number,
        inputFormatters: [
          new BlacklistingTextInputFormatter(new RegExp('[\\.|\\,|\\-|\\*|\\+|\\#]')),
        ],
        controller: aCodeController,
        decoration: InputDecoration(
          labelText: "Animal Code",
//          labelText: AppTranslations.of(context).text("animal_form_code"),
          prefixText:
              mblController.text.length != 10 ? "" : mblController.text + "-",
        ),

        onSaved: (String animals) {
//          String animalCode = mblController.text + "-" + animals;
//          animal.speciesCode = animalCode;
          animal.speciesCode = animals;
        },
        validator: (animal) {
          if (animal.isEmpty) {
            return " Animal Code can't be empty !!";
//            return AppTranslations.of(context).text("code_validate");
          }
        });

    //Raised Save Button
    RaisedButton rbSave = RaisedButton(
      child: Text(
        "Save",
//        AppTranslations.of(context).text("form_save"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (animalFormKey.currentState.validate()) {
          animalFormKey.currentState.save();
          this.logic.saveAnimal();
        }
      },
    );

    //Raised Update Button
    RaisedButton rbUpdate = RaisedButton(
      child: Text(
        "Update",
//        AppTranslations.of(context).text("form_update"),
        style: TextStyle(color: Colors.white),
      ),
      color: Theme.of(context).primaryColor,
      onPressed: () {
        if (animalFormKey.currentState.validate()) {
          animalFormKey.currentState.save();
          this.logic.updateAnimal();
        }
      },
    );

    //breed select

//    DropdownButton dbBreed = DropdownButton(
//        isDense: true,
//        items: getDropDownMenuItems(),
//        isExpanded: true,
//        elevation: 2,
//      //  value: breedDisplayOnly,
//        hint: Text('Select Breed'),
//        onChanged: (value) {
//          setState(() {
//          //  if (value != null)
//              breedSelected = DsonUtils.toObject(value, Breed());
//            breedDisplayOnly = breedSelected.name;
//            //   animal.breedName = breedSelected;
//          });
//        });

//    DropdownButton dbBreed = DropdownButton(
//      // items: breed.map((String selectedBreed) {
//        isDense: true,
//        items: this.logic.breedList.length==0 ? DropdownMenuItem<String>()  : this.logic.breedList.map((Breed breed) {
//          return DropdownMenuItem<String>(
//            // value: breed == null? "":breed.name,
//            value: DsonUtils.toJsonString(breed),
//            child: Text(breed.name),
//          );
//        }).toList(),
//        isExpanded: true,
//        elevation: 2,
//        value: breedDisplayOnly ,
//        hint: Text('Select Breed'),
//        onChanged: (value) {
//          setState(() {
//            if(value !=null)
//              breedSelected = DsonUtils.toObject(value, Breed());
//            breedDisplayOnly = breedSelected.name;
//            //   animal.breedName = breedSelected;
//          });
//        });

    //db species
    DropdownButton dbSpecies = DropdownButton<Species>(

      hint: Text(
        "Select Animal",
//        AppTranslations.of(context).text("animal_form_species"),
      ),
      items: logic.speciesList.map((speciesSelected) {

        return DropdownMenuItem<Species>(

          child: new Text(speciesSelected.name),
          value: speciesSelected,

        );
      }
      ).toList(),
      value: logic.selectedSpecies,
      isExpanded: true,

      onChanged: (changedSpecies) {
        if (changedSpecies == null) {
          return;
        }

        setState(() {
          FocusScope.of(context).requestFocus(new FocusNode());

          this.logic.selectedBreed = null;
          this.logic.selectedSpecies = changedSpecies;
          Logger.v("changed  " + logic.selectedSpecies.name.toString());
          speciesId = changedSpecies.ids;
//          logic.selectedSpecies;
          this.logic.getBreedList(speciesId);
        });
      },
    );

    //db breed

    DropdownButton dbBreed = DropdownButton<Breed>(
      hint: Text(
"Select Breed",
//        AppTranslations.of(context).text("animal_form_breed"),
      ),
      items: logic.breedList.map((breedSelected) {
        return DropdownMenuItem<Breed>(
          child: new Text(breedSelected.name),
          value: breedSelected,
        );
      }).toList(),
      value: logic.selectedBreed,
      isExpanded: true,
      onChanged: (changedBreed) {
        if (changedBreed == null) {
          return;
        }
        setState(() {
          this.logic.selectedBreed = changedBreed;
          Logger.v("changedBreed " + logic.selectedBreed.name.toString());
        });
      },
    );
//    DropdownButton dbBreed = DropdownButton(
//      hint: Text('Select Breed'),
//      value: breedSelected,
//      //isDense: true,
//      isExpanded: true,
//      onChanged: (changedProvince) {
//        setState(() {
//          breedSelected = changedProvince;
//          animal.breedName = breedSelected;
//        });
//      },
//      items: breed.map((selectedBreed) {
//        return DropdownMenuItem(
//          child: new Text(selectedBreed),
//          value: selectedBreed,
//        );
//      }).toList(),
//    );

    //astrick
    Text astrickText = Text(
      '*',
      style: TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
        fontSize: 18.0,
      ),
    );

    return Form(
      key: animalFormKey,
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
//          Padding(
//            padding: EdgeInsets.only(left: 12.0),
//            child:
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    //  child: dbFarmer,
                    child: tfMobileNumber,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            //     ),

//          Padding(
//            padding: EdgeInsets.only(left: 12.0),
//            child:
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    //  child: dbFarmer,
                    child: tfFarmer,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            //   ),

//            Padding(
//              padding: EdgeInsets.only(left: 18.0, top: 4.0),
//              child: Text(
//                'Species:',
//                style: TextStyle(fontWeight: FontWeight.bold),
//              ),
//            ),

            ListTile(
              title: Row(
                // children: dbSpecies(),
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: dbSpecies,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),

            //breed
//            Padding(
//              padding: EdgeInsets.only(left: 18.0, top: 4.0),
//              child: Text(
//                'Breed:',
//                textAlign: TextAlign.left,
//                style: TextStyle(fontWeight: FontWeight.bold),
//              ),
//            ),

//          Padding(
//            padding: EdgeInsets.only(left: 12.0),
//            child:
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: dbBreed,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            //    ),
//breed
//          ListTile(
//            title: Row(
//              children: <Widget>[
//                boarRadio,
//                Text('Boar'),
//                holistianRadio,
//                Text('Holistian'),
//              ],
//            ),
//          ),

//          Padding(
//            padding: EdgeInsets.only(left: 12.0),
//            child:
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: tfAnimalCode,
                  ),
                  Expanded(
                    flex: 1,
                    child: astrickText,
                  ),
                ],
              ),
            ),
            //  ),

            //Parity
//          Padding(
//            padding: EdgeInsets.only(left: 20.0, top: 4.0),
//            child: Text(
//              'Current Parity: ',
//              textAlign: TextAlign.left,
//              style: TextStyle(fontWeight: FontWeight.bold),
//            ),
//          ),

//          Divider(
//            height: 10.0,
//          ),
//          Padding(
//            padding: EdgeInsets.only(left: 12.0),
//            child:
            ListTile(
              title: Row(
                children: <Widget>[
                  Expanded(
                    flex: 19,
                    child: Visibility(
                      visible: !this.isEdit,
                      child: rbSave,
                      replacement: rbUpdate,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(''),
                  ),
                ],
              ),
            ),
            //  ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    this.logic.onPostInit();

    return Scaffold(
      appBar: this.appBar,
      body: SafeArea(
        child: BlocProvider(
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      //   Log.v("scrolled");
                    },
                    child: SingleChildScrollView(
                      child: animalForm(),
                    ),
                  ),

                  //
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
