import 'package:flutter/material.dart';
import 'package:nbis/features/bloc/ExitRecordBloc/ExitRecordBloc.dart';
import 'package:nbis/features/bloc/blockProvider/BlocProvider.dart';
import 'package:nbis/features/home/ExitRecordPageListLogic.dart';
import 'package:nbis/features/repo/Animal.dart';
import 'package:nbis/features/repo/ExitRecord.dart';
import 'package:nbis/features/repo/ExitRecordRepo.dart';
import 'package:nbis/s/ToastUtils.dart';
//import 'package:nbis/s/localization/AppTranslation.dart';

class ExitRecordPageList extends StatefulWidget {
  Animal animal;

  ExitRecordPageList({this.animal});

  @override
  State<StatefulWidget> createState() {
    return ExitRecordPageListState(animal: animal);
  }
}

class ExitRecordPageListState extends State<ExitRecordPageList> {
  ExitRecordPageListLogic logic;
  ExitRecordBloc exitRecordBloc;
  ExitRecord exitRecord;
  Animal animal;
  var refreshExitRecordListKey = GlobalKey<RefreshIndicatorState>();

  ExitRecordPageListState({this.animal});

  void showToast(String message) {
    ToastUtils.show(message);
  }

  @override
  void initState() {
    super.initState();
    this.exitRecordBloc = ExitRecordBloc(ExitRecordRepo(context));
    this.logic = ExitRecordPageListLogic(this, exitRecordBloc);
//    logic.onPostInit(animal);
    refreshExitRecordList();
  }

  Future<Null> refreshExitRecordList() async {
    refreshExitRecordListKey.currentState?.show(atTop: false);
//    await Future.delayed(Duration(seconds: 1));

    setState(() {
      logic.onPostInit(animal);
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    this
        .logic
        .exitRecordList
        .sort((a, b) => b.dateOfExit.compareTo(a.dateOfExit));
    final tabSync = <Tab>[
      Tab(text: "Not Sync", icon: new Icon(Icons.sync)),
      Tab(text: " Sync", icon: new Icon(Icons.sync))
    ];
    return DefaultTabController(
      length: tabSync.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Exit Animals"
//            AppTranslations.of(context).text("exit_registered_title"),
              ),
//          bottom: TabBar(tabs: tabSync),
        ),

        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 45.0,
            color: Theme.of(context).primaryColor,
            child: GestureDetector(
              onTap: () {
                logic.onAddExitRecord(animal);
              },

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  Divider(
                    indent: 2.0,
                  ),
                  Text(
//                          AppTranslations.of(context).text("exit_register_button"),
                    "Add Exit Animals",
//                        "Add Exit Cattle/Buffalo",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),

                ]),

//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    flex: 3,
//                    child: Text(''),
//                  ),
//                  Expanded(
//                    flex: 6,
//                    child: Row(
//                      children: <Widget>[
//                        Icon(
//                          Icons.add,
//                          color: Colors.white,
//                        ),
//                        Divider(
//                          indent: 2.0,
//                        ),
//                        Text(
////                          AppTranslations.of(context).text("exit_register_button"),
//                          "Add Exit Animals",
////                        "Add Exit Cattle/Buffalo",
//                          style: TextStyle(
//                              color: Colors.white, fontWeight: FontWeight.bold),
//                        ),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Text(''),
//                  ),
//                ],
//              ),
            ),
          ),
        ),
        body: BlocProvider(
          child: SafeArea(
            child: Column(
              children: <Widget>[
//                Expanded(
//                  child: TabBarView(
//                    children: <Widget>[
//                      logic.exitUnSyncRecordList.isEmpty
//                          ? noData()
//                          : getUnSyncExitList(),
//                      logic.exitSyncRecordList.isEmpty
//                          ? noData()
//                          : getSyncExitList(),
//                    ],
//                  ),
//                ),
                Expanded(
                  child: RefreshIndicator(
                    key: refreshExitRecordListKey,
                    child: this.logic.exitRecordList.isEmpty
                        ? noData()
                        : getExitRecordList(),
                    onRefresh: refreshExitRecordList,
                  ),
                ),
//
              ],
            ),
          ),
        ),
//      floatingActionButton: Padding(
//        padding: EdgeInsets.only(bottom: 1.0),
//        child: FloatingActionButton(
//            child: Icon(Icons.add),
//            tooltip: 'Add Exit Cattle',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//        this.logic.onAddExitRecord();
//            }),
//      ),

//      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//      floatingActionButton: Column(
//        crossAxisAlignment: CrossAxisAlignment.stretch,
//        mainAxisSize: MainAxisSize.min,
//        children: <Widget>[
//          FloatingActionButton.extended(
//            isExtended: true,
//            label: Text("Add Exit Record"),
//            icon: Image.asset("images/hdpi/add.png",color: Colors.white,),
//            tooltip: 'Add exit',
//            backgroundColor: Theme.of(context).primaryColor,
//            onPressed: () {
//              this.logic.onAddExitRecord();
//            },
//          ),
//        ],
//      ),
      ),
    );
  }

  Widget noData() {
    return Container(
      child: Center(
//        child: Text(
//          AppTranslations.of(context).text("exit_noData"),
//        ),
        child: Text("No Exit Animal Data has been found !!"),
      ),
    );
  }

//listView exit Record

  ListView getExitRecordList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.exitRecordList.length,
        itemBuilder: (BuildContext context, int position) {
          exitRecord = this.logic.exitRecordList[position];

          editButton() {
            DateTime lastestDate = DateTime.now();

            if (exitRecord.syncDate != null) {
              String exitRecordSyncDate = exitRecord.syncDate == null
                  ? ""
                  : exitRecord.syncDate.split(".").first;
              DateTime syncDate = DateTime.tryParse(exitRecordSyncDate);
              DateTime addedDate = syncDate.add(new Duration(hours: 24));

//              if (syncDate.add(new Duration(hours: 24)) == lastestDate && exitRecord.syncToServer == true) {
//              if (lastestDate.difference(syncDate).inHours >= 24 && exitRecord.syncToServer == true) {
              if (lastestDate.compareTo(addedDate) == 1) {
//              if (lastestDate.compareTo(addedDate)==1 && exitRecord.syncToServer == true) {
//              if (addedDate.difference(syncDate).inHours >= 24 && exitRecord.syncToServer == true) {
                return Container(
                  child: Text(""),
                );
              } else {
                return GestureDetector(
                  child: Container(
                    width: 40.0,
                    height: 40.0,
                    child: Card(
                      color: Colors.white70,
                      elevation: 3.0,
                      child: Image.asset(
                        "images/edits.png",
                      ),
                    ),
                  ),
                  onTap: () {
                    this.logic.onEditExitRecord(
                        this.logic.exitRecordList[position], position);
                  },
                );

//                return Container(
//                  child:  GestureDetector(
//                    child: Icon(
//                      Icons.edit,
//                      size: 30.0,
//                      color: Colors.black,
//                    ),
//                    onTap: () {
//                      this.logic.onEditExitRecord(
//                          this.logic.exitRecordList[position], position);
//                    },
//                  ),
//                );
              }
            } else {
              return GestureDetector(
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  child: Card(
                    color: Colors.white70,
                    elevation: 3.0,
                    child: Image.asset(
                      "images/edits.png",
                    ),
                  ),
                ),
                onTap: () {
                  this.logic.onEditExitRecord(
                      this.logic.exitRecordList[position], position);
                },
              );

//              return Container(
//                child:   GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditExitRecord(
//                        this.logic.exitRecordList[position], position);
//                  },
//                ),
//              );
            }
          }

          if (exitRecord.syncDate != null) {
//          if(exitRecord.syncToServer ==true){
            return Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                  width: 30.0,
                  height: 30.0,
                  child: exitRecord.syncToServer == true
                      ? Image.asset(
                          "images/xxhdpi/exit.png",
//                      "images/xxhdpi/animals.png",
                          color: Colors.green,
                        )
                      : Image.asset(
                          "images/xxhdpi/exit.png",
//                      "images/xxhdpi/animals.png",
                          color: Colors.red,
                        ),
                ),

//                  title: Text("Exit Date: " + exitRecord.dateOfExit),
//                  subtitle: Text(exitRecord.farmerName==null?"":"Farmer Name: " +exitRecord.farmerName +
//                      "\n" +"Tag: " +exitRecord.mobileNumber +"-"+
//                      exitRecord.speciesCode +
//                      "\n" +  exitRecord.exitReason),

                title: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Exit Date: " + exitRecord.dateOfExit),
                      Text(
                        exitRecord.farmerName == null
                            ? ""
                            : "Farmer Name: " + exitRecord.farmerName,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        "Tag: " +
                            exitRecord.mobileNumber +
                            "-" +
                            exitRecord.speciesCode,
                        style: TextStyle(fontSize: 14.0),
                      ),
                      Text(
                        "Animal Exit Reason: " + exitRecord.exitReason,
                        style: TextStyle(fontSize: 14.0),
                      ),
                    ],
                  ),
                ),

                trailing: editButton(),
              ),
            );
          } else {
            return Dismissible(
              key: Key(this.logic.exitRecordList[position].eId.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (DismissDirection direction) {
                ExitRecord exitRecordDelete =
                    this.logic.exitRecordList[position];
                setState(() {
                  this.logic.exitRecordList.removeAt(position);

                  this
                      .logic
                      .confirmationDelete(exitRecordDelete, animal, position);
                });
              },
              background: Container(
                alignment: AlignmentDirectional.centerEnd,
                color: Colors.red,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Card(
                elevation: 2.0,
                color: Colors.white,
                child: ListTile(
                  leading: Container(
                    width: 30.0,
                    height: 30.0,
                    child: exitRecord.syncToServer == true
                        ? Image.asset(
                            "images/xxhdpi/exit.png",
//                      "images/xxhdpi/animals.png",
                            color: Colors.green,
                          )
                        : Image.asset(
                            "images/xxhdpi/exit.png",
//                      "images/xxhdpi/animals.png",
                            color: Colors.red,
                          ),
                  ),

//                leading: Container(
//                    width: 50.0,
//                    height: 50.0,
//                    decoration: new BoxDecoration(
//                        color: Colors.white,
//                        shape: BoxShape.circle,
//                        image: new DecorationImage(
//                            fit: BoxFit.fill,
//                            image:
//                            new AssetImage("images/xxhdpi/animals.png")))),
//                  title: Text("Exit Date: " + exitRecord.dateOfExit),
//                  subtitle: Text(exitRecord.farmerName==null?"":"Farmer Name: " +exitRecord.farmerName +
//                      "\n" +"Tag: " +exitRecord.mobileNumber +"-"+
//                      exitRecord.speciesCode +
//                      "\n" +  exitRecord.exitReason),

                  title: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Exit Date: " + exitRecord.dateOfExit),
                        Text(
                          exitRecord.farmerName == null
                              ? ""
                              : "Farmer Name: " + exitRecord.farmerName,
                          style: TextStyle(fontSize: 14.0),
                        ),
                        Text(
                          "Tag: " +
                              exitRecord.mobileNumber +
                              "-" +
                              exitRecord.speciesCode,
                          style: TextStyle(fontSize: 14.0),
                        ),
                        Text(
                          "Animal Exit Reason: " + exitRecord.exitReason,
                          style: TextStyle(fontSize: 14.0),
                        ),
                      ],
                    ),
                  ),

                  trailing: editButton(),
//                onTap: () {
//                      this.logic.onEditExitRecord(this.logic.exitRecordList[position], position);
//                },
                ),
              ),
            );
          }
        });
  }

  ListView getUnSyncExitList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.exitUnSyncRecordList.length,
        itemBuilder: (BuildContext context, int position) {
          exitRecord = this.logic.exitUnSyncRecordList[position];
          return Dismissible(
            key: Key(this.logic.exitUnSyncRecordList[position].eId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              ExitRecord exitRecordDelete =
//                  this.logic.exitUnSyncRecordList[position];
//              this.logic.confirmationDelete(exitRecordDelete, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                    width: 50.0,
                    height: 50.0,
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image:
                                new AssetImage("images/xxhdpi/animals.png")))),
                title: Text("Exit Date: " + exitRecord.speciesCode),
                subtitle: Text("Tag: " +
                    exitRecord.mobileNumber +
                    "-" +
                    exitRecord.speciesCode +
                    "\n" +
                    exitRecord.exitReason),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.edit,
                    size: 30.0,
                    color: Colors.black,
                  ),
                  onTap: () {
                    this.logic.onEditExitRecord(
                        this.logic.exitUnSyncRecordList[position], position);
                  },
                ),
//                onTap: () {
//                      this.logic.onEditExitRecord(this.logic.exitRecordList[position], position);
//                },
              ),
            ),
          );
        });
  }

  ListView getSyncExitList() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: logic.exitSyncRecordList.length,
        itemBuilder: (BuildContext context, int position) {
          exitRecord = this.logic.exitSyncRecordList[position];
          return Dismissible(
            key: Key(this.logic.exitSyncRecordList[position].eId.toString()),
            direction: DismissDirection.endToStart,
            onDismissed: (DismissDirection direction) {
//              ExitRecord exitRecordDelete =
//                  this.logic.exitSyncRecordList[position];
//              this.logic.confirmationDelete(exitRecordDelete, position);
            },
            background: Container(
              alignment: AlignmentDirectional.centerEnd,
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
            ),
            child: Card(
              elevation: 2.0,
              color: Colors.white,
              child: ListTile(
                leading: Container(
                    width: 50.0,
                    height: 50.0,
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image:
                                new AssetImage("images/xxhdpi/animals.png")))),
                title: Text("Exit Date: " + exitRecord.dateOfExit),
                subtitle: Text("Exit Reason: " + exitRecord.exitReason),
//                trailing: GestureDetector(
//                  child: Icon(
//                    Icons.edit,
//                    size: 30.0,
//                    color: Colors.black,
//                  ),
//                  onTap: () {
//                    this.logic.onEditExitRecord(this.logic.exitUnSyncRecordList[position], position);
//
//                  },
//                ),
//                onTap: () {
//                      this.logic.onEditExitRecord(this.logic.exitRecordList[position], position);
//                },
              ),
            ),
          );
        });
  }
}
