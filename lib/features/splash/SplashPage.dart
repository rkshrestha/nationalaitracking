import 'package:flutter/material.dart';
import 'package:nbis/features/splash/SplashLogicPage.dart';

class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashPageState();
  }
}

class SplashPageState extends State<SplashPage> {
  SplashLogicPage splashLogic;

  @override
  void initState() {
    super.initState();
    splashLogic = SplashLogicPage(this);
    splashLogic.onPostState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Image.asset('images/mdpi/govLogo_big.png'),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
              ),
              Text(
                "National Livestock Breeding",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                " Information System",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ],
      ),
    );
  }
}
