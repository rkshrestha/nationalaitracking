import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nbis/features/home/HomePage.dart';
import 'package:nbis/features/home/LoginPage.dart';
import 'package:nbis/features/sharedPreferences/SessionManager.dart';
import 'package:nbis/features/splash/SplashPage.dart';
import 'package:nbis/s/Logger.dart';
import 'package:permission/permission.dart';

import 'dart:io' show Platform, exit;

//import 'package:permission_handler/permission_handler.dart';

class SplashLogicPage {
  SplashPageState splashState;
  static BuildContext context;
  String message = '';

  SessionManager sessionManager = SessionManager(context);

  SplashLogicPage(this.splashState);

  void onPostState() {
    Future.delayed(Duration(milliseconds: 3000)).then((_) {
      init();
      //test 1

//      PermissionHandler()
//          .checkPermissionStatus(PermissionGroup.contacts)
//          .then(updateStatus);
      //test 1 end

//      askPermission();

      if (Platform.isAndroid) {
        // Android-specific code
        checkPermission();
      } else if (Platform.isIOS) {
        // iOS-specific code
//        askPermission();
        getSinglePermissionStatus();
      }
    });
  }

  init() async {
    this.sessionManager = SessionManager.getInstance(this.splashState.context);
  }

  void gotoLoginPage() {
    Navigator.pushReplacement(
      this.splashState.context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  void gotoDashPage() {
    Navigator.pushReplacement(
      this.splashState.context,
      MaterialPageRoute(builder: (context) => HomePage()),
    );
  }

//  //permission 2
//  PermissionStatus permissionStatus;
//
//  void updateStatus(PermissionStatus status) {
//    if (permissionStatus != status) {
//      status = permissionStatus;
//    }
//  }
//
//  void askPermission() {
//    PermissionHandler()
//        .requestPermissions([PermissionGroup.contacts]).then(onStateRequested);
//  }
//
//  void onStateRequested(Map<PermissionGroup, PermissionStatus> statuss) {
//    final status = statuss[PermissionGroup.contacts];
//    if (status == PermissionStatus.granted) {
//      var session = this.sessionManager.findOne();
//      if (session == null) {
//        //goto login
//        gotoLoginPage();
//      } else {
//        gotoDashPage();
//      }
//    } else if (status == PermissionStatus.denied ||
//        status == PermissionStatus.unknown) {
//      askPermission();
//    } else {
//      updateStatus(status);
//    }
//  }

//permission 1
  void requestPermission() async {
    List<Permissions> permissions = await Permission.requestPermissions(
        [PermissionName.Storage, PermissionName.Contacts]);
    if (!isPermissionDenied(permissions)) {
      gotoLoginPage();
    } else {
      requestPermission();
    }
  }

  bool isPermissionDenied(List<Permissions> permissions) {
    bool permissionDeny = false;

    for (int i = 0; i <= permissions.length - 1; i++) {
      Logger.v('status: ' + permissions[i].permissionStatus.toString());
      if (permissions[i].permissionStatus == PermissionStatus.deny ||
          permissions[i].permissionStatus == PermissionStatus.notAgain) {
        permissionDeny = true;
        break;
      }
    }
    return permissionDeny;
  }

  void checkPermission() async {
    List<Permissions> permissions = await Permission.getPermissionsStatus(
        [PermissionName.Storage, PermissionName.Contacts]);
    if (!isPermissionDenied(permissions)) {
      var session = this.sessionManager.findOne();
      if (session == null) {
        //goto login
        gotoLoginPage();
      } else {
        gotoDashPage();
      }
    } else {
      requestPermission();
    }
  }

//ios

  PermissionName permissionNameContacts = PermissionName.Contacts;

  var permissionStatusContacts;

  getSinglePermissionStatus() async {
//    PermissionName permissionNameContacts = PermissionName.Contacts;
//    PermissionName permissionNameStorage = PermissionName.Storage;
    Logger.v("testStatusDdddd" + permissionStatusContacts.toString());
    permissionStatusContacts =
        await Permission.getSinglePermissionStatus(permissionNameContacts);
//    var permissionStatusStorage =
//        await Permission.getSinglePermissionStatus(permissionNameStorage);
    if (permissionStatusContacts == PermissionStatus.notAgain ||
        permissionStatusContacts == PermissionStatus.notDecided) {
      requestSinglePermission();
    } else if (permissionStatusContacts == PermissionStatus.deny) {
      splashState.setState(() {
        Permission.openSettings();
      });
    } else {
      var session = this.sessionManager.findOne();
      if (session == null) {
        //goto login
        gotoLoginPage();
      } else {
        gotoDashPage();
      }
    }
  }

  requestSinglePermission() async {
//    PermissionName permissionNameContacts = PermissionName.Contacts;
//    PermissionName permissionNameStorage = PermissionName.Storage;

    permissionStatusContacts =
        await Permission.requestSinglePermission(permissionNameContacts);

//    var permissionStatusStorage =
//        await Permission.requestSinglePermission(permissionNameStorage);
    if (permissionStatusContacts == PermissionStatus.notAgain ||
        permissionStatusContacts == PermissionStatus.notDecided) {
      Logger.v("testStatus" + permissionStatusContacts.toString());
      requestSinglePermission();

//      Permission.openSettings;
    } else if (permissionStatusContacts == PermissionStatus.deny) {
      Logger.v("testStatusD" + permissionStatusContacts.toString());
      exit(0);
    } else {
      gotoLoginPage();
    }
  }
}
